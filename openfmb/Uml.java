// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: uml.proto

package openfmb;

public final class Uml {
  private Uml() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
    registry.add(openfmb.Uml.optionParentMessage);
    registry.add(openfmb.Uml.optionRequiredField);
    registry.add(openfmb.Uml.optionMultiplicityMin);
    registry.add(openfmb.Uml.optionMultiplicityMax);
    registry.add(openfmb.Uml.optionUuid);
    registry.add(openfmb.Uml.optionKey);
    registry.add(openfmb.Uml.optionOpenfmbProfile);
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public static final int OPTION_PARENT_MESSAGE_FIELD_NUMBER = 50000;
  /**
   * <code>extend .google.protobuf.FieldOptions { ... }</code>
   */
  public static final
    com.google.protobuf.GeneratedMessage.GeneratedExtension<
      com.google.protobuf.DescriptorProtos.FieldOptions,
      java.lang.Boolean> optionParentMessage = com.google.protobuf.GeneratedMessage
          .newFileScopedGeneratedExtension(
        java.lang.Boolean.class,
        null);
  public static final int OPTION_REQUIRED_FIELD_FIELD_NUMBER = 50001;
  /**
   * <code>extend .google.protobuf.FieldOptions { ... }</code>
   */
  public static final
    com.google.protobuf.GeneratedMessage.GeneratedExtension<
      com.google.protobuf.DescriptorProtos.FieldOptions,
      java.lang.Boolean> optionRequiredField = com.google.protobuf.GeneratedMessage
          .newFileScopedGeneratedExtension(
        java.lang.Boolean.class,
        null);
  public static final int OPTION_MULTIPLICITY_MIN_FIELD_NUMBER = 50002;
  /**
   * <code>extend .google.protobuf.FieldOptions { ... }</code>
   */
  public static final
    com.google.protobuf.GeneratedMessage.GeneratedExtension<
      com.google.protobuf.DescriptorProtos.FieldOptions,
      java.lang.Integer> optionMultiplicityMin = com.google.protobuf.GeneratedMessage
          .newFileScopedGeneratedExtension(
        java.lang.Integer.class,
        null);
  public static final int OPTION_MULTIPLICITY_MAX_FIELD_NUMBER = 50003;
  /**
   * <code>extend .google.protobuf.FieldOptions { ... }</code>
   */
  public static final
    com.google.protobuf.GeneratedMessage.GeneratedExtension<
      com.google.protobuf.DescriptorProtos.FieldOptions,
      java.lang.Integer> optionMultiplicityMax = com.google.protobuf.GeneratedMessage
          .newFileScopedGeneratedExtension(
        java.lang.Integer.class,
        null);
  public static final int OPTION_UUID_FIELD_NUMBER = 50004;
  /**
   * <code>extend .google.protobuf.FieldOptions { ... }</code>
   */
  public static final
    com.google.protobuf.GeneratedMessage.GeneratedExtension<
      com.google.protobuf.DescriptorProtos.FieldOptions,
      java.lang.Boolean> optionUuid = com.google.protobuf.GeneratedMessage
          .newFileScopedGeneratedExtension(
        java.lang.Boolean.class,
        null);
  public static final int OPTION_KEY_FIELD_NUMBER = 50005;
  /**
   * <code>extend .google.protobuf.FieldOptions { ... }</code>
   */
  public static final
    com.google.protobuf.GeneratedMessage.GeneratedExtension<
      com.google.protobuf.DescriptorProtos.FieldOptions,
      java.lang.Boolean> optionKey = com.google.protobuf.GeneratedMessage
          .newFileScopedGeneratedExtension(
        java.lang.Boolean.class,
        null);
  public static final int OPTION_OPENFMB_PROFILE_FIELD_NUMBER = 51000;
  /**
   * <code>extend .google.protobuf.MessageOptions { ... }</code>
   */
  public static final
    com.google.protobuf.GeneratedMessage.GeneratedExtension<
      com.google.protobuf.DescriptorProtos.MessageOptions,
      java.lang.Boolean> optionOpenfmbProfile = com.google.protobuf.GeneratedMessage
          .newFileScopedGeneratedExtension(
        java.lang.Boolean.class,
        null);

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\tuml.proto\022\003uml\032 google/protobuf/descri" +
      "ptor.proto:>\n\025option_parent_message\022\035.go" +
      "ogle.protobuf.FieldOptions\030\320\206\003 \001(\010:>\n\025op" +
      "tion_required_field\022\035.google.protobuf.Fi" +
      "eldOptions\030\321\206\003 \001(\010:@\n\027option_multiplicit" +
      "y_min\022\035.google.protobuf.FieldOptions\030\322\206\003" +
      " \001(\005:@\n\027option_multiplicity_max\022\035.google" +
      ".protobuf.FieldOptions\030\323\206\003 \001(\005:4\n\013option" +
      "_uuid\022\035.google.protobuf.FieldOptions\030\324\206\003" +
      " \001(\010:3\n\noption_key\022\035.google.protobuf.Fie" +
      "ldOptions\030\325\206\003 \001(\010:A\n\026option_openfmb_prof" +
      "ile\022\037.google.protobuf.MessageOptions\030\270\216\003" +
      " \001(\010B]\n\007openfmbP\001ZFgitlab.com/openfmb/ps" +
      "m/ops/protobuf/go-openfmb-ops-protobuf/v" +
      "2/openfmb\252\002\007openfmbb\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          com.google.protobuf.DescriptorProtos.getDescriptor(),
        });
    optionParentMessage.internalInit(descriptor.getExtensions().get(0));
    optionRequiredField.internalInit(descriptor.getExtensions().get(1));
    optionMultiplicityMin.internalInit(descriptor.getExtensions().get(2));
    optionMultiplicityMax.internalInit(descriptor.getExtensions().get(3));
    optionUuid.internalInit(descriptor.getExtensions().get(4));
    optionKey.internalInit(descriptor.getExtensions().get(5));
    optionOpenfmbProfile.internalInit(descriptor.getExtensions().get(6));
    com.google.protobuf.DescriptorProtos.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
