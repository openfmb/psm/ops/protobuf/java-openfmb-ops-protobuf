// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: resourcemodule/resourcemodule.proto

package openfmb.resourcemodule;

public interface StringControlGGIOOrBuilder extends
    // @@protoc_insertion_point(interface_extends:resourcemodule.StringControlGGIO)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.LogicalNode logicalNode = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the logicalNode field is set.
   */
  boolean hasLogicalNode();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.LogicalNode logicalNode = 1 [(.uml.option_parent_message) = true];</code>
   * @return The logicalNode.
   */
  openfmb.commonmodule.LogicalNode getLogicalNode();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.LogicalNode logicalNode = 1 [(.uml.option_parent_message) = true];</code>
   */
  openfmb.commonmodule.LogicalNodeOrBuilder getLogicalNodeOrBuilder();

  /**
   * <pre>
   * Phase code
   * </pre>
   *
   * <code>.commonmodule.Optional_PhaseCodeKind Phase = 2;</code>
   * @return Whether the phase field is set.
   */
  boolean hasPhase();
  /**
   * <pre>
   * Phase code
   * </pre>
   *
   * <code>.commonmodule.Optional_PhaseCodeKind Phase = 2;</code>
   * @return The phase.
   */
  openfmb.commonmodule.Optional_PhaseCodeKind getPhase();
  /**
   * <pre>
   * Phase code
   * </pre>
   *
   * <code>.commonmodule.Optional_PhaseCodeKind Phase = 2;</code>
   */
  openfmb.commonmodule.Optional_PhaseCodeKindOrBuilder getPhaseOrBuilder();

  /**
   * <pre>
   * String control
   * </pre>
   *
   * <code>.commonmodule.VSC StrOut = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the strOut field is set.
   */
  boolean hasStrOut();
  /**
   * <pre>
   * String control
   * </pre>
   *
   * <code>.commonmodule.VSC StrOut = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The strOut.
   */
  openfmb.commonmodule.VSC getStrOut();
  /**
   * <pre>
   * String control
   * </pre>
   *
   * <code>.commonmodule.VSC StrOut = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.commonmodule.VSCOrBuilder getStrOutOrBuilder();
}
