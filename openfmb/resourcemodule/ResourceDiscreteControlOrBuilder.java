// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: resourcemodule/resourcemodule.proto

package openfmb.resourcemodule;

public interface ResourceDiscreteControlOrBuilder extends
    // @@protoc_insertion_point(interface_extends:resourcemodule.ResourceDiscreteControl)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the identifiedObject field is set.
   */
  boolean hasIdentifiedObject();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
   * @return The identifiedObject.
   */
  openfmb.commonmodule.IdentifiedObject getIdentifiedObject();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
   */
  openfmb.commonmodule.IdentifiedObjectOrBuilder getIdentifiedObjectOrBuilder();

  /**
   * <pre>
   * IEC61850 enhanced control parameters.
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   * @return Whether the check field is set.
   */
  boolean hasCheck();
  /**
   * <pre>
   * IEC61850 enhanced control parameters.
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   * @return The check.
   */
  openfmb.commonmodule.CheckConditions getCheck();
  /**
   * <pre>
   * IEC61850 enhanced control parameters.
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   */
  openfmb.commonmodule.CheckConditionsOrBuilder getCheckOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.AnalogControlGGIO analogControlGGIO = 3 [(.uml.option_multiplicity_min) = 0];</code>
   */
  java.util.List<openfmb.resourcemodule.AnalogControlGGIO> 
      getAnalogControlGGIOList();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.AnalogControlGGIO analogControlGGIO = 3 [(.uml.option_multiplicity_min) = 0];</code>
   */
  openfmb.resourcemodule.AnalogControlGGIO getAnalogControlGGIO(int index);
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.AnalogControlGGIO analogControlGGIO = 3 [(.uml.option_multiplicity_min) = 0];</code>
   */
  int getAnalogControlGGIOCount();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.AnalogControlGGIO analogControlGGIO = 3 [(.uml.option_multiplicity_min) = 0];</code>
   */
  java.util.List<? extends openfmb.resourcemodule.AnalogControlGGIOOrBuilder> 
      getAnalogControlGGIOOrBuilderList();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.AnalogControlGGIO analogControlGGIO = 3 [(.uml.option_multiplicity_min) = 0];</code>
   */
  openfmb.resourcemodule.AnalogControlGGIOOrBuilder getAnalogControlGGIOOrBuilder(
      int index);

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.BooleanControlGGIO booleanControlGGIO = 4 [(.uml.option_multiplicity_min) = 0];</code>
   */
  java.util.List<openfmb.resourcemodule.BooleanControlGGIO> 
      getBooleanControlGGIOList();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.BooleanControlGGIO booleanControlGGIO = 4 [(.uml.option_multiplicity_min) = 0];</code>
   */
  openfmb.resourcemodule.BooleanControlGGIO getBooleanControlGGIO(int index);
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.BooleanControlGGIO booleanControlGGIO = 4 [(.uml.option_multiplicity_min) = 0];</code>
   */
  int getBooleanControlGGIOCount();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.BooleanControlGGIO booleanControlGGIO = 4 [(.uml.option_multiplicity_min) = 0];</code>
   */
  java.util.List<? extends openfmb.resourcemodule.BooleanControlGGIOOrBuilder> 
      getBooleanControlGGIOOrBuilderList();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.BooleanControlGGIO booleanControlGGIO = 4 [(.uml.option_multiplicity_min) = 0];</code>
   */
  openfmb.resourcemodule.BooleanControlGGIOOrBuilder getBooleanControlGGIOOrBuilder(
      int index);

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.IntegerControlGGIO integerControlGGIO = 5 [(.uml.option_multiplicity_min) = 0];</code>
   */
  java.util.List<openfmb.resourcemodule.IntegerControlGGIO> 
      getIntegerControlGGIOList();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.IntegerControlGGIO integerControlGGIO = 5 [(.uml.option_multiplicity_min) = 0];</code>
   */
  openfmb.resourcemodule.IntegerControlGGIO getIntegerControlGGIO(int index);
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.IntegerControlGGIO integerControlGGIO = 5 [(.uml.option_multiplicity_min) = 0];</code>
   */
  int getIntegerControlGGIOCount();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.IntegerControlGGIO integerControlGGIO = 5 [(.uml.option_multiplicity_min) = 0];</code>
   */
  java.util.List<? extends openfmb.resourcemodule.IntegerControlGGIOOrBuilder> 
      getIntegerControlGGIOOrBuilderList();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.IntegerControlGGIO integerControlGGIO = 5 [(.uml.option_multiplicity_min) = 0];</code>
   */
  openfmb.resourcemodule.IntegerControlGGIOOrBuilder getIntegerControlGGIOOrBuilder(
      int index);

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.StringControlGGIO stringControlGGIO = 6 [(.uml.option_multiplicity_min) = 0];</code>
   */
  java.util.List<openfmb.resourcemodule.StringControlGGIO> 
      getStringControlGGIOList();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.StringControlGGIO stringControlGGIO = 6 [(.uml.option_multiplicity_min) = 0];</code>
   */
  openfmb.resourcemodule.StringControlGGIO getStringControlGGIO(int index);
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.StringControlGGIO stringControlGGIO = 6 [(.uml.option_multiplicity_min) = 0];</code>
   */
  int getStringControlGGIOCount();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.StringControlGGIO stringControlGGIO = 6 [(.uml.option_multiplicity_min) = 0];</code>
   */
  java.util.List<? extends openfmb.resourcemodule.StringControlGGIOOrBuilder> 
      getStringControlGGIOOrBuilderList();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .resourcemodule.StringControlGGIO stringControlGGIO = 6 [(.uml.option_multiplicity_min) = 0];</code>
   */
  openfmb.resourcemodule.StringControlGGIOOrBuilder getStringControlGGIOOrBuilder(
      int index);
}
