// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: resourcemodule/resourcemodule.proto

package openfmb.resourcemodule;

public interface ResourceEventProfileOrBuilder extends
    // @@protoc_insertion_point(interface_extends:resourcemodule.ResourceEventProfile)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the eventMessageInfo field is set.
   */
  boolean hasEventMessageInfo();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return The eventMessageInfo.
   */
  openfmb.commonmodule.EventMessageInfo getEventMessageInfo();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   */
  openfmb.commonmodule.EventMessageInfoOrBuilder getEventMessageInfoOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ConductingEquipment conductingEquipment = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the conductingEquipment field is set.
   */
  boolean hasConductingEquipment();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ConductingEquipment conductingEquipment = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The conductingEquipment.
   */
  openfmb.commonmodule.ConductingEquipment getConductingEquipment();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ConductingEquipment conductingEquipment = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.commonmodule.ConductingEquipmentOrBuilder getConductingEquipmentOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.resourcemodule.ResourceEvent resourceEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the resourceEvent field is set.
   */
  boolean hasResourceEvent();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.resourcemodule.ResourceEvent resourceEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The resourceEvent.
   */
  openfmb.resourcemodule.ResourceEvent getResourceEvent();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.resourcemodule.ResourceEvent resourceEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.resourcemodule.ResourceEventOrBuilder getResourceEventOrBuilder();
}
