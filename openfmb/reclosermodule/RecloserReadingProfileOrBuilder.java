// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: reclosermodule/reclosermodule.proto

package openfmb.reclosermodule;

public interface RecloserReadingProfileOrBuilder extends
    // @@protoc_insertion_point(interface_extends:reclosermodule.RecloserReadingProfile)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ReadingMessageInfo readingMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the readingMessageInfo field is set.
   */
  boolean hasReadingMessageInfo();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ReadingMessageInfo readingMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return The readingMessageInfo.
   */
  openfmb.commonmodule.ReadingMessageInfo getReadingMessageInfo();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ReadingMessageInfo readingMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   */
  openfmb.commonmodule.ReadingMessageInfoOrBuilder getReadingMessageInfoOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.reclosermodule.Recloser recloser = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the recloser field is set.
   */
  boolean hasRecloser();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.reclosermodule.Recloser recloser = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The recloser.
   */
  openfmb.reclosermodule.Recloser getRecloser();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.reclosermodule.Recloser recloser = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.reclosermodule.RecloserOrBuilder getRecloserOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .reclosermodule.RecloserReading recloserReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1, (.uml.option_multiplicity_max) = 2];</code>
   */
  java.util.List<openfmb.reclosermodule.RecloserReading> 
      getRecloserReadingList();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .reclosermodule.RecloserReading recloserReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1, (.uml.option_multiplicity_max) = 2];</code>
   */
  openfmb.reclosermodule.RecloserReading getRecloserReading(int index);
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .reclosermodule.RecloserReading recloserReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1, (.uml.option_multiplicity_max) = 2];</code>
   */
  int getRecloserReadingCount();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .reclosermodule.RecloserReading recloserReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1, (.uml.option_multiplicity_max) = 2];</code>
   */
  java.util.List<? extends openfmb.reclosermodule.RecloserReadingOrBuilder> 
      getRecloserReadingOrBuilderList();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .reclosermodule.RecloserReading recloserReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1, (.uml.option_multiplicity_max) = 2];</code>
   */
  openfmb.reclosermodule.RecloserReadingOrBuilder getRecloserReadingOrBuilder(
      int index);
}
