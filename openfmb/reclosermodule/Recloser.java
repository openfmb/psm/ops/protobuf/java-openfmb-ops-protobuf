// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: reclosermodule/reclosermodule.proto

package openfmb.reclosermodule;

/**
 * <pre>
 * Pole-mounted fault interrupter with built-in phase and ground relays, current transformer (CT),
 * and supplemental controls.
 * </pre>
 *
 * Protobuf type {@code reclosermodule.Recloser}
 */
public final class Recloser extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:reclosermodule.Recloser)
    RecloserOrBuilder {
private static final long serialVersionUID = 0L;
  // Use Recloser.newBuilder() to construct.
  private Recloser(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private Recloser() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new Recloser();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.reclosermodule.Reclosermodule.internal_static_reclosermodule_Recloser_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.reclosermodule.Reclosermodule.internal_static_reclosermodule_Recloser_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.reclosermodule.Recloser.class, openfmb.reclosermodule.Recloser.Builder.class);
  }

  public static final int CONDUCTINGEQUIPMENT_FIELD_NUMBER = 1;
  private openfmb.commonmodule.ConductingEquipment conductingEquipment_;
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the conductingEquipment field is set.
   */
  @java.lang.Override
  public boolean hasConductingEquipment() {
    return conductingEquipment_ != null;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
   * @return The conductingEquipment.
   */
  @java.lang.Override
  public openfmb.commonmodule.ConductingEquipment getConductingEquipment() {
    return conductingEquipment_ == null ? openfmb.commonmodule.ConductingEquipment.getDefaultInstance() : conductingEquipment_;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.ConductingEquipmentOrBuilder getConductingEquipmentOrBuilder() {
    return conductingEquipment_ == null ? openfmb.commonmodule.ConductingEquipment.getDefaultInstance() : conductingEquipment_;
  }

  public static final int NORMALOPEN_FIELD_NUMBER = 2;
  private com.google.protobuf.BoolValue normalOpen_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.google.protobuf.BoolValue normalOpen = 2;</code>
   * @return Whether the normalOpen field is set.
   */
  @java.lang.Override
  public boolean hasNormalOpen() {
    return normalOpen_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.google.protobuf.BoolValue normalOpen = 2;</code>
   * @return The normalOpen.
   */
  @java.lang.Override
  public com.google.protobuf.BoolValue getNormalOpen() {
    return normalOpen_ == null ? com.google.protobuf.BoolValue.getDefaultInstance() : normalOpen_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.google.protobuf.BoolValue normalOpen = 2;</code>
   */
  @java.lang.Override
  public com.google.protobuf.BoolValueOrBuilder getNormalOpenOrBuilder() {
    return normalOpen_ == null ? com.google.protobuf.BoolValue.getDefaultInstance() : normalOpen_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (conductingEquipment_ != null) {
      output.writeMessage(1, getConductingEquipment());
    }
    if (normalOpen_ != null) {
      output.writeMessage(2, getNormalOpen());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (conductingEquipment_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getConductingEquipment());
    }
    if (normalOpen_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getNormalOpen());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.reclosermodule.Recloser)) {
      return super.equals(obj);
    }
    openfmb.reclosermodule.Recloser other = (openfmb.reclosermodule.Recloser) obj;

    if (hasConductingEquipment() != other.hasConductingEquipment()) return false;
    if (hasConductingEquipment()) {
      if (!getConductingEquipment()
          .equals(other.getConductingEquipment())) return false;
    }
    if (hasNormalOpen() != other.hasNormalOpen()) return false;
    if (hasNormalOpen()) {
      if (!getNormalOpen()
          .equals(other.getNormalOpen())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasConductingEquipment()) {
      hash = (37 * hash) + CONDUCTINGEQUIPMENT_FIELD_NUMBER;
      hash = (53 * hash) + getConductingEquipment().hashCode();
    }
    if (hasNormalOpen()) {
      hash = (37 * hash) + NORMALOPEN_FIELD_NUMBER;
      hash = (53 * hash) + getNormalOpen().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.reclosermodule.Recloser parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.reclosermodule.Recloser parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.reclosermodule.Recloser parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.reclosermodule.Recloser parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.reclosermodule.Recloser parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.reclosermodule.Recloser parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.reclosermodule.Recloser parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.reclosermodule.Recloser parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.reclosermodule.Recloser parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.reclosermodule.Recloser parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.reclosermodule.Recloser parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.reclosermodule.Recloser parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.reclosermodule.Recloser prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Pole-mounted fault interrupter with built-in phase and ground relays, current transformer (CT),
   * and supplemental controls.
   * </pre>
   *
   * Protobuf type {@code reclosermodule.Recloser}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:reclosermodule.Recloser)
      openfmb.reclosermodule.RecloserOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.reclosermodule.Reclosermodule.internal_static_reclosermodule_Recloser_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.reclosermodule.Reclosermodule.internal_static_reclosermodule_Recloser_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.reclosermodule.Recloser.class, openfmb.reclosermodule.Recloser.Builder.class);
    }

    // Construct using openfmb.reclosermodule.Recloser.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      conductingEquipment_ = null;
      if (conductingEquipmentBuilder_ != null) {
        conductingEquipmentBuilder_.dispose();
        conductingEquipmentBuilder_ = null;
      }
      normalOpen_ = null;
      if (normalOpenBuilder_ != null) {
        normalOpenBuilder_.dispose();
        normalOpenBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.reclosermodule.Reclosermodule.internal_static_reclosermodule_Recloser_descriptor;
    }

    @java.lang.Override
    public openfmb.reclosermodule.Recloser getDefaultInstanceForType() {
      return openfmb.reclosermodule.Recloser.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.reclosermodule.Recloser build() {
      openfmb.reclosermodule.Recloser result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.reclosermodule.Recloser buildPartial() {
      openfmb.reclosermodule.Recloser result = new openfmb.reclosermodule.Recloser(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.reclosermodule.Recloser result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.conductingEquipment_ = conductingEquipmentBuilder_ == null
            ? conductingEquipment_
            : conductingEquipmentBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.normalOpen_ = normalOpenBuilder_ == null
            ? normalOpen_
            : normalOpenBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.reclosermodule.Recloser) {
        return mergeFrom((openfmb.reclosermodule.Recloser)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.reclosermodule.Recloser other) {
      if (other == openfmb.reclosermodule.Recloser.getDefaultInstance()) return this;
      if (other.hasConductingEquipment()) {
        mergeConductingEquipment(other.getConductingEquipment());
      }
      if (other.hasNormalOpen()) {
        mergeNormalOpen(other.getNormalOpen());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getConductingEquipmentFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getNormalOpenFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.ConductingEquipment conductingEquipment_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ConductingEquipment, openfmb.commonmodule.ConductingEquipment.Builder, openfmb.commonmodule.ConductingEquipmentOrBuilder> conductingEquipmentBuilder_;
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     * @return Whether the conductingEquipment field is set.
     */
    public boolean hasConductingEquipment() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     * @return The conductingEquipment.
     */
    public openfmb.commonmodule.ConductingEquipment getConductingEquipment() {
      if (conductingEquipmentBuilder_ == null) {
        return conductingEquipment_ == null ? openfmb.commonmodule.ConductingEquipment.getDefaultInstance() : conductingEquipment_;
      } else {
        return conductingEquipmentBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setConductingEquipment(openfmb.commonmodule.ConductingEquipment value) {
      if (conductingEquipmentBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        conductingEquipment_ = value;
      } else {
        conductingEquipmentBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setConductingEquipment(
        openfmb.commonmodule.ConductingEquipment.Builder builderForValue) {
      if (conductingEquipmentBuilder_ == null) {
        conductingEquipment_ = builderForValue.build();
      } else {
        conductingEquipmentBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder mergeConductingEquipment(openfmb.commonmodule.ConductingEquipment value) {
      if (conductingEquipmentBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          conductingEquipment_ != null &&
          conductingEquipment_ != openfmb.commonmodule.ConductingEquipment.getDefaultInstance()) {
          getConductingEquipmentBuilder().mergeFrom(value);
        } else {
          conductingEquipment_ = value;
        }
      } else {
        conductingEquipmentBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder clearConductingEquipment() {
      bitField0_ = (bitField0_ & ~0x00000001);
      conductingEquipment_ = null;
      if (conductingEquipmentBuilder_ != null) {
        conductingEquipmentBuilder_.dispose();
        conductingEquipmentBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.ConductingEquipment.Builder getConductingEquipmentBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getConductingEquipmentFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.ConductingEquipmentOrBuilder getConductingEquipmentOrBuilder() {
      if (conductingEquipmentBuilder_ != null) {
        return conductingEquipmentBuilder_.getMessageOrBuilder();
      } else {
        return conductingEquipment_ == null ?
            openfmb.commonmodule.ConductingEquipment.getDefaultInstance() : conductingEquipment_;
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ConductingEquipment, openfmb.commonmodule.ConductingEquipment.Builder, openfmb.commonmodule.ConductingEquipmentOrBuilder> 
        getConductingEquipmentFieldBuilder() {
      if (conductingEquipmentBuilder_ == null) {
        conductingEquipmentBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.ConductingEquipment, openfmb.commonmodule.ConductingEquipment.Builder, openfmb.commonmodule.ConductingEquipmentOrBuilder>(
                getConductingEquipment(),
                getParentForChildren(),
                isClean());
        conductingEquipment_ = null;
      }
      return conductingEquipmentBuilder_;
    }

    private com.google.protobuf.BoolValue normalOpen_;
    private com.google.protobuf.SingleFieldBuilderV3<
        com.google.protobuf.BoolValue, com.google.protobuf.BoolValue.Builder, com.google.protobuf.BoolValueOrBuilder> normalOpenBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.google.protobuf.BoolValue normalOpen = 2;</code>
     * @return Whether the normalOpen field is set.
     */
    public boolean hasNormalOpen() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.google.protobuf.BoolValue normalOpen = 2;</code>
     * @return The normalOpen.
     */
    public com.google.protobuf.BoolValue getNormalOpen() {
      if (normalOpenBuilder_ == null) {
        return normalOpen_ == null ? com.google.protobuf.BoolValue.getDefaultInstance() : normalOpen_;
      } else {
        return normalOpenBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.google.protobuf.BoolValue normalOpen = 2;</code>
     */
    public Builder setNormalOpen(com.google.protobuf.BoolValue value) {
      if (normalOpenBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        normalOpen_ = value;
      } else {
        normalOpenBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.google.protobuf.BoolValue normalOpen = 2;</code>
     */
    public Builder setNormalOpen(
        com.google.protobuf.BoolValue.Builder builderForValue) {
      if (normalOpenBuilder_ == null) {
        normalOpen_ = builderForValue.build();
      } else {
        normalOpenBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.google.protobuf.BoolValue normalOpen = 2;</code>
     */
    public Builder mergeNormalOpen(com.google.protobuf.BoolValue value) {
      if (normalOpenBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          normalOpen_ != null &&
          normalOpen_ != com.google.protobuf.BoolValue.getDefaultInstance()) {
          getNormalOpenBuilder().mergeFrom(value);
        } else {
          normalOpen_ = value;
        }
      } else {
        normalOpenBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.google.protobuf.BoolValue normalOpen = 2;</code>
     */
    public Builder clearNormalOpen() {
      bitField0_ = (bitField0_ & ~0x00000002);
      normalOpen_ = null;
      if (normalOpenBuilder_ != null) {
        normalOpenBuilder_.dispose();
        normalOpenBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.google.protobuf.BoolValue normalOpen = 2;</code>
     */
    public com.google.protobuf.BoolValue.Builder getNormalOpenBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getNormalOpenFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.google.protobuf.BoolValue normalOpen = 2;</code>
     */
    public com.google.protobuf.BoolValueOrBuilder getNormalOpenOrBuilder() {
      if (normalOpenBuilder_ != null) {
        return normalOpenBuilder_.getMessageOrBuilder();
      } else {
        return normalOpen_ == null ?
            com.google.protobuf.BoolValue.getDefaultInstance() : normalOpen_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.google.protobuf.BoolValue normalOpen = 2;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        com.google.protobuf.BoolValue, com.google.protobuf.BoolValue.Builder, com.google.protobuf.BoolValueOrBuilder> 
        getNormalOpenFieldBuilder() {
      if (normalOpenBuilder_ == null) {
        normalOpenBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            com.google.protobuf.BoolValue, com.google.protobuf.BoolValue.Builder, com.google.protobuf.BoolValueOrBuilder>(
                getNormalOpen(),
                getParentForChildren(),
                isClean());
        normalOpen_ = null;
      }
      return normalOpenBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:reclosermodule.Recloser)
  }

  // @@protoc_insertion_point(class_scope:reclosermodule.Recloser)
  private static final openfmb.reclosermodule.Recloser DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.reclosermodule.Recloser();
  }

  public static openfmb.reclosermodule.Recloser getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<Recloser>
      PARSER = new com.google.protobuf.AbstractParser<Recloser>() {
    @java.lang.Override
    public Recloser parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<Recloser> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<Recloser> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.reclosermodule.Recloser getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

