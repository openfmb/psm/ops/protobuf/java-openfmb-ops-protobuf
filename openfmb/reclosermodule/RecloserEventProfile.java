// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: reclosermodule/reclosermodule.proto

package openfmb.reclosermodule;

/**
 * <pre>
 * Recloser event profile
 * </pre>
 *
 * Protobuf type {@code reclosermodule.RecloserEventProfile}
 */
public final class RecloserEventProfile extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:reclosermodule.RecloserEventProfile)
    RecloserEventProfileOrBuilder {
private static final long serialVersionUID = 0L;
  // Use RecloserEventProfile.newBuilder() to construct.
  private RecloserEventProfile(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private RecloserEventProfile() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new RecloserEventProfile();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.reclosermodule.Reclosermodule.internal_static_reclosermodule_RecloserEventProfile_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.reclosermodule.Reclosermodule.internal_static_reclosermodule_RecloserEventProfile_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.reclosermodule.RecloserEventProfile.class, openfmb.reclosermodule.RecloserEventProfile.Builder.class);
  }

  public static final int EVENTMESSAGEINFO_FIELD_NUMBER = 1;
  private openfmb.commonmodule.EventMessageInfo eventMessageInfo_;
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the eventMessageInfo field is set.
   */
  @java.lang.Override
  public boolean hasEventMessageInfo() {
    return eventMessageInfo_ != null;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return The eventMessageInfo.
   */
  @java.lang.Override
  public openfmb.commonmodule.EventMessageInfo getEventMessageInfo() {
    return eventMessageInfo_ == null ? openfmb.commonmodule.EventMessageInfo.getDefaultInstance() : eventMessageInfo_;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.EventMessageInfoOrBuilder getEventMessageInfoOrBuilder() {
    return eventMessageInfo_ == null ? openfmb.commonmodule.EventMessageInfo.getDefaultInstance() : eventMessageInfo_;
  }

  public static final int RECLOSER_FIELD_NUMBER = 2;
  private openfmb.reclosermodule.Recloser recloser_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.reclosermodule.Recloser recloser = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the recloser field is set.
   */
  @java.lang.Override
  public boolean hasRecloser() {
    return recloser_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.reclosermodule.Recloser recloser = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The recloser.
   */
  @java.lang.Override
  public openfmb.reclosermodule.Recloser getRecloser() {
    return recloser_ == null ? openfmb.reclosermodule.Recloser.getDefaultInstance() : recloser_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.reclosermodule.Recloser recloser = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.reclosermodule.RecloserOrBuilder getRecloserOrBuilder() {
    return recloser_ == null ? openfmb.reclosermodule.Recloser.getDefaultInstance() : recloser_;
  }

  public static final int RECLOSEREVENT_FIELD_NUMBER = 3;
  private openfmb.reclosermodule.RecloserEvent recloserEvent_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.reclosermodule.RecloserEvent recloserEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the recloserEvent field is set.
   */
  @java.lang.Override
  public boolean hasRecloserEvent() {
    return recloserEvent_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.reclosermodule.RecloserEvent recloserEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The recloserEvent.
   */
  @java.lang.Override
  public openfmb.reclosermodule.RecloserEvent getRecloserEvent() {
    return recloserEvent_ == null ? openfmb.reclosermodule.RecloserEvent.getDefaultInstance() : recloserEvent_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.reclosermodule.RecloserEvent recloserEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.reclosermodule.RecloserEventOrBuilder getRecloserEventOrBuilder() {
    return recloserEvent_ == null ? openfmb.reclosermodule.RecloserEvent.getDefaultInstance() : recloserEvent_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (eventMessageInfo_ != null) {
      output.writeMessage(1, getEventMessageInfo());
    }
    if (recloser_ != null) {
      output.writeMessage(2, getRecloser());
    }
    if (recloserEvent_ != null) {
      output.writeMessage(3, getRecloserEvent());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (eventMessageInfo_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getEventMessageInfo());
    }
    if (recloser_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getRecloser());
    }
    if (recloserEvent_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(3, getRecloserEvent());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.reclosermodule.RecloserEventProfile)) {
      return super.equals(obj);
    }
    openfmb.reclosermodule.RecloserEventProfile other = (openfmb.reclosermodule.RecloserEventProfile) obj;

    if (hasEventMessageInfo() != other.hasEventMessageInfo()) return false;
    if (hasEventMessageInfo()) {
      if (!getEventMessageInfo()
          .equals(other.getEventMessageInfo())) return false;
    }
    if (hasRecloser() != other.hasRecloser()) return false;
    if (hasRecloser()) {
      if (!getRecloser()
          .equals(other.getRecloser())) return false;
    }
    if (hasRecloserEvent() != other.hasRecloserEvent()) return false;
    if (hasRecloserEvent()) {
      if (!getRecloserEvent()
          .equals(other.getRecloserEvent())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasEventMessageInfo()) {
      hash = (37 * hash) + EVENTMESSAGEINFO_FIELD_NUMBER;
      hash = (53 * hash) + getEventMessageInfo().hashCode();
    }
    if (hasRecloser()) {
      hash = (37 * hash) + RECLOSER_FIELD_NUMBER;
      hash = (53 * hash) + getRecloser().hashCode();
    }
    if (hasRecloserEvent()) {
      hash = (37 * hash) + RECLOSEREVENT_FIELD_NUMBER;
      hash = (53 * hash) + getRecloserEvent().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.reclosermodule.RecloserEventProfile parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.reclosermodule.RecloserEventProfile parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.reclosermodule.RecloserEventProfile parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.reclosermodule.RecloserEventProfile parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.reclosermodule.RecloserEventProfile parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.reclosermodule.RecloserEventProfile parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.reclosermodule.RecloserEventProfile parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.reclosermodule.RecloserEventProfile parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.reclosermodule.RecloserEventProfile parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.reclosermodule.RecloserEventProfile parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.reclosermodule.RecloserEventProfile parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.reclosermodule.RecloserEventProfile parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.reclosermodule.RecloserEventProfile prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Recloser event profile
   * </pre>
   *
   * Protobuf type {@code reclosermodule.RecloserEventProfile}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:reclosermodule.RecloserEventProfile)
      openfmb.reclosermodule.RecloserEventProfileOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.reclosermodule.Reclosermodule.internal_static_reclosermodule_RecloserEventProfile_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.reclosermodule.Reclosermodule.internal_static_reclosermodule_RecloserEventProfile_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.reclosermodule.RecloserEventProfile.class, openfmb.reclosermodule.RecloserEventProfile.Builder.class);
    }

    // Construct using openfmb.reclosermodule.RecloserEventProfile.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      eventMessageInfo_ = null;
      if (eventMessageInfoBuilder_ != null) {
        eventMessageInfoBuilder_.dispose();
        eventMessageInfoBuilder_ = null;
      }
      recloser_ = null;
      if (recloserBuilder_ != null) {
        recloserBuilder_.dispose();
        recloserBuilder_ = null;
      }
      recloserEvent_ = null;
      if (recloserEventBuilder_ != null) {
        recloserEventBuilder_.dispose();
        recloserEventBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.reclosermodule.Reclosermodule.internal_static_reclosermodule_RecloserEventProfile_descriptor;
    }

    @java.lang.Override
    public openfmb.reclosermodule.RecloserEventProfile getDefaultInstanceForType() {
      return openfmb.reclosermodule.RecloserEventProfile.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.reclosermodule.RecloserEventProfile build() {
      openfmb.reclosermodule.RecloserEventProfile result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.reclosermodule.RecloserEventProfile buildPartial() {
      openfmb.reclosermodule.RecloserEventProfile result = new openfmb.reclosermodule.RecloserEventProfile(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.reclosermodule.RecloserEventProfile result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.eventMessageInfo_ = eventMessageInfoBuilder_ == null
            ? eventMessageInfo_
            : eventMessageInfoBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.recloser_ = recloserBuilder_ == null
            ? recloser_
            : recloserBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000004) != 0)) {
        result.recloserEvent_ = recloserEventBuilder_ == null
            ? recloserEvent_
            : recloserEventBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.reclosermodule.RecloserEventProfile) {
        return mergeFrom((openfmb.reclosermodule.RecloserEventProfile)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.reclosermodule.RecloserEventProfile other) {
      if (other == openfmb.reclosermodule.RecloserEventProfile.getDefaultInstance()) return this;
      if (other.hasEventMessageInfo()) {
        mergeEventMessageInfo(other.getEventMessageInfo());
      }
      if (other.hasRecloser()) {
        mergeRecloser(other.getRecloser());
      }
      if (other.hasRecloserEvent()) {
        mergeRecloserEvent(other.getRecloserEvent());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getEventMessageInfoFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getRecloserFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            case 26: {
              input.readMessage(
                  getRecloserEventFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000004;
              break;
            } // case 26
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.EventMessageInfo eventMessageInfo_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.EventMessageInfo, openfmb.commonmodule.EventMessageInfo.Builder, openfmb.commonmodule.EventMessageInfoOrBuilder> eventMessageInfoBuilder_;
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     * @return Whether the eventMessageInfo field is set.
     */
    public boolean hasEventMessageInfo() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     * @return The eventMessageInfo.
     */
    public openfmb.commonmodule.EventMessageInfo getEventMessageInfo() {
      if (eventMessageInfoBuilder_ == null) {
        return eventMessageInfo_ == null ? openfmb.commonmodule.EventMessageInfo.getDefaultInstance() : eventMessageInfo_;
      } else {
        return eventMessageInfoBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setEventMessageInfo(openfmb.commonmodule.EventMessageInfo value) {
      if (eventMessageInfoBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        eventMessageInfo_ = value;
      } else {
        eventMessageInfoBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setEventMessageInfo(
        openfmb.commonmodule.EventMessageInfo.Builder builderForValue) {
      if (eventMessageInfoBuilder_ == null) {
        eventMessageInfo_ = builderForValue.build();
      } else {
        eventMessageInfoBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder mergeEventMessageInfo(openfmb.commonmodule.EventMessageInfo value) {
      if (eventMessageInfoBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          eventMessageInfo_ != null &&
          eventMessageInfo_ != openfmb.commonmodule.EventMessageInfo.getDefaultInstance()) {
          getEventMessageInfoBuilder().mergeFrom(value);
        } else {
          eventMessageInfo_ = value;
        }
      } else {
        eventMessageInfoBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder clearEventMessageInfo() {
      bitField0_ = (bitField0_ & ~0x00000001);
      eventMessageInfo_ = null;
      if (eventMessageInfoBuilder_ != null) {
        eventMessageInfoBuilder_.dispose();
        eventMessageInfoBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.EventMessageInfo.Builder getEventMessageInfoBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getEventMessageInfoFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.EventMessageInfoOrBuilder getEventMessageInfoOrBuilder() {
      if (eventMessageInfoBuilder_ != null) {
        return eventMessageInfoBuilder_.getMessageOrBuilder();
      } else {
        return eventMessageInfo_ == null ?
            openfmb.commonmodule.EventMessageInfo.getDefaultInstance() : eventMessageInfo_;
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.EventMessageInfo, openfmb.commonmodule.EventMessageInfo.Builder, openfmb.commonmodule.EventMessageInfoOrBuilder> 
        getEventMessageInfoFieldBuilder() {
      if (eventMessageInfoBuilder_ == null) {
        eventMessageInfoBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.EventMessageInfo, openfmb.commonmodule.EventMessageInfo.Builder, openfmb.commonmodule.EventMessageInfoOrBuilder>(
                getEventMessageInfo(),
                getParentForChildren(),
                isClean());
        eventMessageInfo_ = null;
      }
      return eventMessageInfoBuilder_;
    }

    private openfmb.reclosermodule.Recloser recloser_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.reclosermodule.Recloser, openfmb.reclosermodule.Recloser.Builder, openfmb.reclosermodule.RecloserOrBuilder> recloserBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.Recloser recloser = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return Whether the recloser field is set.
     */
    public boolean hasRecloser() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.Recloser recloser = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The recloser.
     */
    public openfmb.reclosermodule.Recloser getRecloser() {
      if (recloserBuilder_ == null) {
        return recloser_ == null ? openfmb.reclosermodule.Recloser.getDefaultInstance() : recloser_;
      } else {
        return recloserBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.Recloser recloser = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setRecloser(openfmb.reclosermodule.Recloser value) {
      if (recloserBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        recloser_ = value;
      } else {
        recloserBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.Recloser recloser = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setRecloser(
        openfmb.reclosermodule.Recloser.Builder builderForValue) {
      if (recloserBuilder_ == null) {
        recloser_ = builderForValue.build();
      } else {
        recloserBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.Recloser recloser = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder mergeRecloser(openfmb.reclosermodule.Recloser value) {
      if (recloserBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          recloser_ != null &&
          recloser_ != openfmb.reclosermodule.Recloser.getDefaultInstance()) {
          getRecloserBuilder().mergeFrom(value);
        } else {
          recloser_ = value;
        }
      } else {
        recloserBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.Recloser recloser = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder clearRecloser() {
      bitField0_ = (bitField0_ & ~0x00000002);
      recloser_ = null;
      if (recloserBuilder_ != null) {
        recloserBuilder_.dispose();
        recloserBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.Recloser recloser = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.reclosermodule.Recloser.Builder getRecloserBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getRecloserFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.Recloser recloser = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.reclosermodule.RecloserOrBuilder getRecloserOrBuilder() {
      if (recloserBuilder_ != null) {
        return recloserBuilder_.getMessageOrBuilder();
      } else {
        return recloser_ == null ?
            openfmb.reclosermodule.Recloser.getDefaultInstance() : recloser_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.Recloser recloser = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.reclosermodule.Recloser, openfmb.reclosermodule.Recloser.Builder, openfmb.reclosermodule.RecloserOrBuilder> 
        getRecloserFieldBuilder() {
      if (recloserBuilder_ == null) {
        recloserBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.reclosermodule.Recloser, openfmb.reclosermodule.Recloser.Builder, openfmb.reclosermodule.RecloserOrBuilder>(
                getRecloser(),
                getParentForChildren(),
                isClean());
        recloser_ = null;
      }
      return recloserBuilder_;
    }

    private openfmb.reclosermodule.RecloserEvent recloserEvent_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.reclosermodule.RecloserEvent, openfmb.reclosermodule.RecloserEvent.Builder, openfmb.reclosermodule.RecloserEventOrBuilder> recloserEventBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserEvent recloserEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return Whether the recloserEvent field is set.
     */
    public boolean hasRecloserEvent() {
      return ((bitField0_ & 0x00000004) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserEvent recloserEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The recloserEvent.
     */
    public openfmb.reclosermodule.RecloserEvent getRecloserEvent() {
      if (recloserEventBuilder_ == null) {
        return recloserEvent_ == null ? openfmb.reclosermodule.RecloserEvent.getDefaultInstance() : recloserEvent_;
      } else {
        return recloserEventBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserEvent recloserEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setRecloserEvent(openfmb.reclosermodule.RecloserEvent value) {
      if (recloserEventBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        recloserEvent_ = value;
      } else {
        recloserEventBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserEvent recloserEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setRecloserEvent(
        openfmb.reclosermodule.RecloserEvent.Builder builderForValue) {
      if (recloserEventBuilder_ == null) {
        recloserEvent_ = builderForValue.build();
      } else {
        recloserEventBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserEvent recloserEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder mergeRecloserEvent(openfmb.reclosermodule.RecloserEvent value) {
      if (recloserEventBuilder_ == null) {
        if (((bitField0_ & 0x00000004) != 0) &&
          recloserEvent_ != null &&
          recloserEvent_ != openfmb.reclosermodule.RecloserEvent.getDefaultInstance()) {
          getRecloserEventBuilder().mergeFrom(value);
        } else {
          recloserEvent_ = value;
        }
      } else {
        recloserEventBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserEvent recloserEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder clearRecloserEvent() {
      bitField0_ = (bitField0_ & ~0x00000004);
      recloserEvent_ = null;
      if (recloserEventBuilder_ != null) {
        recloserEventBuilder_.dispose();
        recloserEventBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserEvent recloserEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.reclosermodule.RecloserEvent.Builder getRecloserEventBuilder() {
      bitField0_ |= 0x00000004;
      onChanged();
      return getRecloserEventFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserEvent recloserEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.reclosermodule.RecloserEventOrBuilder getRecloserEventOrBuilder() {
      if (recloserEventBuilder_ != null) {
        return recloserEventBuilder_.getMessageOrBuilder();
      } else {
        return recloserEvent_ == null ?
            openfmb.reclosermodule.RecloserEvent.getDefaultInstance() : recloserEvent_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserEvent recloserEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.reclosermodule.RecloserEvent, openfmb.reclosermodule.RecloserEvent.Builder, openfmb.reclosermodule.RecloserEventOrBuilder> 
        getRecloserEventFieldBuilder() {
      if (recloserEventBuilder_ == null) {
        recloserEventBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.reclosermodule.RecloserEvent, openfmb.reclosermodule.RecloserEvent.Builder, openfmb.reclosermodule.RecloserEventOrBuilder>(
                getRecloserEvent(),
                getParentForChildren(),
                isClean());
        recloserEvent_ = null;
      }
      return recloserEventBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:reclosermodule.RecloserEventProfile)
  }

  // @@protoc_insertion_point(class_scope:reclosermodule.RecloserEventProfile)
  private static final openfmb.reclosermodule.RecloserEventProfile DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.reclosermodule.RecloserEventProfile();
  }

  public static openfmb.reclosermodule.RecloserEventProfile getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<RecloserEventProfile>
      PARSER = new com.google.protobuf.AbstractParser<RecloserEventProfile>() {
    @java.lang.Override
    public RecloserEventProfile parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<RecloserEventProfile> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<RecloserEventProfile> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.reclosermodule.RecloserEventProfile getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

