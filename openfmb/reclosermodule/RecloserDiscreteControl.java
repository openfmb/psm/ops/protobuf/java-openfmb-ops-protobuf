// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: reclosermodule/reclosermodule.proto

package openfmb.reclosermodule;

/**
 * <pre>
 * Recloser discrete control
 * </pre>
 *
 * Protobuf type {@code reclosermodule.RecloserDiscreteControl}
 */
public final class RecloserDiscreteControl extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:reclosermodule.RecloserDiscreteControl)
    RecloserDiscreteControlOrBuilder {
private static final long serialVersionUID = 0L;
  // Use RecloserDiscreteControl.newBuilder() to construct.
  private RecloserDiscreteControl(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private RecloserDiscreteControl() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new RecloserDiscreteControl();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.reclosermodule.Reclosermodule.internal_static_reclosermodule_RecloserDiscreteControl_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.reclosermodule.Reclosermodule.internal_static_reclosermodule_RecloserDiscreteControl_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.reclosermodule.RecloserDiscreteControl.class, openfmb.reclosermodule.RecloserDiscreteControl.Builder.class);
  }

  public static final int CONTROLVALUE_FIELD_NUMBER = 1;
  private openfmb.commonmodule.ControlValue controlValue_;
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the controlValue field is set.
   */
  @java.lang.Override
  public boolean hasControlValue() {
    return controlValue_ != null;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
   * @return The controlValue.
   */
  @java.lang.Override
  public openfmb.commonmodule.ControlValue getControlValue() {
    return controlValue_ == null ? openfmb.commonmodule.ControlValue.getDefaultInstance() : controlValue_;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.ControlValueOrBuilder getControlValueOrBuilder() {
    return controlValue_ == null ? openfmb.commonmodule.ControlValue.getDefaultInstance() : controlValue_;
  }

  public static final int CHECK_FIELD_NUMBER = 2;
  private openfmb.commonmodule.CheckConditions check_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   * @return Whether the check field is set.
   */
  @java.lang.Override
  public boolean hasCheck() {
    return check_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   * @return The check.
   */
  @java.lang.Override
  public openfmb.commonmodule.CheckConditions getCheck() {
    return check_ == null ? openfmb.commonmodule.CheckConditions.getDefaultInstance() : check_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.CheckConditionsOrBuilder getCheckOrBuilder() {
    return check_ == null ? openfmb.commonmodule.CheckConditions.getDefaultInstance() : check_;
  }

  public static final int RECLOSERDISCRETECONTROLXCBR_FIELD_NUMBER = 3;
  private openfmb.reclosermodule.RecloserDiscreteControlXCBR recloserDiscreteControlXCBR_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.reclosermodule.RecloserDiscreteControlXCBR recloserDiscreteControlXCBR = 3;</code>
   * @return Whether the recloserDiscreteControlXCBR field is set.
   */
  @java.lang.Override
  public boolean hasRecloserDiscreteControlXCBR() {
    return recloserDiscreteControlXCBR_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.reclosermodule.RecloserDiscreteControlXCBR recloserDiscreteControlXCBR = 3;</code>
   * @return The recloserDiscreteControlXCBR.
   */
  @java.lang.Override
  public openfmb.reclosermodule.RecloserDiscreteControlXCBR getRecloserDiscreteControlXCBR() {
    return recloserDiscreteControlXCBR_ == null ? openfmb.reclosermodule.RecloserDiscreteControlXCBR.getDefaultInstance() : recloserDiscreteControlXCBR_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.reclosermodule.RecloserDiscreteControlXCBR recloserDiscreteControlXCBR = 3;</code>
   */
  @java.lang.Override
  public openfmb.reclosermodule.RecloserDiscreteControlXCBROrBuilder getRecloserDiscreteControlXCBROrBuilder() {
    return recloserDiscreteControlXCBR_ == null ? openfmb.reclosermodule.RecloserDiscreteControlXCBR.getDefaultInstance() : recloserDiscreteControlXCBR_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (controlValue_ != null) {
      output.writeMessage(1, getControlValue());
    }
    if (check_ != null) {
      output.writeMessage(2, getCheck());
    }
    if (recloserDiscreteControlXCBR_ != null) {
      output.writeMessage(3, getRecloserDiscreteControlXCBR());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (controlValue_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getControlValue());
    }
    if (check_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getCheck());
    }
    if (recloserDiscreteControlXCBR_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(3, getRecloserDiscreteControlXCBR());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.reclosermodule.RecloserDiscreteControl)) {
      return super.equals(obj);
    }
    openfmb.reclosermodule.RecloserDiscreteControl other = (openfmb.reclosermodule.RecloserDiscreteControl) obj;

    if (hasControlValue() != other.hasControlValue()) return false;
    if (hasControlValue()) {
      if (!getControlValue()
          .equals(other.getControlValue())) return false;
    }
    if (hasCheck() != other.hasCheck()) return false;
    if (hasCheck()) {
      if (!getCheck()
          .equals(other.getCheck())) return false;
    }
    if (hasRecloserDiscreteControlXCBR() != other.hasRecloserDiscreteControlXCBR()) return false;
    if (hasRecloserDiscreteControlXCBR()) {
      if (!getRecloserDiscreteControlXCBR()
          .equals(other.getRecloserDiscreteControlXCBR())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasControlValue()) {
      hash = (37 * hash) + CONTROLVALUE_FIELD_NUMBER;
      hash = (53 * hash) + getControlValue().hashCode();
    }
    if (hasCheck()) {
      hash = (37 * hash) + CHECK_FIELD_NUMBER;
      hash = (53 * hash) + getCheck().hashCode();
    }
    if (hasRecloserDiscreteControlXCBR()) {
      hash = (37 * hash) + RECLOSERDISCRETECONTROLXCBR_FIELD_NUMBER;
      hash = (53 * hash) + getRecloserDiscreteControlXCBR().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.reclosermodule.RecloserDiscreteControl parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.reclosermodule.RecloserDiscreteControl parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.reclosermodule.RecloserDiscreteControl parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.reclosermodule.RecloserDiscreteControl parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.reclosermodule.RecloserDiscreteControl parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.reclosermodule.RecloserDiscreteControl parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.reclosermodule.RecloserDiscreteControl parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.reclosermodule.RecloserDiscreteControl parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.reclosermodule.RecloserDiscreteControl parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.reclosermodule.RecloserDiscreteControl parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.reclosermodule.RecloserDiscreteControl parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.reclosermodule.RecloserDiscreteControl parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.reclosermodule.RecloserDiscreteControl prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Recloser discrete control
   * </pre>
   *
   * Protobuf type {@code reclosermodule.RecloserDiscreteControl}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:reclosermodule.RecloserDiscreteControl)
      openfmb.reclosermodule.RecloserDiscreteControlOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.reclosermodule.Reclosermodule.internal_static_reclosermodule_RecloserDiscreteControl_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.reclosermodule.Reclosermodule.internal_static_reclosermodule_RecloserDiscreteControl_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.reclosermodule.RecloserDiscreteControl.class, openfmb.reclosermodule.RecloserDiscreteControl.Builder.class);
    }

    // Construct using openfmb.reclosermodule.RecloserDiscreteControl.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      controlValue_ = null;
      if (controlValueBuilder_ != null) {
        controlValueBuilder_.dispose();
        controlValueBuilder_ = null;
      }
      check_ = null;
      if (checkBuilder_ != null) {
        checkBuilder_.dispose();
        checkBuilder_ = null;
      }
      recloserDiscreteControlXCBR_ = null;
      if (recloserDiscreteControlXCBRBuilder_ != null) {
        recloserDiscreteControlXCBRBuilder_.dispose();
        recloserDiscreteControlXCBRBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.reclosermodule.Reclosermodule.internal_static_reclosermodule_RecloserDiscreteControl_descriptor;
    }

    @java.lang.Override
    public openfmb.reclosermodule.RecloserDiscreteControl getDefaultInstanceForType() {
      return openfmb.reclosermodule.RecloserDiscreteControl.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.reclosermodule.RecloserDiscreteControl build() {
      openfmb.reclosermodule.RecloserDiscreteControl result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.reclosermodule.RecloserDiscreteControl buildPartial() {
      openfmb.reclosermodule.RecloserDiscreteControl result = new openfmb.reclosermodule.RecloserDiscreteControl(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.reclosermodule.RecloserDiscreteControl result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.controlValue_ = controlValueBuilder_ == null
            ? controlValue_
            : controlValueBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.check_ = checkBuilder_ == null
            ? check_
            : checkBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000004) != 0)) {
        result.recloserDiscreteControlXCBR_ = recloserDiscreteControlXCBRBuilder_ == null
            ? recloserDiscreteControlXCBR_
            : recloserDiscreteControlXCBRBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.reclosermodule.RecloserDiscreteControl) {
        return mergeFrom((openfmb.reclosermodule.RecloserDiscreteControl)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.reclosermodule.RecloserDiscreteControl other) {
      if (other == openfmb.reclosermodule.RecloserDiscreteControl.getDefaultInstance()) return this;
      if (other.hasControlValue()) {
        mergeControlValue(other.getControlValue());
      }
      if (other.hasCheck()) {
        mergeCheck(other.getCheck());
      }
      if (other.hasRecloserDiscreteControlXCBR()) {
        mergeRecloserDiscreteControlXCBR(other.getRecloserDiscreteControlXCBR());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getControlValueFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getCheckFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            case 26: {
              input.readMessage(
                  getRecloserDiscreteControlXCBRFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000004;
              break;
            } // case 26
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.ControlValue controlValue_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ControlValue, openfmb.commonmodule.ControlValue.Builder, openfmb.commonmodule.ControlValueOrBuilder> controlValueBuilder_;
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     * @return Whether the controlValue field is set.
     */
    public boolean hasControlValue() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     * @return The controlValue.
     */
    public openfmb.commonmodule.ControlValue getControlValue() {
      if (controlValueBuilder_ == null) {
        return controlValue_ == null ? openfmb.commonmodule.ControlValue.getDefaultInstance() : controlValue_;
      } else {
        return controlValueBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setControlValue(openfmb.commonmodule.ControlValue value) {
      if (controlValueBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        controlValue_ = value;
      } else {
        controlValueBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setControlValue(
        openfmb.commonmodule.ControlValue.Builder builderForValue) {
      if (controlValueBuilder_ == null) {
        controlValue_ = builderForValue.build();
      } else {
        controlValueBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder mergeControlValue(openfmb.commonmodule.ControlValue value) {
      if (controlValueBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          controlValue_ != null &&
          controlValue_ != openfmb.commonmodule.ControlValue.getDefaultInstance()) {
          getControlValueBuilder().mergeFrom(value);
        } else {
          controlValue_ = value;
        }
      } else {
        controlValueBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder clearControlValue() {
      bitField0_ = (bitField0_ & ~0x00000001);
      controlValue_ = null;
      if (controlValueBuilder_ != null) {
        controlValueBuilder_.dispose();
        controlValueBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.ControlValue.Builder getControlValueBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getControlValueFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.ControlValueOrBuilder getControlValueOrBuilder() {
      if (controlValueBuilder_ != null) {
        return controlValueBuilder_.getMessageOrBuilder();
      } else {
        return controlValue_ == null ?
            openfmb.commonmodule.ControlValue.getDefaultInstance() : controlValue_;
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ControlValue, openfmb.commonmodule.ControlValue.Builder, openfmb.commonmodule.ControlValueOrBuilder> 
        getControlValueFieldBuilder() {
      if (controlValueBuilder_ == null) {
        controlValueBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.ControlValue, openfmb.commonmodule.ControlValue.Builder, openfmb.commonmodule.ControlValueOrBuilder>(
                getControlValue(),
                getParentForChildren(),
                isClean());
        controlValue_ = null;
      }
      return controlValueBuilder_;
    }

    private openfmb.commonmodule.CheckConditions check_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.CheckConditions, openfmb.commonmodule.CheckConditions.Builder, openfmb.commonmodule.CheckConditionsOrBuilder> checkBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     * @return Whether the check field is set.
     */
    public boolean hasCheck() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     * @return The check.
     */
    public openfmb.commonmodule.CheckConditions getCheck() {
      if (checkBuilder_ == null) {
        return check_ == null ? openfmb.commonmodule.CheckConditions.getDefaultInstance() : check_;
      } else {
        return checkBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public Builder setCheck(openfmb.commonmodule.CheckConditions value) {
      if (checkBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        check_ = value;
      } else {
        checkBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public Builder setCheck(
        openfmb.commonmodule.CheckConditions.Builder builderForValue) {
      if (checkBuilder_ == null) {
        check_ = builderForValue.build();
      } else {
        checkBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public Builder mergeCheck(openfmb.commonmodule.CheckConditions value) {
      if (checkBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          check_ != null &&
          check_ != openfmb.commonmodule.CheckConditions.getDefaultInstance()) {
          getCheckBuilder().mergeFrom(value);
        } else {
          check_ = value;
        }
      } else {
        checkBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public Builder clearCheck() {
      bitField0_ = (bitField0_ & ~0x00000002);
      check_ = null;
      if (checkBuilder_ != null) {
        checkBuilder_.dispose();
        checkBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public openfmb.commonmodule.CheckConditions.Builder getCheckBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getCheckFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public openfmb.commonmodule.CheckConditionsOrBuilder getCheckOrBuilder() {
      if (checkBuilder_ != null) {
        return checkBuilder_.getMessageOrBuilder();
      } else {
        return check_ == null ?
            openfmb.commonmodule.CheckConditions.getDefaultInstance() : check_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.CheckConditions, openfmb.commonmodule.CheckConditions.Builder, openfmb.commonmodule.CheckConditionsOrBuilder> 
        getCheckFieldBuilder() {
      if (checkBuilder_ == null) {
        checkBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.CheckConditions, openfmb.commonmodule.CheckConditions.Builder, openfmb.commonmodule.CheckConditionsOrBuilder>(
                getCheck(),
                getParentForChildren(),
                isClean());
        check_ = null;
      }
      return checkBuilder_;
    }

    private openfmb.reclosermodule.RecloserDiscreteControlXCBR recloserDiscreteControlXCBR_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.reclosermodule.RecloserDiscreteControlXCBR, openfmb.reclosermodule.RecloserDiscreteControlXCBR.Builder, openfmb.reclosermodule.RecloserDiscreteControlXCBROrBuilder> recloserDiscreteControlXCBRBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserDiscreteControlXCBR recloserDiscreteControlXCBR = 3;</code>
     * @return Whether the recloserDiscreteControlXCBR field is set.
     */
    public boolean hasRecloserDiscreteControlXCBR() {
      return ((bitField0_ & 0x00000004) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserDiscreteControlXCBR recloserDiscreteControlXCBR = 3;</code>
     * @return The recloserDiscreteControlXCBR.
     */
    public openfmb.reclosermodule.RecloserDiscreteControlXCBR getRecloserDiscreteControlXCBR() {
      if (recloserDiscreteControlXCBRBuilder_ == null) {
        return recloserDiscreteControlXCBR_ == null ? openfmb.reclosermodule.RecloserDiscreteControlXCBR.getDefaultInstance() : recloserDiscreteControlXCBR_;
      } else {
        return recloserDiscreteControlXCBRBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserDiscreteControlXCBR recloserDiscreteControlXCBR = 3;</code>
     */
    public Builder setRecloserDiscreteControlXCBR(openfmb.reclosermodule.RecloserDiscreteControlXCBR value) {
      if (recloserDiscreteControlXCBRBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        recloserDiscreteControlXCBR_ = value;
      } else {
        recloserDiscreteControlXCBRBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserDiscreteControlXCBR recloserDiscreteControlXCBR = 3;</code>
     */
    public Builder setRecloserDiscreteControlXCBR(
        openfmb.reclosermodule.RecloserDiscreteControlXCBR.Builder builderForValue) {
      if (recloserDiscreteControlXCBRBuilder_ == null) {
        recloserDiscreteControlXCBR_ = builderForValue.build();
      } else {
        recloserDiscreteControlXCBRBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserDiscreteControlXCBR recloserDiscreteControlXCBR = 3;</code>
     */
    public Builder mergeRecloserDiscreteControlXCBR(openfmb.reclosermodule.RecloserDiscreteControlXCBR value) {
      if (recloserDiscreteControlXCBRBuilder_ == null) {
        if (((bitField0_ & 0x00000004) != 0) &&
          recloserDiscreteControlXCBR_ != null &&
          recloserDiscreteControlXCBR_ != openfmb.reclosermodule.RecloserDiscreteControlXCBR.getDefaultInstance()) {
          getRecloserDiscreteControlXCBRBuilder().mergeFrom(value);
        } else {
          recloserDiscreteControlXCBR_ = value;
        }
      } else {
        recloserDiscreteControlXCBRBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserDiscreteControlXCBR recloserDiscreteControlXCBR = 3;</code>
     */
    public Builder clearRecloserDiscreteControlXCBR() {
      bitField0_ = (bitField0_ & ~0x00000004);
      recloserDiscreteControlXCBR_ = null;
      if (recloserDiscreteControlXCBRBuilder_ != null) {
        recloserDiscreteControlXCBRBuilder_.dispose();
        recloserDiscreteControlXCBRBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserDiscreteControlXCBR recloserDiscreteControlXCBR = 3;</code>
     */
    public openfmb.reclosermodule.RecloserDiscreteControlXCBR.Builder getRecloserDiscreteControlXCBRBuilder() {
      bitField0_ |= 0x00000004;
      onChanged();
      return getRecloserDiscreteControlXCBRFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserDiscreteControlXCBR recloserDiscreteControlXCBR = 3;</code>
     */
    public openfmb.reclosermodule.RecloserDiscreteControlXCBROrBuilder getRecloserDiscreteControlXCBROrBuilder() {
      if (recloserDiscreteControlXCBRBuilder_ != null) {
        return recloserDiscreteControlXCBRBuilder_.getMessageOrBuilder();
      } else {
        return recloserDiscreteControlXCBR_ == null ?
            openfmb.reclosermodule.RecloserDiscreteControlXCBR.getDefaultInstance() : recloserDiscreteControlXCBR_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.reclosermodule.RecloserDiscreteControlXCBR recloserDiscreteControlXCBR = 3;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.reclosermodule.RecloserDiscreteControlXCBR, openfmb.reclosermodule.RecloserDiscreteControlXCBR.Builder, openfmb.reclosermodule.RecloserDiscreteControlXCBROrBuilder> 
        getRecloserDiscreteControlXCBRFieldBuilder() {
      if (recloserDiscreteControlXCBRBuilder_ == null) {
        recloserDiscreteControlXCBRBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.reclosermodule.RecloserDiscreteControlXCBR, openfmb.reclosermodule.RecloserDiscreteControlXCBR.Builder, openfmb.reclosermodule.RecloserDiscreteControlXCBROrBuilder>(
                getRecloserDiscreteControlXCBR(),
                getParentForChildren(),
                isClean());
        recloserDiscreteControlXCBR_ = null;
      }
      return recloserDiscreteControlXCBRBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:reclosermodule.RecloserDiscreteControl)
  }

  // @@protoc_insertion_point(class_scope:reclosermodule.RecloserDiscreteControl)
  private static final openfmb.reclosermodule.RecloserDiscreteControl DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.reclosermodule.RecloserDiscreteControl();
  }

  public static openfmb.reclosermodule.RecloserDiscreteControl getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<RecloserDiscreteControl>
      PARSER = new com.google.protobuf.AbstractParser<RecloserDiscreteControl>() {
    @java.lang.Override
    public RecloserDiscreteControl parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<RecloserDiscreteControl> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<RecloserDiscreteControl> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.reclosermodule.RecloserDiscreteControl getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

