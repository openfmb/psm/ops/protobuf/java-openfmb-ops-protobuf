// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: regulatormodule/regulatormodule.proto

package openfmb.regulatormodule;

public interface RegulatorCSGOrBuilder extends
    // @@protoc_insertion_point(interface_extends:regulatormodule.RegulatorCSG)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * The array with the points specifying a curve shape.
   * </pre>
   *
   * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  java.util.List<openfmb.regulatormodule.RegulatorPoint> 
      getCrvPtsList();
  /**
   * <pre>
   * The array with the points specifying a curve shape.
   * </pre>
   *
   * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.regulatormodule.RegulatorPoint getCrvPts(int index);
  /**
   * <pre>
   * The array with the points specifying a curve shape.
   * </pre>
   *
   * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  int getCrvPtsCount();
  /**
   * <pre>
   * The array with the points specifying a curve shape.
   * </pre>
   *
   * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  java.util.List<? extends openfmb.regulatormodule.RegulatorPointOrBuilder> 
      getCrvPtsOrBuilderList();
  /**
   * <pre>
   * The array with the points specifying a curve shape.
   * </pre>
   *
   * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.regulatormodule.RegulatorPointOrBuilder getCrvPtsOrBuilder(
      int index);
}
