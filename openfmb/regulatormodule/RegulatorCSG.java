// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: regulatormodule/regulatormodule.proto

package openfmb.regulatormodule;

/**
 * <pre>
 * Curve shape setting (FC=SP) (CSG_SP)
 * </pre>
 *
 * Protobuf type {@code regulatormodule.RegulatorCSG}
 */
public final class RegulatorCSG extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:regulatormodule.RegulatorCSG)
    RegulatorCSGOrBuilder {
private static final long serialVersionUID = 0L;
  // Use RegulatorCSG.newBuilder() to construct.
  private RegulatorCSG(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private RegulatorCSG() {
    crvPts_ = java.util.Collections.emptyList();
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new RegulatorCSG();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.regulatormodule.Regulatormodule.internal_static_regulatormodule_RegulatorCSG_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.regulatormodule.Regulatormodule.internal_static_regulatormodule_RegulatorCSG_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.regulatormodule.RegulatorCSG.class, openfmb.regulatormodule.RegulatorCSG.Builder.class);
  }

  public static final int CRVPTS_FIELD_NUMBER = 1;
  @SuppressWarnings("serial")
  private java.util.List<openfmb.regulatormodule.RegulatorPoint> crvPts_;
  /**
   * <pre>
   * The array with the points specifying a curve shape.
   * </pre>
   *
   * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public java.util.List<openfmb.regulatormodule.RegulatorPoint> getCrvPtsList() {
    return crvPts_;
  }
  /**
   * <pre>
   * The array with the points specifying a curve shape.
   * </pre>
   *
   * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public java.util.List<? extends openfmb.regulatormodule.RegulatorPointOrBuilder> 
      getCrvPtsOrBuilderList() {
    return crvPts_;
  }
  /**
   * <pre>
   * The array with the points specifying a curve shape.
   * </pre>
   *
   * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public int getCrvPtsCount() {
    return crvPts_.size();
  }
  /**
   * <pre>
   * The array with the points specifying a curve shape.
   * </pre>
   *
   * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.regulatormodule.RegulatorPoint getCrvPts(int index) {
    return crvPts_.get(index);
  }
  /**
   * <pre>
   * The array with the points specifying a curve shape.
   * </pre>
   *
   * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.regulatormodule.RegulatorPointOrBuilder getCrvPtsOrBuilder(
      int index) {
    return crvPts_.get(index);
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    for (int i = 0; i < crvPts_.size(); i++) {
      output.writeMessage(1, crvPts_.get(i));
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    for (int i = 0; i < crvPts_.size(); i++) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, crvPts_.get(i));
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.regulatormodule.RegulatorCSG)) {
      return super.equals(obj);
    }
    openfmb.regulatormodule.RegulatorCSG other = (openfmb.regulatormodule.RegulatorCSG) obj;

    if (!getCrvPtsList()
        .equals(other.getCrvPtsList())) return false;
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (getCrvPtsCount() > 0) {
      hash = (37 * hash) + CRVPTS_FIELD_NUMBER;
      hash = (53 * hash) + getCrvPtsList().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.regulatormodule.RegulatorCSG parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.regulatormodule.RegulatorCSG parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.regulatormodule.RegulatorCSG parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.regulatormodule.RegulatorCSG parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.regulatormodule.RegulatorCSG parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.regulatormodule.RegulatorCSG parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.regulatormodule.RegulatorCSG parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.regulatormodule.RegulatorCSG parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.regulatormodule.RegulatorCSG parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.regulatormodule.RegulatorCSG parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.regulatormodule.RegulatorCSG parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.regulatormodule.RegulatorCSG parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.regulatormodule.RegulatorCSG prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Curve shape setting (FC=SP) (CSG_SP)
   * </pre>
   *
   * Protobuf type {@code regulatormodule.RegulatorCSG}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:regulatormodule.RegulatorCSG)
      openfmb.regulatormodule.RegulatorCSGOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.regulatormodule.Regulatormodule.internal_static_regulatormodule_RegulatorCSG_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.regulatormodule.Regulatormodule.internal_static_regulatormodule_RegulatorCSG_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.regulatormodule.RegulatorCSG.class, openfmb.regulatormodule.RegulatorCSG.Builder.class);
    }

    // Construct using openfmb.regulatormodule.RegulatorCSG.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      if (crvPtsBuilder_ == null) {
        crvPts_ = java.util.Collections.emptyList();
      } else {
        crvPts_ = null;
        crvPtsBuilder_.clear();
      }
      bitField0_ = (bitField0_ & ~0x00000001);
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.regulatormodule.Regulatormodule.internal_static_regulatormodule_RegulatorCSG_descriptor;
    }

    @java.lang.Override
    public openfmb.regulatormodule.RegulatorCSG getDefaultInstanceForType() {
      return openfmb.regulatormodule.RegulatorCSG.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.regulatormodule.RegulatorCSG build() {
      openfmb.regulatormodule.RegulatorCSG result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.regulatormodule.RegulatorCSG buildPartial() {
      openfmb.regulatormodule.RegulatorCSG result = new openfmb.regulatormodule.RegulatorCSG(this);
      buildPartialRepeatedFields(result);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartialRepeatedFields(openfmb.regulatormodule.RegulatorCSG result) {
      if (crvPtsBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0)) {
          crvPts_ = java.util.Collections.unmodifiableList(crvPts_);
          bitField0_ = (bitField0_ & ~0x00000001);
        }
        result.crvPts_ = crvPts_;
      } else {
        result.crvPts_ = crvPtsBuilder_.build();
      }
    }

    private void buildPartial0(openfmb.regulatormodule.RegulatorCSG result) {
      int from_bitField0_ = bitField0_;
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.regulatormodule.RegulatorCSG) {
        return mergeFrom((openfmb.regulatormodule.RegulatorCSG)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.regulatormodule.RegulatorCSG other) {
      if (other == openfmb.regulatormodule.RegulatorCSG.getDefaultInstance()) return this;
      if (crvPtsBuilder_ == null) {
        if (!other.crvPts_.isEmpty()) {
          if (crvPts_.isEmpty()) {
            crvPts_ = other.crvPts_;
            bitField0_ = (bitField0_ & ~0x00000001);
          } else {
            ensureCrvPtsIsMutable();
            crvPts_.addAll(other.crvPts_);
          }
          onChanged();
        }
      } else {
        if (!other.crvPts_.isEmpty()) {
          if (crvPtsBuilder_.isEmpty()) {
            crvPtsBuilder_.dispose();
            crvPtsBuilder_ = null;
            crvPts_ = other.crvPts_;
            bitField0_ = (bitField0_ & ~0x00000001);
            crvPtsBuilder_ = 
              com.google.protobuf.GeneratedMessageV3.alwaysUseFieldBuilders ?
                 getCrvPtsFieldBuilder() : null;
          } else {
            crvPtsBuilder_.addAllMessages(other.crvPts_);
          }
        }
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              openfmb.regulatormodule.RegulatorPoint m =
                  input.readMessage(
                      openfmb.regulatormodule.RegulatorPoint.parser(),
                      extensionRegistry);
              if (crvPtsBuilder_ == null) {
                ensureCrvPtsIsMutable();
                crvPts_.add(m);
              } else {
                crvPtsBuilder_.addMessage(m);
              }
              break;
            } // case 10
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private java.util.List<openfmb.regulatormodule.RegulatorPoint> crvPts_ =
      java.util.Collections.emptyList();
    private void ensureCrvPtsIsMutable() {
      if (!((bitField0_ & 0x00000001) != 0)) {
        crvPts_ = new java.util.ArrayList<openfmb.regulatormodule.RegulatorPoint>(crvPts_);
        bitField0_ |= 0x00000001;
       }
    }

    private com.google.protobuf.RepeatedFieldBuilderV3<
        openfmb.regulatormodule.RegulatorPoint, openfmb.regulatormodule.RegulatorPoint.Builder, openfmb.regulatormodule.RegulatorPointOrBuilder> crvPtsBuilder_;

    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public java.util.List<openfmb.regulatormodule.RegulatorPoint> getCrvPtsList() {
      if (crvPtsBuilder_ == null) {
        return java.util.Collections.unmodifiableList(crvPts_);
      } else {
        return crvPtsBuilder_.getMessageList();
      }
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public int getCrvPtsCount() {
      if (crvPtsBuilder_ == null) {
        return crvPts_.size();
      } else {
        return crvPtsBuilder_.getCount();
      }
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.regulatormodule.RegulatorPoint getCrvPts(int index) {
      if (crvPtsBuilder_ == null) {
        return crvPts_.get(index);
      } else {
        return crvPtsBuilder_.getMessage(index);
      }
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setCrvPts(
        int index, openfmb.regulatormodule.RegulatorPoint value) {
      if (crvPtsBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureCrvPtsIsMutable();
        crvPts_.set(index, value);
        onChanged();
      } else {
        crvPtsBuilder_.setMessage(index, value);
      }
      return this;
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setCrvPts(
        int index, openfmb.regulatormodule.RegulatorPoint.Builder builderForValue) {
      if (crvPtsBuilder_ == null) {
        ensureCrvPtsIsMutable();
        crvPts_.set(index, builderForValue.build());
        onChanged();
      } else {
        crvPtsBuilder_.setMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder addCrvPts(openfmb.regulatormodule.RegulatorPoint value) {
      if (crvPtsBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureCrvPtsIsMutable();
        crvPts_.add(value);
        onChanged();
      } else {
        crvPtsBuilder_.addMessage(value);
      }
      return this;
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder addCrvPts(
        int index, openfmb.regulatormodule.RegulatorPoint value) {
      if (crvPtsBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureCrvPtsIsMutable();
        crvPts_.add(index, value);
        onChanged();
      } else {
        crvPtsBuilder_.addMessage(index, value);
      }
      return this;
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder addCrvPts(
        openfmb.regulatormodule.RegulatorPoint.Builder builderForValue) {
      if (crvPtsBuilder_ == null) {
        ensureCrvPtsIsMutable();
        crvPts_.add(builderForValue.build());
        onChanged();
      } else {
        crvPtsBuilder_.addMessage(builderForValue.build());
      }
      return this;
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder addCrvPts(
        int index, openfmb.regulatormodule.RegulatorPoint.Builder builderForValue) {
      if (crvPtsBuilder_ == null) {
        ensureCrvPtsIsMutable();
        crvPts_.add(index, builderForValue.build());
        onChanged();
      } else {
        crvPtsBuilder_.addMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder addAllCrvPts(
        java.lang.Iterable<? extends openfmb.regulatormodule.RegulatorPoint> values) {
      if (crvPtsBuilder_ == null) {
        ensureCrvPtsIsMutable();
        com.google.protobuf.AbstractMessageLite.Builder.addAll(
            values, crvPts_);
        onChanged();
      } else {
        crvPtsBuilder_.addAllMessages(values);
      }
      return this;
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder clearCrvPts() {
      if (crvPtsBuilder_ == null) {
        crvPts_ = java.util.Collections.emptyList();
        bitField0_ = (bitField0_ & ~0x00000001);
        onChanged();
      } else {
        crvPtsBuilder_.clear();
      }
      return this;
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder removeCrvPts(int index) {
      if (crvPtsBuilder_ == null) {
        ensureCrvPtsIsMutable();
        crvPts_.remove(index);
        onChanged();
      } else {
        crvPtsBuilder_.remove(index);
      }
      return this;
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.regulatormodule.RegulatorPoint.Builder getCrvPtsBuilder(
        int index) {
      return getCrvPtsFieldBuilder().getBuilder(index);
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.regulatormodule.RegulatorPointOrBuilder getCrvPtsOrBuilder(
        int index) {
      if (crvPtsBuilder_ == null) {
        return crvPts_.get(index);  } else {
        return crvPtsBuilder_.getMessageOrBuilder(index);
      }
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public java.util.List<? extends openfmb.regulatormodule.RegulatorPointOrBuilder> 
         getCrvPtsOrBuilderList() {
      if (crvPtsBuilder_ != null) {
        return crvPtsBuilder_.getMessageOrBuilderList();
      } else {
        return java.util.Collections.unmodifiableList(crvPts_);
      }
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.regulatormodule.RegulatorPoint.Builder addCrvPtsBuilder() {
      return getCrvPtsFieldBuilder().addBuilder(
          openfmb.regulatormodule.RegulatorPoint.getDefaultInstance());
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.regulatormodule.RegulatorPoint.Builder addCrvPtsBuilder(
        int index) {
      return getCrvPtsFieldBuilder().addBuilder(
          index, openfmb.regulatormodule.RegulatorPoint.getDefaultInstance());
    }
    /**
     * <pre>
     * The array with the points specifying a curve shape.
     * </pre>
     *
     * <code>repeated .regulatormodule.RegulatorPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public java.util.List<openfmb.regulatormodule.RegulatorPoint.Builder> 
         getCrvPtsBuilderList() {
      return getCrvPtsFieldBuilder().getBuilderList();
    }
    private com.google.protobuf.RepeatedFieldBuilderV3<
        openfmb.regulatormodule.RegulatorPoint, openfmb.regulatormodule.RegulatorPoint.Builder, openfmb.regulatormodule.RegulatorPointOrBuilder> 
        getCrvPtsFieldBuilder() {
      if (crvPtsBuilder_ == null) {
        crvPtsBuilder_ = new com.google.protobuf.RepeatedFieldBuilderV3<
            openfmb.regulatormodule.RegulatorPoint, openfmb.regulatormodule.RegulatorPoint.Builder, openfmb.regulatormodule.RegulatorPointOrBuilder>(
                crvPts_,
                ((bitField0_ & 0x00000001) != 0),
                getParentForChildren(),
                isClean());
        crvPts_ = null;
      }
      return crvPtsBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:regulatormodule.RegulatorCSG)
  }

  // @@protoc_insertion_point(class_scope:regulatormodule.RegulatorCSG)
  private static final openfmb.regulatormodule.RegulatorCSG DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.regulatormodule.RegulatorCSG();
  }

  public static openfmb.regulatormodule.RegulatorCSG getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<RegulatorCSG>
      PARSER = new com.google.protobuf.AbstractParser<RegulatorCSG>() {
    @java.lang.Override
    public RegulatorCSG parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<RegulatorCSG> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<RegulatorCSG> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.regulatormodule.RegulatorCSG getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

