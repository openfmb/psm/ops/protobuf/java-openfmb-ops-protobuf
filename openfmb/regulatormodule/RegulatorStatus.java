// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: regulatormodule/regulatormodule.proto

package openfmb.regulatormodule;

/**
 * <pre>
 * Regulator status
 * </pre>
 *
 * Protobuf type {@code regulatormodule.RegulatorStatus}
 */
public final class RegulatorStatus extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:regulatormodule.RegulatorStatus)
    RegulatorStatusOrBuilder {
private static final long serialVersionUID = 0L;
  // Use RegulatorStatus.newBuilder() to construct.
  private RegulatorStatus(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private RegulatorStatus() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new RegulatorStatus();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.regulatormodule.Regulatormodule.internal_static_regulatormodule_RegulatorStatus_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.regulatormodule.Regulatormodule.internal_static_regulatormodule_RegulatorStatus_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.regulatormodule.RegulatorStatus.class, openfmb.regulatormodule.RegulatorStatus.Builder.class);
  }

  public static final int STATUSVALUE_FIELD_NUMBER = 1;
  private openfmb.commonmodule.StatusValue statusValue_;
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the statusValue field is set.
   */
  @java.lang.Override
  public boolean hasStatusValue() {
    return statusValue_ != null;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
   * @return The statusValue.
   */
  @java.lang.Override
  public openfmb.commonmodule.StatusValue getStatusValue() {
    return statusValue_ == null ? openfmb.commonmodule.StatusValue.getDefaultInstance() : statusValue_;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.StatusValueOrBuilder getStatusValueOrBuilder() {
    return statusValue_ == null ? openfmb.commonmodule.StatusValue.getDefaultInstance() : statusValue_;
  }

  public static final int REGULATOREVENTANDSTATUSANCR_FIELD_NUMBER = 2;
  private openfmb.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR = 2;</code>
   * @return Whether the regulatorEventAndStatusANCR field is set.
   */
  @java.lang.Override
  public boolean hasRegulatorEventAndStatusANCR() {
    return regulatorEventAndStatusANCR_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR = 2;</code>
   * @return The regulatorEventAndStatusANCR.
   */
  @java.lang.Override
  public openfmb.regulatormodule.RegulatorEventAndStatusANCR getRegulatorEventAndStatusANCR() {
    return regulatorEventAndStatusANCR_ == null ? openfmb.regulatormodule.RegulatorEventAndStatusANCR.getDefaultInstance() : regulatorEventAndStatusANCR_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR = 2;</code>
   */
  @java.lang.Override
  public openfmb.regulatormodule.RegulatorEventAndStatusANCROrBuilder getRegulatorEventAndStatusANCROrBuilder() {
    return regulatorEventAndStatusANCR_ == null ? openfmb.regulatormodule.RegulatorEventAndStatusANCR.getDefaultInstance() : regulatorEventAndStatusANCR_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (statusValue_ != null) {
      output.writeMessage(1, getStatusValue());
    }
    if (regulatorEventAndStatusANCR_ != null) {
      output.writeMessage(2, getRegulatorEventAndStatusANCR());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (statusValue_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getStatusValue());
    }
    if (regulatorEventAndStatusANCR_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getRegulatorEventAndStatusANCR());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.regulatormodule.RegulatorStatus)) {
      return super.equals(obj);
    }
    openfmb.regulatormodule.RegulatorStatus other = (openfmb.regulatormodule.RegulatorStatus) obj;

    if (hasStatusValue() != other.hasStatusValue()) return false;
    if (hasStatusValue()) {
      if (!getStatusValue()
          .equals(other.getStatusValue())) return false;
    }
    if (hasRegulatorEventAndStatusANCR() != other.hasRegulatorEventAndStatusANCR()) return false;
    if (hasRegulatorEventAndStatusANCR()) {
      if (!getRegulatorEventAndStatusANCR()
          .equals(other.getRegulatorEventAndStatusANCR())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasStatusValue()) {
      hash = (37 * hash) + STATUSVALUE_FIELD_NUMBER;
      hash = (53 * hash) + getStatusValue().hashCode();
    }
    if (hasRegulatorEventAndStatusANCR()) {
      hash = (37 * hash) + REGULATOREVENTANDSTATUSANCR_FIELD_NUMBER;
      hash = (53 * hash) + getRegulatorEventAndStatusANCR().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.regulatormodule.RegulatorStatus parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.regulatormodule.RegulatorStatus parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.regulatormodule.RegulatorStatus parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.regulatormodule.RegulatorStatus parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.regulatormodule.RegulatorStatus parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.regulatormodule.RegulatorStatus parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.regulatormodule.RegulatorStatus parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.regulatormodule.RegulatorStatus parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.regulatormodule.RegulatorStatus parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.regulatormodule.RegulatorStatus parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.regulatormodule.RegulatorStatus parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.regulatormodule.RegulatorStatus parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.regulatormodule.RegulatorStatus prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Regulator status
   * </pre>
   *
   * Protobuf type {@code regulatormodule.RegulatorStatus}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:regulatormodule.RegulatorStatus)
      openfmb.regulatormodule.RegulatorStatusOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.regulatormodule.Regulatormodule.internal_static_regulatormodule_RegulatorStatus_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.regulatormodule.Regulatormodule.internal_static_regulatormodule_RegulatorStatus_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.regulatormodule.RegulatorStatus.class, openfmb.regulatormodule.RegulatorStatus.Builder.class);
    }

    // Construct using openfmb.regulatormodule.RegulatorStatus.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      statusValue_ = null;
      if (statusValueBuilder_ != null) {
        statusValueBuilder_.dispose();
        statusValueBuilder_ = null;
      }
      regulatorEventAndStatusANCR_ = null;
      if (regulatorEventAndStatusANCRBuilder_ != null) {
        regulatorEventAndStatusANCRBuilder_.dispose();
        regulatorEventAndStatusANCRBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.regulatormodule.Regulatormodule.internal_static_regulatormodule_RegulatorStatus_descriptor;
    }

    @java.lang.Override
    public openfmb.regulatormodule.RegulatorStatus getDefaultInstanceForType() {
      return openfmb.regulatormodule.RegulatorStatus.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.regulatormodule.RegulatorStatus build() {
      openfmb.regulatormodule.RegulatorStatus result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.regulatormodule.RegulatorStatus buildPartial() {
      openfmb.regulatormodule.RegulatorStatus result = new openfmb.regulatormodule.RegulatorStatus(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.regulatormodule.RegulatorStatus result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.statusValue_ = statusValueBuilder_ == null
            ? statusValue_
            : statusValueBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.regulatorEventAndStatusANCR_ = regulatorEventAndStatusANCRBuilder_ == null
            ? regulatorEventAndStatusANCR_
            : regulatorEventAndStatusANCRBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.regulatormodule.RegulatorStatus) {
        return mergeFrom((openfmb.regulatormodule.RegulatorStatus)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.regulatormodule.RegulatorStatus other) {
      if (other == openfmb.regulatormodule.RegulatorStatus.getDefaultInstance()) return this;
      if (other.hasStatusValue()) {
        mergeStatusValue(other.getStatusValue());
      }
      if (other.hasRegulatorEventAndStatusANCR()) {
        mergeRegulatorEventAndStatusANCR(other.getRegulatorEventAndStatusANCR());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getStatusValueFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getRegulatorEventAndStatusANCRFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.StatusValue statusValue_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.StatusValue, openfmb.commonmodule.StatusValue.Builder, openfmb.commonmodule.StatusValueOrBuilder> statusValueBuilder_;
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     * @return Whether the statusValue field is set.
     */
    public boolean hasStatusValue() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     * @return The statusValue.
     */
    public openfmb.commonmodule.StatusValue getStatusValue() {
      if (statusValueBuilder_ == null) {
        return statusValue_ == null ? openfmb.commonmodule.StatusValue.getDefaultInstance() : statusValue_;
      } else {
        return statusValueBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setStatusValue(openfmb.commonmodule.StatusValue value) {
      if (statusValueBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        statusValue_ = value;
      } else {
        statusValueBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setStatusValue(
        openfmb.commonmodule.StatusValue.Builder builderForValue) {
      if (statusValueBuilder_ == null) {
        statusValue_ = builderForValue.build();
      } else {
        statusValueBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder mergeStatusValue(openfmb.commonmodule.StatusValue value) {
      if (statusValueBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          statusValue_ != null &&
          statusValue_ != openfmb.commonmodule.StatusValue.getDefaultInstance()) {
          getStatusValueBuilder().mergeFrom(value);
        } else {
          statusValue_ = value;
        }
      } else {
        statusValueBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder clearStatusValue() {
      bitField0_ = (bitField0_ & ~0x00000001);
      statusValue_ = null;
      if (statusValueBuilder_ != null) {
        statusValueBuilder_.dispose();
        statusValueBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.StatusValue.Builder getStatusValueBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getStatusValueFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.StatusValueOrBuilder getStatusValueOrBuilder() {
      if (statusValueBuilder_ != null) {
        return statusValueBuilder_.getMessageOrBuilder();
      } else {
        return statusValue_ == null ?
            openfmb.commonmodule.StatusValue.getDefaultInstance() : statusValue_;
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.StatusValue, openfmb.commonmodule.StatusValue.Builder, openfmb.commonmodule.StatusValueOrBuilder> 
        getStatusValueFieldBuilder() {
      if (statusValueBuilder_ == null) {
        statusValueBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.StatusValue, openfmb.commonmodule.StatusValue.Builder, openfmb.commonmodule.StatusValueOrBuilder>(
                getStatusValue(),
                getParentForChildren(),
                isClean());
        statusValue_ = null;
      }
      return statusValueBuilder_;
    }

    private openfmb.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.regulatormodule.RegulatorEventAndStatusANCR, openfmb.regulatormodule.RegulatorEventAndStatusANCR.Builder, openfmb.regulatormodule.RegulatorEventAndStatusANCROrBuilder> regulatorEventAndStatusANCRBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR = 2;</code>
     * @return Whether the regulatorEventAndStatusANCR field is set.
     */
    public boolean hasRegulatorEventAndStatusANCR() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR = 2;</code>
     * @return The regulatorEventAndStatusANCR.
     */
    public openfmb.regulatormodule.RegulatorEventAndStatusANCR getRegulatorEventAndStatusANCR() {
      if (regulatorEventAndStatusANCRBuilder_ == null) {
        return regulatorEventAndStatusANCR_ == null ? openfmb.regulatormodule.RegulatorEventAndStatusANCR.getDefaultInstance() : regulatorEventAndStatusANCR_;
      } else {
        return regulatorEventAndStatusANCRBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR = 2;</code>
     */
    public Builder setRegulatorEventAndStatusANCR(openfmb.regulatormodule.RegulatorEventAndStatusANCR value) {
      if (regulatorEventAndStatusANCRBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        regulatorEventAndStatusANCR_ = value;
      } else {
        regulatorEventAndStatusANCRBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR = 2;</code>
     */
    public Builder setRegulatorEventAndStatusANCR(
        openfmb.regulatormodule.RegulatorEventAndStatusANCR.Builder builderForValue) {
      if (regulatorEventAndStatusANCRBuilder_ == null) {
        regulatorEventAndStatusANCR_ = builderForValue.build();
      } else {
        regulatorEventAndStatusANCRBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR = 2;</code>
     */
    public Builder mergeRegulatorEventAndStatusANCR(openfmb.regulatormodule.RegulatorEventAndStatusANCR value) {
      if (regulatorEventAndStatusANCRBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          regulatorEventAndStatusANCR_ != null &&
          regulatorEventAndStatusANCR_ != openfmb.regulatormodule.RegulatorEventAndStatusANCR.getDefaultInstance()) {
          getRegulatorEventAndStatusANCRBuilder().mergeFrom(value);
        } else {
          regulatorEventAndStatusANCR_ = value;
        }
      } else {
        regulatorEventAndStatusANCRBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR = 2;</code>
     */
    public Builder clearRegulatorEventAndStatusANCR() {
      bitField0_ = (bitField0_ & ~0x00000002);
      regulatorEventAndStatusANCR_ = null;
      if (regulatorEventAndStatusANCRBuilder_ != null) {
        regulatorEventAndStatusANCRBuilder_.dispose();
        regulatorEventAndStatusANCRBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR = 2;</code>
     */
    public openfmb.regulatormodule.RegulatorEventAndStatusANCR.Builder getRegulatorEventAndStatusANCRBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getRegulatorEventAndStatusANCRFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR = 2;</code>
     */
    public openfmb.regulatormodule.RegulatorEventAndStatusANCROrBuilder getRegulatorEventAndStatusANCROrBuilder() {
      if (regulatorEventAndStatusANCRBuilder_ != null) {
        return regulatorEventAndStatusANCRBuilder_.getMessageOrBuilder();
      } else {
        return regulatorEventAndStatusANCR_ == null ?
            openfmb.regulatormodule.RegulatorEventAndStatusANCR.getDefaultInstance() : regulatorEventAndStatusANCR_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR = 2;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.regulatormodule.RegulatorEventAndStatusANCR, openfmb.regulatormodule.RegulatorEventAndStatusANCR.Builder, openfmb.regulatormodule.RegulatorEventAndStatusANCROrBuilder> 
        getRegulatorEventAndStatusANCRFieldBuilder() {
      if (regulatorEventAndStatusANCRBuilder_ == null) {
        regulatorEventAndStatusANCRBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.regulatormodule.RegulatorEventAndStatusANCR, openfmb.regulatormodule.RegulatorEventAndStatusANCR.Builder, openfmb.regulatormodule.RegulatorEventAndStatusANCROrBuilder>(
                getRegulatorEventAndStatusANCR(),
                getParentForChildren(),
                isClean());
        regulatorEventAndStatusANCR_ = null;
      }
      return regulatorEventAndStatusANCRBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:regulatormodule.RegulatorStatus)
  }

  // @@protoc_insertion_point(class_scope:regulatormodule.RegulatorStatus)
  private static final openfmb.regulatormodule.RegulatorStatus DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.regulatormodule.RegulatorStatus();
  }

  public static openfmb.regulatormodule.RegulatorStatus getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<RegulatorStatus>
      PARSER = new com.google.protobuf.AbstractParser<RegulatorStatus>() {
    @java.lang.Override
    public RegulatorStatus parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<RegulatorStatus> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<RegulatorStatus> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.regulatormodule.RegulatorStatus getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

