// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: regulatormodule/regulatormodule.proto

package openfmb.regulatormodule;

public interface RegulatorStatusOrBuilder extends
    // @@protoc_insertion_point(interface_extends:regulatormodule.RegulatorStatus)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the statusValue field is set.
   */
  boolean hasStatusValue();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
   * @return The statusValue.
   */
  openfmb.commonmodule.StatusValue getStatusValue();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
   */
  openfmb.commonmodule.StatusValueOrBuilder getStatusValueOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR = 2;</code>
   * @return Whether the regulatorEventAndStatusANCR field is set.
   */
  boolean hasRegulatorEventAndStatusANCR();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR = 2;</code>
   * @return The regulatorEventAndStatusANCR.
   */
  openfmb.regulatormodule.RegulatorEventAndStatusANCR getRegulatorEventAndStatusANCR();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.regulatormodule.RegulatorEventAndStatusANCR regulatorEventAndStatusANCR = 2;</code>
   */
  openfmb.regulatormodule.RegulatorEventAndStatusANCROrBuilder getRegulatorEventAndStatusANCROrBuilder();
}
