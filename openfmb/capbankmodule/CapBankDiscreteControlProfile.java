// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: capbankmodule/capbankmodule.proto

package openfmb.capbankmodule;

/**
 * <pre>
 * Cap bank discrete control profile.  Instructs an end device (or an end device group) to perform
 * a specified action.
 * </pre>
 *
 * Protobuf type {@code capbankmodule.CapBankDiscreteControlProfile}
 */
public final class CapBankDiscreteControlProfile extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:capbankmodule.CapBankDiscreteControlProfile)
    CapBankDiscreteControlProfileOrBuilder {
private static final long serialVersionUID = 0L;
  // Use CapBankDiscreteControlProfile.newBuilder() to construct.
  private CapBankDiscreteControlProfile(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private CapBankDiscreteControlProfile() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new CapBankDiscreteControlProfile();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.capbankmodule.Capbankmodule.internal_static_capbankmodule_CapBankDiscreteControlProfile_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.capbankmodule.Capbankmodule.internal_static_capbankmodule_CapBankDiscreteControlProfile_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.capbankmodule.CapBankDiscreteControlProfile.class, openfmb.capbankmodule.CapBankDiscreteControlProfile.Builder.class);
  }

  public static final int CONTROLMESSAGEINFO_FIELD_NUMBER = 1;
  private openfmb.commonmodule.ControlMessageInfo controlMessageInfo_;
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the controlMessageInfo field is set.
   */
  @java.lang.Override
  public boolean hasControlMessageInfo() {
    return controlMessageInfo_ != null;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return The controlMessageInfo.
   */
  @java.lang.Override
  public openfmb.commonmodule.ControlMessageInfo getControlMessageInfo() {
    return controlMessageInfo_ == null ? openfmb.commonmodule.ControlMessageInfo.getDefaultInstance() : controlMessageInfo_;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.ControlMessageInfoOrBuilder getControlMessageInfoOrBuilder() {
    return controlMessageInfo_ == null ? openfmb.commonmodule.ControlMessageInfo.getDefaultInstance() : controlMessageInfo_;
  }

  public static final int CAPBANKCONTROL_FIELD_NUMBER = 2;
  private openfmb.capbankmodule.CapBankDiscreteControl capBankControl_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.capbankmodule.CapBankDiscreteControl capBankControl = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the capBankControl field is set.
   */
  @java.lang.Override
  public boolean hasCapBankControl() {
    return capBankControl_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.capbankmodule.CapBankDiscreteControl capBankControl = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The capBankControl.
   */
  @java.lang.Override
  public openfmb.capbankmodule.CapBankDiscreteControl getCapBankControl() {
    return capBankControl_ == null ? openfmb.capbankmodule.CapBankDiscreteControl.getDefaultInstance() : capBankControl_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.capbankmodule.CapBankDiscreteControl capBankControl = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.capbankmodule.CapBankDiscreteControlOrBuilder getCapBankControlOrBuilder() {
    return capBankControl_ == null ? openfmb.capbankmodule.CapBankDiscreteControl.getDefaultInstance() : capBankControl_;
  }

  public static final int CAPBANKSYSTEM_FIELD_NUMBER = 3;
  private openfmb.capbankmodule.CapBankSystem capBankSystem_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.capbankmodule.CapBankSystem capBankSystem = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the capBankSystem field is set.
   */
  @java.lang.Override
  public boolean hasCapBankSystem() {
    return capBankSystem_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.capbankmodule.CapBankSystem capBankSystem = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The capBankSystem.
   */
  @java.lang.Override
  public openfmb.capbankmodule.CapBankSystem getCapBankSystem() {
    return capBankSystem_ == null ? openfmb.capbankmodule.CapBankSystem.getDefaultInstance() : capBankSystem_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.capbankmodule.CapBankSystem capBankSystem = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.capbankmodule.CapBankSystemOrBuilder getCapBankSystemOrBuilder() {
    return capBankSystem_ == null ? openfmb.capbankmodule.CapBankSystem.getDefaultInstance() : capBankSystem_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (controlMessageInfo_ != null) {
      output.writeMessage(1, getControlMessageInfo());
    }
    if (capBankControl_ != null) {
      output.writeMessage(2, getCapBankControl());
    }
    if (capBankSystem_ != null) {
      output.writeMessage(3, getCapBankSystem());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (controlMessageInfo_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getControlMessageInfo());
    }
    if (capBankControl_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getCapBankControl());
    }
    if (capBankSystem_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(3, getCapBankSystem());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.capbankmodule.CapBankDiscreteControlProfile)) {
      return super.equals(obj);
    }
    openfmb.capbankmodule.CapBankDiscreteControlProfile other = (openfmb.capbankmodule.CapBankDiscreteControlProfile) obj;

    if (hasControlMessageInfo() != other.hasControlMessageInfo()) return false;
    if (hasControlMessageInfo()) {
      if (!getControlMessageInfo()
          .equals(other.getControlMessageInfo())) return false;
    }
    if (hasCapBankControl() != other.hasCapBankControl()) return false;
    if (hasCapBankControl()) {
      if (!getCapBankControl()
          .equals(other.getCapBankControl())) return false;
    }
    if (hasCapBankSystem() != other.hasCapBankSystem()) return false;
    if (hasCapBankSystem()) {
      if (!getCapBankSystem()
          .equals(other.getCapBankSystem())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasControlMessageInfo()) {
      hash = (37 * hash) + CONTROLMESSAGEINFO_FIELD_NUMBER;
      hash = (53 * hash) + getControlMessageInfo().hashCode();
    }
    if (hasCapBankControl()) {
      hash = (37 * hash) + CAPBANKCONTROL_FIELD_NUMBER;
      hash = (53 * hash) + getCapBankControl().hashCode();
    }
    if (hasCapBankSystem()) {
      hash = (37 * hash) + CAPBANKSYSTEM_FIELD_NUMBER;
      hash = (53 * hash) + getCapBankSystem().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.capbankmodule.CapBankDiscreteControlProfile parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.capbankmodule.CapBankDiscreteControlProfile parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.capbankmodule.CapBankDiscreteControlProfile parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.capbankmodule.CapBankDiscreteControlProfile parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.capbankmodule.CapBankDiscreteControlProfile parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.capbankmodule.CapBankDiscreteControlProfile parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.capbankmodule.CapBankDiscreteControlProfile parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.capbankmodule.CapBankDiscreteControlProfile parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.capbankmodule.CapBankDiscreteControlProfile parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.capbankmodule.CapBankDiscreteControlProfile parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.capbankmodule.CapBankDiscreteControlProfile parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.capbankmodule.CapBankDiscreteControlProfile parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.capbankmodule.CapBankDiscreteControlProfile prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Cap bank discrete control profile.  Instructs an end device (or an end device group) to perform
   * a specified action.
   * </pre>
   *
   * Protobuf type {@code capbankmodule.CapBankDiscreteControlProfile}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:capbankmodule.CapBankDiscreteControlProfile)
      openfmb.capbankmodule.CapBankDiscreteControlProfileOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.capbankmodule.Capbankmodule.internal_static_capbankmodule_CapBankDiscreteControlProfile_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.capbankmodule.Capbankmodule.internal_static_capbankmodule_CapBankDiscreteControlProfile_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.capbankmodule.CapBankDiscreteControlProfile.class, openfmb.capbankmodule.CapBankDiscreteControlProfile.Builder.class);
    }

    // Construct using openfmb.capbankmodule.CapBankDiscreteControlProfile.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      controlMessageInfo_ = null;
      if (controlMessageInfoBuilder_ != null) {
        controlMessageInfoBuilder_.dispose();
        controlMessageInfoBuilder_ = null;
      }
      capBankControl_ = null;
      if (capBankControlBuilder_ != null) {
        capBankControlBuilder_.dispose();
        capBankControlBuilder_ = null;
      }
      capBankSystem_ = null;
      if (capBankSystemBuilder_ != null) {
        capBankSystemBuilder_.dispose();
        capBankSystemBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.capbankmodule.Capbankmodule.internal_static_capbankmodule_CapBankDiscreteControlProfile_descriptor;
    }

    @java.lang.Override
    public openfmb.capbankmodule.CapBankDiscreteControlProfile getDefaultInstanceForType() {
      return openfmb.capbankmodule.CapBankDiscreteControlProfile.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.capbankmodule.CapBankDiscreteControlProfile build() {
      openfmb.capbankmodule.CapBankDiscreteControlProfile result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.capbankmodule.CapBankDiscreteControlProfile buildPartial() {
      openfmb.capbankmodule.CapBankDiscreteControlProfile result = new openfmb.capbankmodule.CapBankDiscreteControlProfile(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.capbankmodule.CapBankDiscreteControlProfile result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.controlMessageInfo_ = controlMessageInfoBuilder_ == null
            ? controlMessageInfo_
            : controlMessageInfoBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.capBankControl_ = capBankControlBuilder_ == null
            ? capBankControl_
            : capBankControlBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000004) != 0)) {
        result.capBankSystem_ = capBankSystemBuilder_ == null
            ? capBankSystem_
            : capBankSystemBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.capbankmodule.CapBankDiscreteControlProfile) {
        return mergeFrom((openfmb.capbankmodule.CapBankDiscreteControlProfile)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.capbankmodule.CapBankDiscreteControlProfile other) {
      if (other == openfmb.capbankmodule.CapBankDiscreteControlProfile.getDefaultInstance()) return this;
      if (other.hasControlMessageInfo()) {
        mergeControlMessageInfo(other.getControlMessageInfo());
      }
      if (other.hasCapBankControl()) {
        mergeCapBankControl(other.getCapBankControl());
      }
      if (other.hasCapBankSystem()) {
        mergeCapBankSystem(other.getCapBankSystem());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getControlMessageInfoFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getCapBankControlFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            case 26: {
              input.readMessage(
                  getCapBankSystemFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000004;
              break;
            } // case 26
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.ControlMessageInfo controlMessageInfo_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ControlMessageInfo, openfmb.commonmodule.ControlMessageInfo.Builder, openfmb.commonmodule.ControlMessageInfoOrBuilder> controlMessageInfoBuilder_;
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     * @return Whether the controlMessageInfo field is set.
     */
    public boolean hasControlMessageInfo() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     * @return The controlMessageInfo.
     */
    public openfmb.commonmodule.ControlMessageInfo getControlMessageInfo() {
      if (controlMessageInfoBuilder_ == null) {
        return controlMessageInfo_ == null ? openfmb.commonmodule.ControlMessageInfo.getDefaultInstance() : controlMessageInfo_;
      } else {
        return controlMessageInfoBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setControlMessageInfo(openfmb.commonmodule.ControlMessageInfo value) {
      if (controlMessageInfoBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        controlMessageInfo_ = value;
      } else {
        controlMessageInfoBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setControlMessageInfo(
        openfmb.commonmodule.ControlMessageInfo.Builder builderForValue) {
      if (controlMessageInfoBuilder_ == null) {
        controlMessageInfo_ = builderForValue.build();
      } else {
        controlMessageInfoBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder mergeControlMessageInfo(openfmb.commonmodule.ControlMessageInfo value) {
      if (controlMessageInfoBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          controlMessageInfo_ != null &&
          controlMessageInfo_ != openfmb.commonmodule.ControlMessageInfo.getDefaultInstance()) {
          getControlMessageInfoBuilder().mergeFrom(value);
        } else {
          controlMessageInfo_ = value;
        }
      } else {
        controlMessageInfoBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder clearControlMessageInfo() {
      bitField0_ = (bitField0_ & ~0x00000001);
      controlMessageInfo_ = null;
      if (controlMessageInfoBuilder_ != null) {
        controlMessageInfoBuilder_.dispose();
        controlMessageInfoBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.ControlMessageInfo.Builder getControlMessageInfoBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getControlMessageInfoFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.ControlMessageInfoOrBuilder getControlMessageInfoOrBuilder() {
      if (controlMessageInfoBuilder_ != null) {
        return controlMessageInfoBuilder_.getMessageOrBuilder();
      } else {
        return controlMessageInfo_ == null ?
            openfmb.commonmodule.ControlMessageInfo.getDefaultInstance() : controlMessageInfo_;
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ControlMessageInfo, openfmb.commonmodule.ControlMessageInfo.Builder, openfmb.commonmodule.ControlMessageInfoOrBuilder> 
        getControlMessageInfoFieldBuilder() {
      if (controlMessageInfoBuilder_ == null) {
        controlMessageInfoBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.ControlMessageInfo, openfmb.commonmodule.ControlMessageInfo.Builder, openfmb.commonmodule.ControlMessageInfoOrBuilder>(
                getControlMessageInfo(),
                getParentForChildren(),
                isClean());
        controlMessageInfo_ = null;
      }
      return controlMessageInfoBuilder_;
    }

    private openfmb.capbankmodule.CapBankDiscreteControl capBankControl_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.capbankmodule.CapBankDiscreteControl, openfmb.capbankmodule.CapBankDiscreteControl.Builder, openfmb.capbankmodule.CapBankDiscreteControlOrBuilder> capBankControlBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankDiscreteControl capBankControl = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return Whether the capBankControl field is set.
     */
    public boolean hasCapBankControl() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankDiscreteControl capBankControl = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The capBankControl.
     */
    public openfmb.capbankmodule.CapBankDiscreteControl getCapBankControl() {
      if (capBankControlBuilder_ == null) {
        return capBankControl_ == null ? openfmb.capbankmodule.CapBankDiscreteControl.getDefaultInstance() : capBankControl_;
      } else {
        return capBankControlBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankDiscreteControl capBankControl = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setCapBankControl(openfmb.capbankmodule.CapBankDiscreteControl value) {
      if (capBankControlBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        capBankControl_ = value;
      } else {
        capBankControlBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankDiscreteControl capBankControl = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setCapBankControl(
        openfmb.capbankmodule.CapBankDiscreteControl.Builder builderForValue) {
      if (capBankControlBuilder_ == null) {
        capBankControl_ = builderForValue.build();
      } else {
        capBankControlBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankDiscreteControl capBankControl = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder mergeCapBankControl(openfmb.capbankmodule.CapBankDiscreteControl value) {
      if (capBankControlBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          capBankControl_ != null &&
          capBankControl_ != openfmb.capbankmodule.CapBankDiscreteControl.getDefaultInstance()) {
          getCapBankControlBuilder().mergeFrom(value);
        } else {
          capBankControl_ = value;
        }
      } else {
        capBankControlBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankDiscreteControl capBankControl = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder clearCapBankControl() {
      bitField0_ = (bitField0_ & ~0x00000002);
      capBankControl_ = null;
      if (capBankControlBuilder_ != null) {
        capBankControlBuilder_.dispose();
        capBankControlBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankDiscreteControl capBankControl = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.capbankmodule.CapBankDiscreteControl.Builder getCapBankControlBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getCapBankControlFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankDiscreteControl capBankControl = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.capbankmodule.CapBankDiscreteControlOrBuilder getCapBankControlOrBuilder() {
      if (capBankControlBuilder_ != null) {
        return capBankControlBuilder_.getMessageOrBuilder();
      } else {
        return capBankControl_ == null ?
            openfmb.capbankmodule.CapBankDiscreteControl.getDefaultInstance() : capBankControl_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankDiscreteControl capBankControl = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.capbankmodule.CapBankDiscreteControl, openfmb.capbankmodule.CapBankDiscreteControl.Builder, openfmb.capbankmodule.CapBankDiscreteControlOrBuilder> 
        getCapBankControlFieldBuilder() {
      if (capBankControlBuilder_ == null) {
        capBankControlBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.capbankmodule.CapBankDiscreteControl, openfmb.capbankmodule.CapBankDiscreteControl.Builder, openfmb.capbankmodule.CapBankDiscreteControlOrBuilder>(
                getCapBankControl(),
                getParentForChildren(),
                isClean());
        capBankControl_ = null;
      }
      return capBankControlBuilder_;
    }

    private openfmb.capbankmodule.CapBankSystem capBankSystem_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.capbankmodule.CapBankSystem, openfmb.capbankmodule.CapBankSystem.Builder, openfmb.capbankmodule.CapBankSystemOrBuilder> capBankSystemBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankSystem capBankSystem = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return Whether the capBankSystem field is set.
     */
    public boolean hasCapBankSystem() {
      return ((bitField0_ & 0x00000004) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankSystem capBankSystem = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The capBankSystem.
     */
    public openfmb.capbankmodule.CapBankSystem getCapBankSystem() {
      if (capBankSystemBuilder_ == null) {
        return capBankSystem_ == null ? openfmb.capbankmodule.CapBankSystem.getDefaultInstance() : capBankSystem_;
      } else {
        return capBankSystemBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankSystem capBankSystem = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setCapBankSystem(openfmb.capbankmodule.CapBankSystem value) {
      if (capBankSystemBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        capBankSystem_ = value;
      } else {
        capBankSystemBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankSystem capBankSystem = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setCapBankSystem(
        openfmb.capbankmodule.CapBankSystem.Builder builderForValue) {
      if (capBankSystemBuilder_ == null) {
        capBankSystem_ = builderForValue.build();
      } else {
        capBankSystemBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankSystem capBankSystem = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder mergeCapBankSystem(openfmb.capbankmodule.CapBankSystem value) {
      if (capBankSystemBuilder_ == null) {
        if (((bitField0_ & 0x00000004) != 0) &&
          capBankSystem_ != null &&
          capBankSystem_ != openfmb.capbankmodule.CapBankSystem.getDefaultInstance()) {
          getCapBankSystemBuilder().mergeFrom(value);
        } else {
          capBankSystem_ = value;
        }
      } else {
        capBankSystemBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankSystem capBankSystem = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder clearCapBankSystem() {
      bitField0_ = (bitField0_ & ~0x00000004);
      capBankSystem_ = null;
      if (capBankSystemBuilder_ != null) {
        capBankSystemBuilder_.dispose();
        capBankSystemBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankSystem capBankSystem = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.capbankmodule.CapBankSystem.Builder getCapBankSystemBuilder() {
      bitField0_ |= 0x00000004;
      onChanged();
      return getCapBankSystemFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankSystem capBankSystem = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.capbankmodule.CapBankSystemOrBuilder getCapBankSystemOrBuilder() {
      if (capBankSystemBuilder_ != null) {
        return capBankSystemBuilder_.getMessageOrBuilder();
      } else {
        return capBankSystem_ == null ?
            openfmb.capbankmodule.CapBankSystem.getDefaultInstance() : capBankSystem_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankSystem capBankSystem = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.capbankmodule.CapBankSystem, openfmb.capbankmodule.CapBankSystem.Builder, openfmb.capbankmodule.CapBankSystemOrBuilder> 
        getCapBankSystemFieldBuilder() {
      if (capBankSystemBuilder_ == null) {
        capBankSystemBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.capbankmodule.CapBankSystem, openfmb.capbankmodule.CapBankSystem.Builder, openfmb.capbankmodule.CapBankSystemOrBuilder>(
                getCapBankSystem(),
                getParentForChildren(),
                isClean());
        capBankSystem_ = null;
      }
      return capBankSystemBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:capbankmodule.CapBankDiscreteControlProfile)
  }

  // @@protoc_insertion_point(class_scope:capbankmodule.CapBankDiscreteControlProfile)
  private static final openfmb.capbankmodule.CapBankDiscreteControlProfile DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.capbankmodule.CapBankDiscreteControlProfile();
  }

  public static openfmb.capbankmodule.CapBankDiscreteControlProfile getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<CapBankDiscreteControlProfile>
      PARSER = new com.google.protobuf.AbstractParser<CapBankDiscreteControlProfile>() {
    @java.lang.Override
    public CapBankDiscreteControlProfile parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<CapBankDiscreteControlProfile> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<CapBankDiscreteControlProfile> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.capbankmodule.CapBankDiscreteControlProfile getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

