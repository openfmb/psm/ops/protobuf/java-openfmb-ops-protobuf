// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: capbankmodule/capbankmodule.proto

package openfmb.capbankmodule;

public interface CapBankReadingOrBuilder extends
    // @@protoc_insertion_point(interface_extends:capbankmodule.CapBankReading)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ConductingEquipmentTerminalReading conductingEquipmentTerminalReading = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the conductingEquipmentTerminalReading field is set.
   */
  boolean hasConductingEquipmentTerminalReading();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ConductingEquipmentTerminalReading conductingEquipmentTerminalReading = 1 [(.uml.option_parent_message) = true];</code>
   * @return The conductingEquipmentTerminalReading.
   */
  openfmb.commonmodule.ConductingEquipmentTerminalReading getConductingEquipmentTerminalReading();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ConductingEquipmentTerminalReading conductingEquipmentTerminalReading = 1 [(.uml.option_parent_message) = true];</code>
   */
  openfmb.commonmodule.ConductingEquipmentTerminalReadingOrBuilder getConductingEquipmentTerminalReadingOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.PhaseMMTN phaseMMTN = 2;</code>
   * @return Whether the phaseMMTN field is set.
   */
  boolean hasPhaseMMTN();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.PhaseMMTN phaseMMTN = 2;</code>
   * @return The phaseMMTN.
   */
  openfmb.commonmodule.PhaseMMTN getPhaseMMTN();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.PhaseMMTN phaseMMTN = 2;</code>
   */
  openfmb.commonmodule.PhaseMMTNOrBuilder getPhaseMMTNOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ReadingMMTR readingMMTR = 3;</code>
   * @return Whether the readingMMTR field is set.
   */
  boolean hasReadingMMTR();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ReadingMMTR readingMMTR = 3;</code>
   * @return The readingMMTR.
   */
  openfmb.commonmodule.ReadingMMTR getReadingMMTR();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ReadingMMTR readingMMTR = 3;</code>
   */
  openfmb.commonmodule.ReadingMMTROrBuilder getReadingMMTROrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ReadingMMXU readingMMXU = 4;</code>
   * @return Whether the readingMMXU field is set.
   */
  boolean hasReadingMMXU();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ReadingMMXU readingMMXU = 4;</code>
   * @return The readingMMXU.
   */
  openfmb.commonmodule.ReadingMMXU getReadingMMXU();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ReadingMMXU readingMMXU = 4;</code>
   */
  openfmb.commonmodule.ReadingMMXUOrBuilder getReadingMMXUOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ReadingMMXU secondaryReadingMMXU = 5;</code>
   * @return Whether the secondaryReadingMMXU field is set.
   */
  boolean hasSecondaryReadingMMXU();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ReadingMMXU secondaryReadingMMXU = 5;</code>
   * @return The secondaryReadingMMXU.
   */
  openfmb.commonmodule.ReadingMMXU getSecondaryReadingMMXU();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ReadingMMXU secondaryReadingMMXU = 5;</code>
   */
  openfmb.commonmodule.ReadingMMXUOrBuilder getSecondaryReadingMMXUOrBuilder();
}
