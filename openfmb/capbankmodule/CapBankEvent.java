// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: capbankmodule/capbankmodule.proto

package openfmb.capbankmodule;

/**
 * <pre>
 * Cap bank event
 * </pre>
 *
 * Protobuf type {@code capbankmodule.CapBankEvent}
 */
public final class CapBankEvent extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:capbankmodule.CapBankEvent)
    CapBankEventOrBuilder {
private static final long serialVersionUID = 0L;
  // Use CapBankEvent.newBuilder() to construct.
  private CapBankEvent(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private CapBankEvent() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new CapBankEvent();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.capbankmodule.Capbankmodule.internal_static_capbankmodule_CapBankEvent_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.capbankmodule.Capbankmodule.internal_static_capbankmodule_CapBankEvent_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.capbankmodule.CapBankEvent.class, openfmb.capbankmodule.CapBankEvent.Builder.class);
  }

  public static final int EVENTVALUE_FIELD_NUMBER = 1;
  private openfmb.commonmodule.EventValue eventValue_;
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.EventValue eventValue = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the eventValue field is set.
   */
  @java.lang.Override
  public boolean hasEventValue() {
    return eventValue_ != null;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.EventValue eventValue = 1 [(.uml.option_parent_message) = true];</code>
   * @return The eventValue.
   */
  @java.lang.Override
  public openfmb.commonmodule.EventValue getEventValue() {
    return eventValue_ == null ? openfmb.commonmodule.EventValue.getDefaultInstance() : eventValue_;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.EventValue eventValue = 1 [(.uml.option_parent_message) = true];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.EventValueOrBuilder getEventValueOrBuilder() {
    return eventValue_ == null ? openfmb.commonmodule.EventValue.getDefaultInstance() : eventValue_;
  }

  public static final int CAPBANKEVENTANDSTATUSYPSH_FIELD_NUMBER = 2;
  private openfmb.capbankmodule.CapBankEventAndStatusYPSH capBankEventAndStatusYPSH_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.capbankmodule.CapBankEventAndStatusYPSH CapBankEventAndStatusYPSH = 2;</code>
   * @return Whether the capBankEventAndStatusYPSH field is set.
   */
  @java.lang.Override
  public boolean hasCapBankEventAndStatusYPSH() {
    return capBankEventAndStatusYPSH_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.capbankmodule.CapBankEventAndStatusYPSH CapBankEventAndStatusYPSH = 2;</code>
   * @return The capBankEventAndStatusYPSH.
   */
  @java.lang.Override
  public openfmb.capbankmodule.CapBankEventAndStatusYPSH getCapBankEventAndStatusYPSH() {
    return capBankEventAndStatusYPSH_ == null ? openfmb.capbankmodule.CapBankEventAndStatusYPSH.getDefaultInstance() : capBankEventAndStatusYPSH_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.capbankmodule.CapBankEventAndStatusYPSH CapBankEventAndStatusYPSH = 2;</code>
   */
  @java.lang.Override
  public openfmb.capbankmodule.CapBankEventAndStatusYPSHOrBuilder getCapBankEventAndStatusYPSHOrBuilder() {
    return capBankEventAndStatusYPSH_ == null ? openfmb.capbankmodule.CapBankEventAndStatusYPSH.getDefaultInstance() : capBankEventAndStatusYPSH_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (eventValue_ != null) {
      output.writeMessage(1, getEventValue());
    }
    if (capBankEventAndStatusYPSH_ != null) {
      output.writeMessage(2, getCapBankEventAndStatusYPSH());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (eventValue_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getEventValue());
    }
    if (capBankEventAndStatusYPSH_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getCapBankEventAndStatusYPSH());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.capbankmodule.CapBankEvent)) {
      return super.equals(obj);
    }
    openfmb.capbankmodule.CapBankEvent other = (openfmb.capbankmodule.CapBankEvent) obj;

    if (hasEventValue() != other.hasEventValue()) return false;
    if (hasEventValue()) {
      if (!getEventValue()
          .equals(other.getEventValue())) return false;
    }
    if (hasCapBankEventAndStatusYPSH() != other.hasCapBankEventAndStatusYPSH()) return false;
    if (hasCapBankEventAndStatusYPSH()) {
      if (!getCapBankEventAndStatusYPSH()
          .equals(other.getCapBankEventAndStatusYPSH())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasEventValue()) {
      hash = (37 * hash) + EVENTVALUE_FIELD_NUMBER;
      hash = (53 * hash) + getEventValue().hashCode();
    }
    if (hasCapBankEventAndStatusYPSH()) {
      hash = (37 * hash) + CAPBANKEVENTANDSTATUSYPSH_FIELD_NUMBER;
      hash = (53 * hash) + getCapBankEventAndStatusYPSH().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.capbankmodule.CapBankEvent parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.capbankmodule.CapBankEvent parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.capbankmodule.CapBankEvent parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.capbankmodule.CapBankEvent parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.capbankmodule.CapBankEvent parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.capbankmodule.CapBankEvent parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.capbankmodule.CapBankEvent parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.capbankmodule.CapBankEvent parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.capbankmodule.CapBankEvent parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.capbankmodule.CapBankEvent parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.capbankmodule.CapBankEvent parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.capbankmodule.CapBankEvent parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.capbankmodule.CapBankEvent prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Cap bank event
   * </pre>
   *
   * Protobuf type {@code capbankmodule.CapBankEvent}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:capbankmodule.CapBankEvent)
      openfmb.capbankmodule.CapBankEventOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.capbankmodule.Capbankmodule.internal_static_capbankmodule_CapBankEvent_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.capbankmodule.Capbankmodule.internal_static_capbankmodule_CapBankEvent_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.capbankmodule.CapBankEvent.class, openfmb.capbankmodule.CapBankEvent.Builder.class);
    }

    // Construct using openfmb.capbankmodule.CapBankEvent.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      eventValue_ = null;
      if (eventValueBuilder_ != null) {
        eventValueBuilder_.dispose();
        eventValueBuilder_ = null;
      }
      capBankEventAndStatusYPSH_ = null;
      if (capBankEventAndStatusYPSHBuilder_ != null) {
        capBankEventAndStatusYPSHBuilder_.dispose();
        capBankEventAndStatusYPSHBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.capbankmodule.Capbankmodule.internal_static_capbankmodule_CapBankEvent_descriptor;
    }

    @java.lang.Override
    public openfmb.capbankmodule.CapBankEvent getDefaultInstanceForType() {
      return openfmb.capbankmodule.CapBankEvent.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.capbankmodule.CapBankEvent build() {
      openfmb.capbankmodule.CapBankEvent result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.capbankmodule.CapBankEvent buildPartial() {
      openfmb.capbankmodule.CapBankEvent result = new openfmb.capbankmodule.CapBankEvent(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.capbankmodule.CapBankEvent result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.eventValue_ = eventValueBuilder_ == null
            ? eventValue_
            : eventValueBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.capBankEventAndStatusYPSH_ = capBankEventAndStatusYPSHBuilder_ == null
            ? capBankEventAndStatusYPSH_
            : capBankEventAndStatusYPSHBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.capbankmodule.CapBankEvent) {
        return mergeFrom((openfmb.capbankmodule.CapBankEvent)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.capbankmodule.CapBankEvent other) {
      if (other == openfmb.capbankmodule.CapBankEvent.getDefaultInstance()) return this;
      if (other.hasEventValue()) {
        mergeEventValue(other.getEventValue());
      }
      if (other.hasCapBankEventAndStatusYPSH()) {
        mergeCapBankEventAndStatusYPSH(other.getCapBankEventAndStatusYPSH());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getEventValueFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getCapBankEventAndStatusYPSHFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.EventValue eventValue_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.EventValue, openfmb.commonmodule.EventValue.Builder, openfmb.commonmodule.EventValueOrBuilder> eventValueBuilder_;
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventValue eventValue = 1 [(.uml.option_parent_message) = true];</code>
     * @return Whether the eventValue field is set.
     */
    public boolean hasEventValue() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventValue eventValue = 1 [(.uml.option_parent_message) = true];</code>
     * @return The eventValue.
     */
    public openfmb.commonmodule.EventValue getEventValue() {
      if (eventValueBuilder_ == null) {
        return eventValue_ == null ? openfmb.commonmodule.EventValue.getDefaultInstance() : eventValue_;
      } else {
        return eventValueBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventValue eventValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setEventValue(openfmb.commonmodule.EventValue value) {
      if (eventValueBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        eventValue_ = value;
      } else {
        eventValueBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventValue eventValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setEventValue(
        openfmb.commonmodule.EventValue.Builder builderForValue) {
      if (eventValueBuilder_ == null) {
        eventValue_ = builderForValue.build();
      } else {
        eventValueBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventValue eventValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder mergeEventValue(openfmb.commonmodule.EventValue value) {
      if (eventValueBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          eventValue_ != null &&
          eventValue_ != openfmb.commonmodule.EventValue.getDefaultInstance()) {
          getEventValueBuilder().mergeFrom(value);
        } else {
          eventValue_ = value;
        }
      } else {
        eventValueBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventValue eventValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder clearEventValue() {
      bitField0_ = (bitField0_ & ~0x00000001);
      eventValue_ = null;
      if (eventValueBuilder_ != null) {
        eventValueBuilder_.dispose();
        eventValueBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventValue eventValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.EventValue.Builder getEventValueBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getEventValueFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventValue eventValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.EventValueOrBuilder getEventValueOrBuilder() {
      if (eventValueBuilder_ != null) {
        return eventValueBuilder_.getMessageOrBuilder();
      } else {
        return eventValue_ == null ?
            openfmb.commonmodule.EventValue.getDefaultInstance() : eventValue_;
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventValue eventValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.EventValue, openfmb.commonmodule.EventValue.Builder, openfmb.commonmodule.EventValueOrBuilder> 
        getEventValueFieldBuilder() {
      if (eventValueBuilder_ == null) {
        eventValueBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.EventValue, openfmb.commonmodule.EventValue.Builder, openfmb.commonmodule.EventValueOrBuilder>(
                getEventValue(),
                getParentForChildren(),
                isClean());
        eventValue_ = null;
      }
      return eventValueBuilder_;
    }

    private openfmb.capbankmodule.CapBankEventAndStatusYPSH capBankEventAndStatusYPSH_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.capbankmodule.CapBankEventAndStatusYPSH, openfmb.capbankmodule.CapBankEventAndStatusYPSH.Builder, openfmb.capbankmodule.CapBankEventAndStatusYPSHOrBuilder> capBankEventAndStatusYPSHBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankEventAndStatusYPSH CapBankEventAndStatusYPSH = 2;</code>
     * @return Whether the capBankEventAndStatusYPSH field is set.
     */
    public boolean hasCapBankEventAndStatusYPSH() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankEventAndStatusYPSH CapBankEventAndStatusYPSH = 2;</code>
     * @return The capBankEventAndStatusYPSH.
     */
    public openfmb.capbankmodule.CapBankEventAndStatusYPSH getCapBankEventAndStatusYPSH() {
      if (capBankEventAndStatusYPSHBuilder_ == null) {
        return capBankEventAndStatusYPSH_ == null ? openfmb.capbankmodule.CapBankEventAndStatusYPSH.getDefaultInstance() : capBankEventAndStatusYPSH_;
      } else {
        return capBankEventAndStatusYPSHBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankEventAndStatusYPSH CapBankEventAndStatusYPSH = 2;</code>
     */
    public Builder setCapBankEventAndStatusYPSH(openfmb.capbankmodule.CapBankEventAndStatusYPSH value) {
      if (capBankEventAndStatusYPSHBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        capBankEventAndStatusYPSH_ = value;
      } else {
        capBankEventAndStatusYPSHBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankEventAndStatusYPSH CapBankEventAndStatusYPSH = 2;</code>
     */
    public Builder setCapBankEventAndStatusYPSH(
        openfmb.capbankmodule.CapBankEventAndStatusYPSH.Builder builderForValue) {
      if (capBankEventAndStatusYPSHBuilder_ == null) {
        capBankEventAndStatusYPSH_ = builderForValue.build();
      } else {
        capBankEventAndStatusYPSHBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankEventAndStatusYPSH CapBankEventAndStatusYPSH = 2;</code>
     */
    public Builder mergeCapBankEventAndStatusYPSH(openfmb.capbankmodule.CapBankEventAndStatusYPSH value) {
      if (capBankEventAndStatusYPSHBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          capBankEventAndStatusYPSH_ != null &&
          capBankEventAndStatusYPSH_ != openfmb.capbankmodule.CapBankEventAndStatusYPSH.getDefaultInstance()) {
          getCapBankEventAndStatusYPSHBuilder().mergeFrom(value);
        } else {
          capBankEventAndStatusYPSH_ = value;
        }
      } else {
        capBankEventAndStatusYPSHBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankEventAndStatusYPSH CapBankEventAndStatusYPSH = 2;</code>
     */
    public Builder clearCapBankEventAndStatusYPSH() {
      bitField0_ = (bitField0_ & ~0x00000002);
      capBankEventAndStatusYPSH_ = null;
      if (capBankEventAndStatusYPSHBuilder_ != null) {
        capBankEventAndStatusYPSHBuilder_.dispose();
        capBankEventAndStatusYPSHBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankEventAndStatusYPSH CapBankEventAndStatusYPSH = 2;</code>
     */
    public openfmb.capbankmodule.CapBankEventAndStatusYPSH.Builder getCapBankEventAndStatusYPSHBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getCapBankEventAndStatusYPSHFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankEventAndStatusYPSH CapBankEventAndStatusYPSH = 2;</code>
     */
    public openfmb.capbankmodule.CapBankEventAndStatusYPSHOrBuilder getCapBankEventAndStatusYPSHOrBuilder() {
      if (capBankEventAndStatusYPSHBuilder_ != null) {
        return capBankEventAndStatusYPSHBuilder_.getMessageOrBuilder();
      } else {
        return capBankEventAndStatusYPSH_ == null ?
            openfmb.capbankmodule.CapBankEventAndStatusYPSH.getDefaultInstance() : capBankEventAndStatusYPSH_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankEventAndStatusYPSH CapBankEventAndStatusYPSH = 2;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.capbankmodule.CapBankEventAndStatusYPSH, openfmb.capbankmodule.CapBankEventAndStatusYPSH.Builder, openfmb.capbankmodule.CapBankEventAndStatusYPSHOrBuilder> 
        getCapBankEventAndStatusYPSHFieldBuilder() {
      if (capBankEventAndStatusYPSHBuilder_ == null) {
        capBankEventAndStatusYPSHBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.capbankmodule.CapBankEventAndStatusYPSH, openfmb.capbankmodule.CapBankEventAndStatusYPSH.Builder, openfmb.capbankmodule.CapBankEventAndStatusYPSHOrBuilder>(
                getCapBankEventAndStatusYPSH(),
                getParentForChildren(),
                isClean());
        capBankEventAndStatusYPSH_ = null;
      }
      return capBankEventAndStatusYPSHBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:capbankmodule.CapBankEvent)
  }

  // @@protoc_insertion_point(class_scope:capbankmodule.CapBankEvent)
  private static final openfmb.capbankmodule.CapBankEvent DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.capbankmodule.CapBankEvent();
  }

  public static openfmb.capbankmodule.CapBankEvent getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<CapBankEvent>
      PARSER = new com.google.protobuf.AbstractParser<CapBankEvent>() {
    @java.lang.Override
    public CapBankEvent parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<CapBankEvent> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<CapBankEvent> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.capbankmodule.CapBankEvent getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

