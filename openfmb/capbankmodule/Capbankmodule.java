// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: capbankmodule/capbankmodule.proto

package openfmb.capbankmodule;

public final class Capbankmodule {
  private Capbankmodule() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankSystem_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankSystem_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankControlYPSH_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankControlYPSH_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankPoint_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankPoint_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankCSG_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankCSG_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankControlScheduleFSCH_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankControlScheduleFSCH_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankControlFSCC_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankControlFSCC_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankControl_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankControl_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankControlProfile_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankControlProfile_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankDiscreteControlYPSH_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankDiscreteControlYPSH_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankDiscreteControl_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankDiscreteControl_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankDiscreteControlProfile_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankDiscreteControlProfile_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankEventAndStatusYPSH_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankEventAndStatusYPSH_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankEvent_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankEvent_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankEventProfile_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankEventProfile_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankReading_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankReading_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankReadingProfile_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankReadingProfile_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankStatus_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankStatus_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_capbankmodule_CapBankStatusProfile_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_capbankmodule_CapBankStatusProfile_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n!capbankmodule/capbankmodule.proto\022\rcap" +
      "bankmodule\032\tuml.proto\032\037commonmodule/comm" +
      "onmodule.proto\"U\n\rCapBankSystem\022D\n\023condu" +
      "ctingEquipment\030\001 \001(\0132!.commonmodule.Cond" +
      "uctingEquipmentB\004\200\265\030\001\"\372\005\n\022CapBankControl" +
      "YPSH\022&\n\006AmpLmt\030\001 \001(\0132\026.commonmodule.Phas" +
      "eSPC\022(\n\010AmpThdHi\030\002 \001(\0132\026.commonmodule.Ph" +
      "aseAPC\022(\n\010AmpThdLo\030\003 \001(\0132\026.commonmodule." +
      "PhaseAPC\022-\n\013CtlModeAuto\030\004 \001(\0132\030.commonmo" +
      "dule.ControlSPC\022.\n\014CtlModeOvrRd\030\005 \001(\0132\030." +
      "commonmodule.ControlSPC\022,\n\nCtlModeRem\030\006 " +
      "\001(\0132\030.commonmodule.ControlSPC\0229\n\007DirMode" +
      "\030\007 \001(\0132(.commonmodule.Optional_Direction" +
      "ModeKind\022#\n\003Pos\030\010 \001(\0132\026.commonmodule.Pha" +
      "seSPC\022)\n\007TempLmt\030\t \001(\0132\030.commonmodule.Co" +
      "ntrolSPC\022+\n\tTempThdHi\030\n \001(\0132\030.commonmodu" +
      "le.ControlAPC\022+\n\tTempThdLo\030\013 \001(\0132\030.commo" +
      "nmodule.ControlAPC\022&\n\006VArLmt\030\014 \001(\0132\026.com" +
      "monmodule.PhaseSPC\022(\n\010VArThdHi\030\r \001(\0132\026.c" +
      "ommonmodule.PhaseAPC\022(\n\010VArThdLo\030\016 \001(\0132\026" +
      ".commonmodule.PhaseAPC\022&\n\006VolLmt\030\017 \001(\0132\026" +
      ".commonmodule.PhaseSPC\022(\n\010VolThdHi\030\020 \001(\013" +
      "2\026.commonmodule.PhaseAPC\022(\n\010VolThdLo\030\021 \001" +
      "(\0132\026.commonmodule.PhaseAPC\"x\n\014CapBankPoi" +
      "nt\0222\n\007control\030\001 \001(\0132!.capbankmodule.CapB" +
      "ankControlYPSH\0224\n\tstartTime\030\002 \001(\0132\027.comm" +
      "onmodule.TimestampB\010\210\265\030\001\220\265\030\001\"C\n\nCapBankC" +
      "SG\0225\n\006crvPts\030\001 \003(\0132\033.capbankmodule.CapBa" +
      "nkPointB\010\210\265\030\001\220\265\030\001\"Q\n\032CapBankControlSched" +
      "uleFSCH\0223\n\006ValCSG\030\001 \001(\0132\031.capbankmodule." +
      "CapBankCSGB\010\210\265\030\001\220\265\030\001\"\231\001\n\022CapBankControlF" +
      "SCC\0224\n\013controlFSCC\030\001 \001(\0132\031.commonmodule." +
      "ControlFSCCB\004\200\265\030\001\022M\n\032capBankControlSched" +
      "uleFSCH\030\002 \001(\0132).capbankmodule.CapBankCon" +
      "trolScheduleFSCH\"\265\001\n\016CapBankControl\0226\n\014c" +
      "ontrolValue\030\001 \001(\0132\032.commonmodule.Control" +
      "ValueB\004\200\265\030\001\022,\n\005check\030\002 \001(\0132\035.commonmodul" +
      "e.CheckConditions\022=\n\022capBankControlFSCC\030" +
      "\003 \001(\0132!.capbankmodule.CapBankControlFSCC" +
      "\"\341\001\n\025CapBankControlProfile\022B\n\022controlMes" +
      "sageInfo\030\001 \001(\0132 .commonmodule.ControlMes" +
      "sageInfoB\004\200\265\030\001\022?\n\016capBankControl\030\002 \001(\0132\035" +
      ".capbankmodule.CapBankControlB\010\210\265\030\001\220\265\030\001\022" +
      "=\n\rcapBankSystem\030\003 \001(\0132\034.capbankmodule.C" +
      "apBankSystemB\010\210\265\030\001\220\265\030\001:\004\300\363\030\001\"\232\001\n\032CapBank" +
      "DiscreteControlYPSH\022H\n\025logicalNodeForCon" +
      "trol\030\001 \001(\0132#.commonmodule.LogicalNodeFor" +
      "ControlB\004\200\265\030\001\0222\n\007control\030\002 \001(\0132!.capbank" +
      "module.CapBankControlYPSH\"\315\001\n\026CapBankDis" +
      "creteControl\0226\n\014controlValue\030\001 \001(\0132\032.com" +
      "monmodule.ControlValueB\004\200\265\030\001\022,\n\005check\030\002 " +
      "\001(\0132\035.commonmodule.CheckConditions\022M\n\032ca" +
      "pBankDiscreteControlYPSH\030\003 \001(\0132).capbank" +
      "module.CapBankDiscreteControlYPSH\"\361\001\n\035Ca" +
      "pBankDiscreteControlProfile\022B\n\022controlMe" +
      "ssageInfo\030\001 \001(\0132 .commonmodule.ControlMe" +
      "ssageInfoB\004\200\265\030\001\022G\n\016capBankControl\030\002 \001(\0132" +
      "%.capbankmodule.CapBankDiscreteControlB\010" +
      "\210\265\030\001\220\265\030\001\022=\n\rcapBankSystem\030\003 \001(\0132\034.capban" +
      "kmodule.CapBankSystemB\010\210\265\030\001\220\265\030\001:\004\300\363\030\001\"\322\003" +
      "\n\031CapBankEventAndStatusYPSH\022V\n\034logicalNo" +
      "deForEventAndStatus\030\001 \001(\0132*.commonmodule" +
      ".LogicalNodeForEventAndStatusB\004\200\265\030\001\022&\n\006A" +
      "mpLmt\030\002 \001(\0132\026.commonmodule.PhaseSPS\0227\n\007C" +
      "tlMode\030\003 \001(\0132&.commonmodule.Optional_Con" +
      "trolModeKind\022&\n\006DirRev\030\004 \001(\0132\026.commonmod" +
      "ule.PhaseSPS\0226\n\013DynamicTest\030\005 \001(\0132!.comm" +
      "onmodule.ENS_DynamicTestKind\022#\n\003Pos\030\006 \001(" +
      "\0132\026.commonmodule.PhaseDPS\022\'\n\007TempLmt\030\007 \001" +
      "(\0132\026.commonmodule.PhaseSPS\022&\n\006VArLmt\030\010 \001" +
      "(\0132\026.commonmodule.PhaseSPS\022&\n\006VolLmt\030\t \001" +
      "(\0132\026.commonmodule.PhaseSPS\"\217\001\n\014CapBankEv" +
      "ent\0222\n\neventValue\030\001 \001(\0132\030.commonmodule.E" +
      "ventValueB\004\200\265\030\001\022K\n\031CapBankEventAndStatus" +
      "YPSH\030\002 \001(\0132(.capbankmodule.CapBankEventA" +
      "ndStatusYPSH\"\327\001\n\023CapBankEventProfile\022>\n\020" +
      "eventMessageInfo\030\001 \001(\0132\036.commonmodule.Ev" +
      "entMessageInfoB\004\200\265\030\001\022;\n\014capBankEvent\030\002 \001" +
      "(\0132\033.capbankmodule.CapBankEventB\010\210\265\030\001\220\265\030" +
      "\001\022=\n\rcapBankSystem\030\003 \001(\0132\034.capbankmodule" +
      ".CapBankSystemB\010\210\265\030\001\220\265\030\001:\004\300\363\030\001\"\271\002\n\016CapBa" +
      "nkReading\022b\n\"conductingEquipmentTerminal" +
      "Reading\030\001 \001(\01320.commonmodule.ConductingE" +
      "quipmentTerminalReadingB\004\200\265\030\001\022*\n\tphaseMM" +
      "TN\030\002 \001(\0132\027.commonmodule.PhaseMMTN\022.\n\013rea" +
      "dingMMTR\030\003 \001(\0132\031.commonmodule.ReadingMMT" +
      "R\022.\n\013readingMMXU\030\004 \001(\0132\031.commonmodule.Re" +
      "adingMMXU\0227\n\024secondaryReadingMMXU\030\005 \001(\0132" +
      "\031.commonmodule.ReadingMMXU\"\341\001\n\025CapBankRe" +
      "adingProfile\022B\n\022readingMessageInfo\030\001 \001(\013" +
      "2 .commonmodule.ReadingMessageInfoB\004\200\265\030\001" +
      "\022?\n\016capBankReading\030\002 \001(\0132\035.capbankmodule" +
      ".CapBankReadingB\010\210\265\030\001\220\265\030\001\022=\n\rcapBankSyst" +
      "em\030\003 \001(\0132\034.capbankmodule.CapBankSystemB\010" +
      "\210\265\030\001\220\265\030\001:\004\300\363\030\001\"\222\001\n\rCapBankStatus\0224\n\013stat" +
      "usValue\030\001 \001(\0132\031.commonmodule.StatusValue" +
      "B\004\200\265\030\001\022K\n\031capBankEventAndStatusYPSH\030\002 \001(" +
      "\0132(.capbankmodule.CapBankEventAndStatusY" +
      "PSH\"\334\001\n\024CapBankStatusProfile\022@\n\021statusMe" +
      "ssageInfo\030\001 \001(\0132\037.commonmodule.StatusMes" +
      "sageInfoB\004\200\265\030\001\022=\n\rcapBankStatus\030\002 \001(\0132\034." +
      "capbankmodule.CapBankStatusB\010\210\265\030\001\220\265\030\001\022=\n" +
      "\rcapBankSystem\030\003 \001(\0132\034.capbankmodule.Cap" +
      "BankSystemB\010\210\265\030\001\220\265\030\001:\004\300\363\030\001B\207\001\n\025openfmb.c" +
      "apbankmoduleP\001ZTgitlab.com/openfmb/psm/o" +
      "ps/protobuf/go-openfmb-ops-protobuf/v2/o" +
      "penfmb/capbankmodule\252\002\025openfmb.capbankmo" +
      "duleb\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          openfmb.Uml.getDescriptor(),
          openfmb.commonmodule.Commonmodule.getDescriptor(),
        });
    internal_static_capbankmodule_CapBankSystem_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_capbankmodule_CapBankSystem_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankSystem_descriptor,
        new java.lang.String[] { "ConductingEquipment", });
    internal_static_capbankmodule_CapBankControlYPSH_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_capbankmodule_CapBankControlYPSH_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankControlYPSH_descriptor,
        new java.lang.String[] { "AmpLmt", "AmpThdHi", "AmpThdLo", "CtlModeAuto", "CtlModeOvrRd", "CtlModeRem", "DirMode", "Pos", "TempLmt", "TempThdHi", "TempThdLo", "VArLmt", "VArThdHi", "VArThdLo", "VolLmt", "VolThdHi", "VolThdLo", });
    internal_static_capbankmodule_CapBankPoint_descriptor =
      getDescriptor().getMessageTypes().get(2);
    internal_static_capbankmodule_CapBankPoint_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankPoint_descriptor,
        new java.lang.String[] { "Control", "StartTime", });
    internal_static_capbankmodule_CapBankCSG_descriptor =
      getDescriptor().getMessageTypes().get(3);
    internal_static_capbankmodule_CapBankCSG_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankCSG_descriptor,
        new java.lang.String[] { "CrvPts", });
    internal_static_capbankmodule_CapBankControlScheduleFSCH_descriptor =
      getDescriptor().getMessageTypes().get(4);
    internal_static_capbankmodule_CapBankControlScheduleFSCH_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankControlScheduleFSCH_descriptor,
        new java.lang.String[] { "ValCSG", });
    internal_static_capbankmodule_CapBankControlFSCC_descriptor =
      getDescriptor().getMessageTypes().get(5);
    internal_static_capbankmodule_CapBankControlFSCC_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankControlFSCC_descriptor,
        new java.lang.String[] { "ControlFSCC", "CapBankControlScheduleFSCH", });
    internal_static_capbankmodule_CapBankControl_descriptor =
      getDescriptor().getMessageTypes().get(6);
    internal_static_capbankmodule_CapBankControl_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankControl_descriptor,
        new java.lang.String[] { "ControlValue", "Check", "CapBankControlFSCC", });
    internal_static_capbankmodule_CapBankControlProfile_descriptor =
      getDescriptor().getMessageTypes().get(7);
    internal_static_capbankmodule_CapBankControlProfile_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankControlProfile_descriptor,
        new java.lang.String[] { "ControlMessageInfo", "CapBankControl", "CapBankSystem", });
    internal_static_capbankmodule_CapBankDiscreteControlYPSH_descriptor =
      getDescriptor().getMessageTypes().get(8);
    internal_static_capbankmodule_CapBankDiscreteControlYPSH_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankDiscreteControlYPSH_descriptor,
        new java.lang.String[] { "LogicalNodeForControl", "Control", });
    internal_static_capbankmodule_CapBankDiscreteControl_descriptor =
      getDescriptor().getMessageTypes().get(9);
    internal_static_capbankmodule_CapBankDiscreteControl_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankDiscreteControl_descriptor,
        new java.lang.String[] { "ControlValue", "Check", "CapBankDiscreteControlYPSH", });
    internal_static_capbankmodule_CapBankDiscreteControlProfile_descriptor =
      getDescriptor().getMessageTypes().get(10);
    internal_static_capbankmodule_CapBankDiscreteControlProfile_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankDiscreteControlProfile_descriptor,
        new java.lang.String[] { "ControlMessageInfo", "CapBankControl", "CapBankSystem", });
    internal_static_capbankmodule_CapBankEventAndStatusYPSH_descriptor =
      getDescriptor().getMessageTypes().get(11);
    internal_static_capbankmodule_CapBankEventAndStatusYPSH_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankEventAndStatusYPSH_descriptor,
        new java.lang.String[] { "LogicalNodeForEventAndStatus", "AmpLmt", "CtlMode", "DirRev", "DynamicTest", "Pos", "TempLmt", "VArLmt", "VolLmt", });
    internal_static_capbankmodule_CapBankEvent_descriptor =
      getDescriptor().getMessageTypes().get(12);
    internal_static_capbankmodule_CapBankEvent_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankEvent_descriptor,
        new java.lang.String[] { "EventValue", "CapBankEventAndStatusYPSH", });
    internal_static_capbankmodule_CapBankEventProfile_descriptor =
      getDescriptor().getMessageTypes().get(13);
    internal_static_capbankmodule_CapBankEventProfile_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankEventProfile_descriptor,
        new java.lang.String[] { "EventMessageInfo", "CapBankEvent", "CapBankSystem", });
    internal_static_capbankmodule_CapBankReading_descriptor =
      getDescriptor().getMessageTypes().get(14);
    internal_static_capbankmodule_CapBankReading_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankReading_descriptor,
        new java.lang.String[] { "ConductingEquipmentTerminalReading", "PhaseMMTN", "ReadingMMTR", "ReadingMMXU", "SecondaryReadingMMXU", });
    internal_static_capbankmodule_CapBankReadingProfile_descriptor =
      getDescriptor().getMessageTypes().get(15);
    internal_static_capbankmodule_CapBankReadingProfile_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankReadingProfile_descriptor,
        new java.lang.String[] { "ReadingMessageInfo", "CapBankReading", "CapBankSystem", });
    internal_static_capbankmodule_CapBankStatus_descriptor =
      getDescriptor().getMessageTypes().get(16);
    internal_static_capbankmodule_CapBankStatus_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankStatus_descriptor,
        new java.lang.String[] { "StatusValue", "CapBankEventAndStatusYPSH", });
    internal_static_capbankmodule_CapBankStatusProfile_descriptor =
      getDescriptor().getMessageTypes().get(17);
    internal_static_capbankmodule_CapBankStatusProfile_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_capbankmodule_CapBankStatusProfile_descriptor,
        new java.lang.String[] { "StatusMessageInfo", "CapBankStatus", "CapBankSystem", });
    com.google.protobuf.ExtensionRegistry registry =
        com.google.protobuf.ExtensionRegistry.newInstance();
    registry.add(openfmb.Uml.optionMultiplicityMin);
    registry.add(openfmb.Uml.optionOpenfmbProfile);
    registry.add(openfmb.Uml.optionParentMessage);
    registry.add(openfmb.Uml.optionRequiredField);
    com.google.protobuf.Descriptors.FileDescriptor
        .internalUpdateFileDescriptor(descriptor, registry);
    openfmb.Uml.getDescriptor();
    openfmb.commonmodule.Commonmodule.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
