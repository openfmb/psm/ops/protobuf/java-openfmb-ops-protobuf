// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: capbankmodule/capbankmodule.proto

package openfmb.capbankmodule;

/**
 * <pre>
 * CapBank control
 * </pre>
 *
 * Protobuf type {@code capbankmodule.CapBankControl}
 */
public final class CapBankControl extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:capbankmodule.CapBankControl)
    CapBankControlOrBuilder {
private static final long serialVersionUID = 0L;
  // Use CapBankControl.newBuilder() to construct.
  private CapBankControl(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private CapBankControl() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new CapBankControl();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.capbankmodule.Capbankmodule.internal_static_capbankmodule_CapBankControl_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.capbankmodule.Capbankmodule.internal_static_capbankmodule_CapBankControl_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.capbankmodule.CapBankControl.class, openfmb.capbankmodule.CapBankControl.Builder.class);
  }

  public static final int CONTROLVALUE_FIELD_NUMBER = 1;
  private openfmb.commonmodule.ControlValue controlValue_;
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the controlValue field is set.
   */
  @java.lang.Override
  public boolean hasControlValue() {
    return controlValue_ != null;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
   * @return The controlValue.
   */
  @java.lang.Override
  public openfmb.commonmodule.ControlValue getControlValue() {
    return controlValue_ == null ? openfmb.commonmodule.ControlValue.getDefaultInstance() : controlValue_;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.ControlValueOrBuilder getControlValueOrBuilder() {
    return controlValue_ == null ? openfmb.commonmodule.ControlValue.getDefaultInstance() : controlValue_;
  }

  public static final int CHECK_FIELD_NUMBER = 2;
  private openfmb.commonmodule.CheckConditions check_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   * @return Whether the check field is set.
   */
  @java.lang.Override
  public boolean hasCheck() {
    return check_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   * @return The check.
   */
  @java.lang.Override
  public openfmb.commonmodule.CheckConditions getCheck() {
    return check_ == null ? openfmb.commonmodule.CheckConditions.getDefaultInstance() : check_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.CheckConditionsOrBuilder getCheckOrBuilder() {
    return check_ == null ? openfmb.commonmodule.CheckConditions.getDefaultInstance() : check_;
  }

  public static final int CAPBANKCONTROLFSCC_FIELD_NUMBER = 3;
  private openfmb.capbankmodule.CapBankControlFSCC capBankControlFSCC_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.capbankmodule.CapBankControlFSCC capBankControlFSCC = 3;</code>
   * @return Whether the capBankControlFSCC field is set.
   */
  @java.lang.Override
  public boolean hasCapBankControlFSCC() {
    return capBankControlFSCC_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.capbankmodule.CapBankControlFSCC capBankControlFSCC = 3;</code>
   * @return The capBankControlFSCC.
   */
  @java.lang.Override
  public openfmb.capbankmodule.CapBankControlFSCC getCapBankControlFSCC() {
    return capBankControlFSCC_ == null ? openfmb.capbankmodule.CapBankControlFSCC.getDefaultInstance() : capBankControlFSCC_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.capbankmodule.CapBankControlFSCC capBankControlFSCC = 3;</code>
   */
  @java.lang.Override
  public openfmb.capbankmodule.CapBankControlFSCCOrBuilder getCapBankControlFSCCOrBuilder() {
    return capBankControlFSCC_ == null ? openfmb.capbankmodule.CapBankControlFSCC.getDefaultInstance() : capBankControlFSCC_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (controlValue_ != null) {
      output.writeMessage(1, getControlValue());
    }
    if (check_ != null) {
      output.writeMessage(2, getCheck());
    }
    if (capBankControlFSCC_ != null) {
      output.writeMessage(3, getCapBankControlFSCC());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (controlValue_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getControlValue());
    }
    if (check_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getCheck());
    }
    if (capBankControlFSCC_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(3, getCapBankControlFSCC());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.capbankmodule.CapBankControl)) {
      return super.equals(obj);
    }
    openfmb.capbankmodule.CapBankControl other = (openfmb.capbankmodule.CapBankControl) obj;

    if (hasControlValue() != other.hasControlValue()) return false;
    if (hasControlValue()) {
      if (!getControlValue()
          .equals(other.getControlValue())) return false;
    }
    if (hasCheck() != other.hasCheck()) return false;
    if (hasCheck()) {
      if (!getCheck()
          .equals(other.getCheck())) return false;
    }
    if (hasCapBankControlFSCC() != other.hasCapBankControlFSCC()) return false;
    if (hasCapBankControlFSCC()) {
      if (!getCapBankControlFSCC()
          .equals(other.getCapBankControlFSCC())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasControlValue()) {
      hash = (37 * hash) + CONTROLVALUE_FIELD_NUMBER;
      hash = (53 * hash) + getControlValue().hashCode();
    }
    if (hasCheck()) {
      hash = (37 * hash) + CHECK_FIELD_NUMBER;
      hash = (53 * hash) + getCheck().hashCode();
    }
    if (hasCapBankControlFSCC()) {
      hash = (37 * hash) + CAPBANKCONTROLFSCC_FIELD_NUMBER;
      hash = (53 * hash) + getCapBankControlFSCC().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.capbankmodule.CapBankControl parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.capbankmodule.CapBankControl parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.capbankmodule.CapBankControl parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.capbankmodule.CapBankControl parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.capbankmodule.CapBankControl parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.capbankmodule.CapBankControl parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.capbankmodule.CapBankControl parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.capbankmodule.CapBankControl parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.capbankmodule.CapBankControl parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.capbankmodule.CapBankControl parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.capbankmodule.CapBankControl parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.capbankmodule.CapBankControl parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.capbankmodule.CapBankControl prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * CapBank control
   * </pre>
   *
   * Protobuf type {@code capbankmodule.CapBankControl}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:capbankmodule.CapBankControl)
      openfmb.capbankmodule.CapBankControlOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.capbankmodule.Capbankmodule.internal_static_capbankmodule_CapBankControl_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.capbankmodule.Capbankmodule.internal_static_capbankmodule_CapBankControl_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.capbankmodule.CapBankControl.class, openfmb.capbankmodule.CapBankControl.Builder.class);
    }

    // Construct using openfmb.capbankmodule.CapBankControl.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      controlValue_ = null;
      if (controlValueBuilder_ != null) {
        controlValueBuilder_.dispose();
        controlValueBuilder_ = null;
      }
      check_ = null;
      if (checkBuilder_ != null) {
        checkBuilder_.dispose();
        checkBuilder_ = null;
      }
      capBankControlFSCC_ = null;
      if (capBankControlFSCCBuilder_ != null) {
        capBankControlFSCCBuilder_.dispose();
        capBankControlFSCCBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.capbankmodule.Capbankmodule.internal_static_capbankmodule_CapBankControl_descriptor;
    }

    @java.lang.Override
    public openfmb.capbankmodule.CapBankControl getDefaultInstanceForType() {
      return openfmb.capbankmodule.CapBankControl.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.capbankmodule.CapBankControl build() {
      openfmb.capbankmodule.CapBankControl result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.capbankmodule.CapBankControl buildPartial() {
      openfmb.capbankmodule.CapBankControl result = new openfmb.capbankmodule.CapBankControl(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.capbankmodule.CapBankControl result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.controlValue_ = controlValueBuilder_ == null
            ? controlValue_
            : controlValueBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.check_ = checkBuilder_ == null
            ? check_
            : checkBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000004) != 0)) {
        result.capBankControlFSCC_ = capBankControlFSCCBuilder_ == null
            ? capBankControlFSCC_
            : capBankControlFSCCBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.capbankmodule.CapBankControl) {
        return mergeFrom((openfmb.capbankmodule.CapBankControl)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.capbankmodule.CapBankControl other) {
      if (other == openfmb.capbankmodule.CapBankControl.getDefaultInstance()) return this;
      if (other.hasControlValue()) {
        mergeControlValue(other.getControlValue());
      }
      if (other.hasCheck()) {
        mergeCheck(other.getCheck());
      }
      if (other.hasCapBankControlFSCC()) {
        mergeCapBankControlFSCC(other.getCapBankControlFSCC());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getControlValueFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getCheckFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            case 26: {
              input.readMessage(
                  getCapBankControlFSCCFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000004;
              break;
            } // case 26
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.ControlValue controlValue_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ControlValue, openfmb.commonmodule.ControlValue.Builder, openfmb.commonmodule.ControlValueOrBuilder> controlValueBuilder_;
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     * @return Whether the controlValue field is set.
     */
    public boolean hasControlValue() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     * @return The controlValue.
     */
    public openfmb.commonmodule.ControlValue getControlValue() {
      if (controlValueBuilder_ == null) {
        return controlValue_ == null ? openfmb.commonmodule.ControlValue.getDefaultInstance() : controlValue_;
      } else {
        return controlValueBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setControlValue(openfmb.commonmodule.ControlValue value) {
      if (controlValueBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        controlValue_ = value;
      } else {
        controlValueBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setControlValue(
        openfmb.commonmodule.ControlValue.Builder builderForValue) {
      if (controlValueBuilder_ == null) {
        controlValue_ = builderForValue.build();
      } else {
        controlValueBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder mergeControlValue(openfmb.commonmodule.ControlValue value) {
      if (controlValueBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          controlValue_ != null &&
          controlValue_ != openfmb.commonmodule.ControlValue.getDefaultInstance()) {
          getControlValueBuilder().mergeFrom(value);
        } else {
          controlValue_ = value;
        }
      } else {
        controlValueBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder clearControlValue() {
      bitField0_ = (bitField0_ & ~0x00000001);
      controlValue_ = null;
      if (controlValueBuilder_ != null) {
        controlValueBuilder_.dispose();
        controlValueBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.ControlValue.Builder getControlValueBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getControlValueFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.ControlValueOrBuilder getControlValueOrBuilder() {
      if (controlValueBuilder_ != null) {
        return controlValueBuilder_.getMessageOrBuilder();
      } else {
        return controlValue_ == null ?
            openfmb.commonmodule.ControlValue.getDefaultInstance() : controlValue_;
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ControlValue, openfmb.commonmodule.ControlValue.Builder, openfmb.commonmodule.ControlValueOrBuilder> 
        getControlValueFieldBuilder() {
      if (controlValueBuilder_ == null) {
        controlValueBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.ControlValue, openfmb.commonmodule.ControlValue.Builder, openfmb.commonmodule.ControlValueOrBuilder>(
                getControlValue(),
                getParentForChildren(),
                isClean());
        controlValue_ = null;
      }
      return controlValueBuilder_;
    }

    private openfmb.commonmodule.CheckConditions check_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.CheckConditions, openfmb.commonmodule.CheckConditions.Builder, openfmb.commonmodule.CheckConditionsOrBuilder> checkBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     * @return Whether the check field is set.
     */
    public boolean hasCheck() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     * @return The check.
     */
    public openfmb.commonmodule.CheckConditions getCheck() {
      if (checkBuilder_ == null) {
        return check_ == null ? openfmb.commonmodule.CheckConditions.getDefaultInstance() : check_;
      } else {
        return checkBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public Builder setCheck(openfmb.commonmodule.CheckConditions value) {
      if (checkBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        check_ = value;
      } else {
        checkBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public Builder setCheck(
        openfmb.commonmodule.CheckConditions.Builder builderForValue) {
      if (checkBuilder_ == null) {
        check_ = builderForValue.build();
      } else {
        checkBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public Builder mergeCheck(openfmb.commonmodule.CheckConditions value) {
      if (checkBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          check_ != null &&
          check_ != openfmb.commonmodule.CheckConditions.getDefaultInstance()) {
          getCheckBuilder().mergeFrom(value);
        } else {
          check_ = value;
        }
      } else {
        checkBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public Builder clearCheck() {
      bitField0_ = (bitField0_ & ~0x00000002);
      check_ = null;
      if (checkBuilder_ != null) {
        checkBuilder_.dispose();
        checkBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public openfmb.commonmodule.CheckConditions.Builder getCheckBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getCheckFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public openfmb.commonmodule.CheckConditionsOrBuilder getCheckOrBuilder() {
      if (checkBuilder_ != null) {
        return checkBuilder_.getMessageOrBuilder();
      } else {
        return check_ == null ?
            openfmb.commonmodule.CheckConditions.getDefaultInstance() : check_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.CheckConditions, openfmb.commonmodule.CheckConditions.Builder, openfmb.commonmodule.CheckConditionsOrBuilder> 
        getCheckFieldBuilder() {
      if (checkBuilder_ == null) {
        checkBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.CheckConditions, openfmb.commonmodule.CheckConditions.Builder, openfmb.commonmodule.CheckConditionsOrBuilder>(
                getCheck(),
                getParentForChildren(),
                isClean());
        check_ = null;
      }
      return checkBuilder_;
    }

    private openfmb.capbankmodule.CapBankControlFSCC capBankControlFSCC_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.capbankmodule.CapBankControlFSCC, openfmb.capbankmodule.CapBankControlFSCC.Builder, openfmb.capbankmodule.CapBankControlFSCCOrBuilder> capBankControlFSCCBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankControlFSCC capBankControlFSCC = 3;</code>
     * @return Whether the capBankControlFSCC field is set.
     */
    public boolean hasCapBankControlFSCC() {
      return ((bitField0_ & 0x00000004) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankControlFSCC capBankControlFSCC = 3;</code>
     * @return The capBankControlFSCC.
     */
    public openfmb.capbankmodule.CapBankControlFSCC getCapBankControlFSCC() {
      if (capBankControlFSCCBuilder_ == null) {
        return capBankControlFSCC_ == null ? openfmb.capbankmodule.CapBankControlFSCC.getDefaultInstance() : capBankControlFSCC_;
      } else {
        return capBankControlFSCCBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankControlFSCC capBankControlFSCC = 3;</code>
     */
    public Builder setCapBankControlFSCC(openfmb.capbankmodule.CapBankControlFSCC value) {
      if (capBankControlFSCCBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        capBankControlFSCC_ = value;
      } else {
        capBankControlFSCCBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankControlFSCC capBankControlFSCC = 3;</code>
     */
    public Builder setCapBankControlFSCC(
        openfmb.capbankmodule.CapBankControlFSCC.Builder builderForValue) {
      if (capBankControlFSCCBuilder_ == null) {
        capBankControlFSCC_ = builderForValue.build();
      } else {
        capBankControlFSCCBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankControlFSCC capBankControlFSCC = 3;</code>
     */
    public Builder mergeCapBankControlFSCC(openfmb.capbankmodule.CapBankControlFSCC value) {
      if (capBankControlFSCCBuilder_ == null) {
        if (((bitField0_ & 0x00000004) != 0) &&
          capBankControlFSCC_ != null &&
          capBankControlFSCC_ != openfmb.capbankmodule.CapBankControlFSCC.getDefaultInstance()) {
          getCapBankControlFSCCBuilder().mergeFrom(value);
        } else {
          capBankControlFSCC_ = value;
        }
      } else {
        capBankControlFSCCBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankControlFSCC capBankControlFSCC = 3;</code>
     */
    public Builder clearCapBankControlFSCC() {
      bitField0_ = (bitField0_ & ~0x00000004);
      capBankControlFSCC_ = null;
      if (capBankControlFSCCBuilder_ != null) {
        capBankControlFSCCBuilder_.dispose();
        capBankControlFSCCBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankControlFSCC capBankControlFSCC = 3;</code>
     */
    public openfmb.capbankmodule.CapBankControlFSCC.Builder getCapBankControlFSCCBuilder() {
      bitField0_ |= 0x00000004;
      onChanged();
      return getCapBankControlFSCCFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankControlFSCC capBankControlFSCC = 3;</code>
     */
    public openfmb.capbankmodule.CapBankControlFSCCOrBuilder getCapBankControlFSCCOrBuilder() {
      if (capBankControlFSCCBuilder_ != null) {
        return capBankControlFSCCBuilder_.getMessageOrBuilder();
      } else {
        return capBankControlFSCC_ == null ?
            openfmb.capbankmodule.CapBankControlFSCC.getDefaultInstance() : capBankControlFSCC_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.capbankmodule.CapBankControlFSCC capBankControlFSCC = 3;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.capbankmodule.CapBankControlFSCC, openfmb.capbankmodule.CapBankControlFSCC.Builder, openfmb.capbankmodule.CapBankControlFSCCOrBuilder> 
        getCapBankControlFSCCFieldBuilder() {
      if (capBankControlFSCCBuilder_ == null) {
        capBankControlFSCCBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.capbankmodule.CapBankControlFSCC, openfmb.capbankmodule.CapBankControlFSCC.Builder, openfmb.capbankmodule.CapBankControlFSCCOrBuilder>(
                getCapBankControlFSCC(),
                getParentForChildren(),
                isClean());
        capBankControlFSCC_ = null;
      }
      return capBankControlFSCCBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:capbankmodule.CapBankControl)
  }

  // @@protoc_insertion_point(class_scope:capbankmodule.CapBankControl)
  private static final openfmb.capbankmodule.CapBankControl DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.capbankmodule.CapBankControl();
  }

  public static openfmb.capbankmodule.CapBankControl getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<CapBankControl>
      PARSER = new com.google.protobuf.AbstractParser<CapBankControl>() {
    @java.lang.Override
    public CapBankControl parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<CapBankControl> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<CapBankControl> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.capbankmodule.CapBankControl getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

