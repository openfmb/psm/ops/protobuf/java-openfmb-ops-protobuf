// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: interconnectionmodule/interconnectionmodule.proto

package openfmb.interconnectionmodule;

/**
 * <pre>
 * Interconnection schedule
 * </pre>
 *
 * Protobuf type {@code interconnectionmodule.InterconnectionSchedule}
 */
public final class InterconnectionSchedule extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:interconnectionmodule.InterconnectionSchedule)
    InterconnectionScheduleOrBuilder {
private static final long serialVersionUID = 0L;
  // Use InterconnectionSchedule.newBuilder() to construct.
  private InterconnectionSchedule(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private InterconnectionSchedule() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new InterconnectionSchedule();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.interconnectionmodule.Interconnectionmodule.internal_static_interconnectionmodule_InterconnectionSchedule_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.interconnectionmodule.Interconnectionmodule.internal_static_interconnectionmodule_InterconnectionSchedule_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.interconnectionmodule.InterconnectionSchedule.class, openfmb.interconnectionmodule.InterconnectionSchedule.Builder.class);
  }

  public static final int IDENTIFIEDOBJECT_FIELD_NUMBER = 1;
  private openfmb.commonmodule.IdentifiedObject identifiedObject_;
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the identifiedObject field is set.
   */
  @java.lang.Override
  public boolean hasIdentifiedObject() {
    return identifiedObject_ != null;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
   * @return The identifiedObject.
   */
  @java.lang.Override
  public openfmb.commonmodule.IdentifiedObject getIdentifiedObject() {
    return identifiedObject_ == null ? openfmb.commonmodule.IdentifiedObject.getDefaultInstance() : identifiedObject_;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.IdentifiedObjectOrBuilder getIdentifiedObjectOrBuilder() {
    return identifiedObject_ == null ? openfmb.commonmodule.IdentifiedObject.getDefaultInstance() : identifiedObject_;
  }

  public static final int CHECK_FIELD_NUMBER = 2;
  private openfmb.commonmodule.CheckConditions check_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   * @return Whether the check field is set.
   */
  @java.lang.Override
  public boolean hasCheck() {
    return check_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   * @return The check.
   */
  @java.lang.Override
  public openfmb.commonmodule.CheckConditions getCheck() {
    return check_ == null ? openfmb.commonmodule.CheckConditions.getDefaultInstance() : check_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.CheckConditionsOrBuilder getCheckOrBuilder() {
    return check_ == null ? openfmb.commonmodule.CheckConditions.getDefaultInstance() : check_;
  }

  public static final int INTERCONNECTIONSCHEDULEFSCC_FIELD_NUMBER = 3;
  private openfmb.interconnectionmodule.InterconnectionScheduleFSCC interconnectionScheduleFSCC_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.interconnectionmodule.InterconnectionScheduleFSCC interconnectionScheduleFSCC = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the interconnectionScheduleFSCC field is set.
   */
  @java.lang.Override
  public boolean hasInterconnectionScheduleFSCC() {
    return interconnectionScheduleFSCC_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.interconnectionmodule.InterconnectionScheduleFSCC interconnectionScheduleFSCC = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The interconnectionScheduleFSCC.
   */
  @java.lang.Override
  public openfmb.interconnectionmodule.InterconnectionScheduleFSCC getInterconnectionScheduleFSCC() {
    return interconnectionScheduleFSCC_ == null ? openfmb.interconnectionmodule.InterconnectionScheduleFSCC.getDefaultInstance() : interconnectionScheduleFSCC_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.interconnectionmodule.InterconnectionScheduleFSCC interconnectionScheduleFSCC = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.interconnectionmodule.InterconnectionScheduleFSCCOrBuilder getInterconnectionScheduleFSCCOrBuilder() {
    return interconnectionScheduleFSCC_ == null ? openfmb.interconnectionmodule.InterconnectionScheduleFSCC.getDefaultInstance() : interconnectionScheduleFSCC_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (identifiedObject_ != null) {
      output.writeMessage(1, getIdentifiedObject());
    }
    if (check_ != null) {
      output.writeMessage(2, getCheck());
    }
    if (interconnectionScheduleFSCC_ != null) {
      output.writeMessage(3, getInterconnectionScheduleFSCC());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (identifiedObject_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getIdentifiedObject());
    }
    if (check_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getCheck());
    }
    if (interconnectionScheduleFSCC_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(3, getInterconnectionScheduleFSCC());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.interconnectionmodule.InterconnectionSchedule)) {
      return super.equals(obj);
    }
    openfmb.interconnectionmodule.InterconnectionSchedule other = (openfmb.interconnectionmodule.InterconnectionSchedule) obj;

    if (hasIdentifiedObject() != other.hasIdentifiedObject()) return false;
    if (hasIdentifiedObject()) {
      if (!getIdentifiedObject()
          .equals(other.getIdentifiedObject())) return false;
    }
    if (hasCheck() != other.hasCheck()) return false;
    if (hasCheck()) {
      if (!getCheck()
          .equals(other.getCheck())) return false;
    }
    if (hasInterconnectionScheduleFSCC() != other.hasInterconnectionScheduleFSCC()) return false;
    if (hasInterconnectionScheduleFSCC()) {
      if (!getInterconnectionScheduleFSCC()
          .equals(other.getInterconnectionScheduleFSCC())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasIdentifiedObject()) {
      hash = (37 * hash) + IDENTIFIEDOBJECT_FIELD_NUMBER;
      hash = (53 * hash) + getIdentifiedObject().hashCode();
    }
    if (hasCheck()) {
      hash = (37 * hash) + CHECK_FIELD_NUMBER;
      hash = (53 * hash) + getCheck().hashCode();
    }
    if (hasInterconnectionScheduleFSCC()) {
      hash = (37 * hash) + INTERCONNECTIONSCHEDULEFSCC_FIELD_NUMBER;
      hash = (53 * hash) + getInterconnectionScheduleFSCC().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.interconnectionmodule.InterconnectionSchedule parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.interconnectionmodule.InterconnectionSchedule parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.interconnectionmodule.InterconnectionSchedule parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.interconnectionmodule.InterconnectionSchedule parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.interconnectionmodule.InterconnectionSchedule parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.interconnectionmodule.InterconnectionSchedule parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.interconnectionmodule.InterconnectionSchedule parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.interconnectionmodule.InterconnectionSchedule parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.interconnectionmodule.InterconnectionSchedule parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.interconnectionmodule.InterconnectionSchedule parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.interconnectionmodule.InterconnectionSchedule parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.interconnectionmodule.InterconnectionSchedule parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.interconnectionmodule.InterconnectionSchedule prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Interconnection schedule
   * </pre>
   *
   * Protobuf type {@code interconnectionmodule.InterconnectionSchedule}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:interconnectionmodule.InterconnectionSchedule)
      openfmb.interconnectionmodule.InterconnectionScheduleOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.interconnectionmodule.Interconnectionmodule.internal_static_interconnectionmodule_InterconnectionSchedule_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.interconnectionmodule.Interconnectionmodule.internal_static_interconnectionmodule_InterconnectionSchedule_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.interconnectionmodule.InterconnectionSchedule.class, openfmb.interconnectionmodule.InterconnectionSchedule.Builder.class);
    }

    // Construct using openfmb.interconnectionmodule.InterconnectionSchedule.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      identifiedObject_ = null;
      if (identifiedObjectBuilder_ != null) {
        identifiedObjectBuilder_.dispose();
        identifiedObjectBuilder_ = null;
      }
      check_ = null;
      if (checkBuilder_ != null) {
        checkBuilder_.dispose();
        checkBuilder_ = null;
      }
      interconnectionScheduleFSCC_ = null;
      if (interconnectionScheduleFSCCBuilder_ != null) {
        interconnectionScheduleFSCCBuilder_.dispose();
        interconnectionScheduleFSCCBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.interconnectionmodule.Interconnectionmodule.internal_static_interconnectionmodule_InterconnectionSchedule_descriptor;
    }

    @java.lang.Override
    public openfmb.interconnectionmodule.InterconnectionSchedule getDefaultInstanceForType() {
      return openfmb.interconnectionmodule.InterconnectionSchedule.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.interconnectionmodule.InterconnectionSchedule build() {
      openfmb.interconnectionmodule.InterconnectionSchedule result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.interconnectionmodule.InterconnectionSchedule buildPartial() {
      openfmb.interconnectionmodule.InterconnectionSchedule result = new openfmb.interconnectionmodule.InterconnectionSchedule(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.interconnectionmodule.InterconnectionSchedule result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.identifiedObject_ = identifiedObjectBuilder_ == null
            ? identifiedObject_
            : identifiedObjectBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.check_ = checkBuilder_ == null
            ? check_
            : checkBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000004) != 0)) {
        result.interconnectionScheduleFSCC_ = interconnectionScheduleFSCCBuilder_ == null
            ? interconnectionScheduleFSCC_
            : interconnectionScheduleFSCCBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.interconnectionmodule.InterconnectionSchedule) {
        return mergeFrom((openfmb.interconnectionmodule.InterconnectionSchedule)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.interconnectionmodule.InterconnectionSchedule other) {
      if (other == openfmb.interconnectionmodule.InterconnectionSchedule.getDefaultInstance()) return this;
      if (other.hasIdentifiedObject()) {
        mergeIdentifiedObject(other.getIdentifiedObject());
      }
      if (other.hasCheck()) {
        mergeCheck(other.getCheck());
      }
      if (other.hasInterconnectionScheduleFSCC()) {
        mergeInterconnectionScheduleFSCC(other.getInterconnectionScheduleFSCC());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getIdentifiedObjectFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getCheckFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            case 26: {
              input.readMessage(
                  getInterconnectionScheduleFSCCFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000004;
              break;
            } // case 26
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.IdentifiedObject identifiedObject_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.IdentifiedObject, openfmb.commonmodule.IdentifiedObject.Builder, openfmb.commonmodule.IdentifiedObjectOrBuilder> identifiedObjectBuilder_;
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     * @return Whether the identifiedObject field is set.
     */
    public boolean hasIdentifiedObject() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     * @return The identifiedObject.
     */
    public openfmb.commonmodule.IdentifiedObject getIdentifiedObject() {
      if (identifiedObjectBuilder_ == null) {
        return identifiedObject_ == null ? openfmb.commonmodule.IdentifiedObject.getDefaultInstance() : identifiedObject_;
      } else {
        return identifiedObjectBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setIdentifiedObject(openfmb.commonmodule.IdentifiedObject value) {
      if (identifiedObjectBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        identifiedObject_ = value;
      } else {
        identifiedObjectBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setIdentifiedObject(
        openfmb.commonmodule.IdentifiedObject.Builder builderForValue) {
      if (identifiedObjectBuilder_ == null) {
        identifiedObject_ = builderForValue.build();
      } else {
        identifiedObjectBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder mergeIdentifiedObject(openfmb.commonmodule.IdentifiedObject value) {
      if (identifiedObjectBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          identifiedObject_ != null &&
          identifiedObject_ != openfmb.commonmodule.IdentifiedObject.getDefaultInstance()) {
          getIdentifiedObjectBuilder().mergeFrom(value);
        } else {
          identifiedObject_ = value;
        }
      } else {
        identifiedObjectBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder clearIdentifiedObject() {
      bitField0_ = (bitField0_ & ~0x00000001);
      identifiedObject_ = null;
      if (identifiedObjectBuilder_ != null) {
        identifiedObjectBuilder_.dispose();
        identifiedObjectBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.IdentifiedObject.Builder getIdentifiedObjectBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getIdentifiedObjectFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.IdentifiedObjectOrBuilder getIdentifiedObjectOrBuilder() {
      if (identifiedObjectBuilder_ != null) {
        return identifiedObjectBuilder_.getMessageOrBuilder();
      } else {
        return identifiedObject_ == null ?
            openfmb.commonmodule.IdentifiedObject.getDefaultInstance() : identifiedObject_;
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.IdentifiedObject, openfmb.commonmodule.IdentifiedObject.Builder, openfmb.commonmodule.IdentifiedObjectOrBuilder> 
        getIdentifiedObjectFieldBuilder() {
      if (identifiedObjectBuilder_ == null) {
        identifiedObjectBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.IdentifiedObject, openfmb.commonmodule.IdentifiedObject.Builder, openfmb.commonmodule.IdentifiedObjectOrBuilder>(
                getIdentifiedObject(),
                getParentForChildren(),
                isClean());
        identifiedObject_ = null;
      }
      return identifiedObjectBuilder_;
    }

    private openfmb.commonmodule.CheckConditions check_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.CheckConditions, openfmb.commonmodule.CheckConditions.Builder, openfmb.commonmodule.CheckConditionsOrBuilder> checkBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     * @return Whether the check field is set.
     */
    public boolean hasCheck() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     * @return The check.
     */
    public openfmb.commonmodule.CheckConditions getCheck() {
      if (checkBuilder_ == null) {
        return check_ == null ? openfmb.commonmodule.CheckConditions.getDefaultInstance() : check_;
      } else {
        return checkBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public Builder setCheck(openfmb.commonmodule.CheckConditions value) {
      if (checkBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        check_ = value;
      } else {
        checkBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public Builder setCheck(
        openfmb.commonmodule.CheckConditions.Builder builderForValue) {
      if (checkBuilder_ == null) {
        check_ = builderForValue.build();
      } else {
        checkBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public Builder mergeCheck(openfmb.commonmodule.CheckConditions value) {
      if (checkBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          check_ != null &&
          check_ != openfmb.commonmodule.CheckConditions.getDefaultInstance()) {
          getCheckBuilder().mergeFrom(value);
        } else {
          check_ = value;
        }
      } else {
        checkBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public Builder clearCheck() {
      bitField0_ = (bitField0_ & ~0x00000002);
      check_ = null;
      if (checkBuilder_ != null) {
        checkBuilder_.dispose();
        checkBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public openfmb.commonmodule.CheckConditions.Builder getCheckBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getCheckFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    public openfmb.commonmodule.CheckConditionsOrBuilder getCheckOrBuilder() {
      if (checkBuilder_ != null) {
        return checkBuilder_.getMessageOrBuilder();
      } else {
        return check_ == null ?
            openfmb.commonmodule.CheckConditions.getDefaultInstance() : check_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.CheckConditions check = 2;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.CheckConditions, openfmb.commonmodule.CheckConditions.Builder, openfmb.commonmodule.CheckConditionsOrBuilder> 
        getCheckFieldBuilder() {
      if (checkBuilder_ == null) {
        checkBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.CheckConditions, openfmb.commonmodule.CheckConditions.Builder, openfmb.commonmodule.CheckConditionsOrBuilder>(
                getCheck(),
                getParentForChildren(),
                isClean());
        check_ = null;
      }
      return checkBuilder_;
    }

    private openfmb.interconnectionmodule.InterconnectionScheduleFSCC interconnectionScheduleFSCC_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.interconnectionmodule.InterconnectionScheduleFSCC, openfmb.interconnectionmodule.InterconnectionScheduleFSCC.Builder, openfmb.interconnectionmodule.InterconnectionScheduleFSCCOrBuilder> interconnectionScheduleFSCCBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.interconnectionmodule.InterconnectionScheduleFSCC interconnectionScheduleFSCC = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return Whether the interconnectionScheduleFSCC field is set.
     */
    public boolean hasInterconnectionScheduleFSCC() {
      return ((bitField0_ & 0x00000004) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.interconnectionmodule.InterconnectionScheduleFSCC interconnectionScheduleFSCC = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The interconnectionScheduleFSCC.
     */
    public openfmb.interconnectionmodule.InterconnectionScheduleFSCC getInterconnectionScheduleFSCC() {
      if (interconnectionScheduleFSCCBuilder_ == null) {
        return interconnectionScheduleFSCC_ == null ? openfmb.interconnectionmodule.InterconnectionScheduleFSCC.getDefaultInstance() : interconnectionScheduleFSCC_;
      } else {
        return interconnectionScheduleFSCCBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.interconnectionmodule.InterconnectionScheduleFSCC interconnectionScheduleFSCC = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setInterconnectionScheduleFSCC(openfmb.interconnectionmodule.InterconnectionScheduleFSCC value) {
      if (interconnectionScheduleFSCCBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        interconnectionScheduleFSCC_ = value;
      } else {
        interconnectionScheduleFSCCBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.interconnectionmodule.InterconnectionScheduleFSCC interconnectionScheduleFSCC = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setInterconnectionScheduleFSCC(
        openfmb.interconnectionmodule.InterconnectionScheduleFSCC.Builder builderForValue) {
      if (interconnectionScheduleFSCCBuilder_ == null) {
        interconnectionScheduleFSCC_ = builderForValue.build();
      } else {
        interconnectionScheduleFSCCBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.interconnectionmodule.InterconnectionScheduleFSCC interconnectionScheduleFSCC = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder mergeInterconnectionScheduleFSCC(openfmb.interconnectionmodule.InterconnectionScheduleFSCC value) {
      if (interconnectionScheduleFSCCBuilder_ == null) {
        if (((bitField0_ & 0x00000004) != 0) &&
          interconnectionScheduleFSCC_ != null &&
          interconnectionScheduleFSCC_ != openfmb.interconnectionmodule.InterconnectionScheduleFSCC.getDefaultInstance()) {
          getInterconnectionScheduleFSCCBuilder().mergeFrom(value);
        } else {
          interconnectionScheduleFSCC_ = value;
        }
      } else {
        interconnectionScheduleFSCCBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.interconnectionmodule.InterconnectionScheduleFSCC interconnectionScheduleFSCC = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder clearInterconnectionScheduleFSCC() {
      bitField0_ = (bitField0_ & ~0x00000004);
      interconnectionScheduleFSCC_ = null;
      if (interconnectionScheduleFSCCBuilder_ != null) {
        interconnectionScheduleFSCCBuilder_.dispose();
        interconnectionScheduleFSCCBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.interconnectionmodule.InterconnectionScheduleFSCC interconnectionScheduleFSCC = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.interconnectionmodule.InterconnectionScheduleFSCC.Builder getInterconnectionScheduleFSCCBuilder() {
      bitField0_ |= 0x00000004;
      onChanged();
      return getInterconnectionScheduleFSCCFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.interconnectionmodule.InterconnectionScheduleFSCC interconnectionScheduleFSCC = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.interconnectionmodule.InterconnectionScheduleFSCCOrBuilder getInterconnectionScheduleFSCCOrBuilder() {
      if (interconnectionScheduleFSCCBuilder_ != null) {
        return interconnectionScheduleFSCCBuilder_.getMessageOrBuilder();
      } else {
        return interconnectionScheduleFSCC_ == null ?
            openfmb.interconnectionmodule.InterconnectionScheduleFSCC.getDefaultInstance() : interconnectionScheduleFSCC_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.interconnectionmodule.InterconnectionScheduleFSCC interconnectionScheduleFSCC = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.interconnectionmodule.InterconnectionScheduleFSCC, openfmb.interconnectionmodule.InterconnectionScheduleFSCC.Builder, openfmb.interconnectionmodule.InterconnectionScheduleFSCCOrBuilder> 
        getInterconnectionScheduleFSCCFieldBuilder() {
      if (interconnectionScheduleFSCCBuilder_ == null) {
        interconnectionScheduleFSCCBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.interconnectionmodule.InterconnectionScheduleFSCC, openfmb.interconnectionmodule.InterconnectionScheduleFSCC.Builder, openfmb.interconnectionmodule.InterconnectionScheduleFSCCOrBuilder>(
                getInterconnectionScheduleFSCC(),
                getParentForChildren(),
                isClean());
        interconnectionScheduleFSCC_ = null;
      }
      return interconnectionScheduleFSCCBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:interconnectionmodule.InterconnectionSchedule)
  }

  // @@protoc_insertion_point(class_scope:interconnectionmodule.InterconnectionSchedule)
  private static final openfmb.interconnectionmodule.InterconnectionSchedule DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.interconnectionmodule.InterconnectionSchedule();
  }

  public static openfmb.interconnectionmodule.InterconnectionSchedule getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<InterconnectionSchedule>
      PARSER = new com.google.protobuf.AbstractParser<InterconnectionSchedule>() {
    @java.lang.Override
    public InterconnectionSchedule parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<InterconnectionSchedule> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<InterconnectionSchedule> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.interconnectionmodule.InterconnectionSchedule getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

