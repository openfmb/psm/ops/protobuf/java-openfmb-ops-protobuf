// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: breakermodule/breakermodule.proto

package openfmb.breakermodule;

public interface BreakerStatusProfileOrBuilder extends
    // @@protoc_insertion_point(interface_extends:breakermodule.BreakerStatusProfile)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.StatusMessageInfo statusMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the statusMessageInfo field is set.
   */
  boolean hasStatusMessageInfo();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.StatusMessageInfo statusMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return The statusMessageInfo.
   */
  openfmb.commonmodule.StatusMessageInfo getStatusMessageInfo();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.StatusMessageInfo statusMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   */
  openfmb.commonmodule.StatusMessageInfoOrBuilder getStatusMessageInfoOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.breakermodule.Breaker breaker = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the breaker field is set.
   */
  boolean hasBreaker();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.breakermodule.Breaker breaker = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The breaker.
   */
  openfmb.breakermodule.Breaker getBreaker();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.breakermodule.Breaker breaker = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.breakermodule.BreakerOrBuilder getBreakerOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.breakermodule.BreakerStatus breakerStatus = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the breakerStatus field is set.
   */
  boolean hasBreakerStatus();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.breakermodule.BreakerStatus breakerStatus = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The breakerStatus.
   */
  openfmb.breakermodule.BreakerStatus getBreakerStatus();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.breakermodule.BreakerStatus breakerStatus = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.breakermodule.BreakerStatusOrBuilder getBreakerStatusOrBuilder();
}
