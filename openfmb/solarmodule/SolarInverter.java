// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: solarmodule/solarmodule.proto

package openfmb.solarmodule;

/**
 * <pre>
 * MISSING DOCUMENTATION!!!
 * </pre>
 *
 * Protobuf type {@code solarmodule.SolarInverter}
 */
public final class SolarInverter extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:solarmodule.SolarInverter)
    SolarInverterOrBuilder {
private static final long serialVersionUID = 0L;
  // Use SolarInverter.newBuilder() to construct.
  private SolarInverter(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private SolarInverter() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new SolarInverter();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.solarmodule.Solarmodule.internal_static_solarmodule_SolarInverter_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.solarmodule.Solarmodule.internal_static_solarmodule_SolarInverter_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.solarmodule.SolarInverter.class, openfmb.solarmodule.SolarInverter.Builder.class);
  }

  public static final int CONDUCTINGEQUIPMENT_FIELD_NUMBER = 1;
  private openfmb.commonmodule.ConductingEquipment conductingEquipment_;
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the conductingEquipment field is set.
   */
  @java.lang.Override
  public boolean hasConductingEquipment() {
    return conductingEquipment_ != null;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
   * @return The conductingEquipment.
   */
  @java.lang.Override
  public openfmb.commonmodule.ConductingEquipment getConductingEquipment() {
    return conductingEquipment_ == null ? openfmb.commonmodule.ConductingEquipment.getDefaultInstance() : conductingEquipment_;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.ConductingEquipmentOrBuilder getConductingEquipmentOrBuilder() {
    return conductingEquipment_ == null ? openfmb.commonmodule.ConductingEquipment.getDefaultInstance() : conductingEquipment_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (conductingEquipment_ != null) {
      output.writeMessage(1, getConductingEquipment());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (conductingEquipment_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getConductingEquipment());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.solarmodule.SolarInverter)) {
      return super.equals(obj);
    }
    openfmb.solarmodule.SolarInverter other = (openfmb.solarmodule.SolarInverter) obj;

    if (hasConductingEquipment() != other.hasConductingEquipment()) return false;
    if (hasConductingEquipment()) {
      if (!getConductingEquipment()
          .equals(other.getConductingEquipment())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasConductingEquipment()) {
      hash = (37 * hash) + CONDUCTINGEQUIPMENT_FIELD_NUMBER;
      hash = (53 * hash) + getConductingEquipment().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.solarmodule.SolarInverter parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.solarmodule.SolarInverter parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.solarmodule.SolarInverter parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.solarmodule.SolarInverter parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.solarmodule.SolarInverter parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.solarmodule.SolarInverter parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.solarmodule.SolarInverter parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.solarmodule.SolarInverter parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.solarmodule.SolarInverter parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.solarmodule.SolarInverter parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.solarmodule.SolarInverter parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.solarmodule.SolarInverter parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.solarmodule.SolarInverter prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * Protobuf type {@code solarmodule.SolarInverter}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:solarmodule.SolarInverter)
      openfmb.solarmodule.SolarInverterOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.solarmodule.Solarmodule.internal_static_solarmodule_SolarInverter_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.solarmodule.Solarmodule.internal_static_solarmodule_SolarInverter_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.solarmodule.SolarInverter.class, openfmb.solarmodule.SolarInverter.Builder.class);
    }

    // Construct using openfmb.solarmodule.SolarInverter.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      conductingEquipment_ = null;
      if (conductingEquipmentBuilder_ != null) {
        conductingEquipmentBuilder_.dispose();
        conductingEquipmentBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.solarmodule.Solarmodule.internal_static_solarmodule_SolarInverter_descriptor;
    }

    @java.lang.Override
    public openfmb.solarmodule.SolarInverter getDefaultInstanceForType() {
      return openfmb.solarmodule.SolarInverter.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.solarmodule.SolarInverter build() {
      openfmb.solarmodule.SolarInverter result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.solarmodule.SolarInverter buildPartial() {
      openfmb.solarmodule.SolarInverter result = new openfmb.solarmodule.SolarInverter(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.solarmodule.SolarInverter result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.conductingEquipment_ = conductingEquipmentBuilder_ == null
            ? conductingEquipment_
            : conductingEquipmentBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.solarmodule.SolarInverter) {
        return mergeFrom((openfmb.solarmodule.SolarInverter)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.solarmodule.SolarInverter other) {
      if (other == openfmb.solarmodule.SolarInverter.getDefaultInstance()) return this;
      if (other.hasConductingEquipment()) {
        mergeConductingEquipment(other.getConductingEquipment());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getConductingEquipmentFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.ConductingEquipment conductingEquipment_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ConductingEquipment, openfmb.commonmodule.ConductingEquipment.Builder, openfmb.commonmodule.ConductingEquipmentOrBuilder> conductingEquipmentBuilder_;
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     * @return Whether the conductingEquipment field is set.
     */
    public boolean hasConductingEquipment() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     * @return The conductingEquipment.
     */
    public openfmb.commonmodule.ConductingEquipment getConductingEquipment() {
      if (conductingEquipmentBuilder_ == null) {
        return conductingEquipment_ == null ? openfmb.commonmodule.ConductingEquipment.getDefaultInstance() : conductingEquipment_;
      } else {
        return conductingEquipmentBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setConductingEquipment(openfmb.commonmodule.ConductingEquipment value) {
      if (conductingEquipmentBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        conductingEquipment_ = value;
      } else {
        conductingEquipmentBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setConductingEquipment(
        openfmb.commonmodule.ConductingEquipment.Builder builderForValue) {
      if (conductingEquipmentBuilder_ == null) {
        conductingEquipment_ = builderForValue.build();
      } else {
        conductingEquipmentBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder mergeConductingEquipment(openfmb.commonmodule.ConductingEquipment value) {
      if (conductingEquipmentBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          conductingEquipment_ != null &&
          conductingEquipment_ != openfmb.commonmodule.ConductingEquipment.getDefaultInstance()) {
          getConductingEquipmentBuilder().mergeFrom(value);
        } else {
          conductingEquipment_ = value;
        }
      } else {
        conductingEquipmentBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder clearConductingEquipment() {
      bitField0_ = (bitField0_ & ~0x00000001);
      conductingEquipment_ = null;
      if (conductingEquipmentBuilder_ != null) {
        conductingEquipmentBuilder_.dispose();
        conductingEquipmentBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.ConductingEquipment.Builder getConductingEquipmentBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getConductingEquipmentFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.ConductingEquipmentOrBuilder getConductingEquipmentOrBuilder() {
      if (conductingEquipmentBuilder_ != null) {
        return conductingEquipmentBuilder_.getMessageOrBuilder();
      } else {
        return conductingEquipment_ == null ?
            openfmb.commonmodule.ConductingEquipment.getDefaultInstance() : conductingEquipment_;
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ConductingEquipment conductingEquipment = 1 [(.uml.option_parent_message) = true];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ConductingEquipment, openfmb.commonmodule.ConductingEquipment.Builder, openfmb.commonmodule.ConductingEquipmentOrBuilder> 
        getConductingEquipmentFieldBuilder() {
      if (conductingEquipmentBuilder_ == null) {
        conductingEquipmentBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.ConductingEquipment, openfmb.commonmodule.ConductingEquipment.Builder, openfmb.commonmodule.ConductingEquipmentOrBuilder>(
                getConductingEquipment(),
                getParentForChildren(),
                isClean());
        conductingEquipment_ = null;
      }
      return conductingEquipmentBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:solarmodule.SolarInverter)
  }

  // @@protoc_insertion_point(class_scope:solarmodule.SolarInverter)
  private static final openfmb.solarmodule.SolarInverter DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.solarmodule.SolarInverter();
  }

  public static openfmb.solarmodule.SolarInverter getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<SolarInverter>
      PARSER = new com.google.protobuf.AbstractParser<SolarInverter>() {
    @java.lang.Override
    public SolarInverter parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<SolarInverter> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<SolarInverter> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.solarmodule.SolarInverter getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

