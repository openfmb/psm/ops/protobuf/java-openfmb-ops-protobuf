// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: solarmodule/solarmodule.proto

package openfmb.solarmodule;

public interface SolarCapabilityOrBuilder extends
    // @@protoc_insertion_point(interface_extends:solarmodule.SolarCapability)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.NameplateValue nameplateValue = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the nameplateValue field is set.
   */
  boolean hasNameplateValue();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.NameplateValue nameplateValue = 1 [(.uml.option_parent_message) = true];</code>
   * @return The nameplateValue.
   */
  openfmb.commonmodule.NameplateValue getNameplateValue();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.NameplateValue nameplateValue = 1 [(.uml.option_parent_message) = true];</code>
   */
  openfmb.commonmodule.NameplateValueOrBuilder getNameplateValueOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.solarmodule.SolarCapabilityConfiguration solarCapabilityConfiguration = 2;</code>
   * @return Whether the solarCapabilityConfiguration field is set.
   */
  boolean hasSolarCapabilityConfiguration();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.solarmodule.SolarCapabilityConfiguration solarCapabilityConfiguration = 2;</code>
   * @return The solarCapabilityConfiguration.
   */
  openfmb.solarmodule.SolarCapabilityConfiguration getSolarCapabilityConfiguration();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.solarmodule.SolarCapabilityConfiguration solarCapabilityConfiguration = 2;</code>
   */
  openfmb.solarmodule.SolarCapabilityConfigurationOrBuilder getSolarCapabilityConfigurationOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.solarmodule.SolarCapabilityRatings solarCapabilityRatings = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the solarCapabilityRatings field is set.
   */
  boolean hasSolarCapabilityRatings();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.solarmodule.SolarCapabilityRatings solarCapabilityRatings = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The solarCapabilityRatings.
   */
  openfmb.solarmodule.SolarCapabilityRatings getSolarCapabilityRatings();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.solarmodule.SolarCapabilityRatings solarCapabilityRatings = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.solarmodule.SolarCapabilityRatingsOrBuilder getSolarCapabilityRatingsOrBuilder();
}
