// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: solarmodule/solarmodule.proto

package openfmb.solarmodule;

/**
 * <pre>
 * Solar reading profile
 * </pre>
 *
 * Protobuf type {@code solarmodule.SolarReadingProfile}
 */
public final class SolarReadingProfile extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:solarmodule.SolarReadingProfile)
    SolarReadingProfileOrBuilder {
private static final long serialVersionUID = 0L;
  // Use SolarReadingProfile.newBuilder() to construct.
  private SolarReadingProfile(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private SolarReadingProfile() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new SolarReadingProfile();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.solarmodule.Solarmodule.internal_static_solarmodule_SolarReadingProfile_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.solarmodule.Solarmodule.internal_static_solarmodule_SolarReadingProfile_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.solarmodule.SolarReadingProfile.class, openfmb.solarmodule.SolarReadingProfile.Builder.class);
  }

  public static final int READINGMESSAGEINFO_FIELD_NUMBER = 1;
  private openfmb.commonmodule.ReadingMessageInfo readingMessageInfo_;
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ReadingMessageInfo readingMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the readingMessageInfo field is set.
   */
  @java.lang.Override
  public boolean hasReadingMessageInfo() {
    return readingMessageInfo_ != null;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ReadingMessageInfo readingMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return The readingMessageInfo.
   */
  @java.lang.Override
  public openfmb.commonmodule.ReadingMessageInfo getReadingMessageInfo() {
    return readingMessageInfo_ == null ? openfmb.commonmodule.ReadingMessageInfo.getDefaultInstance() : readingMessageInfo_;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ReadingMessageInfo readingMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.ReadingMessageInfoOrBuilder getReadingMessageInfoOrBuilder() {
    return readingMessageInfo_ == null ? openfmb.commonmodule.ReadingMessageInfo.getDefaultInstance() : readingMessageInfo_;
  }

  public static final int SOLARINVERTER_FIELD_NUMBER = 2;
  private openfmb.solarmodule.SolarInverter solarInverter_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.solarmodule.SolarInverter solarInverter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the solarInverter field is set.
   */
  @java.lang.Override
  public boolean hasSolarInverter() {
    return solarInverter_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.solarmodule.SolarInverter solarInverter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The solarInverter.
   */
  @java.lang.Override
  public openfmb.solarmodule.SolarInverter getSolarInverter() {
    return solarInverter_ == null ? openfmb.solarmodule.SolarInverter.getDefaultInstance() : solarInverter_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.solarmodule.SolarInverter solarInverter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.solarmodule.SolarInverterOrBuilder getSolarInverterOrBuilder() {
    return solarInverter_ == null ? openfmb.solarmodule.SolarInverter.getDefaultInstance() : solarInverter_;
  }

  public static final int SOLARREADING_FIELD_NUMBER = 3;
  private openfmb.solarmodule.SolarReading solarReading_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.solarmodule.SolarReading solarReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the solarReading field is set.
   */
  @java.lang.Override
  public boolean hasSolarReading() {
    return solarReading_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.solarmodule.SolarReading solarReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The solarReading.
   */
  @java.lang.Override
  public openfmb.solarmodule.SolarReading getSolarReading() {
    return solarReading_ == null ? openfmb.solarmodule.SolarReading.getDefaultInstance() : solarReading_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.solarmodule.SolarReading solarReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.solarmodule.SolarReadingOrBuilder getSolarReadingOrBuilder() {
    return solarReading_ == null ? openfmb.solarmodule.SolarReading.getDefaultInstance() : solarReading_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (readingMessageInfo_ != null) {
      output.writeMessage(1, getReadingMessageInfo());
    }
    if (solarInverter_ != null) {
      output.writeMessage(2, getSolarInverter());
    }
    if (solarReading_ != null) {
      output.writeMessage(3, getSolarReading());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (readingMessageInfo_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getReadingMessageInfo());
    }
    if (solarInverter_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getSolarInverter());
    }
    if (solarReading_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(3, getSolarReading());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.solarmodule.SolarReadingProfile)) {
      return super.equals(obj);
    }
    openfmb.solarmodule.SolarReadingProfile other = (openfmb.solarmodule.SolarReadingProfile) obj;

    if (hasReadingMessageInfo() != other.hasReadingMessageInfo()) return false;
    if (hasReadingMessageInfo()) {
      if (!getReadingMessageInfo()
          .equals(other.getReadingMessageInfo())) return false;
    }
    if (hasSolarInverter() != other.hasSolarInverter()) return false;
    if (hasSolarInverter()) {
      if (!getSolarInverter()
          .equals(other.getSolarInverter())) return false;
    }
    if (hasSolarReading() != other.hasSolarReading()) return false;
    if (hasSolarReading()) {
      if (!getSolarReading()
          .equals(other.getSolarReading())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasReadingMessageInfo()) {
      hash = (37 * hash) + READINGMESSAGEINFO_FIELD_NUMBER;
      hash = (53 * hash) + getReadingMessageInfo().hashCode();
    }
    if (hasSolarInverter()) {
      hash = (37 * hash) + SOLARINVERTER_FIELD_NUMBER;
      hash = (53 * hash) + getSolarInverter().hashCode();
    }
    if (hasSolarReading()) {
      hash = (37 * hash) + SOLARREADING_FIELD_NUMBER;
      hash = (53 * hash) + getSolarReading().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.solarmodule.SolarReadingProfile parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.solarmodule.SolarReadingProfile parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.solarmodule.SolarReadingProfile parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.solarmodule.SolarReadingProfile parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.solarmodule.SolarReadingProfile parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.solarmodule.SolarReadingProfile parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.solarmodule.SolarReadingProfile parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.solarmodule.SolarReadingProfile parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.solarmodule.SolarReadingProfile parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.solarmodule.SolarReadingProfile parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.solarmodule.SolarReadingProfile parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.solarmodule.SolarReadingProfile parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.solarmodule.SolarReadingProfile prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Solar reading profile
   * </pre>
   *
   * Protobuf type {@code solarmodule.SolarReadingProfile}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:solarmodule.SolarReadingProfile)
      openfmb.solarmodule.SolarReadingProfileOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.solarmodule.Solarmodule.internal_static_solarmodule_SolarReadingProfile_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.solarmodule.Solarmodule.internal_static_solarmodule_SolarReadingProfile_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.solarmodule.SolarReadingProfile.class, openfmb.solarmodule.SolarReadingProfile.Builder.class);
    }

    // Construct using openfmb.solarmodule.SolarReadingProfile.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      readingMessageInfo_ = null;
      if (readingMessageInfoBuilder_ != null) {
        readingMessageInfoBuilder_.dispose();
        readingMessageInfoBuilder_ = null;
      }
      solarInverter_ = null;
      if (solarInverterBuilder_ != null) {
        solarInverterBuilder_.dispose();
        solarInverterBuilder_ = null;
      }
      solarReading_ = null;
      if (solarReadingBuilder_ != null) {
        solarReadingBuilder_.dispose();
        solarReadingBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.solarmodule.Solarmodule.internal_static_solarmodule_SolarReadingProfile_descriptor;
    }

    @java.lang.Override
    public openfmb.solarmodule.SolarReadingProfile getDefaultInstanceForType() {
      return openfmb.solarmodule.SolarReadingProfile.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.solarmodule.SolarReadingProfile build() {
      openfmb.solarmodule.SolarReadingProfile result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.solarmodule.SolarReadingProfile buildPartial() {
      openfmb.solarmodule.SolarReadingProfile result = new openfmb.solarmodule.SolarReadingProfile(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.solarmodule.SolarReadingProfile result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.readingMessageInfo_ = readingMessageInfoBuilder_ == null
            ? readingMessageInfo_
            : readingMessageInfoBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.solarInverter_ = solarInverterBuilder_ == null
            ? solarInverter_
            : solarInverterBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000004) != 0)) {
        result.solarReading_ = solarReadingBuilder_ == null
            ? solarReading_
            : solarReadingBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.solarmodule.SolarReadingProfile) {
        return mergeFrom((openfmb.solarmodule.SolarReadingProfile)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.solarmodule.SolarReadingProfile other) {
      if (other == openfmb.solarmodule.SolarReadingProfile.getDefaultInstance()) return this;
      if (other.hasReadingMessageInfo()) {
        mergeReadingMessageInfo(other.getReadingMessageInfo());
      }
      if (other.hasSolarInverter()) {
        mergeSolarInverter(other.getSolarInverter());
      }
      if (other.hasSolarReading()) {
        mergeSolarReading(other.getSolarReading());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getReadingMessageInfoFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getSolarInverterFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            case 26: {
              input.readMessage(
                  getSolarReadingFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000004;
              break;
            } // case 26
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.ReadingMessageInfo readingMessageInfo_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ReadingMessageInfo, openfmb.commonmodule.ReadingMessageInfo.Builder, openfmb.commonmodule.ReadingMessageInfoOrBuilder> readingMessageInfoBuilder_;
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ReadingMessageInfo readingMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     * @return Whether the readingMessageInfo field is set.
     */
    public boolean hasReadingMessageInfo() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ReadingMessageInfo readingMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     * @return The readingMessageInfo.
     */
    public openfmb.commonmodule.ReadingMessageInfo getReadingMessageInfo() {
      if (readingMessageInfoBuilder_ == null) {
        return readingMessageInfo_ == null ? openfmb.commonmodule.ReadingMessageInfo.getDefaultInstance() : readingMessageInfo_;
      } else {
        return readingMessageInfoBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ReadingMessageInfo readingMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setReadingMessageInfo(openfmb.commonmodule.ReadingMessageInfo value) {
      if (readingMessageInfoBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        readingMessageInfo_ = value;
      } else {
        readingMessageInfoBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ReadingMessageInfo readingMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setReadingMessageInfo(
        openfmb.commonmodule.ReadingMessageInfo.Builder builderForValue) {
      if (readingMessageInfoBuilder_ == null) {
        readingMessageInfo_ = builderForValue.build();
      } else {
        readingMessageInfoBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ReadingMessageInfo readingMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder mergeReadingMessageInfo(openfmb.commonmodule.ReadingMessageInfo value) {
      if (readingMessageInfoBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          readingMessageInfo_ != null &&
          readingMessageInfo_ != openfmb.commonmodule.ReadingMessageInfo.getDefaultInstance()) {
          getReadingMessageInfoBuilder().mergeFrom(value);
        } else {
          readingMessageInfo_ = value;
        }
      } else {
        readingMessageInfoBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ReadingMessageInfo readingMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder clearReadingMessageInfo() {
      bitField0_ = (bitField0_ & ~0x00000001);
      readingMessageInfo_ = null;
      if (readingMessageInfoBuilder_ != null) {
        readingMessageInfoBuilder_.dispose();
        readingMessageInfoBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ReadingMessageInfo readingMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.ReadingMessageInfo.Builder getReadingMessageInfoBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getReadingMessageInfoFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ReadingMessageInfo readingMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.ReadingMessageInfoOrBuilder getReadingMessageInfoOrBuilder() {
      if (readingMessageInfoBuilder_ != null) {
        return readingMessageInfoBuilder_.getMessageOrBuilder();
      } else {
        return readingMessageInfo_ == null ?
            openfmb.commonmodule.ReadingMessageInfo.getDefaultInstance() : readingMessageInfo_;
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ReadingMessageInfo readingMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ReadingMessageInfo, openfmb.commonmodule.ReadingMessageInfo.Builder, openfmb.commonmodule.ReadingMessageInfoOrBuilder> 
        getReadingMessageInfoFieldBuilder() {
      if (readingMessageInfoBuilder_ == null) {
        readingMessageInfoBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.ReadingMessageInfo, openfmb.commonmodule.ReadingMessageInfo.Builder, openfmb.commonmodule.ReadingMessageInfoOrBuilder>(
                getReadingMessageInfo(),
                getParentForChildren(),
                isClean());
        readingMessageInfo_ = null;
      }
      return readingMessageInfoBuilder_;
    }

    private openfmb.solarmodule.SolarInverter solarInverter_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.solarmodule.SolarInverter, openfmb.solarmodule.SolarInverter.Builder, openfmb.solarmodule.SolarInverterOrBuilder> solarInverterBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarInverter solarInverter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return Whether the solarInverter field is set.
     */
    public boolean hasSolarInverter() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarInverter solarInverter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The solarInverter.
     */
    public openfmb.solarmodule.SolarInverter getSolarInverter() {
      if (solarInverterBuilder_ == null) {
        return solarInverter_ == null ? openfmb.solarmodule.SolarInverter.getDefaultInstance() : solarInverter_;
      } else {
        return solarInverterBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarInverter solarInverter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setSolarInverter(openfmb.solarmodule.SolarInverter value) {
      if (solarInverterBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        solarInverter_ = value;
      } else {
        solarInverterBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarInverter solarInverter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setSolarInverter(
        openfmb.solarmodule.SolarInverter.Builder builderForValue) {
      if (solarInverterBuilder_ == null) {
        solarInverter_ = builderForValue.build();
      } else {
        solarInverterBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarInverter solarInverter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder mergeSolarInverter(openfmb.solarmodule.SolarInverter value) {
      if (solarInverterBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          solarInverter_ != null &&
          solarInverter_ != openfmb.solarmodule.SolarInverter.getDefaultInstance()) {
          getSolarInverterBuilder().mergeFrom(value);
        } else {
          solarInverter_ = value;
        }
      } else {
        solarInverterBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarInverter solarInverter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder clearSolarInverter() {
      bitField0_ = (bitField0_ & ~0x00000002);
      solarInverter_ = null;
      if (solarInverterBuilder_ != null) {
        solarInverterBuilder_.dispose();
        solarInverterBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarInverter solarInverter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.solarmodule.SolarInverter.Builder getSolarInverterBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getSolarInverterFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarInverter solarInverter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.solarmodule.SolarInverterOrBuilder getSolarInverterOrBuilder() {
      if (solarInverterBuilder_ != null) {
        return solarInverterBuilder_.getMessageOrBuilder();
      } else {
        return solarInverter_ == null ?
            openfmb.solarmodule.SolarInverter.getDefaultInstance() : solarInverter_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarInverter solarInverter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.solarmodule.SolarInverter, openfmb.solarmodule.SolarInverter.Builder, openfmb.solarmodule.SolarInverterOrBuilder> 
        getSolarInverterFieldBuilder() {
      if (solarInverterBuilder_ == null) {
        solarInverterBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.solarmodule.SolarInverter, openfmb.solarmodule.SolarInverter.Builder, openfmb.solarmodule.SolarInverterOrBuilder>(
                getSolarInverter(),
                getParentForChildren(),
                isClean());
        solarInverter_ = null;
      }
      return solarInverterBuilder_;
    }

    private openfmb.solarmodule.SolarReading solarReading_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.solarmodule.SolarReading, openfmb.solarmodule.SolarReading.Builder, openfmb.solarmodule.SolarReadingOrBuilder> solarReadingBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarReading solarReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return Whether the solarReading field is set.
     */
    public boolean hasSolarReading() {
      return ((bitField0_ & 0x00000004) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarReading solarReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The solarReading.
     */
    public openfmb.solarmodule.SolarReading getSolarReading() {
      if (solarReadingBuilder_ == null) {
        return solarReading_ == null ? openfmb.solarmodule.SolarReading.getDefaultInstance() : solarReading_;
      } else {
        return solarReadingBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarReading solarReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setSolarReading(openfmb.solarmodule.SolarReading value) {
      if (solarReadingBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        solarReading_ = value;
      } else {
        solarReadingBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarReading solarReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setSolarReading(
        openfmb.solarmodule.SolarReading.Builder builderForValue) {
      if (solarReadingBuilder_ == null) {
        solarReading_ = builderForValue.build();
      } else {
        solarReadingBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarReading solarReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder mergeSolarReading(openfmb.solarmodule.SolarReading value) {
      if (solarReadingBuilder_ == null) {
        if (((bitField0_ & 0x00000004) != 0) &&
          solarReading_ != null &&
          solarReading_ != openfmb.solarmodule.SolarReading.getDefaultInstance()) {
          getSolarReadingBuilder().mergeFrom(value);
        } else {
          solarReading_ = value;
        }
      } else {
        solarReadingBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarReading solarReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder clearSolarReading() {
      bitField0_ = (bitField0_ & ~0x00000004);
      solarReading_ = null;
      if (solarReadingBuilder_ != null) {
        solarReadingBuilder_.dispose();
        solarReadingBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarReading solarReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.solarmodule.SolarReading.Builder getSolarReadingBuilder() {
      bitField0_ |= 0x00000004;
      onChanged();
      return getSolarReadingFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarReading solarReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.solarmodule.SolarReadingOrBuilder getSolarReadingOrBuilder() {
      if (solarReadingBuilder_ != null) {
        return solarReadingBuilder_.getMessageOrBuilder();
      } else {
        return solarReading_ == null ?
            openfmb.solarmodule.SolarReading.getDefaultInstance() : solarReading_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.solarmodule.SolarReading solarReading = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.solarmodule.SolarReading, openfmb.solarmodule.SolarReading.Builder, openfmb.solarmodule.SolarReadingOrBuilder> 
        getSolarReadingFieldBuilder() {
      if (solarReadingBuilder_ == null) {
        solarReadingBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.solarmodule.SolarReading, openfmb.solarmodule.SolarReading.Builder, openfmb.solarmodule.SolarReadingOrBuilder>(
                getSolarReading(),
                getParentForChildren(),
                isClean());
        solarReading_ = null;
      }
      return solarReadingBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:solarmodule.SolarReadingProfile)
  }

  // @@protoc_insertion_point(class_scope:solarmodule.SolarReadingProfile)
  private static final openfmb.solarmodule.SolarReadingProfile DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.solarmodule.SolarReadingProfile();
  }

  public static openfmb.solarmodule.SolarReadingProfile getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<SolarReadingProfile>
      PARSER = new com.google.protobuf.AbstractParser<SolarReadingProfile>() {
    @java.lang.Override
    public SolarReadingProfile parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<SolarReadingProfile> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<SolarReadingProfile> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.solarmodule.SolarReadingProfile getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

