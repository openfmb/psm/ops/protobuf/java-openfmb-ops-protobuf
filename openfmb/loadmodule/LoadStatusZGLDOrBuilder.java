// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: loadmodule/loadmodule.proto

package openfmb.loadmodule;

public interface LoadStatusZGLDOrBuilder extends
    // @@protoc_insertion_point(interface_extends:loadmodule.LoadStatusZGLD)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.loadmodule.LoadEventAndStatusZGLD loadEventAndStatusZGLD = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the loadEventAndStatusZGLD field is set.
   */
  boolean hasLoadEventAndStatusZGLD();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.loadmodule.LoadEventAndStatusZGLD loadEventAndStatusZGLD = 1 [(.uml.option_parent_message) = true];</code>
   * @return The loadEventAndStatusZGLD.
   */
  openfmb.loadmodule.LoadEventAndStatusZGLD getLoadEventAndStatusZGLD();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.loadmodule.LoadEventAndStatusZGLD loadEventAndStatusZGLD = 1 [(.uml.option_parent_message) = true];</code>
   */
  openfmb.loadmodule.LoadEventAndStatusZGLDOrBuilder getLoadEventAndStatusZGLDOrBuilder();
}
