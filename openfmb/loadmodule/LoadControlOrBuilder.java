// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: loadmodule/loadmodule.proto

package openfmb.loadmodule;

public interface LoadControlOrBuilder extends
    // @@protoc_insertion_point(interface_extends:loadmodule.LoadControl)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the controlValue field is set.
   */
  boolean hasControlValue();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
   * @return The controlValue.
   */
  openfmb.commonmodule.ControlValue getControlValue();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ControlValue controlValue = 1 [(.uml.option_parent_message) = true];</code>
   */
  openfmb.commonmodule.ControlValueOrBuilder getControlValueOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   * @return Whether the check field is set.
   */
  boolean hasCheck();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   * @return The check.
   */
  openfmb.commonmodule.CheckConditions getCheck();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   */
  openfmb.commonmodule.CheckConditionsOrBuilder getCheckOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.loadmodule.LoadControlFSCC loadControlFSCC = 3;</code>
   * @return Whether the loadControlFSCC field is set.
   */
  boolean hasLoadControlFSCC();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.loadmodule.LoadControlFSCC loadControlFSCC = 3;</code>
   * @return The loadControlFSCC.
   */
  openfmb.loadmodule.LoadControlFSCC getLoadControlFSCC();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.loadmodule.LoadControlFSCC loadControlFSCC = 3;</code>
   */
  openfmb.loadmodule.LoadControlFSCCOrBuilder getLoadControlFSCCOrBuilder();
}
