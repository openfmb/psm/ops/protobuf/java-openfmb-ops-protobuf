// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: loadmodule/loadmodule.proto

package openfmb.loadmodule;

public interface LoadStatusProfileOrBuilder extends
    // @@protoc_insertion_point(interface_extends:loadmodule.LoadStatusProfile)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.StatusMessageInfo statusMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the statusMessageInfo field is set.
   */
  boolean hasStatusMessageInfo();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.StatusMessageInfo statusMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return The statusMessageInfo.
   */
  openfmb.commonmodule.StatusMessageInfo getStatusMessageInfo();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.StatusMessageInfo statusMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   */
  openfmb.commonmodule.StatusMessageInfoOrBuilder getStatusMessageInfoOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.EnergyConsumer energyConsumer = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the energyConsumer field is set.
   */
  boolean hasEnergyConsumer();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.EnergyConsumer energyConsumer = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The energyConsumer.
   */
  openfmb.commonmodule.EnergyConsumer getEnergyConsumer();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.EnergyConsumer energyConsumer = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.commonmodule.EnergyConsumerOrBuilder getEnergyConsumerOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.loadmodule.LoadStatus loadStatus = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the loadStatus field is set.
   */
  boolean hasLoadStatus();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.loadmodule.LoadStatus loadStatus = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The loadStatus.
   */
  openfmb.loadmodule.LoadStatus getLoadStatus();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.loadmodule.LoadStatus loadStatus = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.loadmodule.LoadStatusOrBuilder getLoadStatusOrBuilder();
}
