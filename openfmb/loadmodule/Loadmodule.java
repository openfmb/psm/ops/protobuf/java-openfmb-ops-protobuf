// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: loadmodule/loadmodule.proto

package openfmb.loadmodule;

public final class Loadmodule {
  private Loadmodule() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_loadmodule_LoadPoint_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_loadmodule_LoadPoint_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_loadmodule_LoadCSG_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_loadmodule_LoadCSG_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_loadmodule_LoadControlScheduleFSCH_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_loadmodule_LoadControlScheduleFSCH_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_loadmodule_LoadControlFSCC_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_loadmodule_LoadControlFSCC_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_loadmodule_LoadControl_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_loadmodule_LoadControl_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_loadmodule_LoadControlProfile_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_loadmodule_LoadControlProfile_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_loadmodule_LoadPointStatus_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_loadmodule_LoadPointStatus_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_loadmodule_LoadEventAndStatusZGLD_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_loadmodule_LoadEventAndStatusZGLD_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_loadmodule_LoadEventZGLD_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_loadmodule_LoadEventZGLD_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_loadmodule_LoadEvent_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_loadmodule_LoadEvent_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_loadmodule_LoadEventProfile_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_loadmodule_LoadEventProfile_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_loadmodule_LoadReading_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_loadmodule_LoadReading_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_loadmodule_LoadReadingProfile_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_loadmodule_LoadReadingProfile_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_loadmodule_LoadStatusZGLD_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_loadmodule_LoadStatusZGLD_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_loadmodule_LoadStatus_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_loadmodule_LoadStatus_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_loadmodule_LoadStatusProfile_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_loadmodule_LoadStatusProfile_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\033loadmodule/loadmodule.proto\022\nloadmodul" +
      "e\032\tuml.proto\032\036google/protobuf/wrappers.p" +
      "roto\032\037commonmodule/commonmodule.proto\"\305\002" +
      "\n\tLoadPoint\022)\n\trampRates\030\001 \001(\0132\026.commonm" +
      "odule.RampRate\022<\n\032reactivePwrSetPointEna" +
      "bled\030\002 \001(\0132\030.commonmodule.ControlSPC\0228\n\026" +
      "realPwrSetPointEnabled\030\003 \001(\0132\030.commonmod" +
      "ule.ControlSPC\022\'\n\005reset\030\004 \001(\0132\030.commonmo" +
      "dule.ControlSPC\022/\n\005state\030\005 \001(\0132 .commonm" +
      "odule.Optional_StateKind\022;\n\tstartTime\030\006 " +
      "\001(\0132\036.commonmodule.ControlTimestampB\010\210\265\030" +
      "\001\220\265\030\001\":\n\007LoadCSG\022/\n\006crvPts\030\001 \003(\0132\025.loadm" +
      "odule.LoadPointB\010\210\265\030\001\220\265\030\001\"I\n\027LoadControl" +
      "ScheduleFSCH\022.\n\007ValDCSG\030\001 \001(\0132\023.loadmodu" +
      "le.LoadCSGB\010\210\265\030\001\220\265\030\001\"\215\001\n\017LoadControlFSCC" +
      "\0224\n\013controlFSCC\030\001 \001(\0132\031.commonmodule.Con" +
      "trolFSCCB\004\200\265\030\001\022D\n\027loadControlScheduleFSC" +
      "H\030\002 \001(\0132#.loadmodule.LoadControlSchedule" +
      "FSCH\"\251\001\n\013LoadControl\0226\n\014controlValue\030\001 \001" +
      "(\0132\032.commonmodule.ControlValueB\004\200\265\030\001\022,\n\005" +
      "check\030\002 \001(\0132\035.commonmodule.CheckConditio" +
      "ns\0224\n\017loadControlFSCC\030\003 \001(\0132\033.loadmodule" +
      ".LoadControlFSCC\"\326\001\n\022LoadControlProfile\022" +
      "B\n\022controlMessageInfo\030\001 \001(\0132 .commonmodu" +
      "le.ControlMessageInfoB\004\200\265\030\001\022>\n\016energyCon" +
      "sumer\030\002 \001(\0132\034.commonmodule.EnergyConsume" +
      "rB\010\210\265\030\001\220\265\030\001\0226\n\013loadControl\030\003 \001(\0132\027.loadm" +
      "odule.LoadControlB\010\210\265\030\001\220\265\030\001:\004\300\363\030\001\"\213\002\n\017Lo" +
      "adPointStatus\022)\n\trampRates\030\001 \001(\0132\026.commo" +
      "nmodule.RampRate\022;\n\032reactivePwrSetPointE" +
      "nabled\030\002 \001(\0132\027.commonmodule.StatusSPS\0227\n" +
      "\026realPwrSetPointEnabled\030\003 \001(\0132\027.commonmo" +
      "dule.StatusSPS\022&\n\005reset\030\004 \001(\0132\027.commonmo" +
      "dule.StatusSPS\022/\n\005state\030\005 \001(\0132 .commonmo" +
      "dule.Optional_StateKind\"\204\002\n\026LoadEventAnd" +
      "StatusZGLD\022V\n\034logicalNodeForEventAndStat" +
      "us\030\001 \001(\0132*.commonmodule.LogicalNodeForEv" +
      "entAndStatusB\004\200\265\030\001\0226\n\013DynamicTest\030\002 \001(\0132" +
      "!.commonmodule.ENS_DynamicTestKind\022(\n\007Em" +
      "gStop\030\003 \001(\0132\027.commonmodule.StatusSPS\0220\n\013" +
      "PointStatus\030\004 \001(\0132\033.loadmodule.LoadPoint" +
      "Status\"Y\n\rLoadEventZGLD\022H\n\026loadEventAndS" +
      "tatusZGLD\030\001 \001(\0132\".loadmodule.LoadEventAn" +
      "dStatusZGLDB\004\200\265\030\001\"q\n\tLoadEvent\0222\n\neventV" +
      "alue\030\001 \001(\0132\030.commonmodule.EventValueB\004\200\265" +
      "\030\001\0220\n\rloadEventZGLD\030\002 \001(\0132\031.loadmodule.L" +
      "oadEventZGLD\"\314\001\n\020LoadEventProfile\022>\n\020eve" +
      "ntMessageInfo\030\001 \001(\0132\036.commonmodule.Event" +
      "MessageInfoB\004\200\265\030\001\022>\n\016energyConsumer\030\002 \001(" +
      "\0132\034.commonmodule.EnergyConsumerB\010\210\265\030\001\220\265\030" +
      "\001\0222\n\tloadEvent\030\003 \001(\0132\025.loadmodule.LoadEv" +
      "entB\010\210\265\030\001\220\265\030\001:\004\300\363\030\001\"\375\001\n\013LoadReading\022b\n\"c" +
      "onductingEquipmentTerminalReading\030\001 \001(\0132" +
      "0.commonmodule.ConductingEquipmentTermin" +
      "alReadingB\004\200\265\030\001\022*\n\tphaseMMTN\030\002 \001(\0132\027.com" +
      "monmodule.PhaseMMTN\022.\n\013readingMMTR\030\003 \001(\013" +
      "2\031.commonmodule.ReadingMMTR\022.\n\013readingMM" +
      "XU\030\004 \001(\0132\031.commonmodule.ReadingMMXU\"\326\001\n\022" +
      "LoadReadingProfile\022B\n\022readingMessageInfo" +
      "\030\001 \001(\0132 .commonmodule.ReadingMessageInfo" +
      "B\004\200\265\030\001\022>\n\016energyConsumer\030\002 \001(\0132\034.commonm" +
      "odule.EnergyConsumerB\010\210\265\030\001\220\265\030\001\0226\n\013loadRe" +
      "ading\030\003 \001(\0132\027.loadmodule.LoadReadingB\010\210\265" +
      "\030\001\220\265\030\001:\004\300\363\030\001\"Z\n\016LoadStatusZGLD\022H\n\026loadEv" +
      "entAndStatusZGLD\030\001 \001(\0132\".loadmodule.Load" +
      "EventAndStatusZGLDB\004\200\265\030\001\"\254\001\n\nLoadStatus\022" +
      "4\n\013statusValue\030\001 \001(\0132\031.commonmodule.Stat" +
      "usValueB\004\200\265\030\001\0224\n\020isUncontrollable\030\002 \001(\0132" +
      "\032.google.protobuf.BoolValue\0222\n\016loadStatu" +
      "sZGLD\030\003 \001(\0132\032.loadmodule.LoadStatusZGLD\"" +
      "\321\001\n\021LoadStatusProfile\022@\n\021statusMessageIn" +
      "fo\030\001 \001(\0132\037.commonmodule.StatusMessageInf" +
      "oB\004\200\265\030\001\022>\n\016energyConsumer\030\002 \001(\0132\034.common" +
      "module.EnergyConsumerB\010\210\265\030\001\220\265\030\001\0224\n\nloadS" +
      "tatus\030\003 \001(\0132\026.loadmodule.LoadStatusB\010\210\265\030" +
      "\001\220\265\030\001:\004\300\363\030\001B~\n\022openfmb.loadmoduleP\001ZQgit" +
      "lab.com/openfmb/psm/ops/protobuf/go-open" +
      "fmb-ops-protobuf/v2/openfmb/loadmodule\252\002" +
      "\022openfmb.loadmoduleb\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          openfmb.Uml.getDescriptor(),
          com.google.protobuf.WrappersProto.getDescriptor(),
          openfmb.commonmodule.Commonmodule.getDescriptor(),
        });
    internal_static_loadmodule_LoadPoint_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_loadmodule_LoadPoint_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_loadmodule_LoadPoint_descriptor,
        new java.lang.String[] { "RampRates", "ReactivePwrSetPointEnabled", "RealPwrSetPointEnabled", "Reset", "State", "StartTime", });
    internal_static_loadmodule_LoadCSG_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_loadmodule_LoadCSG_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_loadmodule_LoadCSG_descriptor,
        new java.lang.String[] { "CrvPts", });
    internal_static_loadmodule_LoadControlScheduleFSCH_descriptor =
      getDescriptor().getMessageTypes().get(2);
    internal_static_loadmodule_LoadControlScheduleFSCH_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_loadmodule_LoadControlScheduleFSCH_descriptor,
        new java.lang.String[] { "ValDCSG", });
    internal_static_loadmodule_LoadControlFSCC_descriptor =
      getDescriptor().getMessageTypes().get(3);
    internal_static_loadmodule_LoadControlFSCC_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_loadmodule_LoadControlFSCC_descriptor,
        new java.lang.String[] { "ControlFSCC", "LoadControlScheduleFSCH", });
    internal_static_loadmodule_LoadControl_descriptor =
      getDescriptor().getMessageTypes().get(4);
    internal_static_loadmodule_LoadControl_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_loadmodule_LoadControl_descriptor,
        new java.lang.String[] { "ControlValue", "Check", "LoadControlFSCC", });
    internal_static_loadmodule_LoadControlProfile_descriptor =
      getDescriptor().getMessageTypes().get(5);
    internal_static_loadmodule_LoadControlProfile_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_loadmodule_LoadControlProfile_descriptor,
        new java.lang.String[] { "ControlMessageInfo", "EnergyConsumer", "LoadControl", });
    internal_static_loadmodule_LoadPointStatus_descriptor =
      getDescriptor().getMessageTypes().get(6);
    internal_static_loadmodule_LoadPointStatus_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_loadmodule_LoadPointStatus_descriptor,
        new java.lang.String[] { "RampRates", "ReactivePwrSetPointEnabled", "RealPwrSetPointEnabled", "Reset", "State", });
    internal_static_loadmodule_LoadEventAndStatusZGLD_descriptor =
      getDescriptor().getMessageTypes().get(7);
    internal_static_loadmodule_LoadEventAndStatusZGLD_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_loadmodule_LoadEventAndStatusZGLD_descriptor,
        new java.lang.String[] { "LogicalNodeForEventAndStatus", "DynamicTest", "EmgStop", "PointStatus", });
    internal_static_loadmodule_LoadEventZGLD_descriptor =
      getDescriptor().getMessageTypes().get(8);
    internal_static_loadmodule_LoadEventZGLD_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_loadmodule_LoadEventZGLD_descriptor,
        new java.lang.String[] { "LoadEventAndStatusZGLD", });
    internal_static_loadmodule_LoadEvent_descriptor =
      getDescriptor().getMessageTypes().get(9);
    internal_static_loadmodule_LoadEvent_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_loadmodule_LoadEvent_descriptor,
        new java.lang.String[] { "EventValue", "LoadEventZGLD", });
    internal_static_loadmodule_LoadEventProfile_descriptor =
      getDescriptor().getMessageTypes().get(10);
    internal_static_loadmodule_LoadEventProfile_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_loadmodule_LoadEventProfile_descriptor,
        new java.lang.String[] { "EventMessageInfo", "EnergyConsumer", "LoadEvent", });
    internal_static_loadmodule_LoadReading_descriptor =
      getDescriptor().getMessageTypes().get(11);
    internal_static_loadmodule_LoadReading_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_loadmodule_LoadReading_descriptor,
        new java.lang.String[] { "ConductingEquipmentTerminalReading", "PhaseMMTN", "ReadingMMTR", "ReadingMMXU", });
    internal_static_loadmodule_LoadReadingProfile_descriptor =
      getDescriptor().getMessageTypes().get(12);
    internal_static_loadmodule_LoadReadingProfile_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_loadmodule_LoadReadingProfile_descriptor,
        new java.lang.String[] { "ReadingMessageInfo", "EnergyConsumer", "LoadReading", });
    internal_static_loadmodule_LoadStatusZGLD_descriptor =
      getDescriptor().getMessageTypes().get(13);
    internal_static_loadmodule_LoadStatusZGLD_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_loadmodule_LoadStatusZGLD_descriptor,
        new java.lang.String[] { "LoadEventAndStatusZGLD", });
    internal_static_loadmodule_LoadStatus_descriptor =
      getDescriptor().getMessageTypes().get(14);
    internal_static_loadmodule_LoadStatus_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_loadmodule_LoadStatus_descriptor,
        new java.lang.String[] { "StatusValue", "IsUncontrollable", "LoadStatusZGLD", });
    internal_static_loadmodule_LoadStatusProfile_descriptor =
      getDescriptor().getMessageTypes().get(15);
    internal_static_loadmodule_LoadStatusProfile_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_loadmodule_LoadStatusProfile_descriptor,
        new java.lang.String[] { "StatusMessageInfo", "EnergyConsumer", "LoadStatus", });
    com.google.protobuf.ExtensionRegistry registry =
        com.google.protobuf.ExtensionRegistry.newInstance();
    registry.add(openfmb.Uml.optionMultiplicityMin);
    registry.add(openfmb.Uml.optionOpenfmbProfile);
    registry.add(openfmb.Uml.optionParentMessage);
    registry.add(openfmb.Uml.optionRequiredField);
    com.google.protobuf.Descriptors.FileDescriptor
        .internalUpdateFileDescriptor(descriptor, registry);
    openfmb.Uml.getDescriptor();
    com.google.protobuf.WrappersProto.getDescriptor();
    openfmb.commonmodule.Commonmodule.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
