// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

/**
 * <pre>
 * Generic control message info.
 * </pre>
 *
 * Protobuf type {@code commonmodule.MessageInfo}
 */
public final class MessageInfo extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:commonmodule.MessageInfo)
    MessageInfoOrBuilder {
private static final long serialVersionUID = 0L;
  // Use MessageInfo.newBuilder() to construct.
  private MessageInfo(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private MessageInfo() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new MessageInfo();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_MessageInfo_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_MessageInfo_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.commonmodule.MessageInfo.class, openfmb.commonmodule.MessageInfo.Builder.class);
  }

  public static final int IDENTIFIEDOBJECT_FIELD_NUMBER = 1;
  private openfmb.commonmodule.IdentifiedObject identifiedObject_;
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the identifiedObject field is set.
   */
  @java.lang.Override
  public boolean hasIdentifiedObject() {
    return identifiedObject_ != null;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
   * @return The identifiedObject.
   */
  @java.lang.Override
  public openfmb.commonmodule.IdentifiedObject getIdentifiedObject() {
    return identifiedObject_ == null ? openfmb.commonmodule.IdentifiedObject.getDefaultInstance() : identifiedObject_;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.IdentifiedObjectOrBuilder getIdentifiedObjectOrBuilder() {
    return identifiedObject_ == null ? openfmb.commonmodule.IdentifiedObject.getDefaultInstance() : identifiedObject_;
  }

  public static final int MESSAGETIMESTAMP_FIELD_NUMBER = 2;
  private openfmb.commonmodule.Timestamp messageTimeStamp_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.Timestamp messageTimeStamp = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the messageTimeStamp field is set.
   */
  @java.lang.Override
  public boolean hasMessageTimeStamp() {
    return messageTimeStamp_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.Timestamp messageTimeStamp = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The messageTimeStamp.
   */
  @java.lang.Override
  public openfmb.commonmodule.Timestamp getMessageTimeStamp() {
    return messageTimeStamp_ == null ? openfmb.commonmodule.Timestamp.getDefaultInstance() : messageTimeStamp_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.Timestamp messageTimeStamp = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.TimestampOrBuilder getMessageTimeStampOrBuilder() {
    return messageTimeStamp_ == null ? openfmb.commonmodule.Timestamp.getDefaultInstance() : messageTimeStamp_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (identifiedObject_ != null) {
      output.writeMessage(1, getIdentifiedObject());
    }
    if (messageTimeStamp_ != null) {
      output.writeMessage(2, getMessageTimeStamp());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (identifiedObject_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getIdentifiedObject());
    }
    if (messageTimeStamp_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getMessageTimeStamp());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.commonmodule.MessageInfo)) {
      return super.equals(obj);
    }
    openfmb.commonmodule.MessageInfo other = (openfmb.commonmodule.MessageInfo) obj;

    if (hasIdentifiedObject() != other.hasIdentifiedObject()) return false;
    if (hasIdentifiedObject()) {
      if (!getIdentifiedObject()
          .equals(other.getIdentifiedObject())) return false;
    }
    if (hasMessageTimeStamp() != other.hasMessageTimeStamp()) return false;
    if (hasMessageTimeStamp()) {
      if (!getMessageTimeStamp()
          .equals(other.getMessageTimeStamp())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasIdentifiedObject()) {
      hash = (37 * hash) + IDENTIFIEDOBJECT_FIELD_NUMBER;
      hash = (53 * hash) + getIdentifiedObject().hashCode();
    }
    if (hasMessageTimeStamp()) {
      hash = (37 * hash) + MESSAGETIMESTAMP_FIELD_NUMBER;
      hash = (53 * hash) + getMessageTimeStamp().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.commonmodule.MessageInfo parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.MessageInfo parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.MessageInfo parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.MessageInfo parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.MessageInfo parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.MessageInfo parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.MessageInfo parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.MessageInfo parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.commonmodule.MessageInfo parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.MessageInfo parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.commonmodule.MessageInfo parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.MessageInfo parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.commonmodule.MessageInfo prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Generic control message info.
   * </pre>
   *
   * Protobuf type {@code commonmodule.MessageInfo}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:commonmodule.MessageInfo)
      openfmb.commonmodule.MessageInfoOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_MessageInfo_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_MessageInfo_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.commonmodule.MessageInfo.class, openfmb.commonmodule.MessageInfo.Builder.class);
    }

    // Construct using openfmb.commonmodule.MessageInfo.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      identifiedObject_ = null;
      if (identifiedObjectBuilder_ != null) {
        identifiedObjectBuilder_.dispose();
        identifiedObjectBuilder_ = null;
      }
      messageTimeStamp_ = null;
      if (messageTimeStampBuilder_ != null) {
        messageTimeStampBuilder_.dispose();
        messageTimeStampBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_MessageInfo_descriptor;
    }

    @java.lang.Override
    public openfmb.commonmodule.MessageInfo getDefaultInstanceForType() {
      return openfmb.commonmodule.MessageInfo.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.commonmodule.MessageInfo build() {
      openfmb.commonmodule.MessageInfo result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.commonmodule.MessageInfo buildPartial() {
      openfmb.commonmodule.MessageInfo result = new openfmb.commonmodule.MessageInfo(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.commonmodule.MessageInfo result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.identifiedObject_ = identifiedObjectBuilder_ == null
            ? identifiedObject_
            : identifiedObjectBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.messageTimeStamp_ = messageTimeStampBuilder_ == null
            ? messageTimeStamp_
            : messageTimeStampBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.commonmodule.MessageInfo) {
        return mergeFrom((openfmb.commonmodule.MessageInfo)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.commonmodule.MessageInfo other) {
      if (other == openfmb.commonmodule.MessageInfo.getDefaultInstance()) return this;
      if (other.hasIdentifiedObject()) {
        mergeIdentifiedObject(other.getIdentifiedObject());
      }
      if (other.hasMessageTimeStamp()) {
        mergeMessageTimeStamp(other.getMessageTimeStamp());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getIdentifiedObjectFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getMessageTimeStampFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.IdentifiedObject identifiedObject_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.IdentifiedObject, openfmb.commonmodule.IdentifiedObject.Builder, openfmb.commonmodule.IdentifiedObjectOrBuilder> identifiedObjectBuilder_;
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     * @return Whether the identifiedObject field is set.
     */
    public boolean hasIdentifiedObject() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     * @return The identifiedObject.
     */
    public openfmb.commonmodule.IdentifiedObject getIdentifiedObject() {
      if (identifiedObjectBuilder_ == null) {
        return identifiedObject_ == null ? openfmb.commonmodule.IdentifiedObject.getDefaultInstance() : identifiedObject_;
      } else {
        return identifiedObjectBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setIdentifiedObject(openfmb.commonmodule.IdentifiedObject value) {
      if (identifiedObjectBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        identifiedObject_ = value;
      } else {
        identifiedObjectBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setIdentifiedObject(
        openfmb.commonmodule.IdentifiedObject.Builder builderForValue) {
      if (identifiedObjectBuilder_ == null) {
        identifiedObject_ = builderForValue.build();
      } else {
        identifiedObjectBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder mergeIdentifiedObject(openfmb.commonmodule.IdentifiedObject value) {
      if (identifiedObjectBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          identifiedObject_ != null &&
          identifiedObject_ != openfmb.commonmodule.IdentifiedObject.getDefaultInstance()) {
          getIdentifiedObjectBuilder().mergeFrom(value);
        } else {
          identifiedObject_ = value;
        }
      } else {
        identifiedObjectBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder clearIdentifiedObject() {
      bitField0_ = (bitField0_ & ~0x00000001);
      identifiedObject_ = null;
      if (identifiedObjectBuilder_ != null) {
        identifiedObjectBuilder_.dispose();
        identifiedObjectBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.IdentifiedObject.Builder getIdentifiedObjectBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getIdentifiedObjectFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.IdentifiedObjectOrBuilder getIdentifiedObjectOrBuilder() {
      if (identifiedObjectBuilder_ != null) {
        return identifiedObjectBuilder_.getMessageOrBuilder();
      } else {
        return identifiedObject_ == null ?
            openfmb.commonmodule.IdentifiedObject.getDefaultInstance() : identifiedObject_;
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.IdentifiedObject, openfmb.commonmodule.IdentifiedObject.Builder, openfmb.commonmodule.IdentifiedObjectOrBuilder> 
        getIdentifiedObjectFieldBuilder() {
      if (identifiedObjectBuilder_ == null) {
        identifiedObjectBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.IdentifiedObject, openfmb.commonmodule.IdentifiedObject.Builder, openfmb.commonmodule.IdentifiedObjectOrBuilder>(
                getIdentifiedObject(),
                getParentForChildren(),
                isClean());
        identifiedObject_ = null;
      }
      return identifiedObjectBuilder_;
    }

    private openfmb.commonmodule.Timestamp messageTimeStamp_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.Timestamp, openfmb.commonmodule.Timestamp.Builder, openfmb.commonmodule.TimestampOrBuilder> messageTimeStampBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.Timestamp messageTimeStamp = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return Whether the messageTimeStamp field is set.
     */
    public boolean hasMessageTimeStamp() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.Timestamp messageTimeStamp = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The messageTimeStamp.
     */
    public openfmb.commonmodule.Timestamp getMessageTimeStamp() {
      if (messageTimeStampBuilder_ == null) {
        return messageTimeStamp_ == null ? openfmb.commonmodule.Timestamp.getDefaultInstance() : messageTimeStamp_;
      } else {
        return messageTimeStampBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.Timestamp messageTimeStamp = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setMessageTimeStamp(openfmb.commonmodule.Timestamp value) {
      if (messageTimeStampBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        messageTimeStamp_ = value;
      } else {
        messageTimeStampBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.Timestamp messageTimeStamp = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setMessageTimeStamp(
        openfmb.commonmodule.Timestamp.Builder builderForValue) {
      if (messageTimeStampBuilder_ == null) {
        messageTimeStamp_ = builderForValue.build();
      } else {
        messageTimeStampBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.Timestamp messageTimeStamp = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder mergeMessageTimeStamp(openfmb.commonmodule.Timestamp value) {
      if (messageTimeStampBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          messageTimeStamp_ != null &&
          messageTimeStamp_ != openfmb.commonmodule.Timestamp.getDefaultInstance()) {
          getMessageTimeStampBuilder().mergeFrom(value);
        } else {
          messageTimeStamp_ = value;
        }
      } else {
        messageTimeStampBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.Timestamp messageTimeStamp = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder clearMessageTimeStamp() {
      bitField0_ = (bitField0_ & ~0x00000002);
      messageTimeStamp_ = null;
      if (messageTimeStampBuilder_ != null) {
        messageTimeStampBuilder_.dispose();
        messageTimeStampBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.Timestamp messageTimeStamp = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.commonmodule.Timestamp.Builder getMessageTimeStampBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getMessageTimeStampFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.Timestamp messageTimeStamp = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.commonmodule.TimestampOrBuilder getMessageTimeStampOrBuilder() {
      if (messageTimeStampBuilder_ != null) {
        return messageTimeStampBuilder_.getMessageOrBuilder();
      } else {
        return messageTimeStamp_ == null ?
            openfmb.commonmodule.Timestamp.getDefaultInstance() : messageTimeStamp_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.Timestamp messageTimeStamp = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.Timestamp, openfmb.commonmodule.Timestamp.Builder, openfmb.commonmodule.TimestampOrBuilder> 
        getMessageTimeStampFieldBuilder() {
      if (messageTimeStampBuilder_ == null) {
        messageTimeStampBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.Timestamp, openfmb.commonmodule.Timestamp.Builder, openfmb.commonmodule.TimestampOrBuilder>(
                getMessageTimeStamp(),
                getParentForChildren(),
                isClean());
        messageTimeStamp_ = null;
      }
      return messageTimeStampBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:commonmodule.MessageInfo)
  }

  // @@protoc_insertion_point(class_scope:commonmodule.MessageInfo)
  private static final openfmb.commonmodule.MessageInfo DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.commonmodule.MessageInfo();
  }

  public static openfmb.commonmodule.MessageInfo getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<MessageInfo>
      PARSER = new com.google.protobuf.AbstractParser<MessageInfo>() {
    @java.lang.Override
    public MessageInfo parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<MessageInfo> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<MessageInfo> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.commonmodule.MessageInfo getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

