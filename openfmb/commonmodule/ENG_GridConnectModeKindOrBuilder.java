// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

public interface ENG_GridConnectModeKindOrBuilder extends
    // @@protoc_insertion_point(interface_extends:commonmodule.ENG_GridConnectModeKind)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * The value of the status setting.
   * </pre>
   *
   * <code>.commonmodule.GridConnectModeKind setVal = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The enum numeric value on the wire for setVal.
   */
  int getSetValValue();
  /**
   * <pre>
   * The value of the status setting.
   * </pre>
   *
   * <code>.commonmodule.GridConnectModeKind setVal = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The setVal.
   */
  openfmb.commonmodule.GridConnectModeKind getSetVal();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.google.protobuf.StringValue setValExtension = 2;</code>
   * @return Whether the setValExtension field is set.
   */
  boolean hasSetValExtension();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.google.protobuf.StringValue setValExtension = 2;</code>
   * @return The setValExtension.
   */
  com.google.protobuf.StringValue getSetValExtension();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.google.protobuf.StringValue setValExtension = 2;</code>
   */
  com.google.protobuf.StringValueOrBuilder getSetValExtensionOrBuilder();
}
