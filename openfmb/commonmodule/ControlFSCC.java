// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

/**
 * <pre>
 * LN: Schedule controller   Name: FSCC  F:    Function (generic) SC:  Schedule Controller C:   
 * Control (execution)
 * </pre>
 *
 * Protobuf type {@code commonmodule.ControlFSCC}
 */
public final class ControlFSCC extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:commonmodule.ControlFSCC)
    ControlFSCCOrBuilder {
private static final long serialVersionUID = 0L;
  // Use ControlFSCC.newBuilder() to construct.
  private ControlFSCC(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private ControlFSCC() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new ControlFSCC();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_ControlFSCC_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_ControlFSCC_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.commonmodule.ControlFSCC.class, openfmb.commonmodule.ControlFSCC.Builder.class);
  }

  public static final int LOGICALNODEFORCONTROL_FIELD_NUMBER = 1;
  private openfmb.commonmodule.LogicalNodeForControl logicalNodeForControl_;
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the logicalNodeForControl field is set.
   */
  @java.lang.Override
  public boolean hasLogicalNodeForControl() {
    return logicalNodeForControl_ != null;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
   * @return The logicalNodeForControl.
   */
  @java.lang.Override
  public openfmb.commonmodule.LogicalNodeForControl getLogicalNodeForControl() {
    return logicalNodeForControl_ == null ? openfmb.commonmodule.LogicalNodeForControl.getDefaultInstance() : logicalNodeForControl_;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.LogicalNodeForControlOrBuilder getLogicalNodeForControlOrBuilder() {
    return logicalNodeForControl_ == null ? openfmb.commonmodule.LogicalNodeForControl.getDefaultInstance() : logicalNodeForControl_;
  }

  public static final int CONTROLSCHEDULEFSCH_FIELD_NUMBER = 2;
  private openfmb.commonmodule.ControlScheduleFSCH controlScheduleFSCH_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ControlScheduleFSCH controlScheduleFSCH = 2;</code>
   * @return Whether the controlScheduleFSCH field is set.
   */
  @java.lang.Override
  public boolean hasControlScheduleFSCH() {
    return controlScheduleFSCH_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ControlScheduleFSCH controlScheduleFSCH = 2;</code>
   * @return The controlScheduleFSCH.
   */
  @java.lang.Override
  public openfmb.commonmodule.ControlScheduleFSCH getControlScheduleFSCH() {
    return controlScheduleFSCH_ == null ? openfmb.commonmodule.ControlScheduleFSCH.getDefaultInstance() : controlScheduleFSCH_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ControlScheduleFSCH controlScheduleFSCH = 2;</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.ControlScheduleFSCHOrBuilder getControlScheduleFSCHOrBuilder() {
    return controlScheduleFSCH_ == null ? openfmb.commonmodule.ControlScheduleFSCH.getDefaultInstance() : controlScheduleFSCH_;
  }

  public static final int ISLANDCONTROLSCHEDULEFSCH_FIELD_NUMBER = 3;
  private openfmb.commonmodule.ControlScheduleFSCH islandControlScheduleFSCH_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ControlScheduleFSCH islandControlScheduleFSCH = 3;</code>
   * @return Whether the islandControlScheduleFSCH field is set.
   */
  @java.lang.Override
  public boolean hasIslandControlScheduleFSCH() {
    return islandControlScheduleFSCH_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ControlScheduleFSCH islandControlScheduleFSCH = 3;</code>
   * @return The islandControlScheduleFSCH.
   */
  @java.lang.Override
  public openfmb.commonmodule.ControlScheduleFSCH getIslandControlScheduleFSCH() {
    return islandControlScheduleFSCH_ == null ? openfmb.commonmodule.ControlScheduleFSCH.getDefaultInstance() : islandControlScheduleFSCH_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ControlScheduleFSCH islandControlScheduleFSCH = 3;</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.ControlScheduleFSCHOrBuilder getIslandControlScheduleFSCHOrBuilder() {
    return islandControlScheduleFSCH_ == null ? openfmb.commonmodule.ControlScheduleFSCH.getDefaultInstance() : islandControlScheduleFSCH_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (logicalNodeForControl_ != null) {
      output.writeMessage(1, getLogicalNodeForControl());
    }
    if (controlScheduleFSCH_ != null) {
      output.writeMessage(2, getControlScheduleFSCH());
    }
    if (islandControlScheduleFSCH_ != null) {
      output.writeMessage(3, getIslandControlScheduleFSCH());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (logicalNodeForControl_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getLogicalNodeForControl());
    }
    if (controlScheduleFSCH_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getControlScheduleFSCH());
    }
    if (islandControlScheduleFSCH_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(3, getIslandControlScheduleFSCH());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.commonmodule.ControlFSCC)) {
      return super.equals(obj);
    }
    openfmb.commonmodule.ControlFSCC other = (openfmb.commonmodule.ControlFSCC) obj;

    if (hasLogicalNodeForControl() != other.hasLogicalNodeForControl()) return false;
    if (hasLogicalNodeForControl()) {
      if (!getLogicalNodeForControl()
          .equals(other.getLogicalNodeForControl())) return false;
    }
    if (hasControlScheduleFSCH() != other.hasControlScheduleFSCH()) return false;
    if (hasControlScheduleFSCH()) {
      if (!getControlScheduleFSCH()
          .equals(other.getControlScheduleFSCH())) return false;
    }
    if (hasIslandControlScheduleFSCH() != other.hasIslandControlScheduleFSCH()) return false;
    if (hasIslandControlScheduleFSCH()) {
      if (!getIslandControlScheduleFSCH()
          .equals(other.getIslandControlScheduleFSCH())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasLogicalNodeForControl()) {
      hash = (37 * hash) + LOGICALNODEFORCONTROL_FIELD_NUMBER;
      hash = (53 * hash) + getLogicalNodeForControl().hashCode();
    }
    if (hasControlScheduleFSCH()) {
      hash = (37 * hash) + CONTROLSCHEDULEFSCH_FIELD_NUMBER;
      hash = (53 * hash) + getControlScheduleFSCH().hashCode();
    }
    if (hasIslandControlScheduleFSCH()) {
      hash = (37 * hash) + ISLANDCONTROLSCHEDULEFSCH_FIELD_NUMBER;
      hash = (53 * hash) + getIslandControlScheduleFSCH().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.commonmodule.ControlFSCC parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.ControlFSCC parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.ControlFSCC parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.ControlFSCC parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.ControlFSCC parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.ControlFSCC parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.ControlFSCC parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.ControlFSCC parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.commonmodule.ControlFSCC parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.ControlFSCC parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.commonmodule.ControlFSCC parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.ControlFSCC parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.commonmodule.ControlFSCC prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * LN: Schedule controller   Name: FSCC  F:    Function (generic) SC:  Schedule Controller C:   
   * Control (execution)
   * </pre>
   *
   * Protobuf type {@code commonmodule.ControlFSCC}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:commonmodule.ControlFSCC)
      openfmb.commonmodule.ControlFSCCOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_ControlFSCC_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_ControlFSCC_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.commonmodule.ControlFSCC.class, openfmb.commonmodule.ControlFSCC.Builder.class);
    }

    // Construct using openfmb.commonmodule.ControlFSCC.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      logicalNodeForControl_ = null;
      if (logicalNodeForControlBuilder_ != null) {
        logicalNodeForControlBuilder_.dispose();
        logicalNodeForControlBuilder_ = null;
      }
      controlScheduleFSCH_ = null;
      if (controlScheduleFSCHBuilder_ != null) {
        controlScheduleFSCHBuilder_.dispose();
        controlScheduleFSCHBuilder_ = null;
      }
      islandControlScheduleFSCH_ = null;
      if (islandControlScheduleFSCHBuilder_ != null) {
        islandControlScheduleFSCHBuilder_.dispose();
        islandControlScheduleFSCHBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_ControlFSCC_descriptor;
    }

    @java.lang.Override
    public openfmb.commonmodule.ControlFSCC getDefaultInstanceForType() {
      return openfmb.commonmodule.ControlFSCC.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.commonmodule.ControlFSCC build() {
      openfmb.commonmodule.ControlFSCC result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.commonmodule.ControlFSCC buildPartial() {
      openfmb.commonmodule.ControlFSCC result = new openfmb.commonmodule.ControlFSCC(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.commonmodule.ControlFSCC result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.logicalNodeForControl_ = logicalNodeForControlBuilder_ == null
            ? logicalNodeForControl_
            : logicalNodeForControlBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.controlScheduleFSCH_ = controlScheduleFSCHBuilder_ == null
            ? controlScheduleFSCH_
            : controlScheduleFSCHBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000004) != 0)) {
        result.islandControlScheduleFSCH_ = islandControlScheduleFSCHBuilder_ == null
            ? islandControlScheduleFSCH_
            : islandControlScheduleFSCHBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.commonmodule.ControlFSCC) {
        return mergeFrom((openfmb.commonmodule.ControlFSCC)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.commonmodule.ControlFSCC other) {
      if (other == openfmb.commonmodule.ControlFSCC.getDefaultInstance()) return this;
      if (other.hasLogicalNodeForControl()) {
        mergeLogicalNodeForControl(other.getLogicalNodeForControl());
      }
      if (other.hasControlScheduleFSCH()) {
        mergeControlScheduleFSCH(other.getControlScheduleFSCH());
      }
      if (other.hasIslandControlScheduleFSCH()) {
        mergeIslandControlScheduleFSCH(other.getIslandControlScheduleFSCH());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getLogicalNodeForControlFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getControlScheduleFSCHFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            case 26: {
              input.readMessage(
                  getIslandControlScheduleFSCHFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000004;
              break;
            } // case 26
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.LogicalNodeForControl logicalNodeForControl_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.LogicalNodeForControl, openfmb.commonmodule.LogicalNodeForControl.Builder, openfmb.commonmodule.LogicalNodeForControlOrBuilder> logicalNodeForControlBuilder_;
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     * @return Whether the logicalNodeForControl field is set.
     */
    public boolean hasLogicalNodeForControl() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     * @return The logicalNodeForControl.
     */
    public openfmb.commonmodule.LogicalNodeForControl getLogicalNodeForControl() {
      if (logicalNodeForControlBuilder_ == null) {
        return logicalNodeForControl_ == null ? openfmb.commonmodule.LogicalNodeForControl.getDefaultInstance() : logicalNodeForControl_;
      } else {
        return logicalNodeForControlBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setLogicalNodeForControl(openfmb.commonmodule.LogicalNodeForControl value) {
      if (logicalNodeForControlBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        logicalNodeForControl_ = value;
      } else {
        logicalNodeForControlBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setLogicalNodeForControl(
        openfmb.commonmodule.LogicalNodeForControl.Builder builderForValue) {
      if (logicalNodeForControlBuilder_ == null) {
        logicalNodeForControl_ = builderForValue.build();
      } else {
        logicalNodeForControlBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder mergeLogicalNodeForControl(openfmb.commonmodule.LogicalNodeForControl value) {
      if (logicalNodeForControlBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          logicalNodeForControl_ != null &&
          logicalNodeForControl_ != openfmb.commonmodule.LogicalNodeForControl.getDefaultInstance()) {
          getLogicalNodeForControlBuilder().mergeFrom(value);
        } else {
          logicalNodeForControl_ = value;
        }
      } else {
        logicalNodeForControlBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder clearLogicalNodeForControl() {
      bitField0_ = (bitField0_ & ~0x00000001);
      logicalNodeForControl_ = null;
      if (logicalNodeForControlBuilder_ != null) {
        logicalNodeForControlBuilder_.dispose();
        logicalNodeForControlBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.LogicalNodeForControl.Builder getLogicalNodeForControlBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getLogicalNodeForControlFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.LogicalNodeForControlOrBuilder getLogicalNodeForControlOrBuilder() {
      if (logicalNodeForControlBuilder_ != null) {
        return logicalNodeForControlBuilder_.getMessageOrBuilder();
      } else {
        return logicalNodeForControl_ == null ?
            openfmb.commonmodule.LogicalNodeForControl.getDefaultInstance() : logicalNodeForControl_;
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.LogicalNodeForControl, openfmb.commonmodule.LogicalNodeForControl.Builder, openfmb.commonmodule.LogicalNodeForControlOrBuilder> 
        getLogicalNodeForControlFieldBuilder() {
      if (logicalNodeForControlBuilder_ == null) {
        logicalNodeForControlBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.LogicalNodeForControl, openfmb.commonmodule.LogicalNodeForControl.Builder, openfmb.commonmodule.LogicalNodeForControlOrBuilder>(
                getLogicalNodeForControl(),
                getParentForChildren(),
                isClean());
        logicalNodeForControl_ = null;
      }
      return logicalNodeForControlBuilder_;
    }

    private openfmb.commonmodule.ControlScheduleFSCH controlScheduleFSCH_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ControlScheduleFSCH, openfmb.commonmodule.ControlScheduleFSCH.Builder, openfmb.commonmodule.ControlScheduleFSCHOrBuilder> controlScheduleFSCHBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH controlScheduleFSCH = 2;</code>
     * @return Whether the controlScheduleFSCH field is set.
     */
    public boolean hasControlScheduleFSCH() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH controlScheduleFSCH = 2;</code>
     * @return The controlScheduleFSCH.
     */
    public openfmb.commonmodule.ControlScheduleFSCH getControlScheduleFSCH() {
      if (controlScheduleFSCHBuilder_ == null) {
        return controlScheduleFSCH_ == null ? openfmb.commonmodule.ControlScheduleFSCH.getDefaultInstance() : controlScheduleFSCH_;
      } else {
        return controlScheduleFSCHBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH controlScheduleFSCH = 2;</code>
     */
    public Builder setControlScheduleFSCH(openfmb.commonmodule.ControlScheduleFSCH value) {
      if (controlScheduleFSCHBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        controlScheduleFSCH_ = value;
      } else {
        controlScheduleFSCHBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH controlScheduleFSCH = 2;</code>
     */
    public Builder setControlScheduleFSCH(
        openfmb.commonmodule.ControlScheduleFSCH.Builder builderForValue) {
      if (controlScheduleFSCHBuilder_ == null) {
        controlScheduleFSCH_ = builderForValue.build();
      } else {
        controlScheduleFSCHBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH controlScheduleFSCH = 2;</code>
     */
    public Builder mergeControlScheduleFSCH(openfmb.commonmodule.ControlScheduleFSCH value) {
      if (controlScheduleFSCHBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          controlScheduleFSCH_ != null &&
          controlScheduleFSCH_ != openfmb.commonmodule.ControlScheduleFSCH.getDefaultInstance()) {
          getControlScheduleFSCHBuilder().mergeFrom(value);
        } else {
          controlScheduleFSCH_ = value;
        }
      } else {
        controlScheduleFSCHBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH controlScheduleFSCH = 2;</code>
     */
    public Builder clearControlScheduleFSCH() {
      bitField0_ = (bitField0_ & ~0x00000002);
      controlScheduleFSCH_ = null;
      if (controlScheduleFSCHBuilder_ != null) {
        controlScheduleFSCHBuilder_.dispose();
        controlScheduleFSCHBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH controlScheduleFSCH = 2;</code>
     */
    public openfmb.commonmodule.ControlScheduleFSCH.Builder getControlScheduleFSCHBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getControlScheduleFSCHFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH controlScheduleFSCH = 2;</code>
     */
    public openfmb.commonmodule.ControlScheduleFSCHOrBuilder getControlScheduleFSCHOrBuilder() {
      if (controlScheduleFSCHBuilder_ != null) {
        return controlScheduleFSCHBuilder_.getMessageOrBuilder();
      } else {
        return controlScheduleFSCH_ == null ?
            openfmb.commonmodule.ControlScheduleFSCH.getDefaultInstance() : controlScheduleFSCH_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH controlScheduleFSCH = 2;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ControlScheduleFSCH, openfmb.commonmodule.ControlScheduleFSCH.Builder, openfmb.commonmodule.ControlScheduleFSCHOrBuilder> 
        getControlScheduleFSCHFieldBuilder() {
      if (controlScheduleFSCHBuilder_ == null) {
        controlScheduleFSCHBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.ControlScheduleFSCH, openfmb.commonmodule.ControlScheduleFSCH.Builder, openfmb.commonmodule.ControlScheduleFSCHOrBuilder>(
                getControlScheduleFSCH(),
                getParentForChildren(),
                isClean());
        controlScheduleFSCH_ = null;
      }
      return controlScheduleFSCHBuilder_;
    }

    private openfmb.commonmodule.ControlScheduleFSCH islandControlScheduleFSCH_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ControlScheduleFSCH, openfmb.commonmodule.ControlScheduleFSCH.Builder, openfmb.commonmodule.ControlScheduleFSCHOrBuilder> islandControlScheduleFSCHBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH islandControlScheduleFSCH = 3;</code>
     * @return Whether the islandControlScheduleFSCH field is set.
     */
    public boolean hasIslandControlScheduleFSCH() {
      return ((bitField0_ & 0x00000004) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH islandControlScheduleFSCH = 3;</code>
     * @return The islandControlScheduleFSCH.
     */
    public openfmb.commonmodule.ControlScheduleFSCH getIslandControlScheduleFSCH() {
      if (islandControlScheduleFSCHBuilder_ == null) {
        return islandControlScheduleFSCH_ == null ? openfmb.commonmodule.ControlScheduleFSCH.getDefaultInstance() : islandControlScheduleFSCH_;
      } else {
        return islandControlScheduleFSCHBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH islandControlScheduleFSCH = 3;</code>
     */
    public Builder setIslandControlScheduleFSCH(openfmb.commonmodule.ControlScheduleFSCH value) {
      if (islandControlScheduleFSCHBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        islandControlScheduleFSCH_ = value;
      } else {
        islandControlScheduleFSCHBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH islandControlScheduleFSCH = 3;</code>
     */
    public Builder setIslandControlScheduleFSCH(
        openfmb.commonmodule.ControlScheduleFSCH.Builder builderForValue) {
      if (islandControlScheduleFSCHBuilder_ == null) {
        islandControlScheduleFSCH_ = builderForValue.build();
      } else {
        islandControlScheduleFSCHBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH islandControlScheduleFSCH = 3;</code>
     */
    public Builder mergeIslandControlScheduleFSCH(openfmb.commonmodule.ControlScheduleFSCH value) {
      if (islandControlScheduleFSCHBuilder_ == null) {
        if (((bitField0_ & 0x00000004) != 0) &&
          islandControlScheduleFSCH_ != null &&
          islandControlScheduleFSCH_ != openfmb.commonmodule.ControlScheduleFSCH.getDefaultInstance()) {
          getIslandControlScheduleFSCHBuilder().mergeFrom(value);
        } else {
          islandControlScheduleFSCH_ = value;
        }
      } else {
        islandControlScheduleFSCHBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH islandControlScheduleFSCH = 3;</code>
     */
    public Builder clearIslandControlScheduleFSCH() {
      bitField0_ = (bitField0_ & ~0x00000004);
      islandControlScheduleFSCH_ = null;
      if (islandControlScheduleFSCHBuilder_ != null) {
        islandControlScheduleFSCHBuilder_.dispose();
        islandControlScheduleFSCHBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH islandControlScheduleFSCH = 3;</code>
     */
    public openfmb.commonmodule.ControlScheduleFSCH.Builder getIslandControlScheduleFSCHBuilder() {
      bitField0_ |= 0x00000004;
      onChanged();
      return getIslandControlScheduleFSCHFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH islandControlScheduleFSCH = 3;</code>
     */
    public openfmb.commonmodule.ControlScheduleFSCHOrBuilder getIslandControlScheduleFSCHOrBuilder() {
      if (islandControlScheduleFSCHBuilder_ != null) {
        return islandControlScheduleFSCHBuilder_.getMessageOrBuilder();
      } else {
        return islandControlScheduleFSCH_ == null ?
            openfmb.commonmodule.ControlScheduleFSCH.getDefaultInstance() : islandControlScheduleFSCH_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlScheduleFSCH islandControlScheduleFSCH = 3;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ControlScheduleFSCH, openfmb.commonmodule.ControlScheduleFSCH.Builder, openfmb.commonmodule.ControlScheduleFSCHOrBuilder> 
        getIslandControlScheduleFSCHFieldBuilder() {
      if (islandControlScheduleFSCHBuilder_ == null) {
        islandControlScheduleFSCHBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.ControlScheduleFSCH, openfmb.commonmodule.ControlScheduleFSCH.Builder, openfmb.commonmodule.ControlScheduleFSCHOrBuilder>(
                getIslandControlScheduleFSCH(),
                getParentForChildren(),
                isClean());
        islandControlScheduleFSCH_ = null;
      }
      return islandControlScheduleFSCHBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:commonmodule.ControlFSCC)
  }

  // @@protoc_insertion_point(class_scope:commonmodule.ControlFSCC)
  private static final openfmb.commonmodule.ControlFSCC DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.commonmodule.ControlFSCC();
  }

  public static openfmb.commonmodule.ControlFSCC getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<ControlFSCC>
      PARSER = new com.google.protobuf.AbstractParser<ControlFSCC>() {
    @java.lang.Override
    public ControlFSCC parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<ControlFSCC> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<ControlFSCC> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.commonmodule.ControlFSCC getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

