// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

/**
 * <pre>
 * Point definition (Point)
 * </pre>
 *
 * Protobuf type {@code commonmodule.VoltVarPoint}
 */
public final class VoltVarPoint extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:commonmodule.VoltVarPoint)
    VoltVarPointOrBuilder {
private static final long serialVersionUID = 0L;
  // Use VoltVarPoint.newBuilder() to construct.
  private VoltVarPoint(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private VoltVarPoint() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new VoltVarPoint();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_VoltVarPoint_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_VoltVarPoint_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.commonmodule.VoltVarPoint.class, openfmb.commonmodule.VoltVarPoint.Builder.class);
  }

  public static final int VARVAL_FIELD_NUMBER = 1;
  private float varVal_ = 0F;
  /**
   * <pre>
   * This is an absolute value field.
   * </pre>
   *
   * <code>float varVal = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The varVal.
   */
  @java.lang.Override
  public float getVarVal() {
    return varVal_;
  }

  public static final int VOLTVAL_FIELD_NUMBER = 2;
  private float voltVal_ = 0F;
  /**
   * <pre>
   * This is an absolute value field.
   * </pre>
   *
   * <code>float voltVal = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The voltVal.
   */
  @java.lang.Override
  public float getVoltVal() {
    return voltVal_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (java.lang.Float.floatToRawIntBits(varVal_) != 0) {
      output.writeFloat(1, varVal_);
    }
    if (java.lang.Float.floatToRawIntBits(voltVal_) != 0) {
      output.writeFloat(2, voltVal_);
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (java.lang.Float.floatToRawIntBits(varVal_) != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeFloatSize(1, varVal_);
    }
    if (java.lang.Float.floatToRawIntBits(voltVal_) != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeFloatSize(2, voltVal_);
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.commonmodule.VoltVarPoint)) {
      return super.equals(obj);
    }
    openfmb.commonmodule.VoltVarPoint other = (openfmb.commonmodule.VoltVarPoint) obj;

    if (java.lang.Float.floatToIntBits(getVarVal())
        != java.lang.Float.floatToIntBits(
            other.getVarVal())) return false;
    if (java.lang.Float.floatToIntBits(getVoltVal())
        != java.lang.Float.floatToIntBits(
            other.getVoltVal())) return false;
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + VARVAL_FIELD_NUMBER;
    hash = (53 * hash) + java.lang.Float.floatToIntBits(
        getVarVal());
    hash = (37 * hash) + VOLTVAL_FIELD_NUMBER;
    hash = (53 * hash) + java.lang.Float.floatToIntBits(
        getVoltVal());
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.commonmodule.VoltVarPoint parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.VoltVarPoint parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.VoltVarPoint parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.VoltVarPoint parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.VoltVarPoint parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.VoltVarPoint parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.VoltVarPoint parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.VoltVarPoint parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.commonmodule.VoltVarPoint parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.VoltVarPoint parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.commonmodule.VoltVarPoint parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.VoltVarPoint parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.commonmodule.VoltVarPoint prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Point definition (Point)
   * </pre>
   *
   * Protobuf type {@code commonmodule.VoltVarPoint}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:commonmodule.VoltVarPoint)
      openfmb.commonmodule.VoltVarPointOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_VoltVarPoint_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_VoltVarPoint_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.commonmodule.VoltVarPoint.class, openfmb.commonmodule.VoltVarPoint.Builder.class);
    }

    // Construct using openfmb.commonmodule.VoltVarPoint.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      varVal_ = 0F;
      voltVal_ = 0F;
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_VoltVarPoint_descriptor;
    }

    @java.lang.Override
    public openfmb.commonmodule.VoltVarPoint getDefaultInstanceForType() {
      return openfmb.commonmodule.VoltVarPoint.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.commonmodule.VoltVarPoint build() {
      openfmb.commonmodule.VoltVarPoint result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.commonmodule.VoltVarPoint buildPartial() {
      openfmb.commonmodule.VoltVarPoint result = new openfmb.commonmodule.VoltVarPoint(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.commonmodule.VoltVarPoint result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.varVal_ = varVal_;
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.voltVal_ = voltVal_;
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.commonmodule.VoltVarPoint) {
        return mergeFrom((openfmb.commonmodule.VoltVarPoint)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.commonmodule.VoltVarPoint other) {
      if (other == openfmb.commonmodule.VoltVarPoint.getDefaultInstance()) return this;
      if (other.getVarVal() != 0F) {
        setVarVal(other.getVarVal());
      }
      if (other.getVoltVal() != 0F) {
        setVoltVal(other.getVoltVal());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 13: {
              varVal_ = input.readFloat();
              bitField0_ |= 0x00000001;
              break;
            } // case 13
            case 21: {
              voltVal_ = input.readFloat();
              bitField0_ |= 0x00000002;
              break;
            } // case 21
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private float varVal_ ;
    /**
     * <pre>
     * This is an absolute value field.
     * </pre>
     *
     * <code>float varVal = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The varVal.
     */
    @java.lang.Override
    public float getVarVal() {
      return varVal_;
    }
    /**
     * <pre>
     * This is an absolute value field.
     * </pre>
     *
     * <code>float varVal = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @param value The varVal to set.
     * @return This builder for chaining.
     */
    public Builder setVarVal(float value) {

      varVal_ = value;
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * This is an absolute value field.
     * </pre>
     *
     * <code>float varVal = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return This builder for chaining.
     */
    public Builder clearVarVal() {
      bitField0_ = (bitField0_ & ~0x00000001);
      varVal_ = 0F;
      onChanged();
      return this;
    }

    private float voltVal_ ;
    /**
     * <pre>
     * This is an absolute value field.
     * </pre>
     *
     * <code>float voltVal = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The voltVal.
     */
    @java.lang.Override
    public float getVoltVal() {
      return voltVal_;
    }
    /**
     * <pre>
     * This is an absolute value field.
     * </pre>
     *
     * <code>float voltVal = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @param value The voltVal to set.
     * @return This builder for chaining.
     */
    public Builder setVoltVal(float value) {

      voltVal_ = value;
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * This is an absolute value field.
     * </pre>
     *
     * <code>float voltVal = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return This builder for chaining.
     */
    public Builder clearVoltVal() {
      bitField0_ = (bitField0_ & ~0x00000002);
      voltVal_ = 0F;
      onChanged();
      return this;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:commonmodule.VoltVarPoint)
  }

  // @@protoc_insertion_point(class_scope:commonmodule.VoltVarPoint)
  private static final openfmb.commonmodule.VoltVarPoint DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.commonmodule.VoltVarPoint();
  }

  public static openfmb.commonmodule.VoltVarPoint getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<VoltVarPoint>
      PARSER = new com.google.protobuf.AbstractParser<VoltVarPoint>() {
    @java.lang.Override
    public VoltVarPoint parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<VoltVarPoint> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<VoltVarPoint> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.commonmodule.VoltVarPoint getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

