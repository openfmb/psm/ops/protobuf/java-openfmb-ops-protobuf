// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

public interface ClearingTimeOrBuilder extends
    // @@protoc_insertion_point(interface_extends:commonmodule.ClearingTime)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>uint64 seconds = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The seconds.
   */
  long getSeconds();

  /**
   * <pre>
   * Partial (sub) second expressed in nanoseconds (10&lt;sup&gt;-9&lt;/sup&gt; second).
   * </pre>
   *
   * <code>uint32 nanoseconds = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The nanoseconds.
   */
  int getNanoseconds();
}
