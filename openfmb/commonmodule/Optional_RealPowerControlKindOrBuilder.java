// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

public interface Optional_RealPowerControlKindOrBuilder extends
    // @@protoc_insertion_point(interface_extends:commonmodule.Optional_RealPowerControlKind)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>.commonmodule.RealPowerControlKind value = 1;</code>
   * @return The enum numeric value on the wire for value.
   */
  int getValueValue();
  /**
   * <code>.commonmodule.RealPowerControlKind value = 1;</code>
   * @return The value.
   */
  openfmb.commonmodule.RealPowerControlKind getValue();
}
