// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

/**
 * <pre>
 * Active Power-Reactive Power (Watt-VAr) Function
 * </pre>
 *
 * Protobuf type {@code commonmodule.WVarCSG}
 */
public final class WVarCSG extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:commonmodule.WVarCSG)
    WVarCSGOrBuilder {
private static final long serialVersionUID = 0L;
  // Use WVarCSG.newBuilder() to construct.
  private WVarCSG(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private WVarCSG() {
    crvPts_ = java.util.Collections.emptyList();
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new WVarCSG();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_WVarCSG_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_WVarCSG_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.commonmodule.WVarCSG.class, openfmb.commonmodule.WVarCSG.Builder.class);
  }

  public static final int CRVPTS_FIELD_NUMBER = 1;
  @SuppressWarnings("serial")
  private java.util.List<openfmb.commonmodule.WVarPoint> crvPts_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public java.util.List<openfmb.commonmodule.WVarPoint> getCrvPtsList() {
    return crvPts_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public java.util.List<? extends openfmb.commonmodule.WVarPointOrBuilder> 
      getCrvPtsOrBuilderList() {
    return crvPts_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public int getCrvPtsCount() {
    return crvPts_.size();
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.WVarPoint getCrvPts(int index) {
    return crvPts_.get(index);
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.WVarPointOrBuilder getCrvPtsOrBuilder(
      int index) {
    return crvPts_.get(index);
  }

  public static final int WVARPARAMETER_FIELD_NUMBER = 2;
  private openfmb.commonmodule.OperationDWVR wVarParameter_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.OperationDWVR wVarParameter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the wVarParameter field is set.
   */
  @java.lang.Override
  public boolean hasWVarParameter() {
    return wVarParameter_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.OperationDWVR wVarParameter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The wVarParameter.
   */
  @java.lang.Override
  public openfmb.commonmodule.OperationDWVR getWVarParameter() {
    return wVarParameter_ == null ? openfmb.commonmodule.OperationDWVR.getDefaultInstance() : wVarParameter_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.OperationDWVR wVarParameter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.OperationDWVROrBuilder getWVarParameterOrBuilder() {
    return wVarParameter_ == null ? openfmb.commonmodule.OperationDWVR.getDefaultInstance() : wVarParameter_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    for (int i = 0; i < crvPts_.size(); i++) {
      output.writeMessage(1, crvPts_.get(i));
    }
    if (wVarParameter_ != null) {
      output.writeMessage(2, getWVarParameter());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    for (int i = 0; i < crvPts_.size(); i++) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, crvPts_.get(i));
    }
    if (wVarParameter_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getWVarParameter());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.commonmodule.WVarCSG)) {
      return super.equals(obj);
    }
    openfmb.commonmodule.WVarCSG other = (openfmb.commonmodule.WVarCSG) obj;

    if (!getCrvPtsList()
        .equals(other.getCrvPtsList())) return false;
    if (hasWVarParameter() != other.hasWVarParameter()) return false;
    if (hasWVarParameter()) {
      if (!getWVarParameter()
          .equals(other.getWVarParameter())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (getCrvPtsCount() > 0) {
      hash = (37 * hash) + CRVPTS_FIELD_NUMBER;
      hash = (53 * hash) + getCrvPtsList().hashCode();
    }
    if (hasWVarParameter()) {
      hash = (37 * hash) + WVARPARAMETER_FIELD_NUMBER;
      hash = (53 * hash) + getWVarParameter().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.commonmodule.WVarCSG parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.WVarCSG parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.WVarCSG parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.WVarCSG parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.WVarCSG parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.WVarCSG parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.WVarCSG parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.WVarCSG parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.commonmodule.WVarCSG parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.WVarCSG parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.commonmodule.WVarCSG parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.WVarCSG parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.commonmodule.WVarCSG prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Active Power-Reactive Power (Watt-VAr) Function
   * </pre>
   *
   * Protobuf type {@code commonmodule.WVarCSG}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:commonmodule.WVarCSG)
      openfmb.commonmodule.WVarCSGOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_WVarCSG_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_WVarCSG_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.commonmodule.WVarCSG.class, openfmb.commonmodule.WVarCSG.Builder.class);
    }

    // Construct using openfmb.commonmodule.WVarCSG.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      if (crvPtsBuilder_ == null) {
        crvPts_ = java.util.Collections.emptyList();
      } else {
        crvPts_ = null;
        crvPtsBuilder_.clear();
      }
      bitField0_ = (bitField0_ & ~0x00000001);
      wVarParameter_ = null;
      if (wVarParameterBuilder_ != null) {
        wVarParameterBuilder_.dispose();
        wVarParameterBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_WVarCSG_descriptor;
    }

    @java.lang.Override
    public openfmb.commonmodule.WVarCSG getDefaultInstanceForType() {
      return openfmb.commonmodule.WVarCSG.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.commonmodule.WVarCSG build() {
      openfmb.commonmodule.WVarCSG result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.commonmodule.WVarCSG buildPartial() {
      openfmb.commonmodule.WVarCSG result = new openfmb.commonmodule.WVarCSG(this);
      buildPartialRepeatedFields(result);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartialRepeatedFields(openfmb.commonmodule.WVarCSG result) {
      if (crvPtsBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0)) {
          crvPts_ = java.util.Collections.unmodifiableList(crvPts_);
          bitField0_ = (bitField0_ & ~0x00000001);
        }
        result.crvPts_ = crvPts_;
      } else {
        result.crvPts_ = crvPtsBuilder_.build();
      }
    }

    private void buildPartial0(openfmb.commonmodule.WVarCSG result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.wVarParameter_ = wVarParameterBuilder_ == null
            ? wVarParameter_
            : wVarParameterBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.commonmodule.WVarCSG) {
        return mergeFrom((openfmb.commonmodule.WVarCSG)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.commonmodule.WVarCSG other) {
      if (other == openfmb.commonmodule.WVarCSG.getDefaultInstance()) return this;
      if (crvPtsBuilder_ == null) {
        if (!other.crvPts_.isEmpty()) {
          if (crvPts_.isEmpty()) {
            crvPts_ = other.crvPts_;
            bitField0_ = (bitField0_ & ~0x00000001);
          } else {
            ensureCrvPtsIsMutable();
            crvPts_.addAll(other.crvPts_);
          }
          onChanged();
        }
      } else {
        if (!other.crvPts_.isEmpty()) {
          if (crvPtsBuilder_.isEmpty()) {
            crvPtsBuilder_.dispose();
            crvPtsBuilder_ = null;
            crvPts_ = other.crvPts_;
            bitField0_ = (bitField0_ & ~0x00000001);
            crvPtsBuilder_ = 
              com.google.protobuf.GeneratedMessageV3.alwaysUseFieldBuilders ?
                 getCrvPtsFieldBuilder() : null;
          } else {
            crvPtsBuilder_.addAllMessages(other.crvPts_);
          }
        }
      }
      if (other.hasWVarParameter()) {
        mergeWVarParameter(other.getWVarParameter());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              openfmb.commonmodule.WVarPoint m =
                  input.readMessage(
                      openfmb.commonmodule.WVarPoint.parser(),
                      extensionRegistry);
              if (crvPtsBuilder_ == null) {
                ensureCrvPtsIsMutable();
                crvPts_.add(m);
              } else {
                crvPtsBuilder_.addMessage(m);
              }
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getWVarParameterFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private java.util.List<openfmb.commonmodule.WVarPoint> crvPts_ =
      java.util.Collections.emptyList();
    private void ensureCrvPtsIsMutable() {
      if (!((bitField0_ & 0x00000001) != 0)) {
        crvPts_ = new java.util.ArrayList<openfmb.commonmodule.WVarPoint>(crvPts_);
        bitField0_ |= 0x00000001;
       }
    }

    private com.google.protobuf.RepeatedFieldBuilderV3<
        openfmb.commonmodule.WVarPoint, openfmb.commonmodule.WVarPoint.Builder, openfmb.commonmodule.WVarPointOrBuilder> crvPtsBuilder_;

    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public java.util.List<openfmb.commonmodule.WVarPoint> getCrvPtsList() {
      if (crvPtsBuilder_ == null) {
        return java.util.Collections.unmodifiableList(crvPts_);
      } else {
        return crvPtsBuilder_.getMessageList();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public int getCrvPtsCount() {
      if (crvPtsBuilder_ == null) {
        return crvPts_.size();
      } else {
        return crvPtsBuilder_.getCount();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.commonmodule.WVarPoint getCrvPts(int index) {
      if (crvPtsBuilder_ == null) {
        return crvPts_.get(index);
      } else {
        return crvPtsBuilder_.getMessage(index);
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setCrvPts(
        int index, openfmb.commonmodule.WVarPoint value) {
      if (crvPtsBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureCrvPtsIsMutable();
        crvPts_.set(index, value);
        onChanged();
      } else {
        crvPtsBuilder_.setMessage(index, value);
      }
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setCrvPts(
        int index, openfmb.commonmodule.WVarPoint.Builder builderForValue) {
      if (crvPtsBuilder_ == null) {
        ensureCrvPtsIsMutable();
        crvPts_.set(index, builderForValue.build());
        onChanged();
      } else {
        crvPtsBuilder_.setMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder addCrvPts(openfmb.commonmodule.WVarPoint value) {
      if (crvPtsBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureCrvPtsIsMutable();
        crvPts_.add(value);
        onChanged();
      } else {
        crvPtsBuilder_.addMessage(value);
      }
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder addCrvPts(
        int index, openfmb.commonmodule.WVarPoint value) {
      if (crvPtsBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureCrvPtsIsMutable();
        crvPts_.add(index, value);
        onChanged();
      } else {
        crvPtsBuilder_.addMessage(index, value);
      }
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder addCrvPts(
        openfmb.commonmodule.WVarPoint.Builder builderForValue) {
      if (crvPtsBuilder_ == null) {
        ensureCrvPtsIsMutable();
        crvPts_.add(builderForValue.build());
        onChanged();
      } else {
        crvPtsBuilder_.addMessage(builderForValue.build());
      }
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder addCrvPts(
        int index, openfmb.commonmodule.WVarPoint.Builder builderForValue) {
      if (crvPtsBuilder_ == null) {
        ensureCrvPtsIsMutable();
        crvPts_.add(index, builderForValue.build());
        onChanged();
      } else {
        crvPtsBuilder_.addMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder addAllCrvPts(
        java.lang.Iterable<? extends openfmb.commonmodule.WVarPoint> values) {
      if (crvPtsBuilder_ == null) {
        ensureCrvPtsIsMutable();
        com.google.protobuf.AbstractMessageLite.Builder.addAll(
            values, crvPts_);
        onChanged();
      } else {
        crvPtsBuilder_.addAllMessages(values);
      }
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder clearCrvPts() {
      if (crvPtsBuilder_ == null) {
        crvPts_ = java.util.Collections.emptyList();
        bitField0_ = (bitField0_ & ~0x00000001);
        onChanged();
      } else {
        crvPtsBuilder_.clear();
      }
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder removeCrvPts(int index) {
      if (crvPtsBuilder_ == null) {
        ensureCrvPtsIsMutable();
        crvPts_.remove(index);
        onChanged();
      } else {
        crvPtsBuilder_.remove(index);
      }
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.commonmodule.WVarPoint.Builder getCrvPtsBuilder(
        int index) {
      return getCrvPtsFieldBuilder().getBuilder(index);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.commonmodule.WVarPointOrBuilder getCrvPtsOrBuilder(
        int index) {
      if (crvPtsBuilder_ == null) {
        return crvPts_.get(index);  } else {
        return crvPtsBuilder_.getMessageOrBuilder(index);
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public java.util.List<? extends openfmb.commonmodule.WVarPointOrBuilder> 
         getCrvPtsOrBuilderList() {
      if (crvPtsBuilder_ != null) {
        return crvPtsBuilder_.getMessageOrBuilderList();
      } else {
        return java.util.Collections.unmodifiableList(crvPts_);
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.commonmodule.WVarPoint.Builder addCrvPtsBuilder() {
      return getCrvPtsFieldBuilder().addBuilder(
          openfmb.commonmodule.WVarPoint.getDefaultInstance());
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.commonmodule.WVarPoint.Builder addCrvPtsBuilder(
        int index) {
      return getCrvPtsFieldBuilder().addBuilder(
          index, openfmb.commonmodule.WVarPoint.getDefaultInstance());
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>repeated .commonmodule.WVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public java.util.List<openfmb.commonmodule.WVarPoint.Builder> 
         getCrvPtsBuilderList() {
      return getCrvPtsFieldBuilder().getBuilderList();
    }
    private com.google.protobuf.RepeatedFieldBuilderV3<
        openfmb.commonmodule.WVarPoint, openfmb.commonmodule.WVarPoint.Builder, openfmb.commonmodule.WVarPointOrBuilder> 
        getCrvPtsFieldBuilder() {
      if (crvPtsBuilder_ == null) {
        crvPtsBuilder_ = new com.google.protobuf.RepeatedFieldBuilderV3<
            openfmb.commonmodule.WVarPoint, openfmb.commonmodule.WVarPoint.Builder, openfmb.commonmodule.WVarPointOrBuilder>(
                crvPts_,
                ((bitField0_ & 0x00000001) != 0),
                getParentForChildren(),
                isClean());
        crvPts_ = null;
      }
      return crvPtsBuilder_;
    }

    private openfmb.commonmodule.OperationDWVR wVarParameter_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.OperationDWVR, openfmb.commonmodule.OperationDWVR.Builder, openfmb.commonmodule.OperationDWVROrBuilder> wVarParameterBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.OperationDWVR wVarParameter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return Whether the wVarParameter field is set.
     */
    public boolean hasWVarParameter() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.OperationDWVR wVarParameter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The wVarParameter.
     */
    public openfmb.commonmodule.OperationDWVR getWVarParameter() {
      if (wVarParameterBuilder_ == null) {
        return wVarParameter_ == null ? openfmb.commonmodule.OperationDWVR.getDefaultInstance() : wVarParameter_;
      } else {
        return wVarParameterBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.OperationDWVR wVarParameter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setWVarParameter(openfmb.commonmodule.OperationDWVR value) {
      if (wVarParameterBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        wVarParameter_ = value;
      } else {
        wVarParameterBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.OperationDWVR wVarParameter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setWVarParameter(
        openfmb.commonmodule.OperationDWVR.Builder builderForValue) {
      if (wVarParameterBuilder_ == null) {
        wVarParameter_ = builderForValue.build();
      } else {
        wVarParameterBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.OperationDWVR wVarParameter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder mergeWVarParameter(openfmb.commonmodule.OperationDWVR value) {
      if (wVarParameterBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          wVarParameter_ != null &&
          wVarParameter_ != openfmb.commonmodule.OperationDWVR.getDefaultInstance()) {
          getWVarParameterBuilder().mergeFrom(value);
        } else {
          wVarParameter_ = value;
        }
      } else {
        wVarParameterBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.OperationDWVR wVarParameter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder clearWVarParameter() {
      bitField0_ = (bitField0_ & ~0x00000002);
      wVarParameter_ = null;
      if (wVarParameterBuilder_ != null) {
        wVarParameterBuilder_.dispose();
        wVarParameterBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.OperationDWVR wVarParameter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.commonmodule.OperationDWVR.Builder getWVarParameterBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getWVarParameterFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.OperationDWVR wVarParameter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.commonmodule.OperationDWVROrBuilder getWVarParameterOrBuilder() {
      if (wVarParameterBuilder_ != null) {
        return wVarParameterBuilder_.getMessageOrBuilder();
      } else {
        return wVarParameter_ == null ?
            openfmb.commonmodule.OperationDWVR.getDefaultInstance() : wVarParameter_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.OperationDWVR wVarParameter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.OperationDWVR, openfmb.commonmodule.OperationDWVR.Builder, openfmb.commonmodule.OperationDWVROrBuilder> 
        getWVarParameterFieldBuilder() {
      if (wVarParameterBuilder_ == null) {
        wVarParameterBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.OperationDWVR, openfmb.commonmodule.OperationDWVR.Builder, openfmb.commonmodule.OperationDWVROrBuilder>(
                getWVarParameter(),
                getParentForChildren(),
                isClean());
        wVarParameter_ = null;
      }
      return wVarParameterBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:commonmodule.WVarCSG)
  }

  // @@protoc_insertion_point(class_scope:commonmodule.WVarCSG)
  private static final openfmb.commonmodule.WVarCSG DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.commonmodule.WVarCSG();
  }

  public static openfmb.commonmodule.WVarCSG getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<WVarCSG>
      PARSER = new com.google.protobuf.AbstractParser<WVarCSG>() {
    @java.lang.Override
    public WVarCSG parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<WVarCSG> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<WVarCSG> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.commonmodule.WVarCSG getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

