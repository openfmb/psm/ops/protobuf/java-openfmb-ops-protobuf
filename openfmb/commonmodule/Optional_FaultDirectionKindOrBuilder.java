// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

public interface Optional_FaultDirectionKindOrBuilder extends
    // @@protoc_insertion_point(interface_extends:commonmodule.Optional_FaultDirectionKind)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>.commonmodule.FaultDirectionKind value = 1;</code>
   * @return The enum numeric value on the wire for value.
   */
  int getValueValue();
  /**
   * <code>.commonmodule.FaultDirectionKind value = 1;</code>
   * @return The value.
   */
  openfmb.commonmodule.FaultDirectionKind getValue();
}
