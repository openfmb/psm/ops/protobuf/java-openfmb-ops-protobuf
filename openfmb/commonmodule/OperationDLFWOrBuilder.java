// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

public interface OperationDLFWOrBuilder extends
    // @@protoc_insertion_point(interface_extends:commonmodule.OperationDLFW)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>bool modEna = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The modEna.
   */
  boolean getModEna();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ClearingTime OplTmmsMax = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the oplTmmsMax field is set.
   */
  boolean hasOplTmmsMax();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ClearingTime OplTmmsMax = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The oplTmmsMax.
   */
  openfmb.commonmodule.ClearingTime getOplTmmsMax();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ClearingTime OplTmmsMax = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.commonmodule.ClearingTimeOrBuilder getOplTmmsMaxOrBuilder();
}
