// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

/**
 * <pre>
 * Unit definition (Unit)
 * </pre>
 *
 * Protobuf type {@code commonmodule.Unit}
 */
public final class Unit extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:commonmodule.Unit)
    UnitOrBuilder {
private static final long serialVersionUID = 0L;
  // Use Unit.newBuilder() to construct.
  private Unit(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private Unit() {
    sIUnit_ = 0;
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new Unit();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_Unit_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_Unit_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.commonmodule.Unit.class, openfmb.commonmodule.Unit.Builder.class);
  }

  public static final int MULTIPLIER_FIELD_NUMBER = 1;
  private openfmb.commonmodule.Optional_UnitMultiplierKind multiplier_;
  /**
   * <pre>
   * (default='') Unit multiplier.
   * </pre>
   *
   * <code>.commonmodule.Optional_UnitMultiplierKind multiplier = 1;</code>
   * @return Whether the multiplier field is set.
   */
  @java.lang.Override
  public boolean hasMultiplier() {
    return multiplier_ != null;
  }
  /**
   * <pre>
   * (default='') Unit multiplier.
   * </pre>
   *
   * <code>.commonmodule.Optional_UnitMultiplierKind multiplier = 1;</code>
   * @return The multiplier.
   */
  @java.lang.Override
  public openfmb.commonmodule.Optional_UnitMultiplierKind getMultiplier() {
    return multiplier_ == null ? openfmb.commonmodule.Optional_UnitMultiplierKind.getDefaultInstance() : multiplier_;
  }
  /**
   * <pre>
   * (default='') Unit multiplier.
   * </pre>
   *
   * <code>.commonmodule.Optional_UnitMultiplierKind multiplier = 1;</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.Optional_UnitMultiplierKindOrBuilder getMultiplierOrBuilder() {
    return multiplier_ == null ? openfmb.commonmodule.Optional_UnitMultiplierKind.getDefaultInstance() : multiplier_;
  }

  public static final int SIUNIT_FIELD_NUMBER = 2;
  private int sIUnit_ = 0;
  /**
   * <pre>
   * SI unit of measure.
   * </pre>
   *
   * <code>.commonmodule.UnitSymbolKind SIUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The enum numeric value on the wire for sIUnit.
   */
  @java.lang.Override public int getSIUnitValue() {
    return sIUnit_;
  }
  /**
   * <pre>
   * SI unit of measure.
   * </pre>
   *
   * <code>.commonmodule.UnitSymbolKind SIUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The sIUnit.
   */
  @java.lang.Override public openfmb.commonmodule.UnitSymbolKind getSIUnit() {
    openfmb.commonmodule.UnitSymbolKind result = openfmb.commonmodule.UnitSymbolKind.forNumber(sIUnit_);
    return result == null ? openfmb.commonmodule.UnitSymbolKind.UNRECOGNIZED : result;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (multiplier_ != null) {
      output.writeMessage(1, getMultiplier());
    }
    if (sIUnit_ != openfmb.commonmodule.UnitSymbolKind.UnitSymbolKind_none.getNumber()) {
      output.writeEnum(2, sIUnit_);
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (multiplier_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getMultiplier());
    }
    if (sIUnit_ != openfmb.commonmodule.UnitSymbolKind.UnitSymbolKind_none.getNumber()) {
      size += com.google.protobuf.CodedOutputStream
        .computeEnumSize(2, sIUnit_);
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.commonmodule.Unit)) {
      return super.equals(obj);
    }
    openfmb.commonmodule.Unit other = (openfmb.commonmodule.Unit) obj;

    if (hasMultiplier() != other.hasMultiplier()) return false;
    if (hasMultiplier()) {
      if (!getMultiplier()
          .equals(other.getMultiplier())) return false;
    }
    if (sIUnit_ != other.sIUnit_) return false;
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasMultiplier()) {
      hash = (37 * hash) + MULTIPLIER_FIELD_NUMBER;
      hash = (53 * hash) + getMultiplier().hashCode();
    }
    hash = (37 * hash) + SIUNIT_FIELD_NUMBER;
    hash = (53 * hash) + sIUnit_;
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.commonmodule.Unit parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.Unit parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.Unit parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.Unit parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.Unit parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.Unit parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.Unit parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.Unit parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.commonmodule.Unit parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.Unit parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.commonmodule.Unit parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.Unit parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.commonmodule.Unit prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Unit definition (Unit)
   * </pre>
   *
   * Protobuf type {@code commonmodule.Unit}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:commonmodule.Unit)
      openfmb.commonmodule.UnitOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_Unit_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_Unit_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.commonmodule.Unit.class, openfmb.commonmodule.Unit.Builder.class);
    }

    // Construct using openfmb.commonmodule.Unit.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      multiplier_ = null;
      if (multiplierBuilder_ != null) {
        multiplierBuilder_.dispose();
        multiplierBuilder_ = null;
      }
      sIUnit_ = 0;
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_Unit_descriptor;
    }

    @java.lang.Override
    public openfmb.commonmodule.Unit getDefaultInstanceForType() {
      return openfmb.commonmodule.Unit.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.commonmodule.Unit build() {
      openfmb.commonmodule.Unit result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.commonmodule.Unit buildPartial() {
      openfmb.commonmodule.Unit result = new openfmb.commonmodule.Unit(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.commonmodule.Unit result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.multiplier_ = multiplierBuilder_ == null
            ? multiplier_
            : multiplierBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.sIUnit_ = sIUnit_;
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.commonmodule.Unit) {
        return mergeFrom((openfmb.commonmodule.Unit)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.commonmodule.Unit other) {
      if (other == openfmb.commonmodule.Unit.getDefaultInstance()) return this;
      if (other.hasMultiplier()) {
        mergeMultiplier(other.getMultiplier());
      }
      if (other.sIUnit_ != 0) {
        setSIUnitValue(other.getSIUnitValue());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getMultiplierFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 16: {
              sIUnit_ = input.readEnum();
              bitField0_ |= 0x00000002;
              break;
            } // case 16
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.Optional_UnitMultiplierKind multiplier_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.Optional_UnitMultiplierKind, openfmb.commonmodule.Optional_UnitMultiplierKind.Builder, openfmb.commonmodule.Optional_UnitMultiplierKindOrBuilder> multiplierBuilder_;
    /**
     * <pre>
     * (default='') Unit multiplier.
     * </pre>
     *
     * <code>.commonmodule.Optional_UnitMultiplierKind multiplier = 1;</code>
     * @return Whether the multiplier field is set.
     */
    public boolean hasMultiplier() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * (default='') Unit multiplier.
     * </pre>
     *
     * <code>.commonmodule.Optional_UnitMultiplierKind multiplier = 1;</code>
     * @return The multiplier.
     */
    public openfmb.commonmodule.Optional_UnitMultiplierKind getMultiplier() {
      if (multiplierBuilder_ == null) {
        return multiplier_ == null ? openfmb.commonmodule.Optional_UnitMultiplierKind.getDefaultInstance() : multiplier_;
      } else {
        return multiplierBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * (default='') Unit multiplier.
     * </pre>
     *
     * <code>.commonmodule.Optional_UnitMultiplierKind multiplier = 1;</code>
     */
    public Builder setMultiplier(openfmb.commonmodule.Optional_UnitMultiplierKind value) {
      if (multiplierBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        multiplier_ = value;
      } else {
        multiplierBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * (default='') Unit multiplier.
     * </pre>
     *
     * <code>.commonmodule.Optional_UnitMultiplierKind multiplier = 1;</code>
     */
    public Builder setMultiplier(
        openfmb.commonmodule.Optional_UnitMultiplierKind.Builder builderForValue) {
      if (multiplierBuilder_ == null) {
        multiplier_ = builderForValue.build();
      } else {
        multiplierBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * (default='') Unit multiplier.
     * </pre>
     *
     * <code>.commonmodule.Optional_UnitMultiplierKind multiplier = 1;</code>
     */
    public Builder mergeMultiplier(openfmb.commonmodule.Optional_UnitMultiplierKind value) {
      if (multiplierBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          multiplier_ != null &&
          multiplier_ != openfmb.commonmodule.Optional_UnitMultiplierKind.getDefaultInstance()) {
          getMultiplierBuilder().mergeFrom(value);
        } else {
          multiplier_ = value;
        }
      } else {
        multiplierBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * (default='') Unit multiplier.
     * </pre>
     *
     * <code>.commonmodule.Optional_UnitMultiplierKind multiplier = 1;</code>
     */
    public Builder clearMultiplier() {
      bitField0_ = (bitField0_ & ~0x00000001);
      multiplier_ = null;
      if (multiplierBuilder_ != null) {
        multiplierBuilder_.dispose();
        multiplierBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * (default='') Unit multiplier.
     * </pre>
     *
     * <code>.commonmodule.Optional_UnitMultiplierKind multiplier = 1;</code>
     */
    public openfmb.commonmodule.Optional_UnitMultiplierKind.Builder getMultiplierBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getMultiplierFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * (default='') Unit multiplier.
     * </pre>
     *
     * <code>.commonmodule.Optional_UnitMultiplierKind multiplier = 1;</code>
     */
    public openfmb.commonmodule.Optional_UnitMultiplierKindOrBuilder getMultiplierOrBuilder() {
      if (multiplierBuilder_ != null) {
        return multiplierBuilder_.getMessageOrBuilder();
      } else {
        return multiplier_ == null ?
            openfmb.commonmodule.Optional_UnitMultiplierKind.getDefaultInstance() : multiplier_;
      }
    }
    /**
     * <pre>
     * (default='') Unit multiplier.
     * </pre>
     *
     * <code>.commonmodule.Optional_UnitMultiplierKind multiplier = 1;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.Optional_UnitMultiplierKind, openfmb.commonmodule.Optional_UnitMultiplierKind.Builder, openfmb.commonmodule.Optional_UnitMultiplierKindOrBuilder> 
        getMultiplierFieldBuilder() {
      if (multiplierBuilder_ == null) {
        multiplierBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.Optional_UnitMultiplierKind, openfmb.commonmodule.Optional_UnitMultiplierKind.Builder, openfmb.commonmodule.Optional_UnitMultiplierKindOrBuilder>(
                getMultiplier(),
                getParentForChildren(),
                isClean());
        multiplier_ = null;
      }
      return multiplierBuilder_;
    }

    private int sIUnit_ = 0;
    /**
     * <pre>
     * SI unit of measure.
     * </pre>
     *
     * <code>.commonmodule.UnitSymbolKind SIUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The enum numeric value on the wire for sIUnit.
     */
    @java.lang.Override public int getSIUnitValue() {
      return sIUnit_;
    }
    /**
     * <pre>
     * SI unit of measure.
     * </pre>
     *
     * <code>.commonmodule.UnitSymbolKind SIUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @param value The enum numeric value on the wire for sIUnit to set.
     * @return This builder for chaining.
     */
    public Builder setSIUnitValue(int value) {
      sIUnit_ = value;
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * SI unit of measure.
     * </pre>
     *
     * <code>.commonmodule.UnitSymbolKind SIUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The sIUnit.
     */
    @java.lang.Override
    public openfmb.commonmodule.UnitSymbolKind getSIUnit() {
      openfmb.commonmodule.UnitSymbolKind result = openfmb.commonmodule.UnitSymbolKind.forNumber(sIUnit_);
      return result == null ? openfmb.commonmodule.UnitSymbolKind.UNRECOGNIZED : result;
    }
    /**
     * <pre>
     * SI unit of measure.
     * </pre>
     *
     * <code>.commonmodule.UnitSymbolKind SIUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @param value The sIUnit to set.
     * @return This builder for chaining.
     */
    public Builder setSIUnit(openfmb.commonmodule.UnitSymbolKind value) {
      if (value == null) {
        throw new NullPointerException();
      }
      bitField0_ |= 0x00000002;
      sIUnit_ = value.getNumber();
      onChanged();
      return this;
    }
    /**
     * <pre>
     * SI unit of measure.
     * </pre>
     *
     * <code>.commonmodule.UnitSymbolKind SIUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return This builder for chaining.
     */
    public Builder clearSIUnit() {
      bitField0_ = (bitField0_ & ~0x00000002);
      sIUnit_ = 0;
      onChanged();
      return this;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:commonmodule.Unit)
  }

  // @@protoc_insertion_point(class_scope:commonmodule.Unit)
  private static final openfmb.commonmodule.Unit DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.commonmodule.Unit();
  }

  public static openfmb.commonmodule.Unit getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<Unit>
      PARSER = new com.google.protobuf.AbstractParser<Unit>() {
    @java.lang.Override
    public Unit parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<Unit> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<Unit> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.commonmodule.Unit getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

