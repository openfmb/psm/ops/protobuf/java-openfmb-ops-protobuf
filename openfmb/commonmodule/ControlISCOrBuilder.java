// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

public interface ControlISCOrBuilder extends
    // @@protoc_insertion_point(interface_extends:commonmodule.ControlISC)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * Service parameter that determines the control activity.
   * </pre>
   *
   * <code>int32 ctlVal = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The ctlVal.
   */
  int getCtlVal();
}
