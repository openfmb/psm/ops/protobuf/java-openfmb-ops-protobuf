// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

/**
 * Protobuf type {@code commonmodule.Optional_TimeAccuracyKind}
 */
public final class Optional_TimeAccuracyKind extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:commonmodule.Optional_TimeAccuracyKind)
    Optional_TimeAccuracyKindOrBuilder {
private static final long serialVersionUID = 0L;
  // Use Optional_TimeAccuracyKind.newBuilder() to construct.
  private Optional_TimeAccuracyKind(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private Optional_TimeAccuracyKind() {
    value_ = 0;
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new Optional_TimeAccuracyKind();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_Optional_TimeAccuracyKind_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_Optional_TimeAccuracyKind_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.commonmodule.Optional_TimeAccuracyKind.class, openfmb.commonmodule.Optional_TimeAccuracyKind.Builder.class);
  }

  public static final int VALUE_FIELD_NUMBER = 1;
  private int value_ = 0;
  /**
   * <code>.commonmodule.TimeAccuracyKind value = 1;</code>
   * @return The enum numeric value on the wire for value.
   */
  @java.lang.Override public int getValueValue() {
    return value_;
  }
  /**
   * <code>.commonmodule.TimeAccuracyKind value = 1;</code>
   * @return The value.
   */
  @java.lang.Override public openfmb.commonmodule.TimeAccuracyKind getValue() {
    openfmb.commonmodule.TimeAccuracyKind result = openfmb.commonmodule.TimeAccuracyKind.forNumber(value_);
    return result == null ? openfmb.commonmodule.TimeAccuracyKind.UNRECOGNIZED : result;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (value_ != openfmb.commonmodule.TimeAccuracyKind.TimeAccuracyKind_UNDEFINED.getNumber()) {
      output.writeEnum(1, value_);
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (value_ != openfmb.commonmodule.TimeAccuracyKind.TimeAccuracyKind_UNDEFINED.getNumber()) {
      size += com.google.protobuf.CodedOutputStream
        .computeEnumSize(1, value_);
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.commonmodule.Optional_TimeAccuracyKind)) {
      return super.equals(obj);
    }
    openfmb.commonmodule.Optional_TimeAccuracyKind other = (openfmb.commonmodule.Optional_TimeAccuracyKind) obj;

    if (value_ != other.value_) return false;
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + VALUE_FIELD_NUMBER;
    hash = (53 * hash) + value_;
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.commonmodule.Optional_TimeAccuracyKind parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.Optional_TimeAccuracyKind parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.Optional_TimeAccuracyKind parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.Optional_TimeAccuracyKind parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.Optional_TimeAccuracyKind parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.commonmodule.Optional_TimeAccuracyKind parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.commonmodule.Optional_TimeAccuracyKind parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.Optional_TimeAccuracyKind parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.commonmodule.Optional_TimeAccuracyKind parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.Optional_TimeAccuracyKind parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.commonmodule.Optional_TimeAccuracyKind parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.commonmodule.Optional_TimeAccuracyKind parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.commonmodule.Optional_TimeAccuracyKind prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code commonmodule.Optional_TimeAccuracyKind}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:commonmodule.Optional_TimeAccuracyKind)
      openfmb.commonmodule.Optional_TimeAccuracyKindOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_Optional_TimeAccuracyKind_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_Optional_TimeAccuracyKind_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.commonmodule.Optional_TimeAccuracyKind.class, openfmb.commonmodule.Optional_TimeAccuracyKind.Builder.class);
    }

    // Construct using openfmb.commonmodule.Optional_TimeAccuracyKind.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      value_ = 0;
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.commonmodule.Commonmodule.internal_static_commonmodule_Optional_TimeAccuracyKind_descriptor;
    }

    @java.lang.Override
    public openfmb.commonmodule.Optional_TimeAccuracyKind getDefaultInstanceForType() {
      return openfmb.commonmodule.Optional_TimeAccuracyKind.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.commonmodule.Optional_TimeAccuracyKind build() {
      openfmb.commonmodule.Optional_TimeAccuracyKind result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.commonmodule.Optional_TimeAccuracyKind buildPartial() {
      openfmb.commonmodule.Optional_TimeAccuracyKind result = new openfmb.commonmodule.Optional_TimeAccuracyKind(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.commonmodule.Optional_TimeAccuracyKind result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.value_ = value_;
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.commonmodule.Optional_TimeAccuracyKind) {
        return mergeFrom((openfmb.commonmodule.Optional_TimeAccuracyKind)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.commonmodule.Optional_TimeAccuracyKind other) {
      if (other == openfmb.commonmodule.Optional_TimeAccuracyKind.getDefaultInstance()) return this;
      if (other.value_ != 0) {
        setValueValue(other.getValueValue());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 8: {
              value_ = input.readEnum();
              bitField0_ |= 0x00000001;
              break;
            } // case 8
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private int value_ = 0;
    /**
     * <code>.commonmodule.TimeAccuracyKind value = 1;</code>
     * @return The enum numeric value on the wire for value.
     */
    @java.lang.Override public int getValueValue() {
      return value_;
    }
    /**
     * <code>.commonmodule.TimeAccuracyKind value = 1;</code>
     * @param value The enum numeric value on the wire for value to set.
     * @return This builder for chaining.
     */
    public Builder setValueValue(int value) {
      value_ = value;
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <code>.commonmodule.TimeAccuracyKind value = 1;</code>
     * @return The value.
     */
    @java.lang.Override
    public openfmb.commonmodule.TimeAccuracyKind getValue() {
      openfmb.commonmodule.TimeAccuracyKind result = openfmb.commonmodule.TimeAccuracyKind.forNumber(value_);
      return result == null ? openfmb.commonmodule.TimeAccuracyKind.UNRECOGNIZED : result;
    }
    /**
     * <code>.commonmodule.TimeAccuracyKind value = 1;</code>
     * @param value The value to set.
     * @return This builder for chaining.
     */
    public Builder setValue(openfmb.commonmodule.TimeAccuracyKind value) {
      if (value == null) {
        throw new NullPointerException();
      }
      bitField0_ |= 0x00000001;
      value_ = value.getNumber();
      onChanged();
      return this;
    }
    /**
     * <code>.commonmodule.TimeAccuracyKind value = 1;</code>
     * @return This builder for chaining.
     */
    public Builder clearValue() {
      bitField0_ = (bitField0_ & ~0x00000001);
      value_ = 0;
      onChanged();
      return this;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:commonmodule.Optional_TimeAccuracyKind)
  }

  // @@protoc_insertion_point(class_scope:commonmodule.Optional_TimeAccuracyKind)
  private static final openfmb.commonmodule.Optional_TimeAccuracyKind DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.commonmodule.Optional_TimeAccuracyKind();
  }

  public static openfmb.commonmodule.Optional_TimeAccuracyKind getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<Optional_TimeAccuracyKind>
      PARSER = new com.google.protobuf.AbstractParser<Optional_TimeAccuracyKind>() {
    @java.lang.Override
    public Optional_TimeAccuracyKind parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<Optional_TimeAccuracyKind> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<Optional_TimeAccuracyKind> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.commonmodule.Optional_TimeAccuracyKind getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

