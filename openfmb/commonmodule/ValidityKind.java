// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

/**
 * <pre>
 * Validity of the value, as condensed information for the client. In case this value is not
 * 'good', some reasons may be found in the 'detailQual'.
 * </pre>
 *
 * Protobuf enum {@code commonmodule.ValidityKind}
 */
public enum ValidityKind
    implements com.google.protobuf.ProtocolMessageEnum {
  /**
   * <pre>
   * Undefined
   * </pre>
   *
   * <code>ValidityKind_UNDEFINED = 0;</code>
   */
  ValidityKind_UNDEFINED(0),
  /**
   * <pre>
   * Supervision function has detected no abnormal condition of either the acquisition function or
   * the information source.
   * </pre>
   *
   * <code>ValidityKind_good = 1;</code>
   */
  ValidityKind_good(1),
  /**
   * <pre>
   * Supervision function has detected an abnormal condition of the acquisition function or the
   * information source (missing or non-operating updating devices). The value is not defined under this
   * condition. It shall be used to indicate to the client that the value may be incorrect and shall not
   * be used.  EXAMPLE If an input unit detects an oscillation of one input it will mark the related
   * information as invalid.
   * </pre>
   *
   * <code>ValidityKind_invalid = 2;</code>
   */
  ValidityKind_invalid(2),
  /**
   * <pre>
   * Reserved
   * </pre>
   *
   * <code>ValidityKind_reserved = 3;</code>
   */
  ValidityKind_reserved(3),
  /**
   * <pre>
   * Supervision function has detected any abnormal behaviour. However, the value could still be
   * valid. It is client's responsibility to determine whether the values should be used.
   * </pre>
   *
   * <code>ValidityKind_questionable = 4;</code>
   */
  ValidityKind_questionable(4),
  UNRECOGNIZED(-1),
  ;

  /**
   * <pre>
   * Undefined
   * </pre>
   *
   * <code>ValidityKind_UNDEFINED = 0;</code>
   */
  public static final int ValidityKind_UNDEFINED_VALUE = 0;
  /**
   * <pre>
   * Supervision function has detected no abnormal condition of either the acquisition function or
   * the information source.
   * </pre>
   *
   * <code>ValidityKind_good = 1;</code>
   */
  public static final int ValidityKind_good_VALUE = 1;
  /**
   * <pre>
   * Supervision function has detected an abnormal condition of the acquisition function or the
   * information source (missing or non-operating updating devices). The value is not defined under this
   * condition. It shall be used to indicate to the client that the value may be incorrect and shall not
   * be used.  EXAMPLE If an input unit detects an oscillation of one input it will mark the related
   * information as invalid.
   * </pre>
   *
   * <code>ValidityKind_invalid = 2;</code>
   */
  public static final int ValidityKind_invalid_VALUE = 2;
  /**
   * <pre>
   * Reserved
   * </pre>
   *
   * <code>ValidityKind_reserved = 3;</code>
   */
  public static final int ValidityKind_reserved_VALUE = 3;
  /**
   * <pre>
   * Supervision function has detected any abnormal behaviour. However, the value could still be
   * valid. It is client's responsibility to determine whether the values should be used.
   * </pre>
   *
   * <code>ValidityKind_questionable = 4;</code>
   */
  public static final int ValidityKind_questionable_VALUE = 4;


  public final int getNumber() {
    if (this == UNRECOGNIZED) {
      throw new java.lang.IllegalArgumentException(
          "Can't get the number of an unknown enum value.");
    }
    return value;
  }

  /**
   * @param value The numeric wire value of the corresponding enum entry.
   * @return The enum associated with the given numeric wire value.
   * @deprecated Use {@link #forNumber(int)} instead.
   */
  @java.lang.Deprecated
  public static ValidityKind valueOf(int value) {
    return forNumber(value);
  }

  /**
   * @param value The numeric wire value of the corresponding enum entry.
   * @return The enum associated with the given numeric wire value.
   */
  public static ValidityKind forNumber(int value) {
    switch (value) {
      case 0: return ValidityKind_UNDEFINED;
      case 1: return ValidityKind_good;
      case 2: return ValidityKind_invalid;
      case 3: return ValidityKind_reserved;
      case 4: return ValidityKind_questionable;
      default: return null;
    }
  }

  public static com.google.protobuf.Internal.EnumLiteMap<ValidityKind>
      internalGetValueMap() {
    return internalValueMap;
  }
  private static final com.google.protobuf.Internal.EnumLiteMap<
      ValidityKind> internalValueMap =
        new com.google.protobuf.Internal.EnumLiteMap<ValidityKind>() {
          public ValidityKind findValueByNumber(int number) {
            return ValidityKind.forNumber(number);
          }
        };

  public final com.google.protobuf.Descriptors.EnumValueDescriptor
      getValueDescriptor() {
    if (this == UNRECOGNIZED) {
      throw new java.lang.IllegalStateException(
          "Can't get the descriptor of an unrecognized enum value.");
    }
    return getDescriptor().getValues().get(ordinal());
  }
  public final com.google.protobuf.Descriptors.EnumDescriptor
      getDescriptorForType() {
    return getDescriptor();
  }
  public static final com.google.protobuf.Descriptors.EnumDescriptor
      getDescriptor() {
    return openfmb.commonmodule.Commonmodule.getDescriptor().getEnumTypes().get(5);
  }

  private static final ValidityKind[] VALUES = values();

  public static ValidityKind valueOf(
      com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
    if (desc.getType() != getDescriptor()) {
      throw new java.lang.IllegalArgumentException(
        "EnumValueDescriptor is not for this type.");
    }
    if (desc.getIndex() == -1) {
      return UNRECOGNIZED;
    }
    return VALUES[desc.getIndex()];
  }

  private final int value;

  private ValidityKind(int value) {
    this.value = value;
  }

  // @@protoc_insertion_point(enum_scope:commonmodule.ValidityKind)
}

