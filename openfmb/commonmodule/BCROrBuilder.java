// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

public interface BCROrBuilder extends
    // @@protoc_insertion_point(interface_extends:commonmodule.BCR)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * Binary counter status represented as an integer value; wraps to 0 at the maximum or minimum
   * value of INT64.
   * </pre>
   *
   * <code>int64 actVal = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The actVal.
   */
  long getActVal();

  /**
   * <pre>
   * Quality of the values in 'actVal', 'frVal'.
   * </pre>
   *
   * <code>.commonmodule.Quality q = 2;</code>
   * @return Whether the q field is set.
   */
  boolean hasQ();
  /**
   * <pre>
   * Quality of the values in 'actVal', 'frVal'.
   * </pre>
   *
   * <code>.commonmodule.Quality q = 2;</code>
   * @return The q.
   */
  openfmb.commonmodule.Quality getQ();
  /**
   * <pre>
   * Quality of the values in 'actVal', 'frVal'.
   * </pre>
   *
   * <code>.commonmodule.Quality q = 2;</code>
   */
  openfmb.commonmodule.QualityOrBuilder getQOrBuilder();

  /**
   * <pre>
   * Timestamp of the last change of value in 'actVal' or 'q'.
   * </pre>
   *
   * <code>.commonmodule.Timestamp t = 3;</code>
   * @return Whether the t field is set.
   */
  boolean hasT();
  /**
   * <pre>
   * Timestamp of the last change of value in 'actVal' or 'q'.
   * </pre>
   *
   * <code>.commonmodule.Timestamp t = 3;</code>
   * @return The t.
   */
  openfmb.commonmodule.Timestamp getT();
  /**
   * <pre>
   * Timestamp of the last change of value in 'actVal' or 'q'.
   * </pre>
   *
   * <code>.commonmodule.Timestamp t = 3;</code>
   */
  openfmb.commonmodule.TimestampOrBuilder getTOrBuilder();
}
