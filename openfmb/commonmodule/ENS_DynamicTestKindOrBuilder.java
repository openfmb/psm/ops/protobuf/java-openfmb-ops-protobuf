// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

public interface ENS_DynamicTestKindOrBuilder extends
    // @@protoc_insertion_point(interface_extends:commonmodule.ENS_DynamicTestKind)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * Quality of the value in 'stVal'.
   * </pre>
   *
   * <code>.commonmodule.Quality q = 1;</code>
   * @return Whether the q field is set.
   */
  boolean hasQ();
  /**
   * <pre>
   * Quality of the value in 'stVal'.
   * </pre>
   *
   * <code>.commonmodule.Quality q = 1;</code>
   * @return The q.
   */
  openfmb.commonmodule.Quality getQ();
  /**
   * <pre>
   * Quality of the value in 'stVal'.
   * </pre>
   *
   * <code>.commonmodule.Quality q = 1;</code>
   */
  openfmb.commonmodule.QualityOrBuilder getQOrBuilder();

  /**
   * <pre>
   * Value of the data.
   * </pre>
   *
   * <code>.commonmodule.DynamicTestKind stVal = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The enum numeric value on the wire for stVal.
   */
  int getStValValue();
  /**
   * <pre>
   * Value of the data.
   * </pre>
   *
   * <code>.commonmodule.DynamicTestKind stVal = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The stVal.
   */
  openfmb.commonmodule.DynamicTestKind getStVal();

  /**
   * <pre>
   * Timestamp of the last change or update event of 'stVal' or the last change of value in 'q'.
   * </pre>
   *
   * <code>.commonmodule.Timestamp t = 3;</code>
   * @return Whether the t field is set.
   */
  boolean hasT();
  /**
   * <pre>
   * Timestamp of the last change or update event of 'stVal' or the last change of value in 'q'.
   * </pre>
   *
   * <code>.commonmodule.Timestamp t = 3;</code>
   * @return The t.
   */
  openfmb.commonmodule.Timestamp getT();
  /**
   * <pre>
   * Timestamp of the last change or update event of 'stVal' or the last change of value in 'q'.
   * </pre>
   *
   * <code>.commonmodule.Timestamp t = 3;</code>
   */
  openfmb.commonmodule.TimestampOrBuilder getTOrBuilder();
}
