// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

public interface ENS_HealthKindOrBuilder extends
    // @@protoc_insertion_point(interface_extends:commonmodule.ENS_HealthKind)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * Textual description of the data. In case it is used within the CDC LPL, the description refers
   * to the logical node.
   * </pre>
   *
   * <code>.google.protobuf.StringValue d = 1;</code>
   * @return Whether the d field is set.
   */
  boolean hasD();
  /**
   * <pre>
   * Textual description of the data. In case it is used within the CDC LPL, the description refers
   * to the logical node.
   * </pre>
   *
   * <code>.google.protobuf.StringValue d = 1;</code>
   * @return The d.
   */
  com.google.protobuf.StringValue getD();
  /**
   * <pre>
   * Textual description of the data. In case it is used within the CDC LPL, the description refers
   * to the logical node.
   * </pre>
   *
   * <code>.google.protobuf.StringValue d = 1;</code>
   */
  com.google.protobuf.StringValueOrBuilder getDOrBuilder();

  /**
   * <pre>
   * Value of the data.
   * </pre>
   *
   * <code>.commonmodule.HealthKind stVal = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The enum numeric value on the wire for stVal.
   */
  int getStValValue();
  /**
   * <pre>
   * Value of the data.
   * </pre>
   *
   * <code>.commonmodule.HealthKind stVal = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The stVal.
   */
  openfmb.commonmodule.HealthKind getStVal();
}
