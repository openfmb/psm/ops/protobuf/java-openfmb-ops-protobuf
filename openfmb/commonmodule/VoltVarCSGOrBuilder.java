// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

public interface VoltVarCSGOrBuilder extends
    // @@protoc_insertion_point(interface_extends:commonmodule.VoltVarCSG)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .commonmodule.VoltVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  java.util.List<openfmb.commonmodule.VoltVarPoint> 
      getCrvPtsList();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .commonmodule.VoltVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.commonmodule.VoltVarPoint getCrvPts(int index);
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .commonmodule.VoltVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  int getCrvPtsCount();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .commonmodule.VoltVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  java.util.List<? extends openfmb.commonmodule.VoltVarPointOrBuilder> 
      getCrvPtsOrBuilderList();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>repeated .commonmodule.VoltVarPoint crvPts = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.commonmodule.VoltVarPointOrBuilder getCrvPtsOrBuilder(
      int index);

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.OperationDVVR vVarParameter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the vVarParameter field is set.
   */
  boolean hasVVarParameter();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.OperationDVVR vVarParameter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The vVarParameter.
   */
  openfmb.commonmodule.OperationDVVR getVVarParameter();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.OperationDVVR vVarParameter = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.commonmodule.OperationDVVROrBuilder getVVarParameterOrBuilder();
}
