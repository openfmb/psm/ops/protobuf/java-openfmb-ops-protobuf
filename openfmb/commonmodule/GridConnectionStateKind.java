// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

/**
 * <pre>
 * State kind
 * </pre>
 *
 * Protobuf enum {@code commonmodule.GridConnectionStateKind}
 */
public enum GridConnectionStateKind
    implements com.google.protobuf.ProtocolMessageEnum {
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>GridConnectionStateKind_disconnected = 0;</code>
   */
  GridConnectionStateKind_disconnected(0),
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>GridConnectionStateKind_connected = 1;</code>
   */
  GridConnectionStateKind_connected(1),
  UNRECOGNIZED(-1),
  ;

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>GridConnectionStateKind_disconnected = 0;</code>
   */
  public static final int GridConnectionStateKind_disconnected_VALUE = 0;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>GridConnectionStateKind_connected = 1;</code>
   */
  public static final int GridConnectionStateKind_connected_VALUE = 1;


  public final int getNumber() {
    if (this == UNRECOGNIZED) {
      throw new java.lang.IllegalArgumentException(
          "Can't get the number of an unknown enum value.");
    }
    return value;
  }

  /**
   * @param value The numeric wire value of the corresponding enum entry.
   * @return The enum associated with the given numeric wire value.
   * @deprecated Use {@link #forNumber(int)} instead.
   */
  @java.lang.Deprecated
  public static GridConnectionStateKind valueOf(int value) {
    return forNumber(value);
  }

  /**
   * @param value The numeric wire value of the corresponding enum entry.
   * @return The enum associated with the given numeric wire value.
   */
  public static GridConnectionStateKind forNumber(int value) {
    switch (value) {
      case 0: return GridConnectionStateKind_disconnected;
      case 1: return GridConnectionStateKind_connected;
      default: return null;
    }
  }

  public static com.google.protobuf.Internal.EnumLiteMap<GridConnectionStateKind>
      internalGetValueMap() {
    return internalValueMap;
  }
  private static final com.google.protobuf.Internal.EnumLiteMap<
      GridConnectionStateKind> internalValueMap =
        new com.google.protobuf.Internal.EnumLiteMap<GridConnectionStateKind>() {
          public GridConnectionStateKind findValueByNumber(int number) {
            return GridConnectionStateKind.forNumber(number);
          }
        };

  public final com.google.protobuf.Descriptors.EnumValueDescriptor
      getValueDescriptor() {
    if (this == UNRECOGNIZED) {
      throw new java.lang.IllegalStateException(
          "Can't get the descriptor of an unrecognized enum value.");
    }
    return getDescriptor().getValues().get(ordinal());
  }
  public final com.google.protobuf.Descriptors.EnumDescriptor
      getDescriptorForType() {
    return getDescriptor();
  }
  public static final com.google.protobuf.Descriptors.EnumDescriptor
      getDescriptor() {
    return openfmb.commonmodule.Commonmodule.getDescriptor().getEnumTypes().get(24);
  }

  private static final GridConnectionStateKind[] VALUES = values();

  public static GridConnectionStateKind valueOf(
      com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
    if (desc.getType() != getDescriptor()) {
      throw new java.lang.IllegalArgumentException(
        "EnumValueDescriptor is not for this type.");
    }
    if (desc.getIndex() == -1) {
      return UNRECOGNIZED;
    }
    return VALUES[desc.getIndex()];
  }

  private final int value;

  private GridConnectionStateKind(int value) {
    this.value = value;
  }

  // @@protoc_insertion_point(enum_scope:commonmodule.GridConnectionStateKind)
}

