// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: commonmodule/commonmodule.proto

package openfmb.commonmodule;

public interface StatusDPSOrBuilder extends
    // @@protoc_insertion_point(interface_extends:commonmodule.StatusDPS)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.Quality q = 1;</code>
   * @return Whether the q field is set.
   */
  boolean hasQ();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.Quality q = 1;</code>
   * @return The q.
   */
  openfmb.commonmodule.Quality getQ();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.Quality q = 1;</code>
   */
  openfmb.commonmodule.QualityOrBuilder getQOrBuilder();

  /**
   * <pre>
   * Status value of the controllable data object.
   * </pre>
   *
   * <code>.commonmodule.DbPosKind stVal = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The enum numeric value on the wire for stVal.
   */
  int getStValValue();
  /**
   * <pre>
   * Status value of the controllable data object.
   * </pre>
   *
   * <code>.commonmodule.DbPosKind stVal = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The stVal.
   */
  openfmb.commonmodule.DbPosKind getStVal();

  /**
   * <pre>
   * Timestamp of the last change of the value in any of 'stVal' or 'q'.
   * </pre>
   *
   * <code>.commonmodule.Timestamp t = 3;</code>
   * @return Whether the t field is set.
   */
  boolean hasT();
  /**
   * <pre>
   * Timestamp of the last change of the value in any of 'stVal' or 'q'.
   * </pre>
   *
   * <code>.commonmodule.Timestamp t = 3;</code>
   * @return The t.
   */
  openfmb.commonmodule.Timestamp getT();
  /**
   * <pre>
   * Timestamp of the last change of the value in any of 'stVal' or 'q'.
   * </pre>
   *
   * <code>.commonmodule.Timestamp t = 3;</code>
   */
  openfmb.commonmodule.TimestampOrBuilder getTOrBuilder();
}
