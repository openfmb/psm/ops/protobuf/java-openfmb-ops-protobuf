// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: generationmodule/generationmodule.proto

package openfmb.generationmodule;

public interface ReactivePowerControlOrBuilder extends
    // @@protoc_insertion_point(interface_extends:generationmodule.ReactivePowerControl)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.generationmodule.DroopParameter droopSetpoint = 1;</code>
   * @return Whether the droopSetpoint field is set.
   */
  boolean hasDroopSetpoint();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.generationmodule.DroopParameter droopSetpoint = 1;</code>
   * @return The droopSetpoint.
   */
  openfmb.generationmodule.DroopParameter getDroopSetpoint();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.generationmodule.DroopParameter droopSetpoint = 1;</code>
   */
  openfmb.generationmodule.DroopParameterOrBuilder getDroopSetpointOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.google.protobuf.FloatValue powerFactorSetpoint = 2;</code>
   * @return Whether the powerFactorSetpoint field is set.
   */
  boolean hasPowerFactorSetpoint();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.google.protobuf.FloatValue powerFactorSetpoint = 2;</code>
   * @return The powerFactorSetpoint.
   */
  com.google.protobuf.FloatValue getPowerFactorSetpoint();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.google.protobuf.FloatValue powerFactorSetpoint = 2;</code>
   */
  com.google.protobuf.FloatValueOrBuilder getPowerFactorSetpointOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.Optional_ReactivePowerControlKind reactivePowerControlMode = 3;</code>
   * @return Whether the reactivePowerControlMode field is set.
   */
  boolean hasReactivePowerControlMode();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.Optional_ReactivePowerControlKind reactivePowerControlMode = 3;</code>
   * @return The reactivePowerControlMode.
   */
  openfmb.commonmodule.Optional_ReactivePowerControlKind getReactivePowerControlMode();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.Optional_ReactivePowerControlKind reactivePowerControlMode = 3;</code>
   */
  openfmb.commonmodule.Optional_ReactivePowerControlKindOrBuilder getReactivePowerControlModeOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.google.protobuf.FloatValue reactivePowerSetpoint = 4;</code>
   * @return Whether the reactivePowerSetpoint field is set.
   */
  boolean hasReactivePowerSetpoint();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.google.protobuf.FloatValue reactivePowerSetpoint = 4;</code>
   * @return The reactivePowerSetpoint.
   */
  com.google.protobuf.FloatValue getReactivePowerSetpoint();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.google.protobuf.FloatValue reactivePowerSetpoint = 4;</code>
   */
  com.google.protobuf.FloatValueOrBuilder getReactivePowerSetpointOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.google.protobuf.FloatValue voltageSetpoint = 5;</code>
   * @return Whether the voltageSetpoint field is set.
   */
  boolean hasVoltageSetpoint();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.google.protobuf.FloatValue voltageSetpoint = 5;</code>
   * @return The voltageSetpoint.
   */
  com.google.protobuf.FloatValue getVoltageSetpoint();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.google.protobuf.FloatValue voltageSetpoint = 5;</code>
   */
  com.google.protobuf.FloatValueOrBuilder getVoltageSetpointOrBuilder();
}
