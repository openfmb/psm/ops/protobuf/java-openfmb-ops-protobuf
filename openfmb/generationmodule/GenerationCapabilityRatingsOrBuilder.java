// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: generationmodule/generationmodule.proto

package openfmb.generationmodule;

public interface GenerationCapabilityRatingsOrBuilder extends
    // @@protoc_insertion_point(interface_extends:generationmodule.GenerationCapabilityRatings)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.SourceCapabilityRatings sourceCapabilityRatings = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the sourceCapabilityRatings field is set.
   */
  boolean hasSourceCapabilityRatings();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.SourceCapabilityRatings sourceCapabilityRatings = 1 [(.uml.option_parent_message) = true];</code>
   * @return The sourceCapabilityRatings.
   */
  openfmb.commonmodule.SourceCapabilityRatings getSourceCapabilityRatings();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.SourceCapabilityRatings sourceCapabilityRatings = 1 [(.uml.option_parent_message) = true];</code>
   */
  openfmb.commonmodule.SourceCapabilityRatingsOrBuilder getSourceCapabilityRatingsOrBuilder();
}
