// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: generationmodule/generationmodule.proto

package openfmb.generationmodule;

/**
 * <pre>
 * Generation event profile
 * </pre>
 *
 * Protobuf type {@code generationmodule.GenerationEventProfile}
 */
public final class GenerationEventProfile extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:generationmodule.GenerationEventProfile)
    GenerationEventProfileOrBuilder {
private static final long serialVersionUID = 0L;
  // Use GenerationEventProfile.newBuilder() to construct.
  private GenerationEventProfile(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private GenerationEventProfile() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new GenerationEventProfile();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.generationmodule.Generationmodule.internal_static_generationmodule_GenerationEventProfile_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.generationmodule.Generationmodule.internal_static_generationmodule_GenerationEventProfile_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.generationmodule.GenerationEventProfile.class, openfmb.generationmodule.GenerationEventProfile.Builder.class);
  }

  public static final int EVENTMESSAGEINFO_FIELD_NUMBER = 1;
  private openfmb.commonmodule.EventMessageInfo eventMessageInfo_;
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the eventMessageInfo field is set.
   */
  @java.lang.Override
  public boolean hasEventMessageInfo() {
    return eventMessageInfo_ != null;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return The eventMessageInfo.
   */
  @java.lang.Override
  public openfmb.commonmodule.EventMessageInfo getEventMessageInfo() {
    return eventMessageInfo_ == null ? openfmb.commonmodule.EventMessageInfo.getDefaultInstance() : eventMessageInfo_;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.EventMessageInfoOrBuilder getEventMessageInfoOrBuilder() {
    return eventMessageInfo_ == null ? openfmb.commonmodule.EventMessageInfo.getDefaultInstance() : eventMessageInfo_;
  }

  public static final int GENERATINGUNIT_FIELD_NUMBER = 2;
  private openfmb.generationmodule.GeneratingUnit generatingUnit_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.generationmodule.GeneratingUnit generatingUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the generatingUnit field is set.
   */
  @java.lang.Override
  public boolean hasGeneratingUnit() {
    return generatingUnit_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.generationmodule.GeneratingUnit generatingUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The generatingUnit.
   */
  @java.lang.Override
  public openfmb.generationmodule.GeneratingUnit getGeneratingUnit() {
    return generatingUnit_ == null ? openfmb.generationmodule.GeneratingUnit.getDefaultInstance() : generatingUnit_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.generationmodule.GeneratingUnit generatingUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.generationmodule.GeneratingUnitOrBuilder getGeneratingUnitOrBuilder() {
    return generatingUnit_ == null ? openfmb.generationmodule.GeneratingUnit.getDefaultInstance() : generatingUnit_;
  }

  public static final int GENERATIONEVENT_FIELD_NUMBER = 3;
  private openfmb.generationmodule.GenerationEvent generationEvent_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.generationmodule.GenerationEvent generationEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the generationEvent field is set.
   */
  @java.lang.Override
  public boolean hasGenerationEvent() {
    return generationEvent_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.generationmodule.GenerationEvent generationEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The generationEvent.
   */
  @java.lang.Override
  public openfmb.generationmodule.GenerationEvent getGenerationEvent() {
    return generationEvent_ == null ? openfmb.generationmodule.GenerationEvent.getDefaultInstance() : generationEvent_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.generationmodule.GenerationEvent generationEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.generationmodule.GenerationEventOrBuilder getGenerationEventOrBuilder() {
    return generationEvent_ == null ? openfmb.generationmodule.GenerationEvent.getDefaultInstance() : generationEvent_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (eventMessageInfo_ != null) {
      output.writeMessage(1, getEventMessageInfo());
    }
    if (generatingUnit_ != null) {
      output.writeMessage(2, getGeneratingUnit());
    }
    if (generationEvent_ != null) {
      output.writeMessage(3, getGenerationEvent());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (eventMessageInfo_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getEventMessageInfo());
    }
    if (generatingUnit_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getGeneratingUnit());
    }
    if (generationEvent_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(3, getGenerationEvent());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.generationmodule.GenerationEventProfile)) {
      return super.equals(obj);
    }
    openfmb.generationmodule.GenerationEventProfile other = (openfmb.generationmodule.GenerationEventProfile) obj;

    if (hasEventMessageInfo() != other.hasEventMessageInfo()) return false;
    if (hasEventMessageInfo()) {
      if (!getEventMessageInfo()
          .equals(other.getEventMessageInfo())) return false;
    }
    if (hasGeneratingUnit() != other.hasGeneratingUnit()) return false;
    if (hasGeneratingUnit()) {
      if (!getGeneratingUnit()
          .equals(other.getGeneratingUnit())) return false;
    }
    if (hasGenerationEvent() != other.hasGenerationEvent()) return false;
    if (hasGenerationEvent()) {
      if (!getGenerationEvent()
          .equals(other.getGenerationEvent())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasEventMessageInfo()) {
      hash = (37 * hash) + EVENTMESSAGEINFO_FIELD_NUMBER;
      hash = (53 * hash) + getEventMessageInfo().hashCode();
    }
    if (hasGeneratingUnit()) {
      hash = (37 * hash) + GENERATINGUNIT_FIELD_NUMBER;
      hash = (53 * hash) + getGeneratingUnit().hashCode();
    }
    if (hasGenerationEvent()) {
      hash = (37 * hash) + GENERATIONEVENT_FIELD_NUMBER;
      hash = (53 * hash) + getGenerationEvent().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.generationmodule.GenerationEventProfile parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.generationmodule.GenerationEventProfile parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.generationmodule.GenerationEventProfile parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.generationmodule.GenerationEventProfile parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.generationmodule.GenerationEventProfile parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.generationmodule.GenerationEventProfile parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.generationmodule.GenerationEventProfile parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.generationmodule.GenerationEventProfile parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.generationmodule.GenerationEventProfile parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.generationmodule.GenerationEventProfile parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.generationmodule.GenerationEventProfile parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.generationmodule.GenerationEventProfile parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.generationmodule.GenerationEventProfile prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Generation event profile
   * </pre>
   *
   * Protobuf type {@code generationmodule.GenerationEventProfile}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:generationmodule.GenerationEventProfile)
      openfmb.generationmodule.GenerationEventProfileOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.generationmodule.Generationmodule.internal_static_generationmodule_GenerationEventProfile_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.generationmodule.Generationmodule.internal_static_generationmodule_GenerationEventProfile_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.generationmodule.GenerationEventProfile.class, openfmb.generationmodule.GenerationEventProfile.Builder.class);
    }

    // Construct using openfmb.generationmodule.GenerationEventProfile.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      eventMessageInfo_ = null;
      if (eventMessageInfoBuilder_ != null) {
        eventMessageInfoBuilder_.dispose();
        eventMessageInfoBuilder_ = null;
      }
      generatingUnit_ = null;
      if (generatingUnitBuilder_ != null) {
        generatingUnitBuilder_.dispose();
        generatingUnitBuilder_ = null;
      }
      generationEvent_ = null;
      if (generationEventBuilder_ != null) {
        generationEventBuilder_.dispose();
        generationEventBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.generationmodule.Generationmodule.internal_static_generationmodule_GenerationEventProfile_descriptor;
    }

    @java.lang.Override
    public openfmb.generationmodule.GenerationEventProfile getDefaultInstanceForType() {
      return openfmb.generationmodule.GenerationEventProfile.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.generationmodule.GenerationEventProfile build() {
      openfmb.generationmodule.GenerationEventProfile result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.generationmodule.GenerationEventProfile buildPartial() {
      openfmb.generationmodule.GenerationEventProfile result = new openfmb.generationmodule.GenerationEventProfile(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.generationmodule.GenerationEventProfile result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.eventMessageInfo_ = eventMessageInfoBuilder_ == null
            ? eventMessageInfo_
            : eventMessageInfoBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.generatingUnit_ = generatingUnitBuilder_ == null
            ? generatingUnit_
            : generatingUnitBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000004) != 0)) {
        result.generationEvent_ = generationEventBuilder_ == null
            ? generationEvent_
            : generationEventBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.generationmodule.GenerationEventProfile) {
        return mergeFrom((openfmb.generationmodule.GenerationEventProfile)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.generationmodule.GenerationEventProfile other) {
      if (other == openfmb.generationmodule.GenerationEventProfile.getDefaultInstance()) return this;
      if (other.hasEventMessageInfo()) {
        mergeEventMessageInfo(other.getEventMessageInfo());
      }
      if (other.hasGeneratingUnit()) {
        mergeGeneratingUnit(other.getGeneratingUnit());
      }
      if (other.hasGenerationEvent()) {
        mergeGenerationEvent(other.getGenerationEvent());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getEventMessageInfoFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getGeneratingUnitFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            case 26: {
              input.readMessage(
                  getGenerationEventFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000004;
              break;
            } // case 26
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.EventMessageInfo eventMessageInfo_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.EventMessageInfo, openfmb.commonmodule.EventMessageInfo.Builder, openfmb.commonmodule.EventMessageInfoOrBuilder> eventMessageInfoBuilder_;
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     * @return Whether the eventMessageInfo field is set.
     */
    public boolean hasEventMessageInfo() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     * @return The eventMessageInfo.
     */
    public openfmb.commonmodule.EventMessageInfo getEventMessageInfo() {
      if (eventMessageInfoBuilder_ == null) {
        return eventMessageInfo_ == null ? openfmb.commonmodule.EventMessageInfo.getDefaultInstance() : eventMessageInfo_;
      } else {
        return eventMessageInfoBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setEventMessageInfo(openfmb.commonmodule.EventMessageInfo value) {
      if (eventMessageInfoBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        eventMessageInfo_ = value;
      } else {
        eventMessageInfoBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setEventMessageInfo(
        openfmb.commonmodule.EventMessageInfo.Builder builderForValue) {
      if (eventMessageInfoBuilder_ == null) {
        eventMessageInfo_ = builderForValue.build();
      } else {
        eventMessageInfoBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder mergeEventMessageInfo(openfmb.commonmodule.EventMessageInfo value) {
      if (eventMessageInfoBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          eventMessageInfo_ != null &&
          eventMessageInfo_ != openfmb.commonmodule.EventMessageInfo.getDefaultInstance()) {
          getEventMessageInfoBuilder().mergeFrom(value);
        } else {
          eventMessageInfo_ = value;
        }
      } else {
        eventMessageInfoBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder clearEventMessageInfo() {
      bitField0_ = (bitField0_ & ~0x00000001);
      eventMessageInfo_ = null;
      if (eventMessageInfoBuilder_ != null) {
        eventMessageInfoBuilder_.dispose();
        eventMessageInfoBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.EventMessageInfo.Builder getEventMessageInfoBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getEventMessageInfoFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.EventMessageInfoOrBuilder getEventMessageInfoOrBuilder() {
      if (eventMessageInfoBuilder_ != null) {
        return eventMessageInfoBuilder_.getMessageOrBuilder();
      } else {
        return eventMessageInfo_ == null ?
            openfmb.commonmodule.EventMessageInfo.getDefaultInstance() : eventMessageInfo_;
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.EventMessageInfo eventMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.EventMessageInfo, openfmb.commonmodule.EventMessageInfo.Builder, openfmb.commonmodule.EventMessageInfoOrBuilder> 
        getEventMessageInfoFieldBuilder() {
      if (eventMessageInfoBuilder_ == null) {
        eventMessageInfoBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.EventMessageInfo, openfmb.commonmodule.EventMessageInfo.Builder, openfmb.commonmodule.EventMessageInfoOrBuilder>(
                getEventMessageInfo(),
                getParentForChildren(),
                isClean());
        eventMessageInfo_ = null;
      }
      return eventMessageInfoBuilder_;
    }

    private openfmb.generationmodule.GeneratingUnit generatingUnit_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.generationmodule.GeneratingUnit, openfmb.generationmodule.GeneratingUnit.Builder, openfmb.generationmodule.GeneratingUnitOrBuilder> generatingUnitBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GeneratingUnit generatingUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return Whether the generatingUnit field is set.
     */
    public boolean hasGeneratingUnit() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GeneratingUnit generatingUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The generatingUnit.
     */
    public openfmb.generationmodule.GeneratingUnit getGeneratingUnit() {
      if (generatingUnitBuilder_ == null) {
        return generatingUnit_ == null ? openfmb.generationmodule.GeneratingUnit.getDefaultInstance() : generatingUnit_;
      } else {
        return generatingUnitBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GeneratingUnit generatingUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setGeneratingUnit(openfmb.generationmodule.GeneratingUnit value) {
      if (generatingUnitBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        generatingUnit_ = value;
      } else {
        generatingUnitBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GeneratingUnit generatingUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setGeneratingUnit(
        openfmb.generationmodule.GeneratingUnit.Builder builderForValue) {
      if (generatingUnitBuilder_ == null) {
        generatingUnit_ = builderForValue.build();
      } else {
        generatingUnitBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GeneratingUnit generatingUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder mergeGeneratingUnit(openfmb.generationmodule.GeneratingUnit value) {
      if (generatingUnitBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          generatingUnit_ != null &&
          generatingUnit_ != openfmb.generationmodule.GeneratingUnit.getDefaultInstance()) {
          getGeneratingUnitBuilder().mergeFrom(value);
        } else {
          generatingUnit_ = value;
        }
      } else {
        generatingUnitBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GeneratingUnit generatingUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder clearGeneratingUnit() {
      bitField0_ = (bitField0_ & ~0x00000002);
      generatingUnit_ = null;
      if (generatingUnitBuilder_ != null) {
        generatingUnitBuilder_.dispose();
        generatingUnitBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GeneratingUnit generatingUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.generationmodule.GeneratingUnit.Builder getGeneratingUnitBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getGeneratingUnitFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GeneratingUnit generatingUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.generationmodule.GeneratingUnitOrBuilder getGeneratingUnitOrBuilder() {
      if (generatingUnitBuilder_ != null) {
        return generatingUnitBuilder_.getMessageOrBuilder();
      } else {
        return generatingUnit_ == null ?
            openfmb.generationmodule.GeneratingUnit.getDefaultInstance() : generatingUnit_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GeneratingUnit generatingUnit = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.generationmodule.GeneratingUnit, openfmb.generationmodule.GeneratingUnit.Builder, openfmb.generationmodule.GeneratingUnitOrBuilder> 
        getGeneratingUnitFieldBuilder() {
      if (generatingUnitBuilder_ == null) {
        generatingUnitBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.generationmodule.GeneratingUnit, openfmb.generationmodule.GeneratingUnit.Builder, openfmb.generationmodule.GeneratingUnitOrBuilder>(
                getGeneratingUnit(),
                getParentForChildren(),
                isClean());
        generatingUnit_ = null;
      }
      return generatingUnitBuilder_;
    }

    private openfmb.generationmodule.GenerationEvent generationEvent_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.generationmodule.GenerationEvent, openfmb.generationmodule.GenerationEvent.Builder, openfmb.generationmodule.GenerationEventOrBuilder> generationEventBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GenerationEvent generationEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return Whether the generationEvent field is set.
     */
    public boolean hasGenerationEvent() {
      return ((bitField0_ & 0x00000004) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GenerationEvent generationEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The generationEvent.
     */
    public openfmb.generationmodule.GenerationEvent getGenerationEvent() {
      if (generationEventBuilder_ == null) {
        return generationEvent_ == null ? openfmb.generationmodule.GenerationEvent.getDefaultInstance() : generationEvent_;
      } else {
        return generationEventBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GenerationEvent generationEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setGenerationEvent(openfmb.generationmodule.GenerationEvent value) {
      if (generationEventBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        generationEvent_ = value;
      } else {
        generationEventBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GenerationEvent generationEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setGenerationEvent(
        openfmb.generationmodule.GenerationEvent.Builder builderForValue) {
      if (generationEventBuilder_ == null) {
        generationEvent_ = builderForValue.build();
      } else {
        generationEventBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GenerationEvent generationEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder mergeGenerationEvent(openfmb.generationmodule.GenerationEvent value) {
      if (generationEventBuilder_ == null) {
        if (((bitField0_ & 0x00000004) != 0) &&
          generationEvent_ != null &&
          generationEvent_ != openfmb.generationmodule.GenerationEvent.getDefaultInstance()) {
          getGenerationEventBuilder().mergeFrom(value);
        } else {
          generationEvent_ = value;
        }
      } else {
        generationEventBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GenerationEvent generationEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder clearGenerationEvent() {
      bitField0_ = (bitField0_ & ~0x00000004);
      generationEvent_ = null;
      if (generationEventBuilder_ != null) {
        generationEventBuilder_.dispose();
        generationEventBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GenerationEvent generationEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.generationmodule.GenerationEvent.Builder getGenerationEventBuilder() {
      bitField0_ |= 0x00000004;
      onChanged();
      return getGenerationEventFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GenerationEvent generationEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.generationmodule.GenerationEventOrBuilder getGenerationEventOrBuilder() {
      if (generationEventBuilder_ != null) {
        return generationEventBuilder_.getMessageOrBuilder();
      } else {
        return generationEvent_ == null ?
            openfmb.generationmodule.GenerationEvent.getDefaultInstance() : generationEvent_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.generationmodule.GenerationEvent generationEvent = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.generationmodule.GenerationEvent, openfmb.generationmodule.GenerationEvent.Builder, openfmb.generationmodule.GenerationEventOrBuilder> 
        getGenerationEventFieldBuilder() {
      if (generationEventBuilder_ == null) {
        generationEventBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.generationmodule.GenerationEvent, openfmb.generationmodule.GenerationEvent.Builder, openfmb.generationmodule.GenerationEventOrBuilder>(
                getGenerationEvent(),
                getParentForChildren(),
                isClean());
        generationEvent_ = null;
      }
      return generationEventBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:generationmodule.GenerationEventProfile)
  }

  // @@protoc_insertion_point(class_scope:generationmodule.GenerationEventProfile)
  private static final openfmb.generationmodule.GenerationEventProfile DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.generationmodule.GenerationEventProfile();
  }

  public static openfmb.generationmodule.GenerationEventProfile getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<GenerationEventProfile>
      PARSER = new com.google.protobuf.AbstractParser<GenerationEventProfile>() {
    @java.lang.Override
    public GenerationEventProfile parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<GenerationEventProfile> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<GenerationEventProfile> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.generationmodule.GenerationEventProfile getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

