// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: generationmodule/generationmodule.proto

package openfmb.generationmodule;

public final class Generationmodule {
  private Generationmodule() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GeneratingUnit_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GeneratingUnit_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationCapabilityConfiguration_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationCapabilityConfiguration_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationCapabilityOverride_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationCapabilityOverride_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationCapabilityOverrideProfile_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationCapabilityOverrideProfile_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationCapabilityRatings_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationCapabilityRatings_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationCapability_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationCapability_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationCapabilityProfile_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationCapabilityProfile_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationPoint_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationPoint_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationCSG_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationCSG_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationControlScheduleFSCH_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationControlScheduleFSCH_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationControlFSCC_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationControlFSCC_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationControl_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationControl_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationControlProfile_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationControlProfile_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_DroopParameter_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_DroopParameter_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_RealPowerControl_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_RealPowerControl_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_ReactivePowerControl_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_ReactivePowerControl_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationDiscreteControl_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationDiscreteControl_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationDiscreteControlProfile_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationDiscreteControlProfile_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationReading_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationReading_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationReadingProfile_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationReadingProfile_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationPointStatus_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationPointStatus_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationEventAndStatusZGEN_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationEventAndStatusZGEN_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationEventZGEN_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationEventZGEN_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationEvent_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationEvent_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationEventProfile_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationEventProfile_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationStatusZGEN_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationStatusZGEN_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationStatus_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationStatus_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_generationmodule_GenerationStatusProfile_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_generationmodule_GenerationStatusProfile_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\'generationmodule/generationmodule.prot" +
      "o\022\020generationmodule\032\tuml.proto\032\036google/p" +
      "rotobuf/wrappers.proto\032\037commonmodule/com" +
      "monmodule.proto\"\210\001\n\016GeneratingUnit\022D\n\023co" +
      "nductingEquipment\030\001 \001(\0132!.commonmodule.C" +
      "onductingEquipmentB\004\200\265\030\001\0220\n\rmaxOperating" +
      "P\030\002 \001(\0132\031.commonmodule.ActivePower\"}\n!Ge" +
      "nerationCapabilityConfiguration\022X\n\035sourc" +
      "eCapabilityConfiguration\030\001 \001(\0132+.commonm" +
      "odule.SourceCapabilityConfigurationB\004\200\265\030" +
      "\001\"\310\001\n\034GenerationCapabilityOverride\022>\n\020id" +
      "entifiedObject\030\001 \001(\0132\036.commonmodule.Iden" +
      "tifiedObjectB\004\200\265\030\001\022h\n!generationCapabili" +
      "tyConfiguration\030\002 \001(\01323.generationmodule" +
      ".GenerationCapabilityConfigurationB\010\210\265\030\001" +
      "\220\265\030\001\"\231\002\n#GenerationCapabilityOverridePro" +
      "file\022H\n\025capabilityMessageInfo\030\001 \001(\0132#.co" +
      "mmonmodule.CapabilityMessageInfoB\004\200\265\030\001\022^" +
      "\n\034generationCapabilityOverride\030\002 \001(\0132..g" +
      "enerationmodule.GenerationCapabilityOver" +
      "rideB\010\210\265\030\001\220\265\030\001\022B\n\016generatingUnit\030\003 \001(\0132 " +
      ".generationmodule.GeneratingUnitB\010\210\265\030\001\220\265" +
      "\030\001:\004\300\363\030\001\"k\n\033GenerationCapabilityRatings\022" +
      "L\n\027sourceCapabilityRatings\030\001 \001(\0132%.commo" +
      "nmodule.SourceCapabilityRatingsB\004\200\265\030\001\"\220\002" +
      "\n\024GenerationCapability\022:\n\016nameplateValue" +
      "\030\001 \001(\0132\034.commonmodule.NameplateValueB\004\200\265" +
      "\030\001\022\\\n\033generationCapabilityRatings\030\002 \001(\0132" +
      "-.generationmodule.GenerationCapabilityR" +
      "atingsB\010\210\265\030\001\220\265\030\001\022^\n!generationCapability" +
      "Configuration\030\003 \001(\01323.generationmodule.G" +
      "enerationCapabilityConfiguration\"\201\002\n\033Gen" +
      "erationCapabilityProfile\022H\n\025capabilityMe" +
      "ssageInfo\030\001 \001(\0132#.commonmodule.Capabilit" +
      "yMessageInfoB\004\200\265\030\001\022N\n\024generationCapabili" +
      "ty\030\002 \001(\0132&.generationmodule.GenerationCa" +
      "pabilityB\010\210\265\030\001\220\265\030\001\022B\n\016generatingUnit\030\003 \001" +
      "(\0132 .generationmodule.GeneratingUnitB\010\210\265" +
      "\030\001\220\265\030\001:\004\300\363\030\001\"\266\t\n\017GenerationPoint\0223\n\021blac" +
      "kStartEnabled\030\001 \001(\0132\030.commonmodule.Contr" +
      "olSPC\022:\n\030frequencySetPointEnabled\030\002 \001(\0132" +
      "\030.commonmodule.ControlSPC\022/\n\npctHzDroop\030" +
      "\003 \001(\0132\033.google.protobuf.FloatValue\022.\n\tpc" +
      "tVDroop\030\004 \001(\0132\033.google.protobuf.FloatVal" +
      "ue\022)\n\trampRates\030\005 \001(\0132\026.commonmodule.Ram" +
      "pRate\022<\n\032reactivePwrSetPointEnabled\030\006 \001(" +
      "\0132\030.commonmodule.ControlSPC\0228\n\026realPwrSe" +
      "tPointEnabled\030\007 \001(\0132\030.commonmodule.Contr" +
      "olSPC\022\'\n\005reset\030\010 \001(\0132\030.commonmodule.Cont" +
      "rolSPC\022/\n\005state\030\t \001(\0132 .commonmodule.Opt" +
      "ional_StateKind\0220\n\016syncBackToGrid\030\n \001(\0132" +
      "\030.commonmodule.ControlSPC\022?\n\035transToIsln" +
      "dOnGridLossEnabled\030\013 \001(\0132\030.commonmodule." +
      "ControlSPC\0228\n\026voltageSetPointEnabled\030\014 \001" +
      "(\0132\030.commonmodule.ControlSPC\022;\n\tstartTim" +
      "e\030\r \001(\0132\036.commonmodule.ControlTimestampB" +
      "\010\210\265\030\001\220\265\030\001\022<\n\025enterServiceOperation\030\016 \001(\013" +
      "2\035.commonmodule.EnterServiceAPC\022*\n\014hzWOp" +
      "eration\030\017 \001(\0132\024.commonmodule.HzWAPC\0220\n\017l" +
      "imitWOperation\030\020 \001(\0132\027.commonmodule.Limi" +
      "tWAPC\022(\n\013pFOperation\030\021 \001(\0132\023.commonmodul" +
      "e.PFSPC\0220\n\021tmHzTripOperation\030\022 \001(\0132\025.com" +
      "monmodule.TmHzCSG\0224\n\023tmVoltTripOperation" +
      "\030\023 \001(\0132\027.commonmodule.TmVoltCSG\022*\n\014vArOp" +
      "eration\030\024 \001(\0132\024.commonmodule.VarSPC\0222\n\020v" +
      "oltVarOperation\030\025 \001(\0132\030.commonmodule.Vol" +
      "tVarCSG\022.\n\016voltWOperation\030\026 \001(\0132\026.common" +
      "module.VoltWCSG\022,\n\rwVarOperation\030\027 \001(\0132\025" +
      ".commonmodule.WVarCSG\"L\n\rGenerationCSG\022;" +
      "\n\006crvPts\030\001 \003(\0132!.generationmodule.Genera" +
      "tionPointB\010\210\265\030\001\220\265\030\001\"[\n\035GenerationControl" +
      "ScheduleFSCH\022:\n\007ValDCSG\030\001 \001(\0132\037.generati" +
      "onmodule.GenerationCSGB\010\210\265\030\001\220\265\030\001\"\245\001\n\025Gen" +
      "erationControlFSCC\0224\n\013controlFSCC\030\001 \001(\0132" +
      "\031.commonmodule.ControlFSCCB\004\200\265\030\001\022V\n\035Gene" +
      "rationControlScheduleFSCH\030\002 \001(\0132/.genera" +
      "tionmodule.GenerationControlScheduleFSCH" +
      "\"\301\001\n\021GenerationControl\0226\n\014controlValue\030\001" +
      " \001(\0132\032.commonmodule.ControlValueB\004\200\265\030\001\022," +
      "\n\005check\030\002 \001(\0132\035.commonmodule.CheckCondit" +
      "ions\022F\n\025generationControlFSCC\030\003 \001(\0132\'.ge" +
      "nerationmodule.GenerationControlFSCC\"\362\001\n" +
      "\030GenerationControlProfile\022B\n\022controlMess" +
      "ageInfo\030\001 \001(\0132 .commonmodule.ControlMess" +
      "ageInfoB\004\200\265\030\001\022B\n\016generatingUnit\030\002 \001(\0132 ." +
      "generationmodule.GeneratingUnitB\010\210\265\030\001\220\265\030" +
      "\001\022H\n\021generationControl\030\003 \001(\0132#.generatio" +
      "nmodule.GenerationControlB\010\210\265\030\001\220\265\030\001:\004\300\363\030" +
      "\001\"q\n\016DroopParameter\022*\n\005slope\030\001 \001(\0132\033.goo" +
      "gle.protobuf.FloatValue\0223\n\016unloadedOffse" +
      "t\030\002 \001(\0132\033.google.protobuf.FloatValue\"\210\002\n" +
      "\020RealPowerControl\0227\n\rdroopSetpoint\030\001 \001(\013" +
      "2 .generationmodule.DroopParameter\0228\n\023is" +
      "ochronousSetpoint\030\002 \001(\0132\033.google.protobu" +
      "f.FloatValue\022I\n\024realPowerControlMode\030\003 \001" +
      "(\0132+.commonmodule.Optional_RealPowerCont" +
      "rolKind\0226\n\021realPowerSetpoint\030\004 \001(\0132\033.goo" +
      "gle.protobuf.FloatValue\"\316\002\n\024ReactivePowe" +
      "rControl\0227\n\rdroopSetpoint\030\001 \001(\0132 .genera" +
      "tionmodule.DroopParameter\0228\n\023powerFactor" +
      "Setpoint\030\002 \001(\0132\033.google.protobuf.FloatVa" +
      "lue\022Q\n\030reactivePowerControlMode\030\003 \001(\0132/." +
      "commonmodule.Optional_ReactivePowerContr" +
      "olKind\022:\n\025reactivePowerSetpoint\030\004 \001(\0132\033." +
      "google.protobuf.FloatValue\0224\n\017voltageSet" +
      "point\030\005 \001(\0132\033.google.protobuf.FloatValue" +
      "\"\205\002\n\031GenerationDiscreteControl\0226\n\014contro" +
      "lValue\030\001 \001(\0132\032.commonmodule.ControlValue" +
      "B\004\200\265\030\001\022,\n\005check\030\002 \001(\0132\035.commonmodule.Che" +
      "ckConditions\022D\n\024ReactivePowerControl\030\003 \001" +
      "(\0132&.generationmodule.ReactivePowerContr" +
      "ol\022<\n\020RealPowerControl\030\004 \001(\0132\".generatio" +
      "nmodule.RealPowerControl\"\212\002\n GenerationD" +
      "iscreteControlProfile\022B\n\022controlMessageI" +
      "nfo\030\001 \001(\0132 .commonmodule.ControlMessageI" +
      "nfoB\004\200\265\030\001\022B\n\016generatingUnit\030\002 \001(\0132 .gene" +
      "rationmodule.GeneratingUnitB\010\210\265\030\001\220\265\030\001\022X\n" +
      "\031generationDiscreteControl\030\003 \001(\0132+.gener" +
      "ationmodule.GenerationDiscreteControlB\010\210" +
      "\265\030\001\220\265\030\001:\004\300\363\030\001\"\203\002\n\021GenerationReading\022b\n\"c" +
      "onductingEquipmentTerminalReading\030\001 \001(\0132" +
      "0.commonmodule.ConductingEquipmentTermin" +
      "alReadingB\004\200\265\030\001\022*\n\tphaseMMTN\030\002 \001(\0132\027.com" +
      "monmodule.PhaseMMTN\022.\n\013readingMMTR\030\003 \001(\013" +
      "2\031.commonmodule.ReadingMMTR\022.\n\013readingMM" +
      "XU\030\004 \001(\0132\031.commonmodule.ReadingMMXU\"\362\001\n\030" +
      "GenerationReadingProfile\022B\n\022readingMessa" +
      "geInfo\030\001 \001(\0132 .commonmodule.ReadingMessa" +
      "geInfoB\004\200\265\030\001\022B\n\016generatingUnit\030\002 \001(\0132 .g" +
      "enerationmodule.GeneratingUnitB\010\210\265\030\001\220\265\030\001" +
      "\022H\n\021generationReading\030\003 \001(\0132#.generation" +
      "module.GenerationReadingB\010\210\265\030\001\220\265\030\001:\004\300\363\030\001" +
      "\"\317\010\n\025GenerationPointStatus\0222\n\021blackStart" +
      "Enabled\030\001 \001(\0132\027.commonmodule.StatusSPS\0229" +
      "\n\030frequencySetPointEnabled\030\002 \001(\0132\027.commo" +
      "nmodule.StatusSPS\022/\n\npctHzDroop\030\003 \001(\0132\033." +
      "google.protobuf.FloatValue\022.\n\tpctVDroop\030" +
      "\004 \001(\0132\033.google.protobuf.FloatValue\022)\n\tra" +
      "mpRates\030\005 \001(\0132\026.commonmodule.RampRate\022;\n" +
      "\032reactivePwrSetPointEnabled\030\006 \001(\0132\027.comm" +
      "onmodule.StatusSPS\0227\n\026realPwrSetPointEna" +
      "bled\030\007 \001(\0132\027.commonmodule.StatusSPS\022/\n\005s" +
      "tate\030\010 \001(\0132 .commonmodule.Optional_State" +
      "Kind\022/\n\016syncBackToGrid\030\t \001(\0132\027.commonmod" +
      "ule.StatusSPS\022>\n\035transToIslndOnGridLossE" +
      "nabled\030\n \001(\0132\027.commonmodule.StatusSPS\0227\n" +
      "\026voltageSetPointEnabled\030\013 \001(\0132\027.commonmo" +
      "dule.StatusSPS\022<\n\025enterServiceOperation\030" +
      "\014 \001(\0132\035.commonmodule.EnterServiceAPC\022*\n\014" +
      "hzWOperation\030\r \001(\0132\024.commonmodule.HzWAPC" +
      "\0220\n\017limitWOperation\030\016 \001(\0132\027.commonmodule" +
      ".LimitWAPC\022(\n\013pFOperation\030\017 \001(\0132\023.common" +
      "module.PFSPC\0220\n\021tmHzTripOperation\030\020 \001(\0132" +
      "\025.commonmodule.TmHzCSG\0224\n\023tmVoltTripOper" +
      "ation\030\021 \001(\0132\027.commonmodule.TmVoltCSG\022*\n\014" +
      "vArOperation\030\022 \001(\0132\024.commonmodule.VarSPC" +
      "\0222\n\020voltVarOperation\030\023 \001(\0132\030.commonmodul" +
      "e.VoltVarCSG\022.\n\016voltWOperation\030\024 \001(\0132\026.c" +
      "ommonmodule.VoltWCSG\022,\n\rwVarOperation\030\025 " +
      "\001(\0132\025.commonmodule.WVarCSG\"\335\004\n\034Generatio" +
      "nEventAndStatusZGEN\022V\n\034logicalNodeForEve" +
      "ntAndStatus\030\001 \001(\0132*.commonmodule.Logical" +
      "NodeForEventAndStatusB\004\200\265\030\001\022)\n\010AuxPwrSt\030" +
      "\002 \001(\0132\027.commonmodule.StatusSPS\0226\n\013Dynami" +
      "cTest\030\003 \001(\0132!.commonmodule.ENS_DynamicTe" +
      "stKind\022(\n\007EmgStop\030\004 \001(\0132\027.commonmodule.S" +
      "tatusSPS\022(\n\007GnSynSt\030\005 \001(\0132\027.commonmodule" +
      ".StatusSPS\022<\n\013PointStatus\030\006 \001(\0132\'.genera" +
      "tionmodule.GenerationPointStatus\022-\n\004Alrm" +
      "\030\007 \001(\0132\037.commonmodule.Optional_AlrmKind\022" +
      "K\n\023GridConnectionState\030\010 \001(\0132..commonmod" +
      "ule.Optional_GridConnectionStateKind\0221\n\013" +
      "ManAlrmInfo\030\t \001(\0132\034.google.protobuf.Stri" +
      "ngValue\022A\n\016OperatingState\030\n \001(\0132).common" +
      "module.Optional_OperatingStateKind\"q\n\023Ge" +
      "nerationEventZGEN\022Z\n\034generationEventAndS" +
      "tatusZGEN\030\001 \001(\0132..generationmodule.Gener" +
      "ationEventAndStatusZGENB\004\200\265\030\001\"\211\001\n\017Genera" +
      "tionEvent\0222\n\neventValue\030\001 \001(\0132\030.commonmo" +
      "dule.EventValueB\004\200\265\030\001\022B\n\023generationEvent" +
      "ZGEN\030\002 \001(\0132%.generationmodule.Generation" +
      "EventZGEN\"\350\001\n\026GenerationEventProfile\022>\n\020" +
      "eventMessageInfo\030\001 \001(\0132\036.commonmodule.Ev" +
      "entMessageInfoB\004\200\265\030\001\022B\n\016generatingUnit\030\002" +
      " \001(\0132 .generationmodule.GeneratingUnitB\010" +
      "\210\265\030\001\220\265\030\001\022D\n\017generationEvent\030\003 \001(\0132!.gene" +
      "rationmodule.GenerationEventB\010\210\265\030\001\220\265\030\001:\004" +
      "\300\363\030\001\"r\n\024GenerationStatusZGEN\022Z\n\034generati" +
      "onEventAndStatusZGEN\030\001 \001(\0132..generationm" +
      "odule.GenerationEventAndStatusZGENB\004\200\265\030\001" +
      "\"\216\001\n\020GenerationStatus\0224\n\013statusValue\030\001 \001" +
      "(\0132\031.commonmodule.StatusValueB\004\200\265\030\001\022D\n\024g" +
      "enerationStatusZGEN\030\002 \001(\0132&.generationmo" +
      "dule.GenerationStatusZGEN\"\355\001\n\027Generation" +
      "StatusProfile\022@\n\021statusMessageInfo\030\001 \001(\013" +
      "2\037.commonmodule.StatusMessageInfoB\004\200\265\030\001\022" +
      "B\n\016generatingUnit\030\002 \001(\0132 .generationmodu" +
      "le.GeneratingUnitB\010\210\265\030\001\220\265\030\001\022F\n\020generatio" +
      "nStatus\030\003 \001(\0132\".generationmodule.Generat" +
      "ionStatusB\010\210\265\030\001\220\265\030\001:\004\300\363\030\001B\220\001\n\030openfmb.ge" +
      "nerationmoduleP\001ZWgitlab.com/openfmb/psm" +
      "/ops/protobuf/go-openfmb-ops-protobuf/v2" +
      "/openfmb/generationmodule\252\002\030openfmb.gene" +
      "rationmoduleb\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          openfmb.Uml.getDescriptor(),
          com.google.protobuf.WrappersProto.getDescriptor(),
          openfmb.commonmodule.Commonmodule.getDescriptor(),
        });
    internal_static_generationmodule_GeneratingUnit_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_generationmodule_GeneratingUnit_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GeneratingUnit_descriptor,
        new java.lang.String[] { "ConductingEquipment", "MaxOperatingP", });
    internal_static_generationmodule_GenerationCapabilityConfiguration_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_generationmodule_GenerationCapabilityConfiguration_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationCapabilityConfiguration_descriptor,
        new java.lang.String[] { "SourceCapabilityConfiguration", });
    internal_static_generationmodule_GenerationCapabilityOverride_descriptor =
      getDescriptor().getMessageTypes().get(2);
    internal_static_generationmodule_GenerationCapabilityOverride_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationCapabilityOverride_descriptor,
        new java.lang.String[] { "IdentifiedObject", "GenerationCapabilityConfiguration", });
    internal_static_generationmodule_GenerationCapabilityOverrideProfile_descriptor =
      getDescriptor().getMessageTypes().get(3);
    internal_static_generationmodule_GenerationCapabilityOverrideProfile_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationCapabilityOverrideProfile_descriptor,
        new java.lang.String[] { "CapabilityMessageInfo", "GenerationCapabilityOverride", "GeneratingUnit", });
    internal_static_generationmodule_GenerationCapabilityRatings_descriptor =
      getDescriptor().getMessageTypes().get(4);
    internal_static_generationmodule_GenerationCapabilityRatings_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationCapabilityRatings_descriptor,
        new java.lang.String[] { "SourceCapabilityRatings", });
    internal_static_generationmodule_GenerationCapability_descriptor =
      getDescriptor().getMessageTypes().get(5);
    internal_static_generationmodule_GenerationCapability_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationCapability_descriptor,
        new java.lang.String[] { "NameplateValue", "GenerationCapabilityRatings", "GenerationCapabilityConfiguration", });
    internal_static_generationmodule_GenerationCapabilityProfile_descriptor =
      getDescriptor().getMessageTypes().get(6);
    internal_static_generationmodule_GenerationCapabilityProfile_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationCapabilityProfile_descriptor,
        new java.lang.String[] { "CapabilityMessageInfo", "GenerationCapability", "GeneratingUnit", });
    internal_static_generationmodule_GenerationPoint_descriptor =
      getDescriptor().getMessageTypes().get(7);
    internal_static_generationmodule_GenerationPoint_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationPoint_descriptor,
        new java.lang.String[] { "BlackStartEnabled", "FrequencySetPointEnabled", "PctHzDroop", "PctVDroop", "RampRates", "ReactivePwrSetPointEnabled", "RealPwrSetPointEnabled", "Reset", "State", "SyncBackToGrid", "TransToIslndOnGridLossEnabled", "VoltageSetPointEnabled", "StartTime", "EnterServiceOperation", "HzWOperation", "LimitWOperation", "PFOperation", "TmHzTripOperation", "TmVoltTripOperation", "VArOperation", "VoltVarOperation", "VoltWOperation", "WVarOperation", });
    internal_static_generationmodule_GenerationCSG_descriptor =
      getDescriptor().getMessageTypes().get(8);
    internal_static_generationmodule_GenerationCSG_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationCSG_descriptor,
        new java.lang.String[] { "CrvPts", });
    internal_static_generationmodule_GenerationControlScheduleFSCH_descriptor =
      getDescriptor().getMessageTypes().get(9);
    internal_static_generationmodule_GenerationControlScheduleFSCH_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationControlScheduleFSCH_descriptor,
        new java.lang.String[] { "ValDCSG", });
    internal_static_generationmodule_GenerationControlFSCC_descriptor =
      getDescriptor().getMessageTypes().get(10);
    internal_static_generationmodule_GenerationControlFSCC_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationControlFSCC_descriptor,
        new java.lang.String[] { "ControlFSCC", "GenerationControlScheduleFSCH", });
    internal_static_generationmodule_GenerationControl_descriptor =
      getDescriptor().getMessageTypes().get(11);
    internal_static_generationmodule_GenerationControl_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationControl_descriptor,
        new java.lang.String[] { "ControlValue", "Check", "GenerationControlFSCC", });
    internal_static_generationmodule_GenerationControlProfile_descriptor =
      getDescriptor().getMessageTypes().get(12);
    internal_static_generationmodule_GenerationControlProfile_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationControlProfile_descriptor,
        new java.lang.String[] { "ControlMessageInfo", "GeneratingUnit", "GenerationControl", });
    internal_static_generationmodule_DroopParameter_descriptor =
      getDescriptor().getMessageTypes().get(13);
    internal_static_generationmodule_DroopParameter_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_DroopParameter_descriptor,
        new java.lang.String[] { "Slope", "UnloadedOffset", });
    internal_static_generationmodule_RealPowerControl_descriptor =
      getDescriptor().getMessageTypes().get(14);
    internal_static_generationmodule_RealPowerControl_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_RealPowerControl_descriptor,
        new java.lang.String[] { "DroopSetpoint", "IsochronousSetpoint", "RealPowerControlMode", "RealPowerSetpoint", });
    internal_static_generationmodule_ReactivePowerControl_descriptor =
      getDescriptor().getMessageTypes().get(15);
    internal_static_generationmodule_ReactivePowerControl_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_ReactivePowerControl_descriptor,
        new java.lang.String[] { "DroopSetpoint", "PowerFactorSetpoint", "ReactivePowerControlMode", "ReactivePowerSetpoint", "VoltageSetpoint", });
    internal_static_generationmodule_GenerationDiscreteControl_descriptor =
      getDescriptor().getMessageTypes().get(16);
    internal_static_generationmodule_GenerationDiscreteControl_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationDiscreteControl_descriptor,
        new java.lang.String[] { "ControlValue", "Check", "ReactivePowerControl", "RealPowerControl", });
    internal_static_generationmodule_GenerationDiscreteControlProfile_descriptor =
      getDescriptor().getMessageTypes().get(17);
    internal_static_generationmodule_GenerationDiscreteControlProfile_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationDiscreteControlProfile_descriptor,
        new java.lang.String[] { "ControlMessageInfo", "GeneratingUnit", "GenerationDiscreteControl", });
    internal_static_generationmodule_GenerationReading_descriptor =
      getDescriptor().getMessageTypes().get(18);
    internal_static_generationmodule_GenerationReading_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationReading_descriptor,
        new java.lang.String[] { "ConductingEquipmentTerminalReading", "PhaseMMTN", "ReadingMMTR", "ReadingMMXU", });
    internal_static_generationmodule_GenerationReadingProfile_descriptor =
      getDescriptor().getMessageTypes().get(19);
    internal_static_generationmodule_GenerationReadingProfile_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationReadingProfile_descriptor,
        new java.lang.String[] { "ReadingMessageInfo", "GeneratingUnit", "GenerationReading", });
    internal_static_generationmodule_GenerationPointStatus_descriptor =
      getDescriptor().getMessageTypes().get(20);
    internal_static_generationmodule_GenerationPointStatus_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationPointStatus_descriptor,
        new java.lang.String[] { "BlackStartEnabled", "FrequencySetPointEnabled", "PctHzDroop", "PctVDroop", "RampRates", "ReactivePwrSetPointEnabled", "RealPwrSetPointEnabled", "State", "SyncBackToGrid", "TransToIslndOnGridLossEnabled", "VoltageSetPointEnabled", "EnterServiceOperation", "HzWOperation", "LimitWOperation", "PFOperation", "TmHzTripOperation", "TmVoltTripOperation", "VArOperation", "VoltVarOperation", "VoltWOperation", "WVarOperation", });
    internal_static_generationmodule_GenerationEventAndStatusZGEN_descriptor =
      getDescriptor().getMessageTypes().get(21);
    internal_static_generationmodule_GenerationEventAndStatusZGEN_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationEventAndStatusZGEN_descriptor,
        new java.lang.String[] { "LogicalNodeForEventAndStatus", "AuxPwrSt", "DynamicTest", "EmgStop", "GnSynSt", "PointStatus", "Alrm", "GridConnectionState", "ManAlrmInfo", "OperatingState", });
    internal_static_generationmodule_GenerationEventZGEN_descriptor =
      getDescriptor().getMessageTypes().get(22);
    internal_static_generationmodule_GenerationEventZGEN_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationEventZGEN_descriptor,
        new java.lang.String[] { "GenerationEventAndStatusZGEN", });
    internal_static_generationmodule_GenerationEvent_descriptor =
      getDescriptor().getMessageTypes().get(23);
    internal_static_generationmodule_GenerationEvent_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationEvent_descriptor,
        new java.lang.String[] { "EventValue", "GenerationEventZGEN", });
    internal_static_generationmodule_GenerationEventProfile_descriptor =
      getDescriptor().getMessageTypes().get(24);
    internal_static_generationmodule_GenerationEventProfile_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationEventProfile_descriptor,
        new java.lang.String[] { "EventMessageInfo", "GeneratingUnit", "GenerationEvent", });
    internal_static_generationmodule_GenerationStatusZGEN_descriptor =
      getDescriptor().getMessageTypes().get(25);
    internal_static_generationmodule_GenerationStatusZGEN_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationStatusZGEN_descriptor,
        new java.lang.String[] { "GenerationEventAndStatusZGEN", });
    internal_static_generationmodule_GenerationStatus_descriptor =
      getDescriptor().getMessageTypes().get(26);
    internal_static_generationmodule_GenerationStatus_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationStatus_descriptor,
        new java.lang.String[] { "StatusValue", "GenerationStatusZGEN", });
    internal_static_generationmodule_GenerationStatusProfile_descriptor =
      getDescriptor().getMessageTypes().get(27);
    internal_static_generationmodule_GenerationStatusProfile_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_generationmodule_GenerationStatusProfile_descriptor,
        new java.lang.String[] { "StatusMessageInfo", "GeneratingUnit", "GenerationStatus", });
    com.google.protobuf.ExtensionRegistry registry =
        com.google.protobuf.ExtensionRegistry.newInstance();
    registry.add(openfmb.Uml.optionMultiplicityMin);
    registry.add(openfmb.Uml.optionOpenfmbProfile);
    registry.add(openfmb.Uml.optionParentMessage);
    registry.add(openfmb.Uml.optionRequiredField);
    com.google.protobuf.Descriptors.FileDescriptor
        .internalUpdateFileDescriptor(descriptor, registry);
    openfmb.Uml.getDescriptor();
    com.google.protobuf.WrappersProto.getDescriptor();
    openfmb.commonmodule.Commonmodule.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
