// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: switchmodule/switchmodule.proto

package openfmb.switchmodule;

public interface SwitchStatusXSWIOrBuilder extends
    // @@protoc_insertion_point(interface_extends:switchmodule.SwitchStatusXSWI)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.LogicalNodeForEventAndStatus logicalNodeForEventAndStatus = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the logicalNodeForEventAndStatus field is set.
   */
  boolean hasLogicalNodeForEventAndStatus();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.LogicalNodeForEventAndStatus logicalNodeForEventAndStatus = 1 [(.uml.option_parent_message) = true];</code>
   * @return The logicalNodeForEventAndStatus.
   */
  openfmb.commonmodule.LogicalNodeForEventAndStatus getLogicalNodeForEventAndStatus();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.LogicalNodeForEventAndStatus logicalNodeForEventAndStatus = 1 [(.uml.option_parent_message) = true];</code>
   */
  openfmb.commonmodule.LogicalNodeForEventAndStatusOrBuilder getLogicalNodeForEventAndStatusOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ENS_DynamicTestKind DynamicTest = 2;</code>
   * @return Whether the dynamicTest field is set.
   */
  boolean hasDynamicTest();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ENS_DynamicTestKind DynamicTest = 2;</code>
   * @return The dynamicTest.
   */
  openfmb.commonmodule.ENS_DynamicTestKind getDynamicTest();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ENS_DynamicTestKind DynamicTest = 2;</code>
   */
  openfmb.commonmodule.ENS_DynamicTestKindOrBuilder getDynamicTestOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.PhaseDPS Pos = 4;</code>
   * @return Whether the pos field is set.
   */
  boolean hasPos();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.PhaseDPS Pos = 4;</code>
   * @return The pos.
   */
  openfmb.commonmodule.PhaseDPS getPos();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.PhaseDPS Pos = 4;</code>
   */
  openfmb.commonmodule.PhaseDPSOrBuilder getPosOrBuilder();

  /**
   * <pre>
   * Fault latch: LT01=51A OR 51B OR 51C
   * </pre>
   *
   * <code>.commonmodule.PhaseSPS ProtectionPickup = 5 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the protectionPickup field is set.
   */
  boolean hasProtectionPickup();
  /**
   * <pre>
   * Fault latch: LT01=51A OR 51B OR 51C
   * </pre>
   *
   * <code>.commonmodule.PhaseSPS ProtectionPickup = 5 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The protectionPickup.
   */
  openfmb.commonmodule.PhaseSPS getProtectionPickup();
  /**
   * <pre>
   * Fault latch: LT01=51A OR 51B OR 51C
   * </pre>
   *
   * <code>.commonmodule.PhaseSPS ProtectionPickup = 5 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.commonmodule.PhaseSPSOrBuilder getProtectionPickupOrBuilder();
}
