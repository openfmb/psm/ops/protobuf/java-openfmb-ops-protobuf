// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: switchmodule/switchmodule.proto

package openfmb.switchmodule;

/**
 * <pre>
 * OpenFMB specialization for switch control:  LN: Circuit switch   Name: XSWI
 * </pre>
 *
 * Protobuf type {@code switchmodule.SwitchDiscreteControlXSWI}
 */
public final class SwitchDiscreteControlXSWI extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:switchmodule.SwitchDiscreteControlXSWI)
    SwitchDiscreteControlXSWIOrBuilder {
private static final long serialVersionUID = 0L;
  // Use SwitchDiscreteControlXSWI.newBuilder() to construct.
  private SwitchDiscreteControlXSWI(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private SwitchDiscreteControlXSWI() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new SwitchDiscreteControlXSWI();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.switchmodule.Switchmodule.internal_static_switchmodule_SwitchDiscreteControlXSWI_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.switchmodule.Switchmodule.internal_static_switchmodule_SwitchDiscreteControlXSWI_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.switchmodule.SwitchDiscreteControlXSWI.class, openfmb.switchmodule.SwitchDiscreteControlXSWI.Builder.class);
  }

  public static final int LOGICALNODEFORCONTROL_FIELD_NUMBER = 1;
  private openfmb.commonmodule.LogicalNodeForControl logicalNodeForControl_;
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the logicalNodeForControl field is set.
   */
  @java.lang.Override
  public boolean hasLogicalNodeForControl() {
    return logicalNodeForControl_ != null;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
   * @return The logicalNodeForControl.
   */
  @java.lang.Override
  public openfmb.commonmodule.LogicalNodeForControl getLogicalNodeForControl() {
    return logicalNodeForControl_ == null ? openfmb.commonmodule.LogicalNodeForControl.getDefaultInstance() : logicalNodeForControl_;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.LogicalNodeForControlOrBuilder getLogicalNodeForControlOrBuilder() {
    return logicalNodeForControl_ == null ? openfmb.commonmodule.LogicalNodeForControl.getDefaultInstance() : logicalNodeForControl_;
  }

  public static final int POS_FIELD_NUMBER = 2;
  private openfmb.commonmodule.PhaseDPC pos_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.PhaseDPC Pos = 2;</code>
   * @return Whether the pos field is set.
   */
  @java.lang.Override
  public boolean hasPos() {
    return pos_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.PhaseDPC Pos = 2;</code>
   * @return The pos.
   */
  @java.lang.Override
  public openfmb.commonmodule.PhaseDPC getPos() {
    return pos_ == null ? openfmb.commonmodule.PhaseDPC.getDefaultInstance() : pos_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.PhaseDPC Pos = 2;</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.PhaseDPCOrBuilder getPosOrBuilder() {
    return pos_ == null ? openfmb.commonmodule.PhaseDPC.getDefaultInstance() : pos_;
  }

  public static final int RESETPROTECTIONPICKUP_FIELD_NUMBER = 3;
  private openfmb.commonmodule.ControlSPC resetProtectionPickup_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ControlSPC ResetProtectionPickup = 3;</code>
   * @return Whether the resetProtectionPickup field is set.
   */
  @java.lang.Override
  public boolean hasResetProtectionPickup() {
    return resetProtectionPickup_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ControlSPC ResetProtectionPickup = 3;</code>
   * @return The resetProtectionPickup.
   */
  @java.lang.Override
  public openfmb.commonmodule.ControlSPC getResetProtectionPickup() {
    return resetProtectionPickup_ == null ? openfmb.commonmodule.ControlSPC.getDefaultInstance() : resetProtectionPickup_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ControlSPC ResetProtectionPickup = 3;</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.ControlSPCOrBuilder getResetProtectionPickupOrBuilder() {
    return resetProtectionPickup_ == null ? openfmb.commonmodule.ControlSPC.getDefaultInstance() : resetProtectionPickup_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (logicalNodeForControl_ != null) {
      output.writeMessage(1, getLogicalNodeForControl());
    }
    if (pos_ != null) {
      output.writeMessage(2, getPos());
    }
    if (resetProtectionPickup_ != null) {
      output.writeMessage(3, getResetProtectionPickup());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (logicalNodeForControl_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getLogicalNodeForControl());
    }
    if (pos_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getPos());
    }
    if (resetProtectionPickup_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(3, getResetProtectionPickup());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.switchmodule.SwitchDiscreteControlXSWI)) {
      return super.equals(obj);
    }
    openfmb.switchmodule.SwitchDiscreteControlXSWI other = (openfmb.switchmodule.SwitchDiscreteControlXSWI) obj;

    if (hasLogicalNodeForControl() != other.hasLogicalNodeForControl()) return false;
    if (hasLogicalNodeForControl()) {
      if (!getLogicalNodeForControl()
          .equals(other.getLogicalNodeForControl())) return false;
    }
    if (hasPos() != other.hasPos()) return false;
    if (hasPos()) {
      if (!getPos()
          .equals(other.getPos())) return false;
    }
    if (hasResetProtectionPickup() != other.hasResetProtectionPickup()) return false;
    if (hasResetProtectionPickup()) {
      if (!getResetProtectionPickup()
          .equals(other.getResetProtectionPickup())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasLogicalNodeForControl()) {
      hash = (37 * hash) + LOGICALNODEFORCONTROL_FIELD_NUMBER;
      hash = (53 * hash) + getLogicalNodeForControl().hashCode();
    }
    if (hasPos()) {
      hash = (37 * hash) + POS_FIELD_NUMBER;
      hash = (53 * hash) + getPos().hashCode();
    }
    if (hasResetProtectionPickup()) {
      hash = (37 * hash) + RESETPROTECTIONPICKUP_FIELD_NUMBER;
      hash = (53 * hash) + getResetProtectionPickup().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.switchmodule.SwitchDiscreteControlXSWI parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.switchmodule.SwitchDiscreteControlXSWI parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.switchmodule.SwitchDiscreteControlXSWI parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.switchmodule.SwitchDiscreteControlXSWI parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.switchmodule.SwitchDiscreteControlXSWI parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.switchmodule.SwitchDiscreteControlXSWI parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.switchmodule.SwitchDiscreteControlXSWI parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.switchmodule.SwitchDiscreteControlXSWI parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.switchmodule.SwitchDiscreteControlXSWI parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.switchmodule.SwitchDiscreteControlXSWI parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.switchmodule.SwitchDiscreteControlXSWI parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.switchmodule.SwitchDiscreteControlXSWI parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.switchmodule.SwitchDiscreteControlXSWI prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * OpenFMB specialization for switch control:  LN: Circuit switch   Name: XSWI
   * </pre>
   *
   * Protobuf type {@code switchmodule.SwitchDiscreteControlXSWI}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:switchmodule.SwitchDiscreteControlXSWI)
      openfmb.switchmodule.SwitchDiscreteControlXSWIOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.switchmodule.Switchmodule.internal_static_switchmodule_SwitchDiscreteControlXSWI_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.switchmodule.Switchmodule.internal_static_switchmodule_SwitchDiscreteControlXSWI_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.switchmodule.SwitchDiscreteControlXSWI.class, openfmb.switchmodule.SwitchDiscreteControlXSWI.Builder.class);
    }

    // Construct using openfmb.switchmodule.SwitchDiscreteControlXSWI.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      logicalNodeForControl_ = null;
      if (logicalNodeForControlBuilder_ != null) {
        logicalNodeForControlBuilder_.dispose();
        logicalNodeForControlBuilder_ = null;
      }
      pos_ = null;
      if (posBuilder_ != null) {
        posBuilder_.dispose();
        posBuilder_ = null;
      }
      resetProtectionPickup_ = null;
      if (resetProtectionPickupBuilder_ != null) {
        resetProtectionPickupBuilder_.dispose();
        resetProtectionPickupBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.switchmodule.Switchmodule.internal_static_switchmodule_SwitchDiscreteControlXSWI_descriptor;
    }

    @java.lang.Override
    public openfmb.switchmodule.SwitchDiscreteControlXSWI getDefaultInstanceForType() {
      return openfmb.switchmodule.SwitchDiscreteControlXSWI.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.switchmodule.SwitchDiscreteControlXSWI build() {
      openfmb.switchmodule.SwitchDiscreteControlXSWI result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.switchmodule.SwitchDiscreteControlXSWI buildPartial() {
      openfmb.switchmodule.SwitchDiscreteControlXSWI result = new openfmb.switchmodule.SwitchDiscreteControlXSWI(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.switchmodule.SwitchDiscreteControlXSWI result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.logicalNodeForControl_ = logicalNodeForControlBuilder_ == null
            ? logicalNodeForControl_
            : logicalNodeForControlBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.pos_ = posBuilder_ == null
            ? pos_
            : posBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000004) != 0)) {
        result.resetProtectionPickup_ = resetProtectionPickupBuilder_ == null
            ? resetProtectionPickup_
            : resetProtectionPickupBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.switchmodule.SwitchDiscreteControlXSWI) {
        return mergeFrom((openfmb.switchmodule.SwitchDiscreteControlXSWI)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.switchmodule.SwitchDiscreteControlXSWI other) {
      if (other == openfmb.switchmodule.SwitchDiscreteControlXSWI.getDefaultInstance()) return this;
      if (other.hasLogicalNodeForControl()) {
        mergeLogicalNodeForControl(other.getLogicalNodeForControl());
      }
      if (other.hasPos()) {
        mergePos(other.getPos());
      }
      if (other.hasResetProtectionPickup()) {
        mergeResetProtectionPickup(other.getResetProtectionPickup());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getLogicalNodeForControlFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getPosFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            case 26: {
              input.readMessage(
                  getResetProtectionPickupFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000004;
              break;
            } // case 26
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.LogicalNodeForControl logicalNodeForControl_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.LogicalNodeForControl, openfmb.commonmodule.LogicalNodeForControl.Builder, openfmb.commonmodule.LogicalNodeForControlOrBuilder> logicalNodeForControlBuilder_;
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     * @return Whether the logicalNodeForControl field is set.
     */
    public boolean hasLogicalNodeForControl() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     * @return The logicalNodeForControl.
     */
    public openfmb.commonmodule.LogicalNodeForControl getLogicalNodeForControl() {
      if (logicalNodeForControlBuilder_ == null) {
        return logicalNodeForControl_ == null ? openfmb.commonmodule.LogicalNodeForControl.getDefaultInstance() : logicalNodeForControl_;
      } else {
        return logicalNodeForControlBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setLogicalNodeForControl(openfmb.commonmodule.LogicalNodeForControl value) {
      if (logicalNodeForControlBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        logicalNodeForControl_ = value;
      } else {
        logicalNodeForControlBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setLogicalNodeForControl(
        openfmb.commonmodule.LogicalNodeForControl.Builder builderForValue) {
      if (logicalNodeForControlBuilder_ == null) {
        logicalNodeForControl_ = builderForValue.build();
      } else {
        logicalNodeForControlBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder mergeLogicalNodeForControl(openfmb.commonmodule.LogicalNodeForControl value) {
      if (logicalNodeForControlBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          logicalNodeForControl_ != null &&
          logicalNodeForControl_ != openfmb.commonmodule.LogicalNodeForControl.getDefaultInstance()) {
          getLogicalNodeForControlBuilder().mergeFrom(value);
        } else {
          logicalNodeForControl_ = value;
        }
      } else {
        logicalNodeForControlBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder clearLogicalNodeForControl() {
      bitField0_ = (bitField0_ & ~0x00000001);
      logicalNodeForControl_ = null;
      if (logicalNodeForControlBuilder_ != null) {
        logicalNodeForControlBuilder_.dispose();
        logicalNodeForControlBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.LogicalNodeForControl.Builder getLogicalNodeForControlBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getLogicalNodeForControlFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.LogicalNodeForControlOrBuilder getLogicalNodeForControlOrBuilder() {
      if (logicalNodeForControlBuilder_ != null) {
        return logicalNodeForControlBuilder_.getMessageOrBuilder();
      } else {
        return logicalNodeForControl_ == null ?
            openfmb.commonmodule.LogicalNodeForControl.getDefaultInstance() : logicalNodeForControl_;
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.LogicalNodeForControl logicalNodeForControl = 1 [(.uml.option_parent_message) = true];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.LogicalNodeForControl, openfmb.commonmodule.LogicalNodeForControl.Builder, openfmb.commonmodule.LogicalNodeForControlOrBuilder> 
        getLogicalNodeForControlFieldBuilder() {
      if (logicalNodeForControlBuilder_ == null) {
        logicalNodeForControlBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.LogicalNodeForControl, openfmb.commonmodule.LogicalNodeForControl.Builder, openfmb.commonmodule.LogicalNodeForControlOrBuilder>(
                getLogicalNodeForControl(),
                getParentForChildren(),
                isClean());
        logicalNodeForControl_ = null;
      }
      return logicalNodeForControlBuilder_;
    }

    private openfmb.commonmodule.PhaseDPC pos_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.PhaseDPC, openfmb.commonmodule.PhaseDPC.Builder, openfmb.commonmodule.PhaseDPCOrBuilder> posBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.PhaseDPC Pos = 2;</code>
     * @return Whether the pos field is set.
     */
    public boolean hasPos() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.PhaseDPC Pos = 2;</code>
     * @return The pos.
     */
    public openfmb.commonmodule.PhaseDPC getPos() {
      if (posBuilder_ == null) {
        return pos_ == null ? openfmb.commonmodule.PhaseDPC.getDefaultInstance() : pos_;
      } else {
        return posBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.PhaseDPC Pos = 2;</code>
     */
    public Builder setPos(openfmb.commonmodule.PhaseDPC value) {
      if (posBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        pos_ = value;
      } else {
        posBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.PhaseDPC Pos = 2;</code>
     */
    public Builder setPos(
        openfmb.commonmodule.PhaseDPC.Builder builderForValue) {
      if (posBuilder_ == null) {
        pos_ = builderForValue.build();
      } else {
        posBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.PhaseDPC Pos = 2;</code>
     */
    public Builder mergePos(openfmb.commonmodule.PhaseDPC value) {
      if (posBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          pos_ != null &&
          pos_ != openfmb.commonmodule.PhaseDPC.getDefaultInstance()) {
          getPosBuilder().mergeFrom(value);
        } else {
          pos_ = value;
        }
      } else {
        posBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.PhaseDPC Pos = 2;</code>
     */
    public Builder clearPos() {
      bitField0_ = (bitField0_ & ~0x00000002);
      pos_ = null;
      if (posBuilder_ != null) {
        posBuilder_.dispose();
        posBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.PhaseDPC Pos = 2;</code>
     */
    public openfmb.commonmodule.PhaseDPC.Builder getPosBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getPosFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.PhaseDPC Pos = 2;</code>
     */
    public openfmb.commonmodule.PhaseDPCOrBuilder getPosOrBuilder() {
      if (posBuilder_ != null) {
        return posBuilder_.getMessageOrBuilder();
      } else {
        return pos_ == null ?
            openfmb.commonmodule.PhaseDPC.getDefaultInstance() : pos_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.PhaseDPC Pos = 2;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.PhaseDPC, openfmb.commonmodule.PhaseDPC.Builder, openfmb.commonmodule.PhaseDPCOrBuilder> 
        getPosFieldBuilder() {
      if (posBuilder_ == null) {
        posBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.PhaseDPC, openfmb.commonmodule.PhaseDPC.Builder, openfmb.commonmodule.PhaseDPCOrBuilder>(
                getPos(),
                getParentForChildren(),
                isClean());
        pos_ = null;
      }
      return posBuilder_;
    }

    private openfmb.commonmodule.ControlSPC resetProtectionPickup_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ControlSPC, openfmb.commonmodule.ControlSPC.Builder, openfmb.commonmodule.ControlSPCOrBuilder> resetProtectionPickupBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlSPC ResetProtectionPickup = 3;</code>
     * @return Whether the resetProtectionPickup field is set.
     */
    public boolean hasResetProtectionPickup() {
      return ((bitField0_ & 0x00000004) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlSPC ResetProtectionPickup = 3;</code>
     * @return The resetProtectionPickup.
     */
    public openfmb.commonmodule.ControlSPC getResetProtectionPickup() {
      if (resetProtectionPickupBuilder_ == null) {
        return resetProtectionPickup_ == null ? openfmb.commonmodule.ControlSPC.getDefaultInstance() : resetProtectionPickup_;
      } else {
        return resetProtectionPickupBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlSPC ResetProtectionPickup = 3;</code>
     */
    public Builder setResetProtectionPickup(openfmb.commonmodule.ControlSPC value) {
      if (resetProtectionPickupBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        resetProtectionPickup_ = value;
      } else {
        resetProtectionPickupBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlSPC ResetProtectionPickup = 3;</code>
     */
    public Builder setResetProtectionPickup(
        openfmb.commonmodule.ControlSPC.Builder builderForValue) {
      if (resetProtectionPickupBuilder_ == null) {
        resetProtectionPickup_ = builderForValue.build();
      } else {
        resetProtectionPickupBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlSPC ResetProtectionPickup = 3;</code>
     */
    public Builder mergeResetProtectionPickup(openfmb.commonmodule.ControlSPC value) {
      if (resetProtectionPickupBuilder_ == null) {
        if (((bitField0_ & 0x00000004) != 0) &&
          resetProtectionPickup_ != null &&
          resetProtectionPickup_ != openfmb.commonmodule.ControlSPC.getDefaultInstance()) {
          getResetProtectionPickupBuilder().mergeFrom(value);
        } else {
          resetProtectionPickup_ = value;
        }
      } else {
        resetProtectionPickupBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlSPC ResetProtectionPickup = 3;</code>
     */
    public Builder clearResetProtectionPickup() {
      bitField0_ = (bitField0_ & ~0x00000004);
      resetProtectionPickup_ = null;
      if (resetProtectionPickupBuilder_ != null) {
        resetProtectionPickupBuilder_.dispose();
        resetProtectionPickupBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlSPC ResetProtectionPickup = 3;</code>
     */
    public openfmb.commonmodule.ControlSPC.Builder getResetProtectionPickupBuilder() {
      bitField0_ |= 0x00000004;
      onChanged();
      return getResetProtectionPickupFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlSPC ResetProtectionPickup = 3;</code>
     */
    public openfmb.commonmodule.ControlSPCOrBuilder getResetProtectionPickupOrBuilder() {
      if (resetProtectionPickupBuilder_ != null) {
        return resetProtectionPickupBuilder_.getMessageOrBuilder();
      } else {
        return resetProtectionPickup_ == null ?
            openfmb.commonmodule.ControlSPC.getDefaultInstance() : resetProtectionPickup_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ControlSPC ResetProtectionPickup = 3;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ControlSPC, openfmb.commonmodule.ControlSPC.Builder, openfmb.commonmodule.ControlSPCOrBuilder> 
        getResetProtectionPickupFieldBuilder() {
      if (resetProtectionPickupBuilder_ == null) {
        resetProtectionPickupBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.ControlSPC, openfmb.commonmodule.ControlSPC.Builder, openfmb.commonmodule.ControlSPCOrBuilder>(
                getResetProtectionPickup(),
                getParentForChildren(),
                isClean());
        resetProtectionPickup_ = null;
      }
      return resetProtectionPickupBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:switchmodule.SwitchDiscreteControlXSWI)
  }

  // @@protoc_insertion_point(class_scope:switchmodule.SwitchDiscreteControlXSWI)
  private static final openfmb.switchmodule.SwitchDiscreteControlXSWI DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.switchmodule.SwitchDiscreteControlXSWI();
  }

  public static openfmb.switchmodule.SwitchDiscreteControlXSWI getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<SwitchDiscreteControlXSWI>
      PARSER = new com.google.protobuf.AbstractParser<SwitchDiscreteControlXSWI>() {
    @java.lang.Override
    public SwitchDiscreteControlXSWI parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<SwitchDiscreteControlXSWI> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<SwitchDiscreteControlXSWI> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.switchmodule.SwitchDiscreteControlXSWI getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

