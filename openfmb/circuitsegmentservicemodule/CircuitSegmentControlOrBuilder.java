// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: circuitsegmentservicemodule/circuitsegmentservicemodule.proto

package openfmb.circuitsegmentservicemodule;

public interface CircuitSegmentControlOrBuilder extends
    // @@protoc_insertion_point(interface_extends:circuitsegmentservicemodule.CircuitSegmentControl)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the identifiedObject field is set.
   */
  boolean hasIdentifiedObject();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
   * @return The identifiedObject.
   */
  openfmb.commonmodule.IdentifiedObject getIdentifiedObject();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.IdentifiedObject identifiedObject = 1 [(.uml.option_parent_message) = true];</code>
   */
  openfmb.commonmodule.IdentifiedObjectOrBuilder getIdentifiedObjectOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   * @return Whether the check field is set.
   */
  boolean hasCheck();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   * @return The check.
   */
  openfmb.commonmodule.CheckConditions getCheck();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.CheckConditions check = 2;</code>
   */
  openfmb.commonmodule.CheckConditionsOrBuilder getCheckOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.circuitsegmentservicemodule.CircuitSegmentControlDCSC circuitSegmentControlDCSC = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the circuitSegmentControlDCSC field is set.
   */
  boolean hasCircuitSegmentControlDCSC();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.circuitsegmentservicemodule.CircuitSegmentControlDCSC circuitSegmentControlDCSC = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The circuitSegmentControlDCSC.
   */
  openfmb.circuitsegmentservicemodule.CircuitSegmentControlDCSC getCircuitSegmentControlDCSC();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.circuitsegmentservicemodule.CircuitSegmentControlDCSC circuitSegmentControlDCSC = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.circuitsegmentservicemodule.CircuitSegmentControlDCSCOrBuilder getCircuitSegmentControlDCSCOrBuilder();
}
