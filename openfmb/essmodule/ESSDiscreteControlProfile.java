// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: essmodule/essmodule.proto

package openfmb.essmodule;

/**
 * <pre>
 * Cap bank discrete control profile.  Instructs an end device (or an end device group) to perform
 * a specified action.
 * </pre>
 *
 * Protobuf type {@code essmodule.ESSDiscreteControlProfile}
 */
public final class ESSDiscreteControlProfile extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:essmodule.ESSDiscreteControlProfile)
    ESSDiscreteControlProfileOrBuilder {
private static final long serialVersionUID = 0L;
  // Use ESSDiscreteControlProfile.newBuilder() to construct.
  private ESSDiscreteControlProfile(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private ESSDiscreteControlProfile() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new ESSDiscreteControlProfile();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.essmodule.Essmodule.internal_static_essmodule_ESSDiscreteControlProfile_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.essmodule.Essmodule.internal_static_essmodule_ESSDiscreteControlProfile_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.essmodule.ESSDiscreteControlProfile.class, openfmb.essmodule.ESSDiscreteControlProfile.Builder.class);
  }

  public static final int CONTROLMESSAGEINFO_FIELD_NUMBER = 1;
  private openfmb.commonmodule.ControlMessageInfo controlMessageInfo_;
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the controlMessageInfo field is set.
   */
  @java.lang.Override
  public boolean hasControlMessageInfo() {
    return controlMessageInfo_ != null;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   * @return The controlMessageInfo.
   */
  @java.lang.Override
  public openfmb.commonmodule.ControlMessageInfo getControlMessageInfo() {
    return controlMessageInfo_ == null ? openfmb.commonmodule.ControlMessageInfo.getDefaultInstance() : controlMessageInfo_;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.ControlMessageInfoOrBuilder getControlMessageInfoOrBuilder() {
    return controlMessageInfo_ == null ? openfmb.commonmodule.ControlMessageInfo.getDefaultInstance() : controlMessageInfo_;
  }

  public static final int ESS_FIELD_NUMBER = 2;
  private openfmb.commonmodule.ESS ess_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ESS ess = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the ess field is set.
   */
  @java.lang.Override
  public boolean hasEss() {
    return ess_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ESS ess = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The ess.
   */
  @java.lang.Override
  public openfmb.commonmodule.ESS getEss() {
    return ess_ == null ? openfmb.commonmodule.ESS.getDefaultInstance() : ess_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ESS ess = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.ESSOrBuilder getEssOrBuilder() {
    return ess_ == null ? openfmb.commonmodule.ESS.getDefaultInstance() : ess_;
  }

  public static final int ESSDISCRETECONTROL_FIELD_NUMBER = 3;
  private openfmb.essmodule.ESSDiscreteControl essDiscreteControl_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.essmodule.ESSDiscreteControl essDiscreteControl = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the essDiscreteControl field is set.
   */
  @java.lang.Override
  public boolean hasEssDiscreteControl() {
    return essDiscreteControl_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.essmodule.ESSDiscreteControl essDiscreteControl = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The essDiscreteControl.
   */
  @java.lang.Override
  public openfmb.essmodule.ESSDiscreteControl getEssDiscreteControl() {
    return essDiscreteControl_ == null ? openfmb.essmodule.ESSDiscreteControl.getDefaultInstance() : essDiscreteControl_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.essmodule.ESSDiscreteControl essDiscreteControl = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.essmodule.ESSDiscreteControlOrBuilder getEssDiscreteControlOrBuilder() {
    return essDiscreteControl_ == null ? openfmb.essmodule.ESSDiscreteControl.getDefaultInstance() : essDiscreteControl_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (controlMessageInfo_ != null) {
      output.writeMessage(1, getControlMessageInfo());
    }
    if (ess_ != null) {
      output.writeMessage(2, getEss());
    }
    if (essDiscreteControl_ != null) {
      output.writeMessage(3, getEssDiscreteControl());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (controlMessageInfo_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getControlMessageInfo());
    }
    if (ess_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getEss());
    }
    if (essDiscreteControl_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(3, getEssDiscreteControl());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.essmodule.ESSDiscreteControlProfile)) {
      return super.equals(obj);
    }
    openfmb.essmodule.ESSDiscreteControlProfile other = (openfmb.essmodule.ESSDiscreteControlProfile) obj;

    if (hasControlMessageInfo() != other.hasControlMessageInfo()) return false;
    if (hasControlMessageInfo()) {
      if (!getControlMessageInfo()
          .equals(other.getControlMessageInfo())) return false;
    }
    if (hasEss() != other.hasEss()) return false;
    if (hasEss()) {
      if (!getEss()
          .equals(other.getEss())) return false;
    }
    if (hasEssDiscreteControl() != other.hasEssDiscreteControl()) return false;
    if (hasEssDiscreteControl()) {
      if (!getEssDiscreteControl()
          .equals(other.getEssDiscreteControl())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasControlMessageInfo()) {
      hash = (37 * hash) + CONTROLMESSAGEINFO_FIELD_NUMBER;
      hash = (53 * hash) + getControlMessageInfo().hashCode();
    }
    if (hasEss()) {
      hash = (37 * hash) + ESS_FIELD_NUMBER;
      hash = (53 * hash) + getEss().hashCode();
    }
    if (hasEssDiscreteControl()) {
      hash = (37 * hash) + ESSDISCRETECONTROL_FIELD_NUMBER;
      hash = (53 * hash) + getEssDiscreteControl().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.essmodule.ESSDiscreteControlProfile parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.essmodule.ESSDiscreteControlProfile parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.essmodule.ESSDiscreteControlProfile parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.essmodule.ESSDiscreteControlProfile parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.essmodule.ESSDiscreteControlProfile parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.essmodule.ESSDiscreteControlProfile parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.essmodule.ESSDiscreteControlProfile parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.essmodule.ESSDiscreteControlProfile parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.essmodule.ESSDiscreteControlProfile parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.essmodule.ESSDiscreteControlProfile parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.essmodule.ESSDiscreteControlProfile parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.essmodule.ESSDiscreteControlProfile parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.essmodule.ESSDiscreteControlProfile prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Cap bank discrete control profile.  Instructs an end device (or an end device group) to perform
   * a specified action.
   * </pre>
   *
   * Protobuf type {@code essmodule.ESSDiscreteControlProfile}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:essmodule.ESSDiscreteControlProfile)
      openfmb.essmodule.ESSDiscreteControlProfileOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.essmodule.Essmodule.internal_static_essmodule_ESSDiscreteControlProfile_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.essmodule.Essmodule.internal_static_essmodule_ESSDiscreteControlProfile_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.essmodule.ESSDiscreteControlProfile.class, openfmb.essmodule.ESSDiscreteControlProfile.Builder.class);
    }

    // Construct using openfmb.essmodule.ESSDiscreteControlProfile.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      controlMessageInfo_ = null;
      if (controlMessageInfoBuilder_ != null) {
        controlMessageInfoBuilder_.dispose();
        controlMessageInfoBuilder_ = null;
      }
      ess_ = null;
      if (essBuilder_ != null) {
        essBuilder_.dispose();
        essBuilder_ = null;
      }
      essDiscreteControl_ = null;
      if (essDiscreteControlBuilder_ != null) {
        essDiscreteControlBuilder_.dispose();
        essDiscreteControlBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.essmodule.Essmodule.internal_static_essmodule_ESSDiscreteControlProfile_descriptor;
    }

    @java.lang.Override
    public openfmb.essmodule.ESSDiscreteControlProfile getDefaultInstanceForType() {
      return openfmb.essmodule.ESSDiscreteControlProfile.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.essmodule.ESSDiscreteControlProfile build() {
      openfmb.essmodule.ESSDiscreteControlProfile result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.essmodule.ESSDiscreteControlProfile buildPartial() {
      openfmb.essmodule.ESSDiscreteControlProfile result = new openfmb.essmodule.ESSDiscreteControlProfile(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.essmodule.ESSDiscreteControlProfile result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.controlMessageInfo_ = controlMessageInfoBuilder_ == null
            ? controlMessageInfo_
            : controlMessageInfoBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.ess_ = essBuilder_ == null
            ? ess_
            : essBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000004) != 0)) {
        result.essDiscreteControl_ = essDiscreteControlBuilder_ == null
            ? essDiscreteControl_
            : essDiscreteControlBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.essmodule.ESSDiscreteControlProfile) {
        return mergeFrom((openfmb.essmodule.ESSDiscreteControlProfile)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.essmodule.ESSDiscreteControlProfile other) {
      if (other == openfmb.essmodule.ESSDiscreteControlProfile.getDefaultInstance()) return this;
      if (other.hasControlMessageInfo()) {
        mergeControlMessageInfo(other.getControlMessageInfo());
      }
      if (other.hasEss()) {
        mergeEss(other.getEss());
      }
      if (other.hasEssDiscreteControl()) {
        mergeEssDiscreteControl(other.getEssDiscreteControl());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getControlMessageInfoFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getEssFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            case 26: {
              input.readMessage(
                  getEssDiscreteControlFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000004;
              break;
            } // case 26
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.ControlMessageInfo controlMessageInfo_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ControlMessageInfo, openfmb.commonmodule.ControlMessageInfo.Builder, openfmb.commonmodule.ControlMessageInfoOrBuilder> controlMessageInfoBuilder_;
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     * @return Whether the controlMessageInfo field is set.
     */
    public boolean hasControlMessageInfo() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     * @return The controlMessageInfo.
     */
    public openfmb.commonmodule.ControlMessageInfo getControlMessageInfo() {
      if (controlMessageInfoBuilder_ == null) {
        return controlMessageInfo_ == null ? openfmb.commonmodule.ControlMessageInfo.getDefaultInstance() : controlMessageInfo_;
      } else {
        return controlMessageInfoBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setControlMessageInfo(openfmb.commonmodule.ControlMessageInfo value) {
      if (controlMessageInfoBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        controlMessageInfo_ = value;
      } else {
        controlMessageInfoBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setControlMessageInfo(
        openfmb.commonmodule.ControlMessageInfo.Builder builderForValue) {
      if (controlMessageInfoBuilder_ == null) {
        controlMessageInfo_ = builderForValue.build();
      } else {
        controlMessageInfoBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder mergeControlMessageInfo(openfmb.commonmodule.ControlMessageInfo value) {
      if (controlMessageInfoBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          controlMessageInfo_ != null &&
          controlMessageInfo_ != openfmb.commonmodule.ControlMessageInfo.getDefaultInstance()) {
          getControlMessageInfoBuilder().mergeFrom(value);
        } else {
          controlMessageInfo_ = value;
        }
      } else {
        controlMessageInfoBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder clearControlMessageInfo() {
      bitField0_ = (bitField0_ & ~0x00000001);
      controlMessageInfo_ = null;
      if (controlMessageInfoBuilder_ != null) {
        controlMessageInfoBuilder_.dispose();
        controlMessageInfoBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.ControlMessageInfo.Builder getControlMessageInfoBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getControlMessageInfoFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.ControlMessageInfoOrBuilder getControlMessageInfoOrBuilder() {
      if (controlMessageInfoBuilder_ != null) {
        return controlMessageInfoBuilder_.getMessageOrBuilder();
      } else {
        return controlMessageInfo_ == null ?
            openfmb.commonmodule.ControlMessageInfo.getDefaultInstance() : controlMessageInfo_;
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.ControlMessageInfo controlMessageInfo = 1 [(.uml.option_parent_message) = true];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ControlMessageInfo, openfmb.commonmodule.ControlMessageInfo.Builder, openfmb.commonmodule.ControlMessageInfoOrBuilder> 
        getControlMessageInfoFieldBuilder() {
      if (controlMessageInfoBuilder_ == null) {
        controlMessageInfoBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.ControlMessageInfo, openfmb.commonmodule.ControlMessageInfo.Builder, openfmb.commonmodule.ControlMessageInfoOrBuilder>(
                getControlMessageInfo(),
                getParentForChildren(),
                isClean());
        controlMessageInfo_ = null;
      }
      return controlMessageInfoBuilder_;
    }

    private openfmb.commonmodule.ESS ess_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ESS, openfmb.commonmodule.ESS.Builder, openfmb.commonmodule.ESSOrBuilder> essBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ESS ess = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return Whether the ess field is set.
     */
    public boolean hasEss() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ESS ess = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The ess.
     */
    public openfmb.commonmodule.ESS getEss() {
      if (essBuilder_ == null) {
        return ess_ == null ? openfmb.commonmodule.ESS.getDefaultInstance() : ess_;
      } else {
        return essBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ESS ess = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setEss(openfmb.commonmodule.ESS value) {
      if (essBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ess_ = value;
      } else {
        essBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ESS ess = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setEss(
        openfmb.commonmodule.ESS.Builder builderForValue) {
      if (essBuilder_ == null) {
        ess_ = builderForValue.build();
      } else {
        essBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ESS ess = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder mergeEss(openfmb.commonmodule.ESS value) {
      if (essBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          ess_ != null &&
          ess_ != openfmb.commonmodule.ESS.getDefaultInstance()) {
          getEssBuilder().mergeFrom(value);
        } else {
          ess_ = value;
        }
      } else {
        essBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ESS ess = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder clearEss() {
      bitField0_ = (bitField0_ & ~0x00000002);
      ess_ = null;
      if (essBuilder_ != null) {
        essBuilder_.dispose();
        essBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ESS ess = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.commonmodule.ESS.Builder getEssBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getEssFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ESS ess = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.commonmodule.ESSOrBuilder getEssOrBuilder() {
      if (essBuilder_ != null) {
        return essBuilder_.getMessageOrBuilder();
      } else {
        return ess_ == null ?
            openfmb.commonmodule.ESS.getDefaultInstance() : ess_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.commonmodule.ESS ess = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.ESS, openfmb.commonmodule.ESS.Builder, openfmb.commonmodule.ESSOrBuilder> 
        getEssFieldBuilder() {
      if (essBuilder_ == null) {
        essBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.ESS, openfmb.commonmodule.ESS.Builder, openfmb.commonmodule.ESSOrBuilder>(
                getEss(),
                getParentForChildren(),
                isClean());
        ess_ = null;
      }
      return essBuilder_;
    }

    private openfmb.essmodule.ESSDiscreteControl essDiscreteControl_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.essmodule.ESSDiscreteControl, openfmb.essmodule.ESSDiscreteControl.Builder, openfmb.essmodule.ESSDiscreteControlOrBuilder> essDiscreteControlBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSDiscreteControl essDiscreteControl = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return Whether the essDiscreteControl field is set.
     */
    public boolean hasEssDiscreteControl() {
      return ((bitField0_ & 0x00000004) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSDiscreteControl essDiscreteControl = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The essDiscreteControl.
     */
    public openfmb.essmodule.ESSDiscreteControl getEssDiscreteControl() {
      if (essDiscreteControlBuilder_ == null) {
        return essDiscreteControl_ == null ? openfmb.essmodule.ESSDiscreteControl.getDefaultInstance() : essDiscreteControl_;
      } else {
        return essDiscreteControlBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSDiscreteControl essDiscreteControl = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setEssDiscreteControl(openfmb.essmodule.ESSDiscreteControl value) {
      if (essDiscreteControlBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        essDiscreteControl_ = value;
      } else {
        essDiscreteControlBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSDiscreteControl essDiscreteControl = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setEssDiscreteControl(
        openfmb.essmodule.ESSDiscreteControl.Builder builderForValue) {
      if (essDiscreteControlBuilder_ == null) {
        essDiscreteControl_ = builderForValue.build();
      } else {
        essDiscreteControlBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSDiscreteControl essDiscreteControl = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder mergeEssDiscreteControl(openfmb.essmodule.ESSDiscreteControl value) {
      if (essDiscreteControlBuilder_ == null) {
        if (((bitField0_ & 0x00000004) != 0) &&
          essDiscreteControl_ != null &&
          essDiscreteControl_ != openfmb.essmodule.ESSDiscreteControl.getDefaultInstance()) {
          getEssDiscreteControlBuilder().mergeFrom(value);
        } else {
          essDiscreteControl_ = value;
        }
      } else {
        essDiscreteControlBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSDiscreteControl essDiscreteControl = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder clearEssDiscreteControl() {
      bitField0_ = (bitField0_ & ~0x00000004);
      essDiscreteControl_ = null;
      if (essDiscreteControlBuilder_ != null) {
        essDiscreteControlBuilder_.dispose();
        essDiscreteControlBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSDiscreteControl essDiscreteControl = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.essmodule.ESSDiscreteControl.Builder getEssDiscreteControlBuilder() {
      bitField0_ |= 0x00000004;
      onChanged();
      return getEssDiscreteControlFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSDiscreteControl essDiscreteControl = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.essmodule.ESSDiscreteControlOrBuilder getEssDiscreteControlOrBuilder() {
      if (essDiscreteControlBuilder_ != null) {
        return essDiscreteControlBuilder_.getMessageOrBuilder();
      } else {
        return essDiscreteControl_ == null ?
            openfmb.essmodule.ESSDiscreteControl.getDefaultInstance() : essDiscreteControl_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSDiscreteControl essDiscreteControl = 3 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.essmodule.ESSDiscreteControl, openfmb.essmodule.ESSDiscreteControl.Builder, openfmb.essmodule.ESSDiscreteControlOrBuilder> 
        getEssDiscreteControlFieldBuilder() {
      if (essDiscreteControlBuilder_ == null) {
        essDiscreteControlBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.essmodule.ESSDiscreteControl, openfmb.essmodule.ESSDiscreteControl.Builder, openfmb.essmodule.ESSDiscreteControlOrBuilder>(
                getEssDiscreteControl(),
                getParentForChildren(),
                isClean());
        essDiscreteControl_ = null;
      }
      return essDiscreteControlBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:essmodule.ESSDiscreteControlProfile)
  }

  // @@protoc_insertion_point(class_scope:essmodule.ESSDiscreteControlProfile)
  private static final openfmb.essmodule.ESSDiscreteControlProfile DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.essmodule.ESSDiscreteControlProfile();
  }

  public static openfmb.essmodule.ESSDiscreteControlProfile getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<ESSDiscreteControlProfile>
      PARSER = new com.google.protobuf.AbstractParser<ESSDiscreteControlProfile>() {
    @java.lang.Override
    public ESSDiscreteControlProfile parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<ESSDiscreteControlProfile> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<ESSDiscreteControlProfile> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.essmodule.ESSDiscreteControlProfile getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

