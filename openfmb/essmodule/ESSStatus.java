// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: essmodule/essmodule.proto

package openfmb.essmodule;

/**
 * <pre>
 * ESS status
 * </pre>
 *
 * Protobuf type {@code essmodule.ESSStatus}
 */
public final class ESSStatus extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:essmodule.ESSStatus)
    ESSStatusOrBuilder {
private static final long serialVersionUID = 0L;
  // Use ESSStatus.newBuilder() to construct.
  private ESSStatus(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private ESSStatus() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new ESSStatus();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.essmodule.Essmodule.internal_static_essmodule_ESSStatus_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.essmodule.Essmodule.internal_static_essmodule_ESSStatus_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.essmodule.ESSStatus.class, openfmb.essmodule.ESSStatus.Builder.class);
  }

  public static final int STATUSVALUE_FIELD_NUMBER = 1;
  private openfmb.commonmodule.StatusValue statusValue_;
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the statusValue field is set.
   */
  @java.lang.Override
  public boolean hasStatusValue() {
    return statusValue_ != null;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
   * @return The statusValue.
   */
  @java.lang.Override
  public openfmb.commonmodule.StatusValue getStatusValue() {
    return statusValue_ == null ? openfmb.commonmodule.StatusValue.getDefaultInstance() : statusValue_;
  }
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
   */
  @java.lang.Override
  public openfmb.commonmodule.StatusValueOrBuilder getStatusValueOrBuilder() {
    return statusValue_ == null ? openfmb.commonmodule.StatusValue.getDefaultInstance() : statusValue_;
  }

  public static final int ESSSTATUSZBAT_FIELD_NUMBER = 2;
  private openfmb.essmodule.EssStatusZBAT essStatusZBAT_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.essmodule.EssStatusZBAT essStatusZBAT = 2;</code>
   * @return Whether the essStatusZBAT field is set.
   */
  @java.lang.Override
  public boolean hasEssStatusZBAT() {
    return essStatusZBAT_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.essmodule.EssStatusZBAT essStatusZBAT = 2;</code>
   * @return The essStatusZBAT.
   */
  @java.lang.Override
  public openfmb.essmodule.EssStatusZBAT getEssStatusZBAT() {
    return essStatusZBAT_ == null ? openfmb.essmodule.EssStatusZBAT.getDefaultInstance() : essStatusZBAT_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.essmodule.EssStatusZBAT essStatusZBAT = 2;</code>
   */
  @java.lang.Override
  public openfmb.essmodule.EssStatusZBATOrBuilder getEssStatusZBATOrBuilder() {
    return essStatusZBAT_ == null ? openfmb.essmodule.EssStatusZBAT.getDefaultInstance() : essStatusZBAT_;
  }

  public static final int ESSSTATUSZGEN_FIELD_NUMBER = 3;
  private openfmb.essmodule.ESSStatusZGEN essStatusZGEN_;
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.essmodule.ESSStatusZGEN essStatusZGEN = 3;</code>
   * @return Whether the essStatusZGEN field is set.
   */
  @java.lang.Override
  public boolean hasEssStatusZGEN() {
    return essStatusZGEN_ != null;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.essmodule.ESSStatusZGEN essStatusZGEN = 3;</code>
   * @return The essStatusZGEN.
   */
  @java.lang.Override
  public openfmb.essmodule.ESSStatusZGEN getEssStatusZGEN() {
    return essStatusZGEN_ == null ? openfmb.essmodule.ESSStatusZGEN.getDefaultInstance() : essStatusZGEN_;
  }
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.essmodule.ESSStatusZGEN essStatusZGEN = 3;</code>
   */
  @java.lang.Override
  public openfmb.essmodule.ESSStatusZGENOrBuilder getEssStatusZGENOrBuilder() {
    return essStatusZGEN_ == null ? openfmb.essmodule.ESSStatusZGEN.getDefaultInstance() : essStatusZGEN_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (statusValue_ != null) {
      output.writeMessage(1, getStatusValue());
    }
    if (essStatusZBAT_ != null) {
      output.writeMessage(2, getEssStatusZBAT());
    }
    if (essStatusZGEN_ != null) {
      output.writeMessage(3, getEssStatusZGEN());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (statusValue_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getStatusValue());
    }
    if (essStatusZBAT_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getEssStatusZBAT());
    }
    if (essStatusZGEN_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(3, getEssStatusZGEN());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.essmodule.ESSStatus)) {
      return super.equals(obj);
    }
    openfmb.essmodule.ESSStatus other = (openfmb.essmodule.ESSStatus) obj;

    if (hasStatusValue() != other.hasStatusValue()) return false;
    if (hasStatusValue()) {
      if (!getStatusValue()
          .equals(other.getStatusValue())) return false;
    }
    if (hasEssStatusZBAT() != other.hasEssStatusZBAT()) return false;
    if (hasEssStatusZBAT()) {
      if (!getEssStatusZBAT()
          .equals(other.getEssStatusZBAT())) return false;
    }
    if (hasEssStatusZGEN() != other.hasEssStatusZGEN()) return false;
    if (hasEssStatusZGEN()) {
      if (!getEssStatusZGEN()
          .equals(other.getEssStatusZGEN())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasStatusValue()) {
      hash = (37 * hash) + STATUSVALUE_FIELD_NUMBER;
      hash = (53 * hash) + getStatusValue().hashCode();
    }
    if (hasEssStatusZBAT()) {
      hash = (37 * hash) + ESSSTATUSZBAT_FIELD_NUMBER;
      hash = (53 * hash) + getEssStatusZBAT().hashCode();
    }
    if (hasEssStatusZGEN()) {
      hash = (37 * hash) + ESSSTATUSZGEN_FIELD_NUMBER;
      hash = (53 * hash) + getEssStatusZGEN().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.essmodule.ESSStatus parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.essmodule.ESSStatus parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.essmodule.ESSStatus parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.essmodule.ESSStatus parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.essmodule.ESSStatus parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.essmodule.ESSStatus parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.essmodule.ESSStatus parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.essmodule.ESSStatus parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.essmodule.ESSStatus parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.essmodule.ESSStatus parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.essmodule.ESSStatus parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.essmodule.ESSStatus parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.essmodule.ESSStatus prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * ESS status
   * </pre>
   *
   * Protobuf type {@code essmodule.ESSStatus}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:essmodule.ESSStatus)
      openfmb.essmodule.ESSStatusOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.essmodule.Essmodule.internal_static_essmodule_ESSStatus_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.essmodule.Essmodule.internal_static_essmodule_ESSStatus_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.essmodule.ESSStatus.class, openfmb.essmodule.ESSStatus.Builder.class);
    }

    // Construct using openfmb.essmodule.ESSStatus.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      statusValue_ = null;
      if (statusValueBuilder_ != null) {
        statusValueBuilder_.dispose();
        statusValueBuilder_ = null;
      }
      essStatusZBAT_ = null;
      if (essStatusZBATBuilder_ != null) {
        essStatusZBATBuilder_.dispose();
        essStatusZBATBuilder_ = null;
      }
      essStatusZGEN_ = null;
      if (essStatusZGENBuilder_ != null) {
        essStatusZGENBuilder_.dispose();
        essStatusZGENBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.essmodule.Essmodule.internal_static_essmodule_ESSStatus_descriptor;
    }

    @java.lang.Override
    public openfmb.essmodule.ESSStatus getDefaultInstanceForType() {
      return openfmb.essmodule.ESSStatus.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.essmodule.ESSStatus build() {
      openfmb.essmodule.ESSStatus result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.essmodule.ESSStatus buildPartial() {
      openfmb.essmodule.ESSStatus result = new openfmb.essmodule.ESSStatus(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.essmodule.ESSStatus result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.statusValue_ = statusValueBuilder_ == null
            ? statusValue_
            : statusValueBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.essStatusZBAT_ = essStatusZBATBuilder_ == null
            ? essStatusZBAT_
            : essStatusZBATBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000004) != 0)) {
        result.essStatusZGEN_ = essStatusZGENBuilder_ == null
            ? essStatusZGEN_
            : essStatusZGENBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.essmodule.ESSStatus) {
        return mergeFrom((openfmb.essmodule.ESSStatus)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.essmodule.ESSStatus other) {
      if (other == openfmb.essmodule.ESSStatus.getDefaultInstance()) return this;
      if (other.hasStatusValue()) {
        mergeStatusValue(other.getStatusValue());
      }
      if (other.hasEssStatusZBAT()) {
        mergeEssStatusZBAT(other.getEssStatusZBAT());
      }
      if (other.hasEssStatusZGEN()) {
        mergeEssStatusZGEN(other.getEssStatusZGEN());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getStatusValueFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getEssStatusZBATFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            case 26: {
              input.readMessage(
                  getEssStatusZGENFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000004;
              break;
            } // case 26
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.commonmodule.StatusValue statusValue_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.StatusValue, openfmb.commonmodule.StatusValue.Builder, openfmb.commonmodule.StatusValueOrBuilder> statusValueBuilder_;
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     * @return Whether the statusValue field is set.
     */
    public boolean hasStatusValue() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     * @return The statusValue.
     */
    public openfmb.commonmodule.StatusValue getStatusValue() {
      if (statusValueBuilder_ == null) {
        return statusValue_ == null ? openfmb.commonmodule.StatusValue.getDefaultInstance() : statusValue_;
      } else {
        return statusValueBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setStatusValue(openfmb.commonmodule.StatusValue value) {
      if (statusValueBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        statusValue_ = value;
      } else {
        statusValueBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder setStatusValue(
        openfmb.commonmodule.StatusValue.Builder builderForValue) {
      if (statusValueBuilder_ == null) {
        statusValue_ = builderForValue.build();
      } else {
        statusValueBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder mergeStatusValue(openfmb.commonmodule.StatusValue value) {
      if (statusValueBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          statusValue_ != null &&
          statusValue_ != openfmb.commonmodule.StatusValue.getDefaultInstance()) {
          getStatusValueBuilder().mergeFrom(value);
        } else {
          statusValue_ = value;
        }
      } else {
        statusValueBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public Builder clearStatusValue() {
      bitField0_ = (bitField0_ & ~0x00000001);
      statusValue_ = null;
      if (statusValueBuilder_ != null) {
        statusValueBuilder_.dispose();
        statusValueBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.StatusValue.Builder getStatusValueBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getStatusValueFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    public openfmb.commonmodule.StatusValueOrBuilder getStatusValueOrBuilder() {
      if (statusValueBuilder_ != null) {
        return statusValueBuilder_.getMessageOrBuilder();
      } else {
        return statusValue_ == null ?
            openfmb.commonmodule.StatusValue.getDefaultInstance() : statusValue_;
      }
    }
    /**
     * <pre>
     * UML inherited base object
     * </pre>
     *
     * <code>.commonmodule.StatusValue statusValue = 1 [(.uml.option_parent_message) = true];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.commonmodule.StatusValue, openfmb.commonmodule.StatusValue.Builder, openfmb.commonmodule.StatusValueOrBuilder> 
        getStatusValueFieldBuilder() {
      if (statusValueBuilder_ == null) {
        statusValueBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.commonmodule.StatusValue, openfmb.commonmodule.StatusValue.Builder, openfmb.commonmodule.StatusValueOrBuilder>(
                getStatusValue(),
                getParentForChildren(),
                isClean());
        statusValue_ = null;
      }
      return statusValueBuilder_;
    }

    private openfmb.essmodule.EssStatusZBAT essStatusZBAT_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.essmodule.EssStatusZBAT, openfmb.essmodule.EssStatusZBAT.Builder, openfmb.essmodule.EssStatusZBATOrBuilder> essStatusZBATBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.EssStatusZBAT essStatusZBAT = 2;</code>
     * @return Whether the essStatusZBAT field is set.
     */
    public boolean hasEssStatusZBAT() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.EssStatusZBAT essStatusZBAT = 2;</code>
     * @return The essStatusZBAT.
     */
    public openfmb.essmodule.EssStatusZBAT getEssStatusZBAT() {
      if (essStatusZBATBuilder_ == null) {
        return essStatusZBAT_ == null ? openfmb.essmodule.EssStatusZBAT.getDefaultInstance() : essStatusZBAT_;
      } else {
        return essStatusZBATBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.EssStatusZBAT essStatusZBAT = 2;</code>
     */
    public Builder setEssStatusZBAT(openfmb.essmodule.EssStatusZBAT value) {
      if (essStatusZBATBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        essStatusZBAT_ = value;
      } else {
        essStatusZBATBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.EssStatusZBAT essStatusZBAT = 2;</code>
     */
    public Builder setEssStatusZBAT(
        openfmb.essmodule.EssStatusZBAT.Builder builderForValue) {
      if (essStatusZBATBuilder_ == null) {
        essStatusZBAT_ = builderForValue.build();
      } else {
        essStatusZBATBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.EssStatusZBAT essStatusZBAT = 2;</code>
     */
    public Builder mergeEssStatusZBAT(openfmb.essmodule.EssStatusZBAT value) {
      if (essStatusZBATBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          essStatusZBAT_ != null &&
          essStatusZBAT_ != openfmb.essmodule.EssStatusZBAT.getDefaultInstance()) {
          getEssStatusZBATBuilder().mergeFrom(value);
        } else {
          essStatusZBAT_ = value;
        }
      } else {
        essStatusZBATBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.EssStatusZBAT essStatusZBAT = 2;</code>
     */
    public Builder clearEssStatusZBAT() {
      bitField0_ = (bitField0_ & ~0x00000002);
      essStatusZBAT_ = null;
      if (essStatusZBATBuilder_ != null) {
        essStatusZBATBuilder_.dispose();
        essStatusZBATBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.EssStatusZBAT essStatusZBAT = 2;</code>
     */
    public openfmb.essmodule.EssStatusZBAT.Builder getEssStatusZBATBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getEssStatusZBATFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.EssStatusZBAT essStatusZBAT = 2;</code>
     */
    public openfmb.essmodule.EssStatusZBATOrBuilder getEssStatusZBATOrBuilder() {
      if (essStatusZBATBuilder_ != null) {
        return essStatusZBATBuilder_.getMessageOrBuilder();
      } else {
        return essStatusZBAT_ == null ?
            openfmb.essmodule.EssStatusZBAT.getDefaultInstance() : essStatusZBAT_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.EssStatusZBAT essStatusZBAT = 2;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.essmodule.EssStatusZBAT, openfmb.essmodule.EssStatusZBAT.Builder, openfmb.essmodule.EssStatusZBATOrBuilder> 
        getEssStatusZBATFieldBuilder() {
      if (essStatusZBATBuilder_ == null) {
        essStatusZBATBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.essmodule.EssStatusZBAT, openfmb.essmodule.EssStatusZBAT.Builder, openfmb.essmodule.EssStatusZBATOrBuilder>(
                getEssStatusZBAT(),
                getParentForChildren(),
                isClean());
        essStatusZBAT_ = null;
      }
      return essStatusZBATBuilder_;
    }

    private openfmb.essmodule.ESSStatusZGEN essStatusZGEN_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.essmodule.ESSStatusZGEN, openfmb.essmodule.ESSStatusZGEN.Builder, openfmb.essmodule.ESSStatusZGENOrBuilder> essStatusZGENBuilder_;
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSStatusZGEN essStatusZGEN = 3;</code>
     * @return Whether the essStatusZGEN field is set.
     */
    public boolean hasEssStatusZGEN() {
      return ((bitField0_ & 0x00000004) != 0);
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSStatusZGEN essStatusZGEN = 3;</code>
     * @return The essStatusZGEN.
     */
    public openfmb.essmodule.ESSStatusZGEN getEssStatusZGEN() {
      if (essStatusZGENBuilder_ == null) {
        return essStatusZGEN_ == null ? openfmb.essmodule.ESSStatusZGEN.getDefaultInstance() : essStatusZGEN_;
      } else {
        return essStatusZGENBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSStatusZGEN essStatusZGEN = 3;</code>
     */
    public Builder setEssStatusZGEN(openfmb.essmodule.ESSStatusZGEN value) {
      if (essStatusZGENBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        essStatusZGEN_ = value;
      } else {
        essStatusZGENBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSStatusZGEN essStatusZGEN = 3;</code>
     */
    public Builder setEssStatusZGEN(
        openfmb.essmodule.ESSStatusZGEN.Builder builderForValue) {
      if (essStatusZGENBuilder_ == null) {
        essStatusZGEN_ = builderForValue.build();
      } else {
        essStatusZGENBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSStatusZGEN essStatusZGEN = 3;</code>
     */
    public Builder mergeEssStatusZGEN(openfmb.essmodule.ESSStatusZGEN value) {
      if (essStatusZGENBuilder_ == null) {
        if (((bitField0_ & 0x00000004) != 0) &&
          essStatusZGEN_ != null &&
          essStatusZGEN_ != openfmb.essmodule.ESSStatusZGEN.getDefaultInstance()) {
          getEssStatusZGENBuilder().mergeFrom(value);
        } else {
          essStatusZGEN_ = value;
        }
      } else {
        essStatusZGENBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000004;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSStatusZGEN essStatusZGEN = 3;</code>
     */
    public Builder clearEssStatusZGEN() {
      bitField0_ = (bitField0_ & ~0x00000004);
      essStatusZGEN_ = null;
      if (essStatusZGENBuilder_ != null) {
        essStatusZGENBuilder_.dispose();
        essStatusZGENBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSStatusZGEN essStatusZGEN = 3;</code>
     */
    public openfmb.essmodule.ESSStatusZGEN.Builder getEssStatusZGENBuilder() {
      bitField0_ |= 0x00000004;
      onChanged();
      return getEssStatusZGENFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSStatusZGEN essStatusZGEN = 3;</code>
     */
    public openfmb.essmodule.ESSStatusZGENOrBuilder getEssStatusZGENOrBuilder() {
      if (essStatusZGENBuilder_ != null) {
        return essStatusZGENBuilder_.getMessageOrBuilder();
      } else {
        return essStatusZGEN_ == null ?
            openfmb.essmodule.ESSStatusZGEN.getDefaultInstance() : essStatusZGEN_;
      }
    }
    /**
     * <pre>
     * MISSING DOCUMENTATION!!!
     * </pre>
     *
     * <code>.essmodule.ESSStatusZGEN essStatusZGEN = 3;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.essmodule.ESSStatusZGEN, openfmb.essmodule.ESSStatusZGEN.Builder, openfmb.essmodule.ESSStatusZGENOrBuilder> 
        getEssStatusZGENFieldBuilder() {
      if (essStatusZGENBuilder_ == null) {
        essStatusZGENBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.essmodule.ESSStatusZGEN, openfmb.essmodule.ESSStatusZGEN.Builder, openfmb.essmodule.ESSStatusZGENOrBuilder>(
                getEssStatusZGEN(),
                getParentForChildren(),
                isClean());
        essStatusZGEN_ = null;
      }
      return essStatusZGENBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:essmodule.ESSStatus)
  }

  // @@protoc_insertion_point(class_scope:essmodule.ESSStatus)
  private static final openfmb.essmodule.ESSStatus DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.essmodule.ESSStatus();
  }

  public static openfmb.essmodule.ESSStatus getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<ESSStatus>
      PARSER = new com.google.protobuf.AbstractParser<ESSStatus>() {
    @java.lang.Override
    public ESSStatus parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<ESSStatus> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<ESSStatus> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.essmodule.ESSStatus getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

