// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: essmodule/essmodule.proto

package openfmb.essmodule;

/**
 * <pre>
 * OpenFMB specialization for control schedule using:  LN: Schedule   Name: FSCH
 * </pre>
 *
 * Protobuf type {@code essmodule.ESSControlScheduleFSCH}
 */
public final class ESSControlScheduleFSCH extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:essmodule.ESSControlScheduleFSCH)
    ESSControlScheduleFSCHOrBuilder {
private static final long serialVersionUID = 0L;
  // Use ESSControlScheduleFSCH.newBuilder() to construct.
  private ESSControlScheduleFSCH(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private ESSControlScheduleFSCH() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new ESSControlScheduleFSCH();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.essmodule.Essmodule.internal_static_essmodule_ESSControlScheduleFSCH_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.essmodule.Essmodule.internal_static_essmodule_ESSControlScheduleFSCH_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.essmodule.ESSControlScheduleFSCH.class, openfmb.essmodule.ESSControlScheduleFSCH.Builder.class);
  }

  public static final int VALDCSG_FIELD_NUMBER = 1;
  private openfmb.essmodule.ESSCSG valDCSG_;
  /**
   * <pre>
   * Discrete value in ESSCSG type
   * </pre>
   *
   * <code>.essmodule.ESSCSG ValDCSG = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the valDCSG field is set.
   */
  @java.lang.Override
  public boolean hasValDCSG() {
    return valDCSG_ != null;
  }
  /**
   * <pre>
   * Discrete value in ESSCSG type
   * </pre>
   *
   * <code>.essmodule.ESSCSG ValDCSG = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The valDCSG.
   */
  @java.lang.Override
  public openfmb.essmodule.ESSCSG getValDCSG() {
    return valDCSG_ == null ? openfmb.essmodule.ESSCSG.getDefaultInstance() : valDCSG_;
  }
  /**
   * <pre>
   * Discrete value in ESSCSG type
   * </pre>
   *
   * <code>.essmodule.ESSCSG ValDCSG = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  @java.lang.Override
  public openfmb.essmodule.ESSCSGOrBuilder getValDCSGOrBuilder() {
    return valDCSG_ == null ? openfmb.essmodule.ESSCSG.getDefaultInstance() : valDCSG_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (valDCSG_ != null) {
      output.writeMessage(1, getValDCSG());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (valDCSG_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getValDCSG());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.essmodule.ESSControlScheduleFSCH)) {
      return super.equals(obj);
    }
    openfmb.essmodule.ESSControlScheduleFSCH other = (openfmb.essmodule.ESSControlScheduleFSCH) obj;

    if (hasValDCSG() != other.hasValDCSG()) return false;
    if (hasValDCSG()) {
      if (!getValDCSG()
          .equals(other.getValDCSG())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasValDCSG()) {
      hash = (37 * hash) + VALDCSG_FIELD_NUMBER;
      hash = (53 * hash) + getValDCSG().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.essmodule.ESSControlScheduleFSCH parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.essmodule.ESSControlScheduleFSCH parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.essmodule.ESSControlScheduleFSCH parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.essmodule.ESSControlScheduleFSCH parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.essmodule.ESSControlScheduleFSCH parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.essmodule.ESSControlScheduleFSCH parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.essmodule.ESSControlScheduleFSCH parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.essmodule.ESSControlScheduleFSCH parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.essmodule.ESSControlScheduleFSCH parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.essmodule.ESSControlScheduleFSCH parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.essmodule.ESSControlScheduleFSCH parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.essmodule.ESSControlScheduleFSCH parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.essmodule.ESSControlScheduleFSCH prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * OpenFMB specialization for control schedule using:  LN: Schedule   Name: FSCH
   * </pre>
   *
   * Protobuf type {@code essmodule.ESSControlScheduleFSCH}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:essmodule.ESSControlScheduleFSCH)
      openfmb.essmodule.ESSControlScheduleFSCHOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.essmodule.Essmodule.internal_static_essmodule_ESSControlScheduleFSCH_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.essmodule.Essmodule.internal_static_essmodule_ESSControlScheduleFSCH_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.essmodule.ESSControlScheduleFSCH.class, openfmb.essmodule.ESSControlScheduleFSCH.Builder.class);
    }

    // Construct using openfmb.essmodule.ESSControlScheduleFSCH.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      valDCSG_ = null;
      if (valDCSGBuilder_ != null) {
        valDCSGBuilder_.dispose();
        valDCSGBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.essmodule.Essmodule.internal_static_essmodule_ESSControlScheduleFSCH_descriptor;
    }

    @java.lang.Override
    public openfmb.essmodule.ESSControlScheduleFSCH getDefaultInstanceForType() {
      return openfmb.essmodule.ESSControlScheduleFSCH.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.essmodule.ESSControlScheduleFSCH build() {
      openfmb.essmodule.ESSControlScheduleFSCH result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.essmodule.ESSControlScheduleFSCH buildPartial() {
      openfmb.essmodule.ESSControlScheduleFSCH result = new openfmb.essmodule.ESSControlScheduleFSCH(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.essmodule.ESSControlScheduleFSCH result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.valDCSG_ = valDCSGBuilder_ == null
            ? valDCSG_
            : valDCSGBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.essmodule.ESSControlScheduleFSCH) {
        return mergeFrom((openfmb.essmodule.ESSControlScheduleFSCH)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.essmodule.ESSControlScheduleFSCH other) {
      if (other == openfmb.essmodule.ESSControlScheduleFSCH.getDefaultInstance()) return this;
      if (other.hasValDCSG()) {
        mergeValDCSG(other.getValDCSG());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getValDCSGFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private openfmb.essmodule.ESSCSG valDCSG_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.essmodule.ESSCSG, openfmb.essmodule.ESSCSG.Builder, openfmb.essmodule.ESSCSGOrBuilder> valDCSGBuilder_;
    /**
     * <pre>
     * Discrete value in ESSCSG type
     * </pre>
     *
     * <code>.essmodule.ESSCSG ValDCSG = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return Whether the valDCSG field is set.
     */
    public boolean hasValDCSG() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * Discrete value in ESSCSG type
     * </pre>
     *
     * <code>.essmodule.ESSCSG ValDCSG = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     * @return The valDCSG.
     */
    public openfmb.essmodule.ESSCSG getValDCSG() {
      if (valDCSGBuilder_ == null) {
        return valDCSG_ == null ? openfmb.essmodule.ESSCSG.getDefaultInstance() : valDCSG_;
      } else {
        return valDCSGBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * Discrete value in ESSCSG type
     * </pre>
     *
     * <code>.essmodule.ESSCSG ValDCSG = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setValDCSG(openfmb.essmodule.ESSCSG value) {
      if (valDCSGBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        valDCSG_ = value;
      } else {
        valDCSGBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * Discrete value in ESSCSG type
     * </pre>
     *
     * <code>.essmodule.ESSCSG ValDCSG = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder setValDCSG(
        openfmb.essmodule.ESSCSG.Builder builderForValue) {
      if (valDCSGBuilder_ == null) {
        valDCSG_ = builderForValue.build();
      } else {
        valDCSGBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * Discrete value in ESSCSG type
     * </pre>
     *
     * <code>.essmodule.ESSCSG ValDCSG = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder mergeValDCSG(openfmb.essmodule.ESSCSG value) {
      if (valDCSGBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          valDCSG_ != null &&
          valDCSG_ != openfmb.essmodule.ESSCSG.getDefaultInstance()) {
          getValDCSGBuilder().mergeFrom(value);
        } else {
          valDCSG_ = value;
        }
      } else {
        valDCSGBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * Discrete value in ESSCSG type
     * </pre>
     *
     * <code>.essmodule.ESSCSG ValDCSG = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public Builder clearValDCSG() {
      bitField0_ = (bitField0_ & ~0x00000001);
      valDCSG_ = null;
      if (valDCSGBuilder_ != null) {
        valDCSGBuilder_.dispose();
        valDCSGBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * Discrete value in ESSCSG type
     * </pre>
     *
     * <code>.essmodule.ESSCSG ValDCSG = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.essmodule.ESSCSG.Builder getValDCSGBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getValDCSGFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * Discrete value in ESSCSG type
     * </pre>
     *
     * <code>.essmodule.ESSCSG ValDCSG = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    public openfmb.essmodule.ESSCSGOrBuilder getValDCSGOrBuilder() {
      if (valDCSGBuilder_ != null) {
        return valDCSGBuilder_.getMessageOrBuilder();
      } else {
        return valDCSG_ == null ?
            openfmb.essmodule.ESSCSG.getDefaultInstance() : valDCSG_;
      }
    }
    /**
     * <pre>
     * Discrete value in ESSCSG type
     * </pre>
     *
     * <code>.essmodule.ESSCSG ValDCSG = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.essmodule.ESSCSG, openfmb.essmodule.ESSCSG.Builder, openfmb.essmodule.ESSCSGOrBuilder> 
        getValDCSGFieldBuilder() {
      if (valDCSGBuilder_ == null) {
        valDCSGBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.essmodule.ESSCSG, openfmb.essmodule.ESSCSG.Builder, openfmb.essmodule.ESSCSGOrBuilder>(
                getValDCSG(),
                getParentForChildren(),
                isClean());
        valDCSG_ = null;
      }
      return valDCSGBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:essmodule.ESSControlScheduleFSCH)
  }

  // @@protoc_insertion_point(class_scope:essmodule.ESSControlScheduleFSCH)
  private static final openfmb.essmodule.ESSControlScheduleFSCH DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.essmodule.ESSControlScheduleFSCH();
  }

  public static openfmb.essmodule.ESSControlScheduleFSCH getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<ESSControlScheduleFSCH>
      PARSER = new com.google.protobuf.AbstractParser<ESSControlScheduleFSCH>() {
    @java.lang.Override
    public ESSControlScheduleFSCH parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<ESSControlScheduleFSCH> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<ESSControlScheduleFSCH> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.essmodule.ESSControlScheduleFSCH getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

