// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: essmodule/essmodule.proto

package openfmb.essmodule;

public interface ESSEventAndStatusZGENOrBuilder extends
    // @@protoc_insertion_point(interface_extends:essmodule.ESSEventAndStatusZGEN)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.LogicalNodeForEventAndStatus logicalNodeForEventAndStatus = 1 [(.uml.option_parent_message) = true];</code>
   * @return Whether the logicalNodeForEventAndStatus field is set.
   */
  boolean hasLogicalNodeForEventAndStatus();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.LogicalNodeForEventAndStatus logicalNodeForEventAndStatus = 1 [(.uml.option_parent_message) = true];</code>
   * @return The logicalNodeForEventAndStatus.
   */
  openfmb.commonmodule.LogicalNodeForEventAndStatus getLogicalNodeForEventAndStatus();
  /**
   * <pre>
   * UML inherited base object
   * </pre>
   *
   * <code>.commonmodule.LogicalNodeForEventAndStatus logicalNodeForEventAndStatus = 1 [(.uml.option_parent_message) = true];</code>
   */
  openfmb.commonmodule.LogicalNodeForEventAndStatusOrBuilder getLogicalNodeForEventAndStatusOrBuilder();

  /**
   * <pre>
   * DC Power On/Off Status; True = DC power on
   * </pre>
   *
   * <code>.commonmodule.StatusSPS AuxPwrSt = 2;</code>
   * @return Whether the auxPwrSt field is set.
   */
  boolean hasAuxPwrSt();
  /**
   * <pre>
   * DC Power On/Off Status; True = DC power on
   * </pre>
   *
   * <code>.commonmodule.StatusSPS AuxPwrSt = 2;</code>
   * @return The auxPwrSt.
   */
  openfmb.commonmodule.StatusSPS getAuxPwrSt();
  /**
   * <pre>
   * DC Power On/Off Status; True = DC power on
   * </pre>
   *
   * <code>.commonmodule.StatusSPS AuxPwrSt = 2;</code>
   */
  openfmb.commonmodule.StatusSPSOrBuilder getAuxPwrStOrBuilder();

  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ENS_DynamicTestKind DynamicTest = 3;</code>
   * @return Whether the dynamicTest field is set.
   */
  boolean hasDynamicTest();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ENS_DynamicTestKind DynamicTest = 3;</code>
   * @return The dynamicTest.
   */
  openfmb.commonmodule.ENS_DynamicTestKind getDynamicTest();
  /**
   * <pre>
   * MISSING DOCUMENTATION!!!
   * </pre>
   *
   * <code>.commonmodule.ENS_DynamicTestKind DynamicTest = 3;</code>
   */
  openfmb.commonmodule.ENS_DynamicTestKindOrBuilder getDynamicTestOrBuilder();

  /**
   * <pre>
   * Emergency stop
   * </pre>
   *
   * <code>.commonmodule.StatusSPS EmgStop = 4;</code>
   * @return Whether the emgStop field is set.
   */
  boolean hasEmgStop();
  /**
   * <pre>
   * Emergency stop
   * </pre>
   *
   * <code>.commonmodule.StatusSPS EmgStop = 4;</code>
   * @return The emgStop.
   */
  openfmb.commonmodule.StatusSPS getEmgStop();
  /**
   * <pre>
   * Emergency stop
   * </pre>
   *
   * <code>.commonmodule.StatusSPS EmgStop = 4;</code>
   */
  openfmb.commonmodule.StatusSPSOrBuilder getEmgStopOrBuilder();

  /**
   * <pre>
   * Generator is synchronized to EPS, or not; True = Synchronized
   * </pre>
   *
   * <code>.commonmodule.StatusSPS GnSynSt = 5;</code>
   * @return Whether the gnSynSt field is set.
   */
  boolean hasGnSynSt();
  /**
   * <pre>
   * Generator is synchronized to EPS, or not; True = Synchronized
   * </pre>
   *
   * <code>.commonmodule.StatusSPS GnSynSt = 5;</code>
   * @return The gnSynSt.
   */
  openfmb.commonmodule.StatusSPS getGnSynSt();
  /**
   * <pre>
   * Generator is synchronized to EPS, or not; True = Synchronized
   * </pre>
   *
   * <code>.commonmodule.StatusSPS GnSynSt = 5;</code>
   */
  openfmb.commonmodule.StatusSPSOrBuilder getGnSynStOrBuilder();

  /**
   * <pre>
   * Point status
   * </pre>
   *
   * <code>.essmodule.ESSPointStatus PointStatus = 6;</code>
   * @return Whether the pointStatus field is set.
   */
  boolean hasPointStatus();
  /**
   * <pre>
   * Point status
   * </pre>
   *
   * <code>.essmodule.ESSPointStatus PointStatus = 6;</code>
   * @return The pointStatus.
   */
  openfmb.essmodule.ESSPointStatus getPointStatus();
  /**
   * <pre>
   * Point status
   * </pre>
   *
   * <code>.essmodule.ESSPointStatus PointStatus = 6;</code>
   */
  openfmb.essmodule.ESSPointStatusOrBuilder getPointStatusOrBuilder();
}
