// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: essmodule/essmodule.proto

package openfmb.essmodule;

public interface ESSCurvePointOrBuilder extends
    // @@protoc_insertion_point(interface_extends:essmodule.ESSCurvePoint)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * The array with the points specifying a curve shape.
   * </pre>
   *
   * <code>.essmodule.ESSPoint control = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the control field is set.
   */
  boolean hasControl();
  /**
   * <pre>
   * The array with the points specifying a curve shape.
   * </pre>
   *
   * <code>.essmodule.ESSPoint control = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The control.
   */
  openfmb.essmodule.ESSPoint getControl();
  /**
   * <pre>
   * The array with the points specifying a curve shape.
   * </pre>
   *
   * <code>.essmodule.ESSPoint control = 1 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.essmodule.ESSPointOrBuilder getControlOrBuilder();

  /**
   * <pre>
   * Start time
   * </pre>
   *
   * <code>.commonmodule.ControlTimestamp startTime = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return Whether the startTime field is set.
   */
  boolean hasStartTime();
  /**
   * <pre>
   * Start time
   * </pre>
   *
   * <code>.commonmodule.ControlTimestamp startTime = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   * @return The startTime.
   */
  openfmb.commonmodule.ControlTimestamp getStartTime();
  /**
   * <pre>
   * Start time
   * </pre>
   *
   * <code>.commonmodule.ControlTimestamp startTime = 2 [(.uml.option_required_field) = true, (.uml.option_multiplicity_min) = 1];</code>
   */
  openfmb.commonmodule.ControlTimestampOrBuilder getStartTimeOrBuilder();
}
