// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: essmodule/essmodule.proto

package openfmb.essmodule;

public interface SOCManagementOrBuilder extends
    // @@protoc_insertion_point(interface_extends:essmodule.SOCManagement)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * uint/1%  Define a dead band (DB) around the SOC set point. When the battery SOC goes outside the
   * dead band, the SOC management executes and bring the SOC back to the set point. Upper DB = set point
   * + dead band plus Lower DB = set point – dead band minus
   * </pre>
   *
   * <code>.google.protobuf.FloatValue socDeadBandMinus = 1;</code>
   * @return Whether the socDeadBandMinus field is set.
   */
  boolean hasSocDeadBandMinus();
  /**
   * <pre>
   * uint/1%  Define a dead band (DB) around the SOC set point. When the battery SOC goes outside the
   * dead band, the SOC management executes and bring the SOC back to the set point. Upper DB = set point
   * + dead band plus Lower DB = set point – dead band minus
   * </pre>
   *
   * <code>.google.protobuf.FloatValue socDeadBandMinus = 1;</code>
   * @return The socDeadBandMinus.
   */
  com.google.protobuf.FloatValue getSocDeadBandMinus();
  /**
   * <pre>
   * uint/1%  Define a dead band (DB) around the SOC set point. When the battery SOC goes outside the
   * dead band, the SOC management executes and bring the SOC back to the set point. Upper DB = set point
   * + dead band plus Lower DB = set point – dead band minus
   * </pre>
   *
   * <code>.google.protobuf.FloatValue socDeadBandMinus = 1;</code>
   */
  com.google.protobuf.FloatValueOrBuilder getSocDeadBandMinusOrBuilder();

  /**
   * <pre>
   * uint/1%  Define a dead band (DB) around the SOC set point. When the battery SOC goes outside the
   * dead band, the SOC management executes and bring the SOC back to the set point. Upper DB = set point
   * + dead band plus Lower DB = set point – dead band minus
   * </pre>
   *
   * <code>.google.protobuf.FloatValue socDeadBandPlus = 2;</code>
   * @return Whether the socDeadBandPlus field is set.
   */
  boolean hasSocDeadBandPlus();
  /**
   * <pre>
   * uint/1%  Define a dead band (DB) around the SOC set point. When the battery SOC goes outside the
   * dead band, the SOC management executes and bring the SOC back to the set point. Upper DB = set point
   * + dead band plus Lower DB = set point – dead band minus
   * </pre>
   *
   * <code>.google.protobuf.FloatValue socDeadBandPlus = 2;</code>
   * @return The socDeadBandPlus.
   */
  com.google.protobuf.FloatValue getSocDeadBandPlus();
  /**
   * <pre>
   * uint/1%  Define a dead band (DB) around the SOC set point. When the battery SOC goes outside the
   * dead band, the SOC management executes and bring the SOC back to the set point. Upper DB = set point
   * + dead band plus Lower DB = set point – dead band minus
   * </pre>
   *
   * <code>.google.protobuf.FloatValue socDeadBandPlus = 2;</code>
   */
  com.google.protobuf.FloatValueOrBuilder getSocDeadBandPlusOrBuilder();

  /**
   * <pre>
   * Control value (TRUE or FALSE)
   * </pre>
   *
   * <code>.google.protobuf.BoolValue socManagementCtl = 3;</code>
   * @return Whether the socManagementCtl field is set.
   */
  boolean hasSocManagementCtl();
  /**
   * <pre>
   * Control value (TRUE or FALSE)
   * </pre>
   *
   * <code>.google.protobuf.BoolValue socManagementCtl = 3;</code>
   * @return The socManagementCtl.
   */
  com.google.protobuf.BoolValue getSocManagementCtl();
  /**
   * <pre>
   * Control value (TRUE or FALSE)
   * </pre>
   *
   * <code>.google.protobuf.BoolValue socManagementCtl = 3;</code>
   */
  com.google.protobuf.BoolValueOrBuilder getSocManagementCtlOrBuilder();

  /**
   * <pre>
   * uint/1kW  Set point used for SOC maintenance
   * </pre>
   *
   * <code>.google.protobuf.FloatValue socPowerSetPoint = 4;</code>
   * @return Whether the socPowerSetPoint field is set.
   */
  boolean hasSocPowerSetPoint();
  /**
   * <pre>
   * uint/1kW  Set point used for SOC maintenance
   * </pre>
   *
   * <code>.google.protobuf.FloatValue socPowerSetPoint = 4;</code>
   * @return The socPowerSetPoint.
   */
  com.google.protobuf.FloatValue getSocPowerSetPoint();
  /**
   * <pre>
   * uint/1kW  Set point used for SOC maintenance
   * </pre>
   *
   * <code>.google.protobuf.FloatValue socPowerSetPoint = 4;</code>
   */
  com.google.protobuf.FloatValueOrBuilder getSocPowerSetPointOrBuilder();

  /**
   * <pre>
   * uint/1%  SOC Target in percentage (%).
   * </pre>
   *
   * <code>.google.protobuf.FloatValue socSetPoint = 5;</code>
   * @return Whether the socSetPoint field is set.
   */
  boolean hasSocSetPoint();
  /**
   * <pre>
   * uint/1%  SOC Target in percentage (%).
   * </pre>
   *
   * <code>.google.protobuf.FloatValue socSetPoint = 5;</code>
   * @return The socSetPoint.
   */
  com.google.protobuf.FloatValue getSocSetPoint();
  /**
   * <pre>
   * uint/1%  SOC Target in percentage (%).
   * </pre>
   *
   * <code>.google.protobuf.FloatValue socSetPoint = 5;</code>
   */
  com.google.protobuf.FloatValueOrBuilder getSocSetPointOrBuilder();
}
