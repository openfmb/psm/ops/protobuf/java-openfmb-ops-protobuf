// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: essmodule/essmodule.proto

package openfmb.essmodule;

public interface VoltageRegulationOrBuilder extends
    // @@protoc_insertion_point(interface_extends:essmodule.VoltageRegulation)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * uint/0.1%  The droops define the reaction of the PCS to under/over voltage events. A droop of 1%
   * means that the PCS will output 100% power if the voltage is 1% of the nominal voltage away from the
   * upper or lower dead band. The minimum droop value possible is 0.8%.
   * </pre>
   *
   * <code>.google.protobuf.FloatValue overVoltageDroop = 1;</code>
   * @return Whether the overVoltageDroop field is set.
   */
  boolean hasOverVoltageDroop();
  /**
   * <pre>
   * uint/0.1%  The droops define the reaction of the PCS to under/over voltage events. A droop of 1%
   * means that the PCS will output 100% power if the voltage is 1% of the nominal voltage away from the
   * upper or lower dead band. The minimum droop value possible is 0.8%.
   * </pre>
   *
   * <code>.google.protobuf.FloatValue overVoltageDroop = 1;</code>
   * @return The overVoltageDroop.
   */
  com.google.protobuf.FloatValue getOverVoltageDroop();
  /**
   * <pre>
   * uint/0.1%  The droops define the reaction of the PCS to under/over voltage events. A droop of 1%
   * means that the PCS will output 100% power if the voltage is 1% of the nominal voltage away from the
   * upper or lower dead band. The minimum droop value possible is 0.8%.
   * </pre>
   *
   * <code>.google.protobuf.FloatValue overVoltageDroop = 1;</code>
   */
  com.google.protobuf.FloatValueOrBuilder getOverVoltageDroopOrBuilder();

  /**
   * <pre>
   * uint/0.1%  The droops define the reaction of the PCS to under/over voltage events. A droop of 1%
   * means that the PCS will output 100% power if the voltage is 1% of the nominal voltage away from the
   * upper or lower dead band. The minimum droop value possible is 0.8%.
   * </pre>
   *
   * <code>.google.protobuf.FloatValue underVoltageDroop = 2;</code>
   * @return Whether the underVoltageDroop field is set.
   */
  boolean hasUnderVoltageDroop();
  /**
   * <pre>
   * uint/0.1%  The droops define the reaction of the PCS to under/over voltage events. A droop of 1%
   * means that the PCS will output 100% power if the voltage is 1% of the nominal voltage away from the
   * upper or lower dead band. The minimum droop value possible is 0.8%.
   * </pre>
   *
   * <code>.google.protobuf.FloatValue underVoltageDroop = 2;</code>
   * @return The underVoltageDroop.
   */
  com.google.protobuf.FloatValue getUnderVoltageDroop();
  /**
   * <pre>
   * uint/0.1%  The droops define the reaction of the PCS to under/over voltage events. A droop of 1%
   * means that the PCS will output 100% power if the voltage is 1% of the nominal voltage away from the
   * upper or lower dead band. The minimum droop value possible is 0.8%.
   * </pre>
   *
   * <code>.google.protobuf.FloatValue underVoltageDroop = 2;</code>
   */
  com.google.protobuf.FloatValueOrBuilder getUnderVoltageDroopOrBuilder();

  /**
   * <pre>
   * uint/0.1V  Voltage regulation is performed when the grid voltage goes beyond the dead bands. The
   * dead bands are defined as follows: Upper DB = voltage set point + dead band plus Lower DB = voltage
   * set point – dead band minus
   * </pre>
   *
   * <code>.google.protobuf.FloatValue voltageDeadBandMinus = 3;</code>
   * @return Whether the voltageDeadBandMinus field is set.
   */
  boolean hasVoltageDeadBandMinus();
  /**
   * <pre>
   * uint/0.1V  Voltage regulation is performed when the grid voltage goes beyond the dead bands. The
   * dead bands are defined as follows: Upper DB = voltage set point + dead band plus Lower DB = voltage
   * set point – dead band minus
   * </pre>
   *
   * <code>.google.protobuf.FloatValue voltageDeadBandMinus = 3;</code>
   * @return The voltageDeadBandMinus.
   */
  com.google.protobuf.FloatValue getVoltageDeadBandMinus();
  /**
   * <pre>
   * uint/0.1V  Voltage regulation is performed when the grid voltage goes beyond the dead bands. The
   * dead bands are defined as follows: Upper DB = voltage set point + dead band plus Lower DB = voltage
   * set point – dead band minus
   * </pre>
   *
   * <code>.google.protobuf.FloatValue voltageDeadBandMinus = 3;</code>
   */
  com.google.protobuf.FloatValueOrBuilder getVoltageDeadBandMinusOrBuilder();

  /**
   * <pre>
   * uint/0.1V  Voltage regulation is performed when the grid voltage goes beyond the dead bands. The
   * dead bands are defined as follows: Upper DB = voltage set point + dead band plus Lower DB = voltage
   * set point – dead band minus
   * </pre>
   *
   * <code>.google.protobuf.FloatValue voltageDeadBandPlus = 4;</code>
   * @return Whether the voltageDeadBandPlus field is set.
   */
  boolean hasVoltageDeadBandPlus();
  /**
   * <pre>
   * uint/0.1V  Voltage regulation is performed when the grid voltage goes beyond the dead bands. The
   * dead bands are defined as follows: Upper DB = voltage set point + dead band plus Lower DB = voltage
   * set point – dead band minus
   * </pre>
   *
   * <code>.google.protobuf.FloatValue voltageDeadBandPlus = 4;</code>
   * @return The voltageDeadBandPlus.
   */
  com.google.protobuf.FloatValue getVoltageDeadBandPlus();
  /**
   * <pre>
   * uint/0.1V  Voltage regulation is performed when the grid voltage goes beyond the dead bands. The
   * dead bands are defined as follows: Upper DB = voltage set point + dead band plus Lower DB = voltage
   * set point – dead band minus
   * </pre>
   *
   * <code>.google.protobuf.FloatValue voltageDeadBandPlus = 4;</code>
   */
  com.google.protobuf.FloatValueOrBuilder getVoltageDeadBandPlusOrBuilder();

  /**
   * <pre>
   * uint/0.1V  Other modes of operation, such as peak shaving, smoothing or SOC management may
   * operate if the grid frequency is within the stable band. Upper stable band = frequency set point +
   * band plus Lower stable band = frequency set point – band minus
   * </pre>
   *
   * <code>.google.protobuf.FloatValue voltageSetPoint = 5;</code>
   * @return Whether the voltageSetPoint field is set.
   */
  boolean hasVoltageSetPoint();
  /**
   * <pre>
   * uint/0.1V  Other modes of operation, such as peak shaving, smoothing or SOC management may
   * operate if the grid frequency is within the stable band. Upper stable band = frequency set point +
   * band plus Lower stable band = frequency set point – band minus
   * </pre>
   *
   * <code>.google.protobuf.FloatValue voltageSetPoint = 5;</code>
   * @return The voltageSetPoint.
   */
  com.google.protobuf.FloatValue getVoltageSetPoint();
  /**
   * <pre>
   * uint/0.1V  Other modes of operation, such as peak shaving, smoothing or SOC management may
   * operate if the grid frequency is within the stable band. Upper stable band = frequency set point +
   * band plus Lower stable band = frequency set point – band minus
   * </pre>
   *
   * <code>.google.protobuf.FloatValue voltageSetPoint = 5;</code>
   */
  com.google.protobuf.FloatValueOrBuilder getVoltageSetPointOrBuilder();
}
