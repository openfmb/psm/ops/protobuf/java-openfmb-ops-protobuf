// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: essmodule/essmodule.proto

package openfmb.essmodule;

/**
 * <pre>
 * ESS inverter high level function to maintain voltage within droop dead bands.
 * </pre>
 *
 * Protobuf type {@code essmodule.VoltageDroop}
 */
public final class VoltageDroop extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:essmodule.VoltageDroop)
    VoltageDroopOrBuilder {
private static final long serialVersionUID = 0L;
  // Use VoltageDroop.newBuilder() to construct.
  private VoltageDroop(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private VoltageDroop() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new VoltageDroop();
  }

  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return openfmb.essmodule.Essmodule.internal_static_essmodule_VoltageDroop_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return openfmb.essmodule.Essmodule.internal_static_essmodule_VoltageDroop_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            openfmb.essmodule.VoltageDroop.class, openfmb.essmodule.VoltageDroop.Builder.class);
  }

  public static final int VOLTAGEDROOPCTL_FIELD_NUMBER = 1;
  private com.google.protobuf.BoolValue voltageDroopCtl_;
  /**
   * <pre>
   * Control value (TRUE or FALSE)
   * </pre>
   *
   * <code>.google.protobuf.BoolValue voltageDroopCtl = 1;</code>
   * @return Whether the voltageDroopCtl field is set.
   */
  @java.lang.Override
  public boolean hasVoltageDroopCtl() {
    return voltageDroopCtl_ != null;
  }
  /**
   * <pre>
   * Control value (TRUE or FALSE)
   * </pre>
   *
   * <code>.google.protobuf.BoolValue voltageDroopCtl = 1;</code>
   * @return The voltageDroopCtl.
   */
  @java.lang.Override
  public com.google.protobuf.BoolValue getVoltageDroopCtl() {
    return voltageDroopCtl_ == null ? com.google.protobuf.BoolValue.getDefaultInstance() : voltageDroopCtl_;
  }
  /**
   * <pre>
   * Control value (TRUE or FALSE)
   * </pre>
   *
   * <code>.google.protobuf.BoolValue voltageDroopCtl = 1;</code>
   */
  @java.lang.Override
  public com.google.protobuf.BoolValueOrBuilder getVoltageDroopCtlOrBuilder() {
    return voltageDroopCtl_ == null ? com.google.protobuf.BoolValue.getDefaultInstance() : voltageDroopCtl_;
  }

  public static final int VOLTAGEREGULATION_FIELD_NUMBER = 2;
  private openfmb.essmodule.VoltageRegulation voltageRegulation_;
  /**
   * <pre>
   * Voltage regulation
   * </pre>
   *
   * <code>.essmodule.VoltageRegulation voltageRegulation = 2;</code>
   * @return Whether the voltageRegulation field is set.
   */
  @java.lang.Override
  public boolean hasVoltageRegulation() {
    return voltageRegulation_ != null;
  }
  /**
   * <pre>
   * Voltage regulation
   * </pre>
   *
   * <code>.essmodule.VoltageRegulation voltageRegulation = 2;</code>
   * @return The voltageRegulation.
   */
  @java.lang.Override
  public openfmb.essmodule.VoltageRegulation getVoltageRegulation() {
    return voltageRegulation_ == null ? openfmb.essmodule.VoltageRegulation.getDefaultInstance() : voltageRegulation_;
  }
  /**
   * <pre>
   * Voltage regulation
   * </pre>
   *
   * <code>.essmodule.VoltageRegulation voltageRegulation = 2;</code>
   */
  @java.lang.Override
  public openfmb.essmodule.VoltageRegulationOrBuilder getVoltageRegulationOrBuilder() {
    return voltageRegulation_ == null ? openfmb.essmodule.VoltageRegulation.getDefaultInstance() : voltageRegulation_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (voltageDroopCtl_ != null) {
      output.writeMessage(1, getVoltageDroopCtl());
    }
    if (voltageRegulation_ != null) {
      output.writeMessage(2, getVoltageRegulation());
    }
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (voltageDroopCtl_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getVoltageDroopCtl());
    }
    if (voltageRegulation_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getVoltageRegulation());
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof openfmb.essmodule.VoltageDroop)) {
      return super.equals(obj);
    }
    openfmb.essmodule.VoltageDroop other = (openfmb.essmodule.VoltageDroop) obj;

    if (hasVoltageDroopCtl() != other.hasVoltageDroopCtl()) return false;
    if (hasVoltageDroopCtl()) {
      if (!getVoltageDroopCtl()
          .equals(other.getVoltageDroopCtl())) return false;
    }
    if (hasVoltageRegulation() != other.hasVoltageRegulation()) return false;
    if (hasVoltageRegulation()) {
      if (!getVoltageRegulation()
          .equals(other.getVoltageRegulation())) return false;
    }
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasVoltageDroopCtl()) {
      hash = (37 * hash) + VOLTAGEDROOPCTL_FIELD_NUMBER;
      hash = (53 * hash) + getVoltageDroopCtl().hashCode();
    }
    if (hasVoltageRegulation()) {
      hash = (37 * hash) + VOLTAGEREGULATION_FIELD_NUMBER;
      hash = (53 * hash) + getVoltageRegulation().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static openfmb.essmodule.VoltageDroop parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.essmodule.VoltageDroop parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.essmodule.VoltageDroop parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.essmodule.VoltageDroop parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.essmodule.VoltageDroop parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static openfmb.essmodule.VoltageDroop parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static openfmb.essmodule.VoltageDroop parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.essmodule.VoltageDroop parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.essmodule.VoltageDroop parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static openfmb.essmodule.VoltageDroop parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static openfmb.essmodule.VoltageDroop parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static openfmb.essmodule.VoltageDroop parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(openfmb.essmodule.VoltageDroop prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * ESS inverter high level function to maintain voltage within droop dead bands.
   * </pre>
   *
   * Protobuf type {@code essmodule.VoltageDroop}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:essmodule.VoltageDroop)
      openfmb.essmodule.VoltageDroopOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return openfmb.essmodule.Essmodule.internal_static_essmodule_VoltageDroop_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return openfmb.essmodule.Essmodule.internal_static_essmodule_VoltageDroop_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              openfmb.essmodule.VoltageDroop.class, openfmb.essmodule.VoltageDroop.Builder.class);
    }

    // Construct using openfmb.essmodule.VoltageDroop.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      voltageDroopCtl_ = null;
      if (voltageDroopCtlBuilder_ != null) {
        voltageDroopCtlBuilder_.dispose();
        voltageDroopCtlBuilder_ = null;
      }
      voltageRegulation_ = null;
      if (voltageRegulationBuilder_ != null) {
        voltageRegulationBuilder_.dispose();
        voltageRegulationBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return openfmb.essmodule.Essmodule.internal_static_essmodule_VoltageDroop_descriptor;
    }

    @java.lang.Override
    public openfmb.essmodule.VoltageDroop getDefaultInstanceForType() {
      return openfmb.essmodule.VoltageDroop.getDefaultInstance();
    }

    @java.lang.Override
    public openfmb.essmodule.VoltageDroop build() {
      openfmb.essmodule.VoltageDroop result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public openfmb.essmodule.VoltageDroop buildPartial() {
      openfmb.essmodule.VoltageDroop result = new openfmb.essmodule.VoltageDroop(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(openfmb.essmodule.VoltageDroop result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.voltageDroopCtl_ = voltageDroopCtlBuilder_ == null
            ? voltageDroopCtl_
            : voltageDroopCtlBuilder_.build();
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.voltageRegulation_ = voltageRegulationBuilder_ == null
            ? voltageRegulation_
            : voltageRegulationBuilder_.build();
      }
    }

    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof openfmb.essmodule.VoltageDroop) {
        return mergeFrom((openfmb.essmodule.VoltageDroop)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(openfmb.essmodule.VoltageDroop other) {
      if (other == openfmb.essmodule.VoltageDroop.getDefaultInstance()) return this;
      if (other.hasVoltageDroopCtl()) {
        mergeVoltageDroopCtl(other.getVoltageDroopCtl());
      }
      if (other.hasVoltageRegulation()) {
        mergeVoltageRegulation(other.getVoltageRegulation());
      }
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              input.readMessage(
                  getVoltageDroopCtlFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              input.readMessage(
                  getVoltageRegulationFieldBuilder().getBuilder(),
                  extensionRegistry);
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private com.google.protobuf.BoolValue voltageDroopCtl_;
    private com.google.protobuf.SingleFieldBuilderV3<
        com.google.protobuf.BoolValue, com.google.protobuf.BoolValue.Builder, com.google.protobuf.BoolValueOrBuilder> voltageDroopCtlBuilder_;
    /**
     * <pre>
     * Control value (TRUE or FALSE)
     * </pre>
     *
     * <code>.google.protobuf.BoolValue voltageDroopCtl = 1;</code>
     * @return Whether the voltageDroopCtl field is set.
     */
    public boolean hasVoltageDroopCtl() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <pre>
     * Control value (TRUE or FALSE)
     * </pre>
     *
     * <code>.google.protobuf.BoolValue voltageDroopCtl = 1;</code>
     * @return The voltageDroopCtl.
     */
    public com.google.protobuf.BoolValue getVoltageDroopCtl() {
      if (voltageDroopCtlBuilder_ == null) {
        return voltageDroopCtl_ == null ? com.google.protobuf.BoolValue.getDefaultInstance() : voltageDroopCtl_;
      } else {
        return voltageDroopCtlBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * Control value (TRUE or FALSE)
     * </pre>
     *
     * <code>.google.protobuf.BoolValue voltageDroopCtl = 1;</code>
     */
    public Builder setVoltageDroopCtl(com.google.protobuf.BoolValue value) {
      if (voltageDroopCtlBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        voltageDroopCtl_ = value;
      } else {
        voltageDroopCtlBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * Control value (TRUE or FALSE)
     * </pre>
     *
     * <code>.google.protobuf.BoolValue voltageDroopCtl = 1;</code>
     */
    public Builder setVoltageDroopCtl(
        com.google.protobuf.BoolValue.Builder builderForValue) {
      if (voltageDroopCtlBuilder_ == null) {
        voltageDroopCtl_ = builderForValue.build();
      } else {
        voltageDroopCtlBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * Control value (TRUE or FALSE)
     * </pre>
     *
     * <code>.google.protobuf.BoolValue voltageDroopCtl = 1;</code>
     */
    public Builder mergeVoltageDroopCtl(com.google.protobuf.BoolValue value) {
      if (voltageDroopCtlBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0) &&
          voltageDroopCtl_ != null &&
          voltageDroopCtl_ != com.google.protobuf.BoolValue.getDefaultInstance()) {
          getVoltageDroopCtlBuilder().mergeFrom(value);
        } else {
          voltageDroopCtl_ = value;
        }
      } else {
        voltageDroopCtlBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * Control value (TRUE or FALSE)
     * </pre>
     *
     * <code>.google.protobuf.BoolValue voltageDroopCtl = 1;</code>
     */
    public Builder clearVoltageDroopCtl() {
      bitField0_ = (bitField0_ & ~0x00000001);
      voltageDroopCtl_ = null;
      if (voltageDroopCtlBuilder_ != null) {
        voltageDroopCtlBuilder_.dispose();
        voltageDroopCtlBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * Control value (TRUE or FALSE)
     * </pre>
     *
     * <code>.google.protobuf.BoolValue voltageDroopCtl = 1;</code>
     */
    public com.google.protobuf.BoolValue.Builder getVoltageDroopCtlBuilder() {
      bitField0_ |= 0x00000001;
      onChanged();
      return getVoltageDroopCtlFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * Control value (TRUE or FALSE)
     * </pre>
     *
     * <code>.google.protobuf.BoolValue voltageDroopCtl = 1;</code>
     */
    public com.google.protobuf.BoolValueOrBuilder getVoltageDroopCtlOrBuilder() {
      if (voltageDroopCtlBuilder_ != null) {
        return voltageDroopCtlBuilder_.getMessageOrBuilder();
      } else {
        return voltageDroopCtl_ == null ?
            com.google.protobuf.BoolValue.getDefaultInstance() : voltageDroopCtl_;
      }
    }
    /**
     * <pre>
     * Control value (TRUE or FALSE)
     * </pre>
     *
     * <code>.google.protobuf.BoolValue voltageDroopCtl = 1;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        com.google.protobuf.BoolValue, com.google.protobuf.BoolValue.Builder, com.google.protobuf.BoolValueOrBuilder> 
        getVoltageDroopCtlFieldBuilder() {
      if (voltageDroopCtlBuilder_ == null) {
        voltageDroopCtlBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            com.google.protobuf.BoolValue, com.google.protobuf.BoolValue.Builder, com.google.protobuf.BoolValueOrBuilder>(
                getVoltageDroopCtl(),
                getParentForChildren(),
                isClean());
        voltageDroopCtl_ = null;
      }
      return voltageDroopCtlBuilder_;
    }

    private openfmb.essmodule.VoltageRegulation voltageRegulation_;
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.essmodule.VoltageRegulation, openfmb.essmodule.VoltageRegulation.Builder, openfmb.essmodule.VoltageRegulationOrBuilder> voltageRegulationBuilder_;
    /**
     * <pre>
     * Voltage regulation
     * </pre>
     *
     * <code>.essmodule.VoltageRegulation voltageRegulation = 2;</code>
     * @return Whether the voltageRegulation field is set.
     */
    public boolean hasVoltageRegulation() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <pre>
     * Voltage regulation
     * </pre>
     *
     * <code>.essmodule.VoltageRegulation voltageRegulation = 2;</code>
     * @return The voltageRegulation.
     */
    public openfmb.essmodule.VoltageRegulation getVoltageRegulation() {
      if (voltageRegulationBuilder_ == null) {
        return voltageRegulation_ == null ? openfmb.essmodule.VoltageRegulation.getDefaultInstance() : voltageRegulation_;
      } else {
        return voltageRegulationBuilder_.getMessage();
      }
    }
    /**
     * <pre>
     * Voltage regulation
     * </pre>
     *
     * <code>.essmodule.VoltageRegulation voltageRegulation = 2;</code>
     */
    public Builder setVoltageRegulation(openfmb.essmodule.VoltageRegulation value) {
      if (voltageRegulationBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        voltageRegulation_ = value;
      } else {
        voltageRegulationBuilder_.setMessage(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * Voltage regulation
     * </pre>
     *
     * <code>.essmodule.VoltageRegulation voltageRegulation = 2;</code>
     */
    public Builder setVoltageRegulation(
        openfmb.essmodule.VoltageRegulation.Builder builderForValue) {
      if (voltageRegulationBuilder_ == null) {
        voltageRegulation_ = builderForValue.build();
      } else {
        voltageRegulationBuilder_.setMessage(builderForValue.build());
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * Voltage regulation
     * </pre>
     *
     * <code>.essmodule.VoltageRegulation voltageRegulation = 2;</code>
     */
    public Builder mergeVoltageRegulation(openfmb.essmodule.VoltageRegulation value) {
      if (voltageRegulationBuilder_ == null) {
        if (((bitField0_ & 0x00000002) != 0) &&
          voltageRegulation_ != null &&
          voltageRegulation_ != openfmb.essmodule.VoltageRegulation.getDefaultInstance()) {
          getVoltageRegulationBuilder().mergeFrom(value);
        } else {
          voltageRegulation_ = value;
        }
      } else {
        voltageRegulationBuilder_.mergeFrom(value);
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * Voltage regulation
     * </pre>
     *
     * <code>.essmodule.VoltageRegulation voltageRegulation = 2;</code>
     */
    public Builder clearVoltageRegulation() {
      bitField0_ = (bitField0_ & ~0x00000002);
      voltageRegulation_ = null;
      if (voltageRegulationBuilder_ != null) {
        voltageRegulationBuilder_.dispose();
        voltageRegulationBuilder_ = null;
      }
      onChanged();
      return this;
    }
    /**
     * <pre>
     * Voltage regulation
     * </pre>
     *
     * <code>.essmodule.VoltageRegulation voltageRegulation = 2;</code>
     */
    public openfmb.essmodule.VoltageRegulation.Builder getVoltageRegulationBuilder() {
      bitField0_ |= 0x00000002;
      onChanged();
      return getVoltageRegulationFieldBuilder().getBuilder();
    }
    /**
     * <pre>
     * Voltage regulation
     * </pre>
     *
     * <code>.essmodule.VoltageRegulation voltageRegulation = 2;</code>
     */
    public openfmb.essmodule.VoltageRegulationOrBuilder getVoltageRegulationOrBuilder() {
      if (voltageRegulationBuilder_ != null) {
        return voltageRegulationBuilder_.getMessageOrBuilder();
      } else {
        return voltageRegulation_ == null ?
            openfmb.essmodule.VoltageRegulation.getDefaultInstance() : voltageRegulation_;
      }
    }
    /**
     * <pre>
     * Voltage regulation
     * </pre>
     *
     * <code>.essmodule.VoltageRegulation voltageRegulation = 2;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        openfmb.essmodule.VoltageRegulation, openfmb.essmodule.VoltageRegulation.Builder, openfmb.essmodule.VoltageRegulationOrBuilder> 
        getVoltageRegulationFieldBuilder() {
      if (voltageRegulationBuilder_ == null) {
        voltageRegulationBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            openfmb.essmodule.VoltageRegulation, openfmb.essmodule.VoltageRegulation.Builder, openfmb.essmodule.VoltageRegulationOrBuilder>(
                getVoltageRegulation(),
                getParentForChildren(),
                isClean());
        voltageRegulation_ = null;
      }
      return voltageRegulationBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:essmodule.VoltageDroop)
  }

  // @@protoc_insertion_point(class_scope:essmodule.VoltageDroop)
  private static final openfmb.essmodule.VoltageDroop DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new openfmb.essmodule.VoltageDroop();
  }

  public static openfmb.essmodule.VoltageDroop getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<VoltageDroop>
      PARSER = new com.google.protobuf.AbstractParser<VoltageDroop>() {
    @java.lang.Override
    public VoltageDroop parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<VoltageDroop> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<VoltageDroop> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public openfmb.essmodule.VoltageDroop getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

